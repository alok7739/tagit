###
@api {get} /api/me/notification Get all notifications
@apiName Get all notifications
@apiGroup Notifications
@apiPermission User
@apiVersion 0.1.0

@apiParam {Integer} [limit] Default 30
@apiParam {Integer} [page] Default 1

@apiSuccessExample Success-Response:
{
    "success": true,
    "code": 0,
    "data": {
        "current_page": 1,
        "first_page_url": "https://tagit.appus.work/api/me/notification?page=1",
        "from": 1,
        "last_page": 1,
        "last_page_url": "https://tagit.appus.work/api/me/notification?page=1",
        "next_page_url": null,
        "path": "https://tagit.appus.work/api/me/notification",
        "per_page": 15,
        "prev_page_url": null,
        "to": 4,
        "total": 4,
        "data": [
            {
                "id": "efbd8681-f1df-49f0-91c6-f8ba5264c860",
                "title": "Your proof has been rejected",
                "message": "Your proof related to Test challenge 2 challenge has been rejected. You can try again!",
                "data": {
                    "type": "proofRejected",
                    "challengeId": 2,
                    "variables": {
                        "challengeName": "Test challenge"
                    }
                },
                "created_at": "1569244380"
            },
            {
                "id": "bd40aa79-6c29-40ce-b99d-31e7845cd2b9",
                "title": "Challenge ended",
                "message": "Test challenge 2 challenge ended. Click to view results!",
                "data": {
                    "type": "challengeEnded",
                    "challengeId": 2,
                    "variables": {
                        "challengeName": "Test challenge"
                    }
                },
                "created_at": "1569244020"
            },
            {
                "id": "eb833fa4-ac81-4aad-9be4-f8fa939825ae",
                "title": "Challenge has been started",
                "message": "Test challenge 2 challenge has been started. Try to win!",
                "data": {
                    "type": "challengeStarted",
                    "challengeId": 2,
                    "variables": {
                        "challengeName": "Test challenge"
                    }
                },
                "created_at": "1569243931"
            },
            {
                "id": "2afc8b3e-484b-4b13-95f7-585921f3ec94",
                "title": "TAGit",
                "message": "test db to all",
                "data": null,
                "created_at": "1569233186"
            },
        ],
    }
}
###

###
@api {delete} /api/me/notification Delete all user's notifications
@apiName Delete all user's notifications
@apiGroup Notifications
@apiPermission User
@apiVersion 0.1.0

@apiSuccessExample Success-Response:
{
    "success": true,
    "code": 0,
    "data": null
}
###

###
@api {delete} /api/me/notification/:id Delete specific user's notification
@apiName Delete specific user's notification
@apiGroup Notifications
@apiPermission User
@apiVersion 0.1.0

@apiSuccessExample Success-Response:
{
    "success": true,
    "code": 0,
    "data": null
}

@apiErrorExample No such notification:
{
    "success": false,
    "code": 16,
    "data": null
}

@apiErrorExample User is not owner of the notification:
{
    "success": false,
    "code": 25,
    "data": null
}
###

