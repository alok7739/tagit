###
@api {get} /api/user/:id Get user by id
@apiName Get user by id
@apiGroup User
@apiPermission User
@apiVersion 0.1.0

@apiSuccessExample Success-Response:
{
    "success": true,
    "code": 0,
    "data": {
        "id": 24,
        "email": "xxxxx@gmail.com",
        "created_at": "1554450631",
        "updated_at": "1555752534",
        "avatar": "https://tagit.appus.work/storage/avatars/ZDW3HM4zqo5CqYKD3dtbOur7VsjJy1mNHr5Z3vnA.jpeg",
        "birthday": "604508113",
        "sex": "male",
        "country": "Saudi arabia",
        "city": "Saudi arabia",
        "full_name": "Test",
        "company": {
            "id": 27,
            "name": "Test",
            "logo": "http://localhost/storage/companies//XagASPQ4lKDC9bayaNkxS3Hg4yvNNibWgoAItxza.jpg",
            "info": "TEST",
            "created_at": "2019-06-26 18:08:26",
            "updated_at": "2019-06-26 18:08:26",
            "type": "commercial"
        }
    }
}
###

