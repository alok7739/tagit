###
@api {post} /api/auth/me/company Join/leave a company
@apiName Join/left a company
@apiGroup Auth
@apiPermission User
@apiVersion 0.1.0

@apiParam {String} company_join_code string min:5 max:50 (pass null to leave current company)

@apiSuccessExample Success-Response:
{
    "success": true,
    "code": 0,
    "data": null
}

@apiErrorExample Wrong join code:
{
    "success": false,
    "code": 15,
    "data": {
        "messages": {
            "company_join_code": [
                1012
            ]
        }
    }
}

@apiErrorExample Join code must contain at least 5 symbols:
{
  "success": false,
  "code": 15,
  "data": {
    "messages": {
      "company_join_code": [
        1001
      ]
    }
  }
}

@apiErrorExample Join code must contain not more than 50 symbols:
{
  "success": false,
  "code": 15,
  "data": {
    "messages": {
      "company_join_code": [
        1002
      ]
    }
  }
}
###
