###
@api {post} /api/auth/devices Add push token to user
@apiName Login Add push token to user
@apiGroup Devices
@apiPermission User
@apiVersion 0.1.0

@apiParam {String} device_id Unique device token
@apiParam {String} token Push token
@apiParam {String} type device type can be ios or android

@apiSuccessExample Success-Response:
{
    "success": true,
    "code": 0,
    "data": null
}

@apiErrorExample Validations errors (one of the possible):
{
    "success": false,
    "code": 15,
    "data": {
        "messages": {
            "device_id": [
                1000
            ]
        }
    }
}
###

###
@api {delete} /api/auth/devices/:device_id Delete user's push token
@apiName Delete user's push token
@apiGroup Devices
@apiPermission User
@apiVersion 0.1.0

@apiSuccessExample Success-Response:
{
    "success": true,
    "code": 0,
    "data": null
}

@apiErrorExample No such device:
{
    "success": false,
    "code": 16,
    "data": null
}

@apiErrorExample User is not owner of device:
{
    "success": false,
    "code": 25,
    "data": null
}
###

