###
@api {get} /api/challenge/{challengeId}/comments Get challenge comments
@apiName Get challenge comments
@apiGroup Challenge
@apiPermission User
@apiVersion 0.1.0

@apiParam {Integer} [page] Default 1
@apiParam {Integer} [limit] Min:1 Max:100 Default 20
@apiParam {Integer} [last_id] Default 0

@apiSuccessExample Success-Response:
{
    "success": true,
    "code": 0,
    "data": {
        "current_page": 1,
        "data": [
            {
                "id": 17,
                "message": "testing8",
                "created_at": "1603278029",
                "user": {
                    "id": 14165,
                    "email": "as@and.com",
                    "created_at": "1602764478",
                    "updated_at": "1602847127",
                    "phone_number": "3332222308",
                    "country_code": "92",
                    "is_registration_completed": true,
                    "avatar": "https://dntyj0kck09bg.cloudfront.net/avatarsYhRrHp1abUVsRt3WxW9AXstfGlXAWN40hZEmxqkk.jpeg",
                    "birthday": "877287600",
                    "sex": "Female",
                    "country": "Pakistan",
                    "city": "islamabad",
                    "full_name": "Testing 308",
                    "coins": 80,
                    "referral_code": "chJPRdwNFcTGjlqLb2Ulodiil8niUpSS",
                    "company": null
                }
            }
        ],
        "first_page_url": "http://localhost:8789/api/challenge/1/comments?page=1",
        "from": 1,
        "last_page": 1,
        "last_page_url": "http://localhost:8789/api/challenge/1/comments?page=1",
        "next_page_url": null,
        "path": "http://localhost:8789/api/challenge/1/comments",
        "per_page": 15,
        "prev_page_url": null,
        "to": 1,
        "total": 1
    }
}
###

###
@api {post} /api/challenge/{challengeId}/comments Post comment
@apiName Post comment on challenge
@apiGroup Challenge
@apiPermission User
@apiVersion 0.1.0

@apiParam {String} [message]

@apiSuccessExample Success-Response:
{
    "success": true,
    "code": 0,
    "data": null
}
###

###
@api {delete} /api/challenge/:challengeId/comments/:commentId Delete comment
@apiName Delete comment
@apiGroup Challenge
@apiPermission User
@apiVersion 0.1.0

@apiSuccessExample Success-Response:
{
    "success": true,
    "code": 0,
    "data": null
}

@apiErrorExample Comment does not belong to this challenge:
{
    "success": false,
    "code": 29,
    "data": null
}
###

