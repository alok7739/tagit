###
@api {post} /api/challenge/{challengeId}/like Like/unlike challenge
@apiName Like/unlike challenge
@apiGroup Challenge
@apiPermission User
@apiVersion 0.1.0

@apiParam {Integer} [like] Available values: 1 or 0. Default 1

@apiSuccessExample Success-Response:
{
    "success": true,
    "code": 0,
    "data": null
}
###

