###
@api {get} /api/feed Get all feeds
@apiName Get all feeds
@apiGroup Feeds
@apiPermission User
@apiVersion 0.1.0

@apiParam {Integer} [limit] Min 1 Default 20
@apiParam {Integer} [page] Number of specific page

@apiSuccessExample Success-Response:
{
    "success": true,
    "code": 0,
    "data": {
        "current_page": 1,
        "data": [
            {
                "id": 2,
                "type": "proof_sent",
                "country": "Saudi Arabia",
                "challenge_id": null,
                "proof_id": 1,
                "created_at": "1563359424",
                "updated_at": "1563359427",
                "challenge": null,
                "proof": {
                    "id": 1,
                    "challenge_id": 1,
                    "user_id": 1,
                    "type": "photo",
                    "items": [
                        "https://tagit.appus.work/storage/proofs/ZB3KOumcOVHWxTmfz6JSSg7BQ7zw9UEhjv7fvBZG.jpeg"
                    ],
                    "status": "accepted",
                    "created_at": "1563171983",
                    "updated_at": "1563171996",
                    "preview": null,
                    "position": 1,
                    "reward": 100,
                    "user": {
                        "id": 1,
                        "email": "myfuns1989@gmail.com",
                        "created_at": "1563171870",
                        "updated_at": "1563180055",
                        "phone_number": "0630383034",
                        "country_code": "+38",
                        "is_registration_completed": true,
                        "avatar": null,
                        "birthday": "604508113",
                        "sex": "male",
                        "country": "Saudi Arabia",
                        "city": "Mecca",
                        "company": null,
                        "full_name": "Test",
                        "coins": 200,
                        "referral_code": "SPXkP5A4PjhDfJ6fnCoCs4PHkZGEjgNC",
                        "total_reward": 100
                    },
                    "challenge": {
                        "id": 1,
                        "company_id": null,
                        "name": "Test challenge",
                        "image": "https://tagit.appus.work/storage/challenges//7C5xGIAZJ6ySlTaLtUKeh4jGe120vomX08g14rEB.jpeg",
                        "description": "test challenge",
                        "link": "https://google.com",
                        "country": "Saudi Arabia",
                        "city": "Mecca",
                        "participants_limit": 100,
                        "proof_type": "photo",
                        "start_date": "1563174000",
                        "end_date": "1568959200",
                        "created_at": "1563171859",
                        "updated_at": "1563171859",
                        "items_count_in_proof": 1,
                        "video_duration": null,
                        "status": "active",
                        "is_participated": true
                    }
                }
            },
            {
                "id": 1,
                "type": "proof_sent",
                "country": "Saudi Arabia",
                "challenge_id": null,
                "proof_id": 1,
                "created_at": "1563171983",
                "updated_at": "1563171983",
                "challenge": null,
                "proof": {
                    "id": 1,
                    "challenge_id": 1,
                    "user_id": 1,
                    "type": "photo",
                    "items": [
                        "https://tagit.appus.work/storage/proofs/ZB3KOumcOVHWxTmfz6JSSg7BQ7zw9UEhjv7fvBZG.jpeg"
                    ],
                    "status": "accepted",
                    "created_at": "1563171983",
                    "updated_at": "1563171996",
                    "preview": null,
                    "position": 1,
                    "reward": 100,
                    "user": {
                        "id": 1,
                        "email": "myfuns1989@gmail.com",
                        "created_at": "1563171870",
                        "updated_at": "1563180055",
                        "phone_number": "0630383034",
                        "country_code": "+38",
                        "is_registration_completed": true,
                        "avatar": null,
                        "birthday": "604508113",
                        "sex": "male",
                        "country": "Saudi Arabia",
                        "city": "Mecca",
                        "company": null,
                        "full_name": "Test",
                        "coins": 200,
                        "referral_code": "SPXkP5A4PjhDfJ6fnCoCs4PHkZGEjgNC",
                        "total_reward": 100
                    },
                    "challenge": {
                        "id": 1,
                        "company_id": null,
                        "name": "Test challenge",
                        "image": "https://tagit.appus.work/storage/challenges//7C5xGIAZJ6ySlTaLtUKeh4jGe120vomX08g14rEB.jpeg",
                        "description": "test challenge",
                        "link": "https://google.com",
                        "country": "Saudi Arabia",
                        "city": "Mecca",
                        "participants_limit": 100,
                        "proof_type": "photo",
                        "start_date": "1563174000",
                        "end_date": "1568959200",
                        "created_at": "1563171859",
                        "updated_at": "1563171859",
                        "items_count_in_proof": 1,
                        "video_duration": null,
                        "status": "active",
                        "is_participated": true
                    }
                }
            }
        ],
        "first_page_url": "https://tagit.appus.work/api/feed?page=1",
        "from": 1,
        "last_page": 1,
        "last_page_url": "https://tagit.appus.work/api/feed?page=1",
        "next_page_url": null,
        "path": "https://tagit.appus.work/api/feed",
        "per_page": 20,
        "prev_page_url": null,
        "to": 2,
        "total": 2
    }
}

@apiErrorExample Wrong "limit" param error #1:
{
    "success": false,
    "code": 15,
    "data": {
        "messages": {
            "id": [
                1001
            ]
        }
    }
}

@apiErrorExample Wrong "limit" param error #2:
{
    "success": false,
    "code": 15,
    "data": {
        "messages": {
            "id": [
                1013
            ]
        }
    }
}

###

###
@api {get} /api/feed/:id Get Feed by Id
@apiName Get Feed by id
@apiGroup Feed
@apiPermission User
@apiVersion 0.1.0

@apiSuccessExample Success-Response:
{
    "success": true,
    "code": 0,
    "data": {
        "id": 6,
        "type": "challenge_ended",
        "country": "Saudi arabia",
        "challenge_id": 425,
        "proof_id": null,
        "created_at": "1555408802",
        "updated_at": "1555408802",
        "challenge": {
            "id": 425,
            "company_id": null,
            "name": "cron5",
            "image": "https://tagit.appus.work/storage/challenges//UlsS8SxLXCdVTtbOggzuglqAwqeNgzzcjeZxSktb.jpg",
            "description": "ffew ef we fwef wf",
            "link": "https://google.com",
            "country": "Saudi arabia",
            "city": "Dubai",
            "participants_limit": 100,
            "proof_type": "photo",
            "start_date": "1555336800",
            "end_date": "1555347600",
            "created_at": "1555343443",
            "updated_at": "1555408803",
            "items_count_in_proof": 1,
            "video_duration": null,
            "status": "end"
        },
        "proof": null
    }
}

@apiSuccess {String} type Type of proof - available values ["challenge_started", "challenge_ended", "proof_sent"]

@apiErrorExample No such item:
{
    "success": false,
    "code": 16,
    "data": null
}
###



