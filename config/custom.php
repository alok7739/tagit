<?php
/**
 * Created by Maksym Ignatchenko, Appus Studio LP on 26.02.19
 *
 */

return [
    'company_logo_max_size' => 1024 * 5,
    'company_logo_path' => 'companies',
    'challenge_logo_max_size' => 1024 * 5,
    'challenge_logo_path' => 'challenges',
    'default_file_max_size' => 1024 * 5,
    'max_items_in_proof' => 5,
    'proofs_files_path' => 'proofs',
    'max_size_proof_image' => 1024 * 5 * 5,
    'max_size_proof_video' => 1024 * 10 * 10,
    'feeds_count_per_page' => 20,
    'results_count_per_page' => 20,
    'participants_count_per_page' => 20,
    'referral_code_length' => 32,
    'rating_results_count_per_page' => 1000,
    'user_avatar_path' => 'avatars',
    'user_avatar_max_size' => 1024 * 5,
    'company_join_code_length' => 8,
    'company_join_code_min_length' => 5,
    'company_join_code_max_length' => 15,
    'user_notifications_per_page' => 30,
    'chat_messages_count_per_page' => 500,
    'comments_count_per_page' => 100,
    'files_count_per_page' => 50,
];
