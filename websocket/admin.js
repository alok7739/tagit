"use strict";
require('dotenv').config({path: __dirname + '/../.env'})

var webSocketsServerPort = process.env.WS_ADMIN_PORT;
var chatApiURL = process.env.WS_API_DOMAIN + '/admin/api/chat/{CC}/{ID}';
var commentsApiURL = process.env.WS_API_DOMAIN + '/admin/api/comments/{ID}';

var webSocketServer = require('websocket').server;
const fs = require('fs');
const https = require('https');
var axios = require('axios');
var clients = [];

/**
 * HTTPS server
 */
const server = https.createServer({
  cert: fs.readFileSync(process.env.WS_SSL_CERT),
  key: fs.readFileSync(process.env.WS_SSL_KEY)
});

server.listen(webSocketsServerPort, function () {
  console.log((new Date()) + " Server is listening on port " + webSocketsServerPort);
});

/**
 * WebSocket server
 */
var wsServer = new webSocketServer({
  httpServer: server
});

wsServer.on('request', function (request) {
  var connection = request.accept(null, request.origin);
  var data = request.resource.replace('/', '').split('/');

  var type = data[0];
  var token = data[1];
  var cc = data.length > 2 ? data[2] : '';
  var id = data.length > 3 ? parseInt(data[3]) : '';
  var cid = type + id;

  if(type == 'chat') {
    var apiURL = chatApiURL;
  }
  else if(type == 'comments') {
    var apiURL = commentsApiURL;
        id = data[2];
        cc = '';
  }
  else {
    var apiURL = null;
  }

  if(!apiURL || !token) {
    console.log((new Date()) + ' Connection can not be establisehed.');
    console.log('data', data);
    
    connection.close();
    return;
  }

  if(!clients[cid]) {
    clients[cid] = [];
  }

  var index = clients[cid].push(connection) - 1;

  console.log((new Date()) + ' Connection established, Type: ' + type + ' ' + id + ' ' + cc);

  // Get last messages
  axios({
    method: 'get',
    url: apiURL.replace('{ID}', id).replace('{CC}', cc),
    headers: { 
      'Accept': 'application/json', 
      'Authorization': 'Bearer ' + token
    }
  })
  .then(function (response) {
    if(response.data.data) {
      connection.sendUTF(JSON.stringify({type: 'messages', data: response.data.data}));
    }
  })
  .catch(function (error) {
    console.log(error);
  });

  // new message received
  connection.on('message', function (message) {
    if (message.type === 'utf8') {
      console.log((new Date()) + ' Message Received, Type: ' + type + ' ' + id + ' ' + cc + ' - Message: ' + message.utf8Data);

      var call_type = message.utf8Data.indexOf('DELETE::') !== -1 ? 'delete' : 'post';
      id = call_type == 'delete' ? message.utf8Data.replace("DELETE::", "") : id;
      cc = call_type == 'delete' ? 'del' : cc;

      // Post to API
      axios({
        method: call_type,
        url: apiURL.replace('{ID}', id).replace('{CC}', cc),
        headers: { 
          'Accept': 'application/json', 
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
        },
        data: JSON.stringify({"message": message.utf8Data})
      })
      .then(function (response) {
        // broadcast message to all connected clients
        var msg = {};

        if(call_type == 'delete') {
          msg = { type: 'delete', data: id };
        }
        else if(response.data.data) {
          msg = { type: 'messages', data: [response.data.data] };
        }

        var json = JSON.stringify(msg);
        for (var i = 0; i < clients[cid].length; i++) {
          clients[cid][i].sendUTF(json);
        }
      })
      .catch(function (error) {
        // console.log(error);
      });
    }
  });

  // user disconnected
  connection.on('close', function (connection) {
    clients[cid].splice(index, 1);
  });
});
