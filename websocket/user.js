"use strict";
require('dotenv').config({path: __dirname + '/../.env'})

var webSocketsServerPort = process.env.WS_USER_PORT;
var chatApiURL = process.env.WS_API_DOMAIN + '/api/chat';
var commentsApiURL = process.env.WS_API_DOMAIN + '/api/challenge/{ID}/comments/{FEED}';

var webSocketServer = require('websocket').server;
const fs = require('fs');
const https = require('https');
const http = require('http');
var axios = require('axios');
var clients = [];

/**
 * HTTPS server
 */
const server = https.createServer({
  cert: fs.readFileSync(process.env.WS_SSL_CERT),
  key: fs.readFileSync(process.env.WS_SSL_KEY)
});

/**
 * HTTP server - For local
 */
// const server = http.createServer(function(request, response) {
//   console.log((new Date()) + ' Received request for ' + request.url);
//   response.writeHead(404);
//   response.end();
// });

server.listen(webSocketsServerPort, function () {
  console.log((new Date()) + " Server is listening on port " + webSocketsServerPort);
});

/**
 * WebSocket server
 */
var wsServer = new webSocketServer({
  httpServer: server
});

wsServer.on('request', function (request) {
  var connection = request.accept(null, request.origin);
  var data = request.resource.replace('/', '').split('/');
  
  var type = data[0];
  var token = data[1];
  var feed = data.length > 2 ? parseInt(data[3]) : 0;
  var id = data.length > 2 ? parseInt(data[2]) : 1;
  var cid = type + id;
  
  if(type == 'chat') {
    var apiURL = chatApiURL;
  }
  else if(type == 'comments') {
    var apiURL = commentsApiURL;
  }
  else {
    var apiURL = null;
  }

  if(!apiURL || !id || !token) {
    console.log((new Date()) + ' Connection can not be establisehed.');
    console.log('data', data);
    
    connection.close();
    return;
  }

  if(!clients[cid]) {
    clients[cid] = [];
  }

  var index = null;
  if(type == 'comments') {
    var index = clients[cid].push(connection) - 1;
    
    console.log((new Date()) + ' Connection established, Type: ' + type + ' - ID: ' + id);
  }

  // Get last messages
  axios({
    method: 'get',
    url: apiURL.replace('{ID}', id).replace('{FEED}', feed),
    headers: { 
      'Accept': 'application/json', 
      'Authorization': 'Bearer ' + token
    }
  })
  .then(function (response) {
    if(response.data.data) {
      if(type == 'chat') {
        cid = response.data.data.cid;

        if(!clients[cid]) {
          clients[cid] = [];
        }

        index = clients[cid].push(connection) - 1;
        
        console.log((new Date()) + ' Connection established, Type: ' + type + ' - ID: ' + cid);
      }

      connection.sendUTF(JSON.stringify({type: 'messages', data: response.data.data.messages}));
    }
  })
  .catch(function (error) {
    console.log(error);
  });

  // new message received
  connection.on('message', function (message) {
    if (message.type === 'utf8') {
      console.log((new Date()) + ' Message Received, Type: ' + type + ' - ID: ' + id + ' - Message: ' + message.utf8Data);

      // Post to API
      axios({
        method: 'post',
        url: apiURL.replace('{ID}', id).replace('{FEED}', feed),
        headers: { 
          'Accept': 'application/json', 
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
        },
        data: JSON.stringify({"message": message.utf8Data})
      })
      .then(function (response) {
        // broadcast message to all connected clients
        if(response.data.data) {
          var json = JSON.stringify({ type: 'messages', data: [response.data.data] });
          for (var i = 0; i < clients[cid].length; i++) {
            clients[cid][i].sendUTF(json);
          }
        }
      })
      .catch(function (error) {
        // console.log(error);
      });
    }
  });

  // user disconnected
  connection.on('close', function (connection) {
    clients[cid].splice(index, 1);
  });
});
