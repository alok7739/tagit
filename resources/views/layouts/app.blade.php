<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="challenge-logo-max-size" content="{{ config('custom.challenge_logo_max_size') }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <title>{{ config('app.name', 'Laravel') }}</title>
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">
    <link href="https://fonts.googleapis.com/css2?family=Baloo+Thambi+2:wght@400;500;600;700;800&display=swap" rel="stylesheet" />
    <link href="{{ URL::asset('build/css/app.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ URL::asset('build/css/vendor.css') }}" rel="stylesheet" type="text/css">
    <link href="//cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
    <style>
        .dataTables_wrapper {
            overflow-x: auto;
        }
    </style>
    @yield('css')
    <script src="{{ URL::asset('build/js/vendor.js') }}"></script>
    <script src="{{ URL::asset('build/js/app.js') }}"></script>
    <script src="//cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
</head>
<body>
    @include('layouts.header')

    <section class="dashboard-screen pb-50 mt-180">
        <div class="container">

            <div id="preloader">
                <div id="status">
                    <div class="spinner"></div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="dashboard-leftside">
                        @yield('content')
                    </div>
                </div>
            </div>
        </div>
    </section>

    @include('layouts.footer')

@include('js_variables')
@yield('script')
</body>
</html>
