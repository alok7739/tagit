<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <title>{{ config('app.name', 'Laravel') }}</title>
    @section('style')
        <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">
        <link href="https://fonts.googleapis.com/css2?family=Baloo+Thambi+2:wght@400;500;600;700;800&display=swap" rel="stylesheet" />
        <link href="{{ URL::asset('build/css/app.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ URL::asset('build/css/vendor.css') }}" rel="stylesheet" type="text/css">
    @show
</head>
<body>
    @include('layouts.header')

    <section class="login-screen h-120">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-md-offset-4">
                    <div class="login-white-div">
                        @yield('content')
                    </div>
                </div>
            </div>
        </div>
    </section>

    @include('layouts.footer')

@section('script')
    <script src="{{ URL::asset('build/js/vendor.js') }}"></script>
    <script src="{{ URL::asset('build/js/app.js') }}"></script>
@show
</body>
</html>