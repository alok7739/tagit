<header>
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand logobg" href="{{ Auth::check() ? url('admin') : url('/') }}">
                    <img class="main-logo" src="/build/images/logo.png" />
                    <!-- <img class="logo-bg" src="/build/images/tagit-logo-bottom.webp" /> -->
                </a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav admin-nav">
                    @include('layouts.menu')
                </ul>
            </div>
        </div>
    </nav>
</header>
