@if(!Auth::check())
    {{-- <li class="{{ActiveLink::checkPricing() ? 'active' : ''}}">
        <a href="{{url('/')}}"><span>Pricing</span></a>
    </li>
    <li class="{{ActiveLink::checkFrontGames() ? 'active' : ''}}">
        <a href="{{ route('games') }}"><span>Games</span></a>
    </li>
    <li class="{{ActiveLink::checkLogin() ? 'active' : ''}}">
        <a href="{{route('admin')}}"><span>Sign In</span></a>
    </li> --}}
@else
    @if(Auth::user()->isSuperAdmin())
        <li class="{{ActiveLink::checkDashboard() ? 'active' : ''}}">
            <a href="{{route('admin')}}"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a>
        </li>

        <li class="dropdown {{ActiveLink::checkAdministration() || ActiveLink::checkUser() ? 'active' : ''}}">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="fa fa-user"></i> Admins & Users<span class="caret"></span></a>
            <ul class="dropdown-menu">
                @can('view', App\Modules\Users\Admin\Models\Admin::class)
                <li><a href="{{route('administration.index')}}">Manage Admins</a></li>
                @endcan
                @can('view', App\Modules\Users\User\Models\User::class)
                <li><a href="{{route('users.index')}}">Manage Users</a></li>
                @endcan
            </ul>
        </li>

        <li class="dropdown {{ActiveLink::checkPackages() || ActiveLink::checkGames() || ActiveLink::checkGamesCategory() ? 'active' : ''}}">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="fa fa-list"></i> Packages <span class="caret"></span></a>
            <ul class="dropdown-menu">
                @can('view', App\Modules\Onboarding\Models\Packages::class)
                <li><a href="{{route('packages.index')}}">Company Packages</a></li>
                @endcan
                @can('view', App\Modules\Onboarding\Models\Games::class)
                <li><a href="{{route('games.index')}}">Individual Games</a></li>
                @endcan
                @can('view', App\Modules\Onboarding\Models\GamesCategory::class)
                <li><a href="{{route('gamescategory.index')}}">Individual Games Categories</a></li>
                @endcan
            </ul>
        </li>

        <li class="dropdown {{ActiveLink::checkChallenge() || ActiveLink::checkChallengeReports() || ActiveLink::checkLibrary() || ActiveLink::checkLibraryCatergory() ? 'active' : ''}}">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="fa fa-globe"></i> Challenges <span class="caret"></span></a>
            <ul class="dropdown-menu">
                <li><a href="{{route('challenge.index')}}">Manage Challenges</a></li>

                @can('view', App\Modules\Challenges\Models\Reports::class)
                <li><a href="{{route('reports.index')}}">Reported Challenges</a></li>
                @endcan

                @can('view', App\Modules\Library\Models\Library::class)
                <li><a href="{{route('library.index')}}">Challenge Library</a></li>
                @endcan

                @can('view', App\Modules\Library\Models\LibraryCategory::class)
                <li><a href="{{route('category.index')}}">Library Categories</a></li>
                @endcan
            </ul>
        </li>

        <li class="{{ActiveLink::checkCompany() ? 'active' : ''}}">
            <a href="{{route('company.index')}}"><i class="fa fa-building"></i> <span>Companies</span></a>
        </li>

        <li class="dropdown {{ActiveLink::checkContent() || ActiveLink::checkContact() ? 'active' : ''}}">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="fa fa-cog"></i> Misc <span class="caret"></span></a>
            <ul class="dropdown-menu">
                @can('view', App\Modules\Misc\Models\Contact::class)
                <li><a href="{{route('contactus.index')}}">Contact Queries</a></li>
                @endcan

                @can('view', App\Modules\Content\Models\Content::class)
                <li><a href="{{route('content.index')}}">Terms & Conditions</a></li>
                @endcan
            </ul>
        </li>
    @endif

    @if(Auth::user()->company_id !== null)
        <li class="{{ActiveLink::checkCompany() ? 'active' : ''}}">
            <a href="{{route('company.show', ['company' => Auth::user()->company_id])}}"><span>Dashboard</span></a>
        </li>

        @if(Auth::user()->status == '1')
        <li class="{{ActiveLink::checkNewChallenge() ? 'active' : ''}}">
            <a href="{{route('challenge.create', ['companyId' => Auth::user()->company_id])}}">New challenge</a>
        </li>
        @endif

        <li class="{{ActiveLink::checkRanking() ? 'active' : ''}}">
            <a href="{{ route('ranking.index') }}"><span>View Ranking</span></a>
        </li>

        <li class="{{ActiveLink::checkLiveRanking() ? 'active' : ''}}">
            <a href="{{ route('ranking.live', ['company_id' => Auth::user()->company_id]) }}"><span>Live Ranking</span></a>
        </li>

        <li class="{{ActiveLink::checkChat() ? 'active' : ''}}">
            <a href="{{ route('chat', ['company' => Auth::user()->company_id]) }}"><span>Chats</span></a>
        </li>

        <li class="{{ActiveLink::checkComments() ? 'active' : ''}}">
            <a href="{{ route('comments', ['company' => Auth::user()->company_id]) }}"><span>Comments</span></a>
        </li>
    @endif

    <li>
        <a href="{!! route('logout') !!}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" title="logout"><i class="fa fa-sign-out"></i> Logout</a>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
        </form>
    </li>
@endif
