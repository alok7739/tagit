<footer class="main-footer">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-4">
                <div class="footer-col">
                    <div class="heading-footer">
                        <h2>تابعنا</h2>
                    </div>
                    <ul>
                        <li>
                            <a href="https://www.facebook.com/tagitgames/"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                        </li>
                        <li>
                            <a href="https://twitter.com/tagitgames"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                        </li>
                        <li>
                            <a href="https://instagram.com/tagitgames?igshid=bwrio97ws5v4"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                        </li>
                        <li>
                            <a href="https://www.youtube.com/channel/UCxCP2jgbWNCCP8CkvGFm-pg"><i class="fa fa-play" aria-hidden="true"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-4 col-md-4">
                <div class="footer-col">
                    <div class="heading-footer">
                        <h2>حمل التطبيق</h2>
                    </div>
                    <a href="https://appurl.io/pnifOPEFV"><img src="/build/images/appstore-icons.webp" /></a>
                </div>
            </div>
            <div class="col-lg-4 col-md-4">
                <div class="footer-col">
                    <div class="heading-footer">
                        <h2>تواصل معنا</h2>
                        <p><a href="mailto:info@tagitgames.com">info@tagitgames.com</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>