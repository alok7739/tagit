<script>
    const VIDEO = '{{ \App\Modules\Challenges\Enums\ProofTypeEnum::VIDEO }}',
          MULTIPLE_VIDEOS = '{{ \App\Modules\Challenges\Enums\ProofTypeEnum::MULTIPLE_VIDEOS }}',
          REQUIRED_VIDEO_DURATION = [
              VIDEO,
              MULTIPLE_VIDEOS
          ],
          CHALLENGE_LOGO_SIGN = '{{ \App\Modules\Files\Enums\FileSignTypeEnum::CHALLENGE_LOGO }}',
          COMPANY_LOGO_SIGN = '{{ \App\Modules\Files\Enums\FileSignTypeEnum::COMPANY_LOGO }}',
          USER_AVATAR_SIGN = '{{ \App\Modules\Files\Enums\FileSignTypeEnum::USER_AVATAR }}',
          CHALLENGE_LOGO_MAX_SIZE = '{{ config('custom.challenge_logo_max_size') }}' * 1024,
          COMPANY_LOGO_MAX_SIZE = '{{ config('custom.company_logo_max_size') }}' * 1024;
          USER_AVATAR_MAX_SIZE = '{{ config('custom.user_avatar_max_size') }}' * 1024;
</script>
