<?php
/**
 * Created by Maksym Ignatchenko, Appus Studio LP on 05.09.19
 *
 */

return [
    'join_code_tip' => 'The code is used to enter the application users into the company. You can enter the code manually, the length of the code can be from 5 to 50 characters. If there is no manual entry, the code will be generated automatically',
];
