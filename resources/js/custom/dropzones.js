$(document).ready(function () {
    $(".challenge-logo-dropzone").dropzone({
        url: "/admin/fileupload",
        init: function () {
            this.on('sending', function(file, xhr, formData) {
                formData.append('sign', CHALLENGE_LOGO_SIGN);
            });
        },
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        maxFiles: 1,
        maxFilesize: CHALLENGE_LOGO_MAX_SIZE / 1024 / 1024,
        addRemoveLinks: true,
        acceptedFiles: "image/*",
        removedfile: function (file) {
            $("input[name=image]").val("");
            $(file._removeLink).parents('.dz-preview').remove();
        },
        success: function (e, response) {
            $("input[name=image]").val(response.data.filepath);
            $(".dashboard-image").attr('src', response.data.fullfilepath).css('display', 'block');
        }
    });

    $(".company-logo-dropzone").dropzone({
        url: "/admin/fileupload",
        init: function () {
            this.on('sending', function(file, xhr, formData) {
                formData.append('sign', COMPANY_LOGO_SIGN);
            });
        },
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        maxFiles: 1,
        maxFilesize: COMPANY_LOGO_MAX_SIZE / 1024 / 1024,
        addRemoveLinks: true,
        acceptedFiles: "image/*",
        removedfile: function (file) {
            $("input[name=logo]").val("");
            $(file._removeLink).parents('.dz-preview').remove();
        },
        success: function (e, response) {
            $("input[name=logo]").val(response.data.filepath);
            $(".dashboard-image").attr('src', response.data.fullfilepath).css('display', 'block');
        }
    });

    $(".user-avatar-dropzone").dropzone({
        url: "/admin/fileupload",
        init: function () {
            this.on('sending', function(file, xhr, formData) {
                formData.append('sign', USER_AVATAR_SIGN);
            });
        },
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        maxFiles: 1,
        maxFilesize: USER_AVATAR_MAX_SIZE / 1024 / 1024,
        addRemoveLinks: true,
        acceptedFiles: "image/*",
        removedfile: function (file) {
            let prevAvatar = $('#prev-avatar').val();
            let prevAvatarPath = $('#prev-avatar-path').val();
            $('#avatar-image').attr('src', prevAvatar);
            $("input[name=logo]").val("");
            $(file._removeLink).parents('.dz-preview').remove();
        },
        success: function (e, response) {
            $("input[name=avatar]").val(response.data.filepath);
            $(".dashboard-image").attr('src', response.data.fullfilepath).css('display', 'block');
        }
    });

    $('#delete-avatar-btn').on("click", function() {
        $(this).hide();
        let defaultAvatar = $('#default-avatar').val();
        $("input[name=avatar]").val("");
        $('#avatar-image').attr('src', defaultAvatar);
    });
});
