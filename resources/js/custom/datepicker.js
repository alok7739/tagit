jQuery(function ($) {
    const options = {
        format: 'mm//yyyy HH:mm',
        todayHighlight: true,
        autoclose: true,
    };
    $('.dateField').datepicker(options);

    $('.air-datepicker').each(function(picker) {
        var date = new Date();
        date.setMinutes(0);
        $(this).datepicker({
            dateFormat: 'yyyy-mm-dd',
            language: {
                days: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
                daysShort: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
                daysMin: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                months: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                monthsShort: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
                today: 'Today',
                clear: 'Clear',
                dateFormat: 'mm/dd/yyyy',
                timeFormat: 'hh:ii aa',
                firstDay: 0
            },
            timepicker: true,
            timeFormat:'hh:ii',
            position: "top left",
            startDate: date,
            minutesStep: 5
            // onShow: function(inst, animationCompleted) {
            //     $('.datepicker--time-sliders').each(function() {
            //         $(this).children().last().css('display', 'none');
            //     });
            // }
        });
    });
});
