$(document).ready(function () {
    if ($("#video-duration-section").find( "[name='video_duration']").val()) {
        $("#video-duration-section").show();
    }
    $("[name='proof_type']").change(function () {
        if (REQUIRED_VIDEO_DURATION.includes($(this).val())) {
            $("#video-duration-section").show();
        } else {
            $("#video-duration-section").hide();
            // $("#video-duration-section").find( "[name='video_duration']").val("");
        }

        if ($(this).val() == 'checkin') {
            $('#location_bound_div').hide();
            $("[name='location_bound']").val('0');
        } else {
            $('#location_bound_div').show();
        }

        $("[name='location_bound']").trigger('change');

        if ($(this).val() !== 'tests' && $(this).val() !== 'questions' && $(this).val() !== 'true_false') {
            $('#text-proofs-block').html('');
        }

        if ($(this).val() == 'qrcode' || $(this).val() == 'checkin') {
            $('#item_count_div').hide();
            $('#item_count_fld').val('1');

            if ($(this).val() == 'qrcode') {
                $('#qr_row').show();
                // $('#location_bound_div').show();
                // $("[name='location_bound']").trigger('change');
            }

            if ($(this).val() == 'checkin') {
                $('#map_row').show();
                $('#qr_row').hide();
                $('#location_bound_div').hide();
                $("[name='location_bound']").val('0')
            }
        } else {
            $('#qr_row').hide();
            $('#item_count_div').show();
        }
    });

    if($('select[name=company_id]').val()) {
        $('#country-component').hide();
        $('#city-component').hide();
    }

    $('select[name=company_id]').change(function() {
        if( $(this).val() ) {
            $('#country-component').hide();
            $('#country-component').find("option").removeAttr('selected');
            $('#country-component').find("select").val("");
            $('#city-component').hide();
            $('#city-component').find("input").val('');
        } else {
            $('#country-component').show();
            $('#city-component').show();
        }
    });

    $("[name='location_bound']").change(function () {
        if( $(this).val() == '1') {
            $('#map_row').show();
        } else {
            $('#map_row').hide();
        }
    });
});
