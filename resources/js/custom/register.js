$(function(){
    $(document).on('click', '.pricing-user-option', function(){
        $('.pricing-user-option.active').removeClass('active');
        $(this).addClass('active');
        calcPrice();
    });

    $(document).on('click', '.pricing-game-option', function(){
        $('.pricing-game-option.active').removeClass('active');
        $(this).addClass('active');
        calcPrice();
    });

    $(document).on('click', '.signup-btn', function(){
        var id = parseInt($(this).attr('data-id'));
        var users = parseInt($('.pricing-user-option.active').attr('data-users'));
        var games = parseInt($('.pricing-game-option.active').attr('data-games'));
        var pid = (Object.keys(pricing_ids).length > 0 && pricing_ids[id] && pricing_ids[id][games] && (pricing_ids[id][games][users] != null) ) ? pricing_ids[id][games][users] : undefined;

        if(typeof pid === "undefined") {
            alert('Current package is not available, please contact for custom packages.');
        } else {
            window.location.href = '/register?package=' + pid;
        }
    });

    if (typeof pids !== 'undefined') {
        calcPrice();
    }

    function calcPrice() {
        var users = parseInt($('.pricing-user-option.active').attr('data-users'));
        var games = parseInt($('.pricing-game-option.active').attr('data-games'));

        $.each(pids, function(i, p){
            var price = (Object.keys(pricing).length > 0 && pricing[p] && pricing[p][games] && (pricing[p][games][users] != null)) ? pricing[p][games][users] : undefined;

            if(typeof price === "undefined") {
                var txt = '';

                $('#package' + p + ' .signup-btn').hide();
                $('#package' + p + ' .contact-btn').show();
            } else {
                var txt = price == 0 ? 'Free' : '$' + numberWithCommas(price * users * games);

                $('#package' + p + ' .signup-btn').show();
                $('#package' + p + ' .contact-btn').hide();
            }

            $('#price-' + p).text(txt);
        });
    }
});

function numberWithCommas(x) {
    return x.toFixed(2).replace('.00', '').toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
