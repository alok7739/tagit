!function ($) {
    "use strict";
    tinymce.init({
        menubar: false,
        statusbar: false,
        force_br_newlines: false,
        force_p_newlines: false,
        forced_root_block: '',
        selector: '#message',
        plugins: [
            'emoticons'
        ],
        toolbar: 'emoticons'
    })
}(window.jQuery);
