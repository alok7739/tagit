<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterChatMessagesAddCompanyCountry extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('chat_messages', function (Blueprint $table) {
            $table->dropColumn('challenge_id');
            $table->integer('company_id')->unsigned()->index()->nullable();
            $table->integer('country_id')->unsigned()->index()->nullable(); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('chat_messages', function (Blueprint $table) {
            $table->dropColumn('company_id');
            $table->dropColumn('country_id');
            $table->integer('challenge_id')->unsigned()->index();
        });
    }
}
