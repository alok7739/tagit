<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGamesChallengesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('games_challenges', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('game_id');
            $table->unsignedInteger('library_id');
            $table->decimal('duration', 8, 2);
            $table->unsignedInteger('dependent')->default('0');
            $table->timestamps();

            $table->foreign('game_id')
                ->references('id')
                ->on('games');

            $table->foreign('library_id')
                ->references('id')
                ->on('library');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('games_challenges');
    }
}
