<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Artisan;

class AddAttemptDurationFieldProofsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('proofs', function (Blueprint $table) {
            // Difference (in seconds) between sent proof timestamp and start date (timestamp) of related challenge.
            $table->integer('attempt_duration')->default(0);
        });

        Artisan::call('handle-proofs-attempt-duration');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('proofs', function (Blueprint $table) {
            $table->dropColumn('attempt_duration');
        });
    }
}
