<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLibraryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('library', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('category_id');
            $table->string('name');
            $table->string('name_ar');
            $table->string('image');
            $table->text('description');
            $table->text('description_ar');
            $table->string('link')->nullable();
            $table->unsignedInteger('participants_limit');
            $table->string('proof_type')->default('test proof');
            $table->integer('items_count_in_proof')->default(1);
            $table->integer('video_duration')->nullable();
            $table->unsignedInteger('location_bound')->default(0);
            $table->string('location')->nullable();
            $table->string('location_latlng', 100)->nullable();
            $table->unsignedInteger('location_radius')->nullable();
            $table->unsignedInteger('status')->default('1');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('library');
    }
}
