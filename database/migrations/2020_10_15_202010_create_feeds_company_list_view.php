<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeedsCompanyListView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            CREATE VIEW feeds_company AS
            SELECT * FROM feeds
            WHERE feeds.id IN (
                SELECT max(f1.id) AS id
                FROM feeds f1
                WHERE f1.company_id = feeds.company_id
                GROUP BY f1.challenge_id
                ORDER BY id
                LIMIT 250
            )
            ORDER BY created_at;
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW feeds_company");
    }
}
