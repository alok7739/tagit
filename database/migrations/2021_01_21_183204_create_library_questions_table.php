<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLibraryQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('library_questions', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('library_id');
            $table->string('text');
            $table->timestamps();

            $table->foreign('library_id')
                ->references('id')
                ->on('library');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('library_questions');
    }
}
