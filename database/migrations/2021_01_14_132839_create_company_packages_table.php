<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyPackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_packages', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id')->unsigned();
            $table->integer('package_id')->unsigned()->nullable();
            $table->integer('users')->unsigned()->nullable();
            $table->integer('games')->unsigned()->nullable();
            $table->decimal('amount', 12, 2)->nullable();
            $table->date('expiry')->nullable();
            $table->unsignedInteger('true_false')->default('0');
            $table->unsignedInteger('sc_ques')->default('0');
            $table->unsignedInteger('mc_ques')->default('0');
            $table->unsignedInteger('checkin')->default('0');
            $table->unsignedInteger('qr_code')->default('0');
            $table->unsignedInteger('images')->default('0');
            $table->unsignedInteger('videos')->default('0');
            $table->unsignedInteger('locking')->default('0');
            $table->text('payment_response')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_packages');
    }
}
