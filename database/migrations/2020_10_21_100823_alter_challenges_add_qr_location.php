<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterChallengesAddQrLocation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('challenges', function (Blueprint $table) {
            $table->string('qr_string', 50)->nullable();
            $table->string('qr_url')->nullable();
            $table->string('location_latlng', 100)->nullable();
            $table->unsignedInteger('location_radius')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('challenges', function (Blueprint $table) {
            $table->dropColumn('qr_string');
            $table->dropColumn('qr_url');
            $table->dropColumn('location_latlng');
            $table->dropColumn('location_radius');
        });
    }
}
