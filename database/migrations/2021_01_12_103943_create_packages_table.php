<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('packages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 50);
            $table->string('name_ar')->nullable();
            $table->string('short_desc')->nullable();
            $table->string('short_desc_ar')->nullable();
            $table->text('long_desc')->nullable();
            $table->text('long_desc_ar')->nullable();
            $table->unsignedInteger('time_limit')->nullable();
            $table->unsignedInteger('true_false')->default('0');
            $table->unsignedInteger('sc_ques')->default('0');
            $table->unsignedInteger('mc_ques')->default('0');
            $table->unsignedInteger('checkin')->default('0');
            $table->unsignedInteger('qr_code')->default('0');
            $table->unsignedInteger('images')->default('0');
            $table->unsignedInteger('videos')->default('0');
            $table->unsignedInteger('locking')->default('0');
            $table->unsignedInteger('sort_order')->nullable();
            $table->unsignedInteger('status')->default('1');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('packages');
    }
}
