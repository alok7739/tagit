<?php
/**
 * Created by Maksym Ignatchenko, Appus Studio LP on 12.07.19
 *
 */

use App\Modules\Challenges\Models\Challenge;
use App\Modules\Users\User\Models\User;
use Illuminate\Database\Seeder;
use App\Modules\Users\Admin\Models\Admin;

class StageTestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (!Admin::first()) {
            $this->call(AdminTableSeeder::class);
        }
        factory(Challenge::class, 30)->create();
        factory(User::class, 100)->create();
    }
}
