<?php

use Illuminate\Database\Seeder;

class PackagesPricingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pricing = [
            [
                'package_id' => '1',
                'users' => '3',
                'games' => '1',
                'price' => '0.00',
                'created_at' => '2021-01-14 09:07:19',
                'updated_at' => '2021-01-14 09:07:19',
            ],
            [
                'package_id' => '1',
                'users' => '10',
                'games' => '1',
                'price' => '5.00',
                'created_at' => '2021-01-14 09:07:19',
                'updated_at' => '2021-01-14 09:07:19',
            ],
            [

                'package_id' => '1',
                'users' => '20',
                'games' => '1',
                'price' => '5.00',
                'created_at' => '2021-01-14 09:07:19',
                'updated_at' => '2021-01-14 09:07:19',
            ],
            [

                'package_id' => '1',
                'users' => '50',
                'games' => '1',
                'price' => '5.00',
                'created_at' => '2021-01-14 09:07:19',
                'updated_at' => '2021-01-14 09:07:19',
            ],
            [

                'package_id' => '1',
                'users' => '100',
                'games' => '1',
                'price' => '5.00',
                'created_at' => '2021-01-14 09:07:19',
                'updated_at' => '2021-01-14 09:09:03',
            ],
            [

                'package_id' => '1',
                'users' => '3',
                'games' => '4',
                'price' => null,
                'created_at' => '2021-01-14 09:07:19',
                'updated_at' => '2021-01-14 09:07:19',
            ],
            [

                'package_id' => '1',
                'users' => '10',
                'games' => '4',
                'price' => '4.50',
                'created_at' => '2021-01-14 09:07:19',
                'updated_at' => '2021-01-14 09:07:19',
            ],
            [

                'package_id' => '1',
                'users' => '20',
                'games' => '4',
                'price' => '4.50',
                'created_at' => '2021-01-14 09:07:19',
                'updated_at' => '2021-01-14 09:07:19',
            ],
            [

                'package_id' => '1',
                'users' => '50',
                'games' => '4',
                'price' => '4.50',
                'created_at' => '2021-01-14 09:07:19',
                'updated_at' => '2021-01-14 09:07:19',
            ],
            [

                'package_id' => '1',
                'users' => '100',
                'games' => '4',
                'price' => '4.50',
                'created_at' => '2021-01-14 09:07:19',
                'updated_at' => '2021-01-14 09:07:19',
            ],
            [

                'package_id' => '1',
                'users' => '3',
                'games' => '8',
                'price' => null,
                'created_at' => '2021-01-14 09:07:19',
                'updated_at' => '2021-01-14 09:07:19',
            ],
            [

                'package_id' => '1',
                'users' => '10',
                'games' => '8',
                'price' => '4.00',
                'created_at' => '2021-01-14 09:07:19',
                'updated_at' => '2021-01-14 09:07:19',
            ],
            [

                'package_id' => '1',
                'users' => '20',
                'games' => '8',
                'price' => '4.00',
                'created_at' => '2021-01-14 09:07:19',
                'updated_at' => '2021-01-14 09:07:19',
            ],
            [

                'package_id' => '1',
                'users' => '50',
                'games' => '8',
                'price' => '4.00',
                'created_at' => '2021-01-14 09:07:19',
                'updated_at' => '2021-01-14 09:07:19',
            ],
            [

                'package_id' => '1',
                'users' => '100',
                'games' => '8',
                'price' => '4.00',
                'created_at' => '2021-01-14 09:07:19',
                'updated_at' => '2021-01-14 09:07:19',
            ],
            [

                'package_id' => '1',
                'users' => '3',
                'games' => '12',
                'price' => null,
                'created_at' => '2021-01-14 09:07:19',
                'updated_at' => '2021-01-14 09:07:19',
            ],
            [

                'package_id' => '1',
                'users' => '10',
                'games' => '12',
                'price' => '3.50',
                'created_at' => '2021-01-14 09:07:19',
                'updated_at' => '2021-01-14 09:07:19',
            ],
            [

                'package_id' => '1',
                'users' => '20',
                'games' => '12',
                'price' => '3.50',
                'created_at' => '2021-01-14 09:07:19',
                'updated_at' => '2021-01-14 09:07:19',
            ],
            [

                'package_id' => '1',
                'users' => '50',
                'games' => '12',
                'price' => '3.50',
                'created_at' => '2021-01-14 09:07:19',
                'updated_at' => '2021-01-14 09:07:19',
            ],
            [

                'package_id' => '1',
                'users' => '100',
                'games' => '12',
                'price' => '3.50',
                'created_at' => '2021-01-14 09:07:19',
                'updated_at' => '2021-01-14 09:07:19',
            ],
            [

                'package_id' => '1',
                'users' => '3',
                'games' => '24',
                'price' => null,
                'created_at' => '2021-01-14 09:07:19',
                'updated_at' => '2021-01-14 09:07:19',
            ],
            [

                'package_id' => '1',
                'users' => '10',
                'games' => '24',
                'price' => '3.00',
                'created_at' => '2021-01-14 09:07:19',
                'updated_at' => '2021-01-14 09:07:19',
            ],
            [

                'package_id' => '1',
                'users' => '20',
                'games' => '24',
                'price' => '3.00',
                'created_at' => '2021-01-14 09:07:19',
                'updated_at' => '2021-01-14 09:07:19',
            ],
            [

                'package_id' => '1',
                'users' => '50',
                'games' => '24',
                'price' => '3.00',
                'created_at' => '2021-01-14 09:07:19',
                'updated_at' => '2021-01-14 09:07:19',
            ],
            [

                'package_id' => '1',
                'users' => '100',
                'games' => '24',
                'price' => '3.00',
                'created_at' => '2021-01-14 09:07:19',
                'updated_at' => '2021-01-14 09:07:19',
            ],
            [

                'package_id' => '1',
                'users' => '3',
                'games' => '48',
                'price' => null,
                'created_at' => '2021-01-14 09:07:19',
                'updated_at' => '2021-01-14 09:07:19',
            ],
            [

                'package_id' => '1',
                'users' => '10',
                'games' => '48',
                'price' => '2.50',
                'created_at' => '2021-01-14 09:07:19',
                'updated_at' => '2021-01-14 09:07:19',
            ],
            [

                'package_id' => '1',
                'users' => '20',
                'games' => '48',
                'price' => '2.50',
                'created_at' => '2021-01-14 09:07:19',
                'updated_at' => '2021-01-14 09:07:19',
            ],
            [

                'package_id' => '1',
                'users' => '50',
                'games' => '48',
                'price' => '2.50',
                'created_at' => '2021-01-14 09:07:19',
                'updated_at' => '2021-01-14 09:07:19',
            ],
            [

                'package_id' => '1',
                'users' => '100',
                'games' => '48',
                'price' => '2.50',
                'created_at' => '2021-01-14 09:07:19',
                'updated_at' => '2021-01-14 09:07:19',
            ],
            [

                'package_id' => '2',
                'users' => '3',
                'games' => '1',
                'price' => '0.00',
                'created_at' => '2021-01-14 09:10:39',
                'updated_at' => '2021-01-14 09:10:39',
            ],
            [

                'package_id' => '2',
                'users' => '10',
                'games' => '1',
                'price' => '20.00',
                'created_at' => '2021-01-14 09:10:39',
                'updated_at' => '2021-01-14 09:10:39',
            ],
            [

                'package_id' => '2',
                'users' => '20',
                'games' => '1',
                'price' => '20.00',
                'created_at' => '2021-01-14 09:10:39',
                'updated_at' => '2021-01-14 09:10:39',
            ],
            [

                'package_id' => '2',
                'users' => '50',
                'games' => '1',
                'price' => '20.00',
                'created_at' => '2021-01-14 09:10:39',
                'updated_at' => '2021-01-14 09:10:39',
            ],
            [

                'package_id' => '2',
                'users' => '100',
                'games' => '1',
                'price' => '20.00',
                'created_at' => '2021-01-14 09:10:39',
                'updated_at' => '2021-01-14 09:10:39',
            ],
            [

                'package_id' => '2',
                'users' => '3',
                'games' => '4',
                'price' => null,
                'created_at' => '2021-01-14 09:10:39',
                'updated_at' => '2021-01-14 09:10:39',
            ],
            [

                'package_id' => '2',
                'users' => '10',
                'games' => '4',
                'price' => '19.50',
                'created_at' => '2021-01-14 09:10:39',
                'updated_at' => '2021-01-14 09:10:39',
            ],
            [

                'package_id' => '2',
                'users' => '20',
                'games' => '4',
                'price' => '19.50',
                'created_at' => '2021-01-14 09:10:39',
                'updated_at' => '2021-01-14 09:10:39',
            ],
            [

                'package_id' => '2',
                'users' => '50',
                'games' => '4',
                'price' => '19.50',
                'created_at' => '2021-01-14 09:10:39',
                'updated_at' => '2021-01-14 09:10:39',
            ],
            [

                'package_id' => '2',
                'users' => '100',
                'games' => '4',
                'price' => '19.50',
                'created_at' => '2021-01-14 09:10:39',
                'updated_at' => '2021-01-14 09:10:39',
            ],
            [

                'package_id' => '2',
                'users' => '3',
                'games' => '8',
                'price' => null,
                'created_at' => '2021-01-14 09:10:39',
                'updated_at' => '2021-01-14 09:10:39',
            ],
            [

                'package_id' => '2',
                'users' => '10',
                'games' => '8',
                'price' => '19.00',
                'created_at' => '2021-01-14 09:10:39',
                'updated_at' => '2021-01-14 09:10:39',
            ],
            [

                'package_id' => '2',
                'users' => '20',
                'games' => '8',
                'price' => '19.00',
                'created_at' => '2021-01-14 09:10:39',
                'updated_at' => '2021-01-14 09:10:39',
            ],
            [

                'package_id' => '2',
                'users' => '50',
                'games' => '8',
                'price' => '19.00',
                'created_at' => '2021-01-14 09:10:39',
                'updated_at' => '2021-01-14 09:10:39',
            ],
            [

                'package_id' => '2',
                'users' => '100',
                'games' => '8',
                'price' => '19.00',
                'created_at' => '2021-01-14 09:10:39',
                'updated_at' => '2021-01-14 09:10:39',
            ],
            [

                'package_id' => '2',
                'users' => '3',
                'games' => '12',
                'price' => null,
                'created_at' => '2021-01-14 09:10:39',
                'updated_at' => '2021-01-14 09:10:39',
            ],
            [

                'package_id' => '2',
                'users' => '10',
                'games' => '12',
                'price' => '18.50',
                'created_at' => '2021-01-14 09:10:39',
                'updated_at' => '2021-01-14 09:10:39',
            ],
            [

                'package_id' => '2',
                'users' => '20',
                'games' => '12',
                'price' => '18.50',
                'created_at' => '2021-01-14 09:10:39',
                'updated_at' => '2021-01-14 09:10:39',
            ],
            [

                'package_id' => '2',
                'users' => '50',
                'games' => '12',
                'price' => '18.50',
                'created_at' => '2021-01-14 09:10:39',
                'updated_at' => '2021-01-14 09:10:39',
            ],
            [

                'package_id' => '2',
                'users' => '100',
                'games' => '12',
                'price' => '18.50',
                'created_at' => '2021-01-14 09:10:39',
                'updated_at' => '2021-01-14 09:10:39',
            ],
            [

                'package_id' => '2',
                'users' => '3',
                'games' => '24',
                'price' => null,
                'created_at' => '2021-01-14 09:10:39',
                'updated_at' => '2021-01-14 09:10:39',
            ],
            [

                'package_id' => '2',
                'users' => '10',
                'games' => '24',
                'price' => '18.00',
                'created_at' => '2021-01-14 09:10:39',
                'updated_at' => '2021-01-14 09:10:39',
            ],
            [

                'package_id' => '2',
                'users' => '20',
                'games' => '24',
                'price' => '18.00',
                'created_at' => '2021-01-14 09:10:39',
                'updated_at' => '2021-01-14 09:10:39',
            ],
            [

                'package_id' => '2',
                'users' => '50',
                'games' => '24',
                'price' => '18.00',
                'created_at' => '2021-01-14 09:10:39',
                'updated_at' => '2021-01-14 09:10:39',
            ],
            [

                'package_id' => '2',
                'users' => '100',
                'games' => '24',
                'price' => '18.00',
                'created_at' => '2021-01-14 09:10:39',
                'updated_at' => '2021-01-14 09:10:39',
            ],
            [

                'package_id' => '2',
                'users' => '3',
                'games' => '48',
                'price' => null,
                'created_at' => '2021-01-14 09:10:39',
                'updated_at' => '2021-01-14 09:10:39',
            ],
            [

                'package_id' => '2',
                'users' => '10',
                'games' => '48',
                'price' => '17.50',
                'created_at' => '2021-01-14 09:10:39',
                'updated_at' => '2021-01-14 09:10:39',
            ],
            [

                'package_id' => '2',
                'users' => '20',
                'games' => '48',
                'price' => '17.50',
                'created_at' => '2021-01-14 09:10:39',
                'updated_at' => '2021-01-14 09:10:39',
            ],
            [

                'package_id' => '2',
                'users' => '50',
                'games' => '48',
                'price' => '17.50',
                'created_at' => '2021-01-14 09:10:39',
                'updated_at' => '2021-01-14 09:10:39',
            ],
            [

                'package_id' => '2',
                'users' => '100',
                'games' => '48',
                'price' => '17.50',
                'created_at' => '2021-01-14 09:10:39',
                'updated_at' => '2021-01-14 09:10:39',
            ],
            [

                'package_id' => '3',
                'users' => '3',
                'games' => '1',
                'price' => '0.00',
                'created_at' => '2021-01-14 09:11:53',
                'updated_at' => '2021-01-14 09:11:53',
            ],
            [

                'package_id' => '3',
                'users' => '10',
                'games' => '1',
                'price' => '30.00',
                'created_at' => '2021-01-14 09:11:53',
                'updated_at' => '2021-01-14 09:11:53',
            ],
            [

                'package_id' => '3',
                'users' => '20',
                'games' => '1',
                'price' => '30.00',
                'created_at' => '2021-01-14 09:11:53',
                'updated_at' => '2021-01-14 09:11:53',
            ],
            [

                'package_id' => '3',
                'users' => '50',
                'games' => '1',
                'price' => '30.00',
                'created_at' => '2021-01-14 09:11:53',
                'updated_at' => '2021-01-14 09:11:53',
            ],
            [

                'package_id' => '3',
                'users' => '100',
                'games' => '1',
                'price' => '30.00',
                'created_at' => '2021-01-14 09:11:53',
                'updated_at' => '2021-01-14 09:11:53',
            ],
            [

                'package_id' => '3',
                'users' => '3',
                'games' => '4',
                'price' => null,
                'created_at' => '2021-01-14 09:11:53',
                'updated_at' => '2021-01-14 09:11:53',
            ],
            [

                'package_id' => '3',
                'users' => '10',
                'games' => '4',
                'price' => '29.50',
                'created_at' => '2021-01-14 09:11:53',
                'updated_at' => '2021-01-14 09:11:53',
            ],
            [

                'package_id' => '3',
                'users' => '20',
                'games' => '4',
                'price' => '29.50',
                'created_at' => '2021-01-14 09:11:53',
                'updated_at' => '2021-01-14 09:11:53',
            ],
            [

                'package_id' => '3',
                'users' => '50',
                'games' => '4',
                'price' => '29.50',
                'created_at' => '2021-01-14 09:11:53',
                'updated_at' => '2021-01-14 09:11:53',
            ],
            [

                'package_id' => '3',
                'users' => '100',
                'games' => '4',
                'price' => '29.50',
                'created_at' => '2021-01-14 09:11:53',
                'updated_at' => '2021-01-14 09:11:53',
            ],
            [

                'package_id' => '3',
                'users' => '3',
                'games' => '8',
                'price' => null,
                'created_at' => '2021-01-14 09:11:53',
                'updated_at' => '2021-01-14 09:11:53',
            ],
            [

                'package_id' => '3',
                'users' => '10',
                'games' => '8',
                'price' => '29.00',
                'created_at' => '2021-01-14 09:11:53',
                'updated_at' => '2021-01-14 09:11:53',
            ],
            [

                'package_id' => '3',
                'users' => '20',
                'games' => '8',
                'price' => '29.00',
                'created_at' => '2021-01-14 09:11:53',
                'updated_at' => '2021-01-14 09:11:53',
            ],
            [

                'package_id' => '3',
                'users' => '50',
                'games' => '8',
                'price' => '29.00',
                'created_at' => '2021-01-14 09:11:53',
                'updated_at' => '2021-01-14 09:11:53',
            ],
            [
                'package_id' => '3',
                'users' => '100',
                'games' => '8',
                'price' => '29.00',
                'created_at' => '2021-01-14 09:11:53',
                'updated_at' => '2021-01-14 09:11:53',
            ],
            [
                'package_id' => '3',
                'users' => '3',
                'games' => '12',
                'price' => null,
                'created_at' => '2021-01-14 09:11:53',
                'updated_at' => '2021-01-14 09:11:53',
            ],
            [
                'package_id' => '3',
                'users' => '10',
                'games' => '12',
                'price' => '28.50',
                'created_at' => '2021-01-14 09:11:53',
                'updated_at' => '2021-01-14 09:11:53',
            ],
            [
                'package_id' => '3',
                'users' => '20',
                'games' => '12',
                'price' => '28.50',
                'created_at' => '2021-01-14 09:11:53',
                'updated_at' => '2021-01-14 09:11:53',
            ],
            [
                'package_id' => '3',
                'users' => '50',
                'games' => '12',
                'price' => '28.50',
                'created_at' => '2021-01-14 09:11:53',
                'updated_at' => '2021-01-14 09:11:53',
            ],
            [
                'package_id' => '3',
                'users' => '100',
                'games' => '12',
                'price' => '28.50',
                'created_at' => '2021-01-14 09:11:53',
                'updated_at' => '2021-01-14 09:11:53',
            ],
            [
                'package_id' => '3',
                'users' => '3',
                'games' => '24',
                'price' => null,
                'created_at' => '2021-01-14 09:11:53',
                'updated_at' => '2021-01-14 09:11:53',
            ],
            [
                'package_id' => '3',
                'users' => '10',
                'games' => '24',
                'price' => '28.00',
                'created_at' => '2021-01-14 09:11:53',
                'updated_at' => '2021-01-14 09:11:53',
            ],
            [
                'package_id' => '3',
                'users' => '20',
                'games' => '24',
                'price' => '28.00',
                'created_at' => '2021-01-14 09:11:53',
                'updated_at' => '2021-01-14 09:11:53',
            ],
            [
                'package_id' => '3',
                'users' => '50',
                'games' => '24',
                'price' => '28.00',
                'created_at' => '2021-01-14 09:11:53',
                'updated_at' => '2021-01-14 09:11:53',
            ],
            [
                'package_id' => '3',
                'users' => '100',
                'games' => '24',
                'price' => '28.00',
                'created_at' => '2021-01-14 09:11:53',
                'updated_at' => '2021-01-14 09:11:53',
            ],
            [
                'package_id' => '3',
                'users' => '3',
                'games' => '48',
                'price' => null,
                'created_at' => '2021-01-14 09:11:53',
                'updated_at' => '2021-01-14 09:11:53',
            ],
            [
                'package_id' => '3',
                'users' => '10',
                'games' => '48',
                'price' => '27.50',
                'created_at' => '2021-01-14 09:11:53',
                'updated_at' => '2021-01-14 09:11:53',
            ],
            [
                'package_id' => '3',
                'users' => '20',
                'games' => '48',
                'price' => '27.50',
                'created_at' => '2021-01-14 09:11:53',
                'updated_at' => '2021-01-14 09:11:53',
            ],
            [
                'package_id' => '3',
                'users' => '50',
                'games' => '48',
                'price' => '27.50',
                'created_at' => '2021-01-14 09:11:53',
                'updated_at' => '2021-01-14 09:11:53',
            ],
            [
                'package_id' => '3',
                'users' => '100',
                'games' => '48',
                'price' => '27.50',
                'created_at' => '2021-01-14 09:11:53',
                'updated_at' => '2021-01-14 09:11:53',
            ],
        ];

        DB::table('packages_pricing')->delete();
        DB::table('packages_pricing')->insert($pricing);
    }
}
