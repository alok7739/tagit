<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoleAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            'id' => 1,
            'name' => 'Super Admin',
        ]);

        DB::table('roles')->insert([
            'id' => 2,
            'name' => 'Admin',
        ]);
    }
}
