<?php

use Illuminate\Database\Seeder;
use App\Enums\AppEnvironmentEnum;
use Illuminate\Support\Facades\App;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RoleAdminSeeder::class);
        $this->call(CountriesSeeder::class);

        if (App::environment(AppEnvironmentEnum::STAGE)) {
            $this->call(StageTestSeeder::class);
        }
    }
}
