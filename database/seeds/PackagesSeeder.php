<?php

use Illuminate\Database\Seeder;

class PackagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $packages = [
            [
                'name' => 'Basic',
                'name_ar' => null,
                'short_desc' => '3 Months Subscription',
                'short_desc_ar' => null,
                'long_desc' => '<ul><li>Game Creation Platform</li><li>20 Challenges/Month</li><li>1 Hour games consultancy/month</li></ul>',
                'long_desc_ar' => null,
                'time_limit' => '3',
                'challenges' => '20',
                'price' => '60',
                'true_false' => '1',
                'sc_ques' => '1',
                'mc_ques' => '1',
                'checkin' => '1',
                'qr_code' => '1',
                'images' => '1',
                'videos' => '0',
                'locking' => '0',
                'sort_order' => '1',
                'status' => '1',
                'created_at' => '2021-01-14 09:07:19',
                'updated_at' => '2021-01-14 09:09:03',
            ], [
                'name' => 'Standard',
                'name_ar' => null,
                'short_desc' => '6 Months Subscription',
                'short_desc_ar' => null,
                'long_desc' => '<ul><li>Game Creation Platform</li><li>40 Challenges/Month</li><li>1 Hour games consultancy/month</li></ul>',
                'long_desc_ar' => null,
                'time_limit' => '6',
                'challenges' => '40',
                'price' => '55',
                'true_false' => '1',
                'sc_ques' => '1',
                'mc_ques' => '1',
                'checkin' => '1',
                'qr_code' => '1',
                'images' => '1',
                'videos' => '0',
                'locking' => '1',
                'sort_order' => '2',
                'status' => '1',
                'created_at' => '2021-01-14 09:10:39',
                'updated_at' => '2021-01-14 09:10:39',
            ], [
                'name' => 'Pro',
                'name_ar' => null,
                'short_desc' => '12 Months Subscription',
                'short_desc_ar' => null,
                'long_desc' => '<ul><li>Game Creation Platform</li><li>60 Challenges/Month</li><li>2 Hours games consultancy/month</li></ul>',
                'long_desc_ar' => null,
                'time_limit' => '12',
                'challenges' => '60',
                'price' => '45',
                'true_false' => '1',
                'sc_ques' => '1',
                'mc_ques' => '1',
                'checkin' => '1',
                'qr_code' => '1',
                'images' => '1',
                'videos' => '1',
                'locking' => '1',
                'sort_order' => '3',
                'status' => '1',
                'created_at' => '2021-01-14 09:11:53',
                'updated_at' => '2021-01-14 09:11:53',
            ]
        ];

        DB::table('packages')->delete();
        DB::table('packages')->insert($packages);
    }
}
