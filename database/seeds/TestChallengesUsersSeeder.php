<?php

use Illuminate\Database\Seeder;
use \App\Modules\Challenges\Models\Challenge;
use App\Modules\Users\User\Models\User;

class TestChallengesUsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $challenges = factory(Challenge::class, 30)->create();
        $users = factory(User::class, 30)->create();
        $challenges->random()->participants()->attach($users->pluck('id'));
    }
}
