<?php

use Faker\Generator as Faker;
use Illuminate\Http\UploadedFile;

/**
 * @param UploadedFile $image
 * @return string|null
 */
$storeChallengeImageFunc = function (UploadedFile $image): ?string
{
    $path = 'challenges';
    try {
        $fileName = $image->storeAs(
            $path,
            pathinfo($image->hashName(), PATHINFO_FILENAME) . '.' . $image->getClientOriginalExtension()
        );
    } catch(Exception $exception) {
        $fileName = null;
    }
    return $fileName;
};

$factory->define(\App\Modules\Challenges\Models\Challenge::class, function (Faker $faker) use ($storeChallengeImageFunc) {
    return [
        'name' => $faker->word,
        'image' => $storeChallengeImageFunc(new UploadedFile($faker->image(null, 300, 300, 'nature'), str_random())),
        'description' => $faker->sentence( rand(1, 6), true ),
        'link' => $faker->url,
        'country' => \App\Modules\Challenges\Enums\CountryEnum::SAUDI_ARABIA,
        'city' => 'fakeCity',
        'participants_limit' => 10000,
        'proof_type' => \App\Modules\Challenges\Enums\ProofTypeEnum::getAll()[array_rand(\App\Modules\Challenges\Enums\ProofTypeEnum::getSingleItemTypes())],
        'items_count_in_proof' =>  1,
        'start_date' => \Carbon\Carbon::now()->toDateTimeString(),
        'end_date' => \Carbon\Carbon::now()->addDays(1000)->toDateTimeString(),
        'status' => \App\Modules\Challenges\Enums\ChallengeStatusEnum::ACTIVE,
    ];
});
