<?php

use Faker\Generator as Faker;
use App\Modules\Users\User\Models\User;
use Illuminate\Support\Facades\Hash;
use \Illuminate\Http\UploadedFile;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'full_name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'password' => Hash::make('password'), // secret
        'country' => \App\Modules\Challenges\Enums\CountryEnum::SAUDI_ARABIA,
        'remember_token' => str_random(10),
        'is_registration_completed' => true,
        'country_code' => 966,
        'phone_number' => $faker->unique()->randomNumber($nbDigits = 9),
        'avatar' => new UploadedFile($faker->image(null, 300, 300, 'people'), str_random()),
    ];
});
