<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

if (\App\Enums\AppEnvironmentEnum::LOCAL === env('APP_ENV')
    || (\App\Enums\AppEnvironmentEnum::DEVELOP == env('APP_ENV'))) {
    Route::post('wipe', 'WipeController');
}


