<?php

namespace App\Console\Commands;

use App\Modules\Challenges\Models\Proof;
use App\Modules\Feeds\Models\Feed;
use App\Modules\Users\User\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class WipeTestData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'wipe-test-data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Wipe test data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $testUsers = User::where('full_name', 'like', "%TestUser%")
            ->orWhere('is_registration_completed', false)
            ->with(['proofs.feed'])
            ->get();
        $testUsersIds = $testUsers->pluck('id');
        DB::table('challenge_user')->whereIn('user_id', $testUsersIds)->delete();
        $usersAvatars = $testUsers->map(function($user) {
            return $user->getOriginal('avatar');
        })->toArray();

        $proofs = $testUsers->map(function($user) {
            return $user->proofs;
        })->collapse();
        $proofsIds = $proofs->pluck('id');
        $proofsFiles = $proofs->map(function($proof) {
            $result = $proof->originalItems;
            if ($proof->preview) {
                $result[] = $proof->getOriginal('preview');
            }
            return $result;
        })->collapse()->toArray();

        $feeds = $proofs->map(function($proof) {
            return $proof->feed;
        });
        $feedsIds = $feeds->pluck('id');

        Feed::destroy($feedsIds);
        Proof::destroy($proofsIds);
        User::destroy($testUsersIds);
        Storage::delete($usersAvatars);
        Storage::delete($proofsFiles);
    }
}
