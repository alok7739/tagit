<?php

namespace App\Console\Commands;

use App\Modules\Challenges\Models\Proof;
use Illuminate\Console\Command;

class HandleProofsAttemptDurationTime extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'handle-proofs-attempt-duration';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Handle attempt duration time for existing proofs';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $proofs = Proof::where('attempt_duration', 0)->with('challenge')->get();
        foreach ($proofs as $proof) {
            $attemptDuration = $proof->created_at->diffInSeconds($proof->challenge->start_date);
            $proof->attempt_duration = $attemptDuration;
            $proof->save();
        }
    }
}
