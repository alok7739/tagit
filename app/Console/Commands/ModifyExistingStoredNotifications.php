<?php

namespace App\Console\Commands;

use App\Modules\Challenges\Jobs\HandleChallengesStatusJob;
use App\Modules\Challenges\Models\Challenge;
use App\Modules\Notifications\Models\DatabaseNotification;
use Illuminate\Console\Command;

class ModifyExistingStoredNotifications extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'modify-notifications';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Modify existing stored notifications (modify old-formated notifications structure  for convinient localization on client side)';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $notifications = DatabaseNotification::all();
        foreach($notifications as $notification) {
            $this->addLocVariables($notification);
        }
    }

    protected function addLocVariables(DatabaseNotification $notification)
    {
        $data = $notification->data;
        if ($data['data'] ?? null) {
            if ($data['data']['challengeId'] ?? null) {
                $challenge = \App\Modules\Challenges\Models\Challenge::find($data['data']['challengeId']);
                $challengeName = $challenge->name ?? 'Deleted challenge';
                $data['data']['variables']['challengeName'] = $challengeName;
                $notification->data = $data;
                $notification->save();
            }
        }
    }
}
