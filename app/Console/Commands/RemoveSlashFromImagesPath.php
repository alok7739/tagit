<?php


namespace App\Console\Commands;


use App\Modules\Challenges\Models\Company;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class RemoveSlashFromImagesPath extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'remove:slash';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Remove double slash from images path';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::update("update companies set logo =  REPLACE ( logo, 'companies//', 'companies/')");
        DB::update("update challenges set image =  REPLACE ( image, 'challenges//', 'challenges/')");
    }

}
