<?php

namespace App\Console\Commands;

use App\Modules\Challenges\Jobs\HandleChallengesStatusJob;
use Illuminate\Console\Command;

class HandleChallengeStatuses extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'handle-challenges';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Handle challenges statuses';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        dispatch(new HandleChallengesStatusJob());
    }
}
