<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class IsUserActive
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param  string|null $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (isset(Auth::user()->status) && Auth::user()->status == '2') {
            return response()->json(['success' => false, 'code' => 401], 401);
        }

        return $next($request);
    }
}
