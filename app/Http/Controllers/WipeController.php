<?php

namespace App\Http\Controllers;

use App\Services\ResponseBuilder\CustomResponseBuilder;
use Illuminate\Support\Facades\DB;

class WipeController extends Controller
{
    public function __invoke()
    {
        DB::table('users')->delete();
        return CustomResponseBuilder::success();
    }
}
