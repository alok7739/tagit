<?php
/**
 * Created by Vitaliy Shabunin, Appus Studio LP on 14.02.2020
 */

namespace App\Services;

class JwtService
{

    protected $header;
    public $body;

    public function __construct(string $token)
    {
        $token = explode('.', $token);
        $this->header = $this->decode($token[0]);
        $this->body = $this->decode($token[1]);
    }

    /**
     * @param string $value
     * @return mixed
     */
    protected function decode(string $value)
    {
        return json_decode(base64_decode($value));
    }

    /**
     * @return string|null
     */
    public function getSub(): ?string
    {
        return $this->body->sub ?? null;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->body->email;
    }

    /**
     * @return bool
     */
    public function isPrivateEmail(): bool
    {
        return $this->body->is_private_email ?? true;
    }

    /**
     * @return bool
     */
    public function isExpired(): bool
    {
        if ($this->body->exp < time()) {
            return true;
        }
        return false;
    }

}
