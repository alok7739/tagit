<?php


namespace App\Services;


use Illuminate\Support\Facades\DB;

class CountriesRepository
{
    private $table = 'countries';

    public static function make()
    {
        return new static();
    }

    public function all()
    {
        return DB::table($this->table)->orderBy('name')->get();
    }

    public function find($id)
    {
        return DB::table($this->table)->where('id', $id)->first();
    }
}
