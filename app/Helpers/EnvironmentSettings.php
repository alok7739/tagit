<?php
/**
 * Created by Maksym Ignatchenko, Appus Studio LP on 27.06.19
 *
 */

namespace App\Helpers;

class EnvironmentSettings
{
    public const POSTGRES_MAX_ID = 2147483627;
}