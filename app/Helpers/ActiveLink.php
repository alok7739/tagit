<?php
/**
 * Created by Artem Petrov, Appus Studio LP on 21.11.2017
 */

namespace App\Helpers;

use App\Modules\Challenges\Http\Controllers\Admin\ChallengeController;
use App\Modules\Challenges\Http\Controllers\Admin\ProofController;
use App\Modules\Companies\Http\Controllers\Admin\CompanyController;
use App\Modules\Content\Http\Controllers\Admin\ContentController;
use App\Modules\Users\Admin\Http\Controllers\AdminController;
use App\Modules\Users\Admin\Http\Controllers\DashboardController;
use App\Modules\Users\User\Http\Controllers\Admin\UserController;
use App\Modules\Users\Admin\Http\Controllers\ChatController;
use App\Modules\Users\Admin\Http\Controllers\CommentsController;
use App\Modules\Challenges\Http\Controllers\Admin\RankingController;
use App\Modules\Challenges\Http\Controllers\Admin\ReportsController;
use App\Modules\Library\Http\Controllers\Admin\CategoryController;
use App\Modules\Library\Http\Controllers\Admin\LibraryController;
use App\Modules\Misc\Http\Controllers\Admin\ContactController;
use App\Modules\Onboarding\Http\Controllers\Admin\CategoryController as GamesCategoryController;
use App\Modules\Onboarding\Http\Controllers\Admin\GamesController;
use App\Modules\Onboarding\Http\Controllers\Admin\PackagesController;
use App\Modules\Onboarding\Http\Controllers\GamesController as FrontGamesController;
use App\Modules\Onboarding\Http\Controllers\PricingController;
use App\Modules\Users\Admin\Http\Controllers\LoginController;
use Illuminate\Support\Facades\Request;

class ActiveLink
{
    /**
     * @return bool
     */
    public static function checkDashboard(): bool
    {
        $controller = self::getControllerInstance();

        return $controller instanceof DashboardController;
    }

    /**
     * @return bool
     */
    public static function checkContent(): bool
    {
        $controller = self::getControllerInstance();

        return $controller instanceof ContentController;
    }

    /**
     * @return bool
     */
    public static function checkUser(): bool
    {
        $controller = self::getControllerInstance();

        return $controller instanceof UserController;
    }

    /**
     * @return bool
     */
    public static function checkCompany(): bool
    {
        $controller = self::getControllerInstance();

        return $controller instanceof CompanyController;
    }

    /**
     * @return bool
     */
    public static function checkChallenge(): bool
    {
        $controller = self::getControllerInstance();

        return $controller instanceof ChallengeController
            || $controller instanceof ProofController;
    }

    /**
     * @return bool
     */
    public static function checkAdministration(): bool
    {
        $controller = self::getControllerInstance();

        return $controller instanceof AdminController;
    }

    /**
     * @return mixed
     */
    protected static function getControllerInstance()
    {
        return Request::route()->getController();
    }

    protected static function getAction()
    {
        $action = Request::route()->getActionName();
        $action = explode('@', $action);
        return end($action);
    }

    public static function checkNewChallenge(): bool
    {
        $controller = self::getControllerInstance();
        $action = self::getAction();

        return $controller instanceof ChallengeController && $action == 'create';
    }

    public static function checkRanking(): bool
    {
        $controller = self::getControllerInstance();
        $action = self::getAction();

        return $controller instanceof RankingController && $action == 'index';
    }

    public static function checkLiveRanking(): bool
    {
        $controller = self::getControllerInstance();
        $action = self::getAction();

        return $controller instanceof RankingController && $action == 'live';
    }

    public static function checkChat(): bool
    {
        $controller = self::getControllerInstance();

        return $controller instanceof ChatController;
    }

    public static function checkComments(): bool
    {
        $controller = self::getControllerInstance();

        return $controller instanceof CommentsController;
    }

    public static function checkLogin(): bool
    {
        $controller = self::getControllerInstance();

        return $controller instanceof LoginController;
    }

    public static function checkPricing(): bool
    {
        $controller = self::getControllerInstance();

        return $controller instanceof PricingController;
    }

    public static function checkPackages(): bool
    {
        $controller = self::getControllerInstance();

        return $controller instanceof PackagesController;
    }

    public static function checkGames(): bool
    {
        $controller = self::getControllerInstance();

        return $controller instanceof GamesController;
    }

    public static function checkGamesCategory(): bool
    {
        $controller = self::getControllerInstance();

        return $controller instanceof GamesCategoryController;
    }

    public static function checkContact(): bool
    {
        $controller = self::getControllerInstance();

        return $controller instanceof ContactController;
    }

    public static function checkChallengeReports(): bool
    {
        $controller = self::getControllerInstance();

        return $controller instanceof ReportsController;
    }

    public static function checkLibraryCatergory(): bool
    {
        $controller = self::getControllerInstance();

        return $controller instanceof CategoryController;
    }

    public static function checkLibrary(): bool
    {
        $controller = self::getControllerInstance();

        return $controller instanceof LibraryController;
    }

    public static function checkFrontGames(): bool
    {
        $controller = self::getControllerInstance();

        return $controller instanceof FrontGamesController;
    }
}
