@extends('layouts.app')
@section('title', 'Edit Category: ' . $category->name)

@section('content')
    <section class="content">
        <div class="clearfix"></div>

        @include('flash::message')
        {!! Form::open(['url'=> route('category.update', ['category' => $category->id]), 'method' => 'PUT']) !!}
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-body">
                        @include('category.edit_fields')
                    </div>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </section>
@endsection
