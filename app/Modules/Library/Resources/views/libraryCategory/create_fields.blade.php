<h3 class="new_heading">Create Category</h3>

<div class="row">
    <div class="col-sm-8">
        <div class="form-group">
            {{ Form::label('name', 'Name: ', ['class' => 'required']) }}
            {!! Form::text('name', null, ['class' => 'form-control', 'maxlength' => 30]) !!}
            @if ($errors->has('name'))
            <div class="text-red">{{ $errors->first('name') }}</div>
            @endif
        </div>
    </div>

    <div class="col-sm-4">
        <div class="form-group">
            {{ Form::label('status', 'Status: ', ['class' => 'required']) }}
            {!! Form::select('status', [1=> 'Active', 0 => 'Inactive'], null, ['id' => 'status', 'class' => 'form-control']) !!}
        </div>
    </div>
</div>

<!-- Submit Field -->
<div class="form-group text-right">
    {!! Form::submit('Save', ['class' => 'btn btn-success']) !!}
</div>
