

<div class="btn-group" role="group">
    <button id="btnGroupDrop1" type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Actions
    </button>

    <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
        <a href="{{ route('library.index', ['category' => $id]) }}" class='dropdown-item'>View Library</a>

        @can('edit', 'App\Modules\Library\Models\LibraryCategory')
        <a href="{{ route('category.edit', $id) }}" class='dropdown-item'>Edit</a>
        @endcan

        @can('destroy', 'App\Modules\Library\Models\LibraryCategory')
            <a href="#" class='dropdown-item' onclick="document.getElementById('delete-{{$id}}-button').click()">
                {!! Form::open(['method'=>'DELETE', 'url'=> route('category.destroy', $id)]) !!}
                <button style="display:none" id="delete-{{$id}}-button" data-toggle="tooltip" data-placement="top" title="Delete"
                        type="submit" class="dropdown-item"
                        onclick="return confirm('Are you sure you want delete this?');">
                </button>
                {!! Form::close() !!}
                Delete
            </a>
        @endcan
    </div>
</div>
