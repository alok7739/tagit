@extends('layouts.app')
@section('title', 'Challenge Library Categories')

@section('content')
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box">
            <div class="box-body">
                <h3 class="new_heading pull-left">Challenge Library Categories</h3>
                @can('create', 'App\Modules\Library\Models\LibraryCategory')
                    <a href="{{route('category.create')}}" class="btn btn-primary pull-right create-article">New Category</a>
                @endcan
                <div class="clearfix"></div>

                @include('libraryCategory.table')
            </div>
        </div>
    </div>
@endsection
