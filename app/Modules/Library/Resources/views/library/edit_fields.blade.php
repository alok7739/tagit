@php
$lat_lng = old('location_latlng') ? explode(',', old('location_latlng')) : $lat_lng;
@endphp

<h3 class="new_heading">Edit Library Challenge</h3>

<div class="row">
    <div class="col-sm-6">
        <ul class="nav nav-tabs">
            <li><a href="#tab_en" class="active" data-toggle="tab"><img src="{{ asset('assets/images/en.png') }}" /> English</a></li>
        </ul>

        <div class="tab-content">
            <div id="tab_en" class="tab-pane fade in active">
                <div class="form-group">
                    {{ Form::label('name', 'Name: ', ['class' => 'required']) }}
                    {!! Form::text('name', $library->name, ['class' => 'form-control', 'maxlength' => 50]) !!}
                    @if ($errors->has('name'))
                    <div class="text-red">{{ $errors->first('name') }}</div>
                    @endif
                </div>

                <div class="form-group">
                    {{ Form::label('description', 'Description: ', ['class' => 'required']) }}
                    {!! Form::textarea('description', $library->description, ['id' => 'desc_en', 'data-lang' => 'en', 'class' => 'form-control ckeditor', 'maxlength' => 1000]) !!}
                    @if ($errors->has('description'))
                    <div class="text-red">{{ $errors->first('description') }}</div>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <div class="col-sm-6">
        <ul class="nav nav-tabs">
            <li><a href="#tab_ar" class="active" data-toggle="tab"><img src="{{ asset('assets/images/ar.png') }}" /> Arabic</a></li>
        </ul>

        <div class="tab-content">
            <div id="tab_ar" class="tab-pane fade in rtl active">
                <div class="form-group">
                    {{ Form::label('name_ar', 'Arabic Name: ', ['class' => 'required']) }}
                    {!! Form::text('name_ar', $library->name_ar, ['class' => 'form-control', 'maxlength' => 50]) !!}
                    @if ($errors->has('name_ar'))
                    <div class="text-red">{{ $errors->first('name_ar') }}</div>
                    @endif
                </div>

                <div class="form-group">
                    {{ Form::label('description', 'Arabic Description: ', ['class' => 'required']) }}
                    {!! Form::textarea('description_ar', $library->description_ar, ['id' => 'desc_ar', 'data-lang' => 'ar', 'class' => 'form-control ckeditor', 'maxlength' => 1000]) !!}
                    @if ($errors->has('description_ar'))
                    <div class="text-red">{{ $errors->first('description_ar') }}</div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-4">
        <div class="form-group">
            {{ Form::label('category_id', 'Category: ', ['class' => 'required']) }}
            {!! Form::select('category_id', $categories, $library->category_id, ['class' => 'form-control', 'placeholder' => 'Select category']) !!}
            @if ($errors->has('category_id'))
            <div class="text-red">{{ $errors->first('category_id') }}</div>
            @endif
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            {{ Form::label('status', 'Status: ', ['class' => 'required']) }}
            {!! Form::select('status', [1 => 'Active', 0 => 'Inactive'], $library->status, ['class' => 'form-control']) !!}
            @if ($errors->has('status'))
            <div class="text-red">{{ $errors->first('status') }}</div>
            @endif
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            {{ Form::label('participants_limit', 'Participants limit: ', ['class' => 'required']) }}
            {!! Form::text('participants_limit', $library->participants_limit, ['class' => 'form-control', 'maxlength' => 10]) !!}
            @if ($errors->has('participants_limit'))
            <div class="text-red">{{ $errors->first('participants_limit') }}</div>
            @endif
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="form-group">
            {{ Form::label('link', 'Link: ') }}
            {!! Form::text('link', $library->link, ['class' => 'form-control', 'maxlength' => 100]) !!}
            @if ($errors->has('link'))
            <div class="text-red">{{ $errors->first('link') }}</div>
            @endif
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-4">
        <div class="form-group">
            {{ Form::label('proof_type', 'Proof type: ', ['class' => 'required']) }}
            {!! Form::select('proof_type', $dto->getProofTypes(), $library->proof_type, ['class' => 'form-control', 'placeholder' => 'Select proof type']) !!}
            @if ($errors->has('proof_type'))
            <div class="text-red">{{ $errors->first('proof_type') }}</div>
            @endif
        </div>
    </div>

    <div class="col-sm-4" id="item_count_div">
        <div class="form-group">
            {{ Form::label('items_count_in_proof', 'Required count of items for proof: ', ['class' => 'required']) }}
            {!! Form::select('items_count_in_proof', $proof_count, $library->items_count_in_proof, ['id' => 'item_count_fld',  'class' => 'form-control', 'placeholder' => 'Select proof items count']) !!}
            @if ($errors->has('items_count_in_proof'))
            <div class="text-red">{{ $errors->first('items_count_in_proof') }}</div>
            @endif
        </div>
    </div>

    <div class="col-sm-4" id="location_bound_div">
        <div class="form-group">
            {{ Form::label('location_bound', 'Location Bound? ', ['class' => 'required']) }}
            {!! Form::select('location_bound', [0 => 'No', 1 => 'Yes'], $library->location_bound, ['class' => 'form-control']) !!}
            @if ($errors->has('location_bound'))
            <div class="text-red">{{ $errors->first('location_bound') }}</div>
            @endif
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div id="text-proofs-block" data-questions="{{ json_encode(old('questions') ? old('questions') : $questions) }}" data-answers="{{ json_encode(old('answers') ? old('answers') : $answers) }}" data-tests="{{ json_encode(old('test_answers') ? old('test_answers') : $test_answers) }}"></div>
        <input type="hidden" name="qids" value="{{ implode(',', $qids) }}" />
    </div>
    @if ($errors->has('questions.*') || $errors->has('answers.*') || $errors->has('test_answers.*.*'))
    <div class="col-md-12 text-red form-group">Answers and questions fields are required. (Question max length - 200. Answer max length - 15)</div>
    @endif
</div>

<div class="row" id="video-duration-section">
    <div class="col-sm-4">
        <div class="form-group">
            {{ Form::label('video_duration', 'Video duration: ', ['class' => 'required']) }}
            {!! Form::select('video_duration', ['15' => '15 seconds', '30' => '30 seconds'], $library->video_duration, ['class' => 'form-control', 'placeholder' => 'Select video duration']) !!}
            @if ($errors->has('video_duration'))
            <div class="text-red">{{ $errors->first('video_duration') }}</div>
            @endif
        </div>
    </div>
</div>

<div class="row" id="map_row" style="display: none;">
    <div class="col-sm-4">
        <div class="form-group">
            {{ Form::label('location_latlng', 'Search Address: ') }}
            <input name="location" id="autocomplete" placeholder="Enter address to search" type="text" class="form-control" value="{{ $library->location }}" />
            {!! Form::hidden('location_latlng', $library->location_latlng, ['id' => 'location_latlng']) !!}
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            {{ Form::label('location_radius', 'Radius: ', ['class' => 'required']) }}
            {!! Form::select('location_radius', $dto->getRadiuses(), $library->location_radius, ['id' => 'radius', 'class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-sm-12">
        <div id="map_canvas" style="width:100%; height:250px;"></div>
    </div>
</div>

<div class="row">
    <div class="col-sm-4">
        <div class="form-group">
            {!! Form::label('cover-image', 'Cover image', ['class' => 'required']) !!}
        </div>
        <div class="form-group dropzone challenge-logo-dropzone dz-clickable"></div>
        @if ($errors->has('image'))
        <div class="text-red">{{ $errors->first('image') }}</div>
        @endif
        {!! Form::hidden('image', $library->image) !!}
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <label>&nbsp;</label>
        </div>
        @if ($library->image)
        <img class="dashboard-image" src="{{$library->image}}">
        @elseif (old('image'))
        <img class="dashboard-image" src="{{Storage::url(old('image'))}}">
        @else
        <img class="dashboard-image" style="display:none" src="">
        @endif
    </div>
</div>

<!-- Submit Field -->
<div class="form-group text-right">
    {!! Form::submit('Save', ['class' => 'btn btn-success']) !!}
</div>

<script src="/build/js/ckeditor/ckeditor.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDc0H6iXPd9ta8HUzURxqHyLD4Slhv50DI&libraries=places&callback=initialize" async defer></script>
<script>
var lat = {{ empty($lat_lng[0]) ? 21.5582185 : $lat_lng[0] }},
    lng = {{ empty($lat_lng[1]) ? 39.1486315 : $lat_lng[1] }};
var map, ac, marker, circle;

function initialize() {
    map = new google.maps.Map(document.getElementById("map_canvas"), {
        center: new google.maps.LatLng(lat, lng),
        zoom: 17,
        disableDefaultUI: false,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    ac = new google.maps.places.Autocomplete((document.getElementById('autocomplete')));

    marker = new google.maps.Marker({
        position: new google.maps.LatLng(lat, lng),
        map: map,
        draggable: true
    });

    circle = new google.maps.Circle({
        strokeColor: "#FF0000",
        strokeOpacity: 0.8,
        strokeWeight: 1,
        fillColor: "#FF0000",
        fillOpacity: 0.35,
        map,
        center: new google.maps.LatLng(lat, lng),
        radius: parseInt($('#radius').val()),
    });

    google.maps.event.addListener(ac, 'place_changed', function() {
        var place = ac.getPlace();

        lat = place.geometry.location.lat();
        lng = place.geometry.location.lng();

        var latlng = new google.maps.LatLng(lat, lng);
        marker.setPosition(latlng);
        map.setCenter(latlng);
        circle.setCenter(latlng);

        $('#location_latlng').val(lat + ',' + lng);
    });

    google.maps.event.addListener(marker, 'dragend', function() {
        var position = marker.getPosition();

        lat = position.lat();
        lng = position.lng();

        var latlng = new google.maps.LatLng(lat, lng);
        circle.setCenter(latlng);

        $('#location_latlng').val(lat + ',' + lng);
    });

    $('#location_latlng').val(lat + ',' + lng);
}

$(function() {
    $('#item_count_fld, #proof_type').on('change', function() {
        if($.inArray($('#proof_type').val(), ['questions', 'tests', 'true_false']) !== -1) {
            addQuestionProofFields($('#item_count_fld').val());
        }
    });

    $('#proof_type').trigger('change');

    $('#radius').change(function(){
        circle.setRadius(parseInt($(this).val()));
    });

    function addQuestionProofFields(quantity) {
        let additionalHtml = '';
        let textProofsBlock = $('#text-proofs-block');
        let oldQuestions = textProofsBlock.data('questions');
        let oldAnswers = textProofsBlock.data('answers');
        let oldTests = textProofsBlock.data('tests');
        let selectedType = $('#proof_type').val();

        for (let i = 0; i < quantity; i++) {
            if (oldAnswers || oldQuestions || oldTests) {
                if (selectedType === 'tests') {
                    additionalHtml += formTestingProofFields(
                        i,
                        oldQuestions[i] ? oldQuestions[i] : '',
                        oldTests !== null ? oldTests[i] ? oldTests[i] : ['', '', '', ''] : ['', '', '', '']
                    )
                } else if (selectedType === 'questions') {
                    additionalHtml += formQuestionProofFields(
                        i,
                        oldQuestions[i] ? oldQuestions[i] : '',
                        oldAnswers !== null ? oldAnswers[i] ? oldAnswers[i] : '' : ''
                    )
                } else if (selectedType === 'true_false') {
                    additionalHtml += formTrueFalseProofFields(
                        i,
                        oldQuestions[i] ? oldQuestions[i] : '',
                        oldAnswers !== null ? oldAnswers[i] ? oldAnswers[i] : '' : ''
                    )
                }
            } else {
                if (selectedType === 'tests') {
                    additionalHtml += formTestingProofFields(i)
                } else if (selectedType === 'questions') {
                    additionalHtml += formQuestionProofFields(i)
                } else if (selectedType === 'true_false') {
                    additionalHtml += formTrueFalseProofFields(i)
                }
            }
        }

        if(additionalHtml) {
            additionalHtml += '<hr />';
        }

        textProofsBlock.html(additionalHtml)
    }

    function formQuestionProofFields(index, oldQuestion = '', oldAnswer = '') {
        return "\n" +
            "<hr />\n" +
            "<div class=\"row\">\n" +
            "    <div class=\"col-md-4\">\n" +
            "        <div class=\"form-group\">\n" +
            "            <textarea class=\"form-control\" name=\"questions[" + index + "]\" type=\"text\" placeholder=\"Enter question\" rows=\"2\">" + oldQuestion + "</textarea>\n" +
            "        </div>\n" +
            "    </div>\n" +
            "    <div class=\"col-md-4\">\n" +
            "        <div class=\"form-group\">\n" +
            "            <textarea class=\"form-control\" name=\"answers[" + index + "]\" type=\"text\" placeholder=\"Enter answers (comma seperated)\" rows=\"2\">" + oldAnswer + "</textarea>\n" +
            "        </div>\n" +
            "    </div>\n" +
            "</div>\n" +
            "\n";
    }

    function formTestingProofFields(index, oldQuestion = '', oldAnswer = ['', '', '', '']) {
        let answer1 = oldAnswer[0] === null ? '' : oldAnswer[0];
        let answer2 = oldAnswer[1] === null ? '' : oldAnswer[1];
        let answer3 = oldAnswer[2] === null ? '' : oldAnswer[2];
        let answer4 = oldAnswer[3] === null ? '' : oldAnswer[3];

        return "\n" +
            "<hr />\n" +
            "<div class=\"row\">\n" +
            "    <div class=\"col-md-4\">\n" +
            "        <div class=\"form-group\">\n" +
            "            <textarea rows='3' class=\"form-control\" name=\"questions[" + index + "]\" type=\"text\" placeholder=\"Enter question\">" + oldQuestion + "</textarea>\n" +
            "        </div>\n" +
            "    </div>\n" +
            "    <div class=\"col-md-8\">\n" +
            "        <div class=\"row\">\n" +
            "            <div class=\"col-md-4\">\n" +
            "                <div class=\"form-group\">\n" +
            "                    <input class=\"form-control\" value=\"" + answer1 + "\" name=\"test_answers[" + index + "][0]\" type=\"text\" placeholder=\"Enter correct answer\">\n" +
            "                </div>\n" +
            "            </div>\n" +
            "        </div>\n" +
            "        <div class=\"row\">\n" +
            "            <div class=\"col-md-4\">\n" +
            "                <div class=\"form-group\">\n" +
            "                    <input class=\"form-control\" value=\"" + answer2 + "\" name=\"test_answers[" + index + "][1]\" type=\"text\" placeholder=\"Enter incorrect answer\">\n" +
            "                </div>\n" +
            "            </div>\n" +
            "            <div class=\"col-md-4\">\n" +
            "                <div class=\"form-group\">\n" +
            "                    <input class=\"form-control\" value=\"" + answer3 + "\" name=\"test_answers[" + index + "][2]\" type=\"text\" placeholder=\"Enter incorrect answer\">\n" +
            "                </div>\n" +
            "            </div>\n" +
            "            <div class=\"col-md-4\">\n" +
            "                <div class=\"form-group\">\n" +
            "                    <input class=\"form-control\" value=\"" + answer4 + "\" name=\"test_answers[" + index + "][3]\" type=\"text\" placeholder=\"Enter incorrect answer\">\n" +
            "                </div>\n" +
            "            </div>\n" +
            "        </div>\n" +
            "    </div>\n" +
            "</div>\n" +
            "\n";
    }

    function formTrueFalseProofFields(index, oldQuestion = '', oldAnswer = '') {
        return "\n" +
            "<hr />\n" +
            "<div class=\"row\">\n" +
            "    <div class=\"col-md-4\">\n" +
            "        <div class=\"form-group\">\n" +
            "            <textarea class=\"form-control\" name=\"questions[" + index + "]\" type=\"text\" placeholder=\"Enter question\">" + oldQuestion + "</textarea>\n" +
            "        </div>\n" +
            "    </div>\n" +
            "    <div class=\"col-md-2\">\n" +
            "        <div class=\"form-group\">\n" +
            "            <select class=\"form-control\" name=\"answers[" + index + "]\">" +
            "                <option value=\"True\">True</option>\n" +
            "                <option value=\"False\" " + (oldAnswer == 'False' ? 'selected' : '') + ">False</option>\n" +
            "            </select>\n" +
            "        </div>\n" +
            "    </div>\n" +
            "</div>\n" +
            "\n";
    }

    $('.ckeditor').each(function() {
        var id = $(this).attr('id');
        var lang = $(this).attr('data-lang');

        ClassicEditor.create(document.querySelector('#' + id), {
                toolbar: {
                    items: [
                        'bold',
                        'italic',
                        'underline',
                        '|',
                        'numberedList',
                        'bulletedList',
                        '|',
                        'strikethrough',
                        'subscript',
                        'superscript'
                    ]
                },
                language: lang,
                licenseKey: '',
            })
            .then(editor => {
                window.editor = editor;
            })
            .catch(error => {});
    })
})
</script>
