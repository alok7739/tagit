<div class="btn-group" role="group">
    <button id="btnGroupDrop1" type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Actions
    </button>

    <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">

        <a class="dropdown-item" href="{{ route('challenge.create', ['companyId' => request()->get('companyId'), 'challengeId' => $id]) }}">Use</a>

        <form action="{{ route('challenge.remove-from-library', ['challenge' => $id]) }}" method="post">
            @csrf
            <button class="dropdown-item">Remove From Library</button>
        </form>
    </div>
</div>
