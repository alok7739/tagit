@extends('layouts.app')
@section('title', 'Edit library challenge')

@section('content')
    <section class="content">
        <div class="clearfix"></div>
        @include('flash::message')
        {!! Form::open(['url'=> route('library.update', ['library' => $library->id]), 'method' => 'PUT', 'files' => true]) !!}
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-body">
                        @include('library.edit_fields')
                    </div>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </section>
@endsection
