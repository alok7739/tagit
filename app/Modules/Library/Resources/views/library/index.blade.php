@extends('layouts.app')
@section('title', 'Library Challenges')

@section('content')
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box">
            <div class="box-body">
                <h3 class="new_heading pull-left">Challenge Library</h3>
                @can('create', 'App\Modules\Library\Models\Library')
                    <a href="{{route('library.create')}}" class="btn btn-primary pull-right create-article">New Challenge</a>
                @endcan
                <div class="clearfix"></div>

                <div class="inner-dashboard-screens">
                    <div class="tabs-listing">
                        <ul class="nav nav-pills">
                            @if($categories)
                                <li><a href="{{route('library.' . $route, ['companyId' => request()->get('companyId')])}}">View All</a></li>
                                @foreach($categories as $k => $v)
                                <li><a href="{{route('library.' . $route, ['companyId' => request()->get('companyId'), 'category' => $k])}}">{{ $v }}</a></li>
                                @endforeach
                            @endif
                        </ul>
                    </div>
                </div>

                @include('library.table')
            </div>
        </div>
    </div>
@endsection
