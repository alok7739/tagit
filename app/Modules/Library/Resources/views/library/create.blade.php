@extends('layouts.app')
@section('title', 'Create library challenge')

@section('content')
    <section class="content">
        <div class="clearfix"></div>
        @include('flash::message')
        {!! Form::open(['route' => 'library.store', 'method' => 'POST', 'files' => true]) !!}
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-body">
                        @include('library.create_fields')
                    </div>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </section>
@endsection
