@extends('layouts.app')
@section('title', 'My Challenges')

@section('content')
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box">
            <div class="box-body">
                <h3 class="new_heading">My Library</h3>
                <div class="clearfix"></div>

                @include('library.table')
            </div>
        </div>
    </div>
@endsection
