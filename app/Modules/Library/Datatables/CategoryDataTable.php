<?php
namespace App\Modules\Library\Datatables;

use App\Modules\Library\Models\LibraryCategory;
use Illuminate\Database\Eloquent\Builder;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Builder as YajraBuilder;
use Yajra\DataTables\Services\DataTable;

class CategoryDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Result from query() method.
     * @return EloquentDataTable
     */
    public function dataTable($query): EloquentDataTable
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable
            ->addColumn('action', 'libraryCategory.datatables_actions')
            ->editColumn('status', function ($query) {
                return $query->status ? 'Active' : 'Inactive';
            })
            ->rawColumns(['name', 'status', 'action']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param Packages $model
     * @return Packages
     */
    public function query(LibraryCategory $model): Builder
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return YajraBuilder
     */
    public function html(): YajraBuilder
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax(url()->current())
            ->addAction(['width' => '10%'])
            ->parameters([
                'dom'     => 'frtip',
                'order'   => [[0, 'asc']],
                'responsive' => true,
                'drawCallback' => '
                    function() {
                        $.SweetAlert.init();

                    }',
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns(): array
    {
        return [
            [
                'name' => 'name',
                'data' => 'name',
                'title' => 'Name',
                'width' => '80%',
            ],
            [
                'name' => 'status',
                'data' => 'status',
                'title' => 'Status',
                'width' => '10%',
            ]
        ];
    }
}
