<?php

namespace App\Modules\Library\Datatables;

use App\Modules\Library\Models\Library;
use App\Modules\Challenges\Enums\ProofTypeEnum;
use App\Modules\Companies\Models\Company;
use Illuminate\Database\Eloquent\Builder;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Builder as YajraBuilder;
use Yajra\DataTables\Services\DataTable;

class PublicLibraryDataTable extends DataTable
{
    protected $category;

    /**
     * Build DataTable class.
     *
     * @param mixed $query Result from query() method.
     * @return EloquentDataTable
     */
    public function dataTable($query): EloquentDataTable
    {
        $dataTable = new EloquentDataTable($query);
        $proof_types = ProofTypeEnum::getNames();

        return $dataTable
            ->addColumn('action', 'library.public_datatables_actions')
            ->editColumn('image', function ($query) {
                return ($query->image ? ("<img height='50' src=" . $query->image) . " />" : (''));
            })
            ->editColumn('proof_type', function($query) use($proof_types) {
                $query->proof_type = $query->proof_type == ProofTypeEnum::MULTIPLE_PHOTOS ? ProofTypeEnum::PHOTO : $query->proof_type;
                $query->proof_type = $query->proof_type == ProofTypeEnum::MULTIPLE_VIDEOS ? ProofTypeEnum::VIDEO : $query->proof_type;
                $query->proof_type = $query->proof_type == ProofTypeEnum::MULTIPLE_SCREENSHOTS ? ProofTypeEnum::SCREENSHOT : $query->proof_type;

                return $proof_types[$query->proof_type];
            })
            ->rawColumns(['image', 'action']);
    }

    public function setCategory(int $category = null): DataTable
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get query source of dataTable.
     *
     * @param Challenge $model
     * @return Builder
     */
    public function query(Library $model): Builder
    {
        $companyId = request()->get('companyId');
        $proof_types = [];

        if($companyId) {
            $proof_types = Company::getAllowedProofTypes($companyId);
        }

        if ($this->category) {
            return $model->with(['category'])
                        ->where('category_id', $this->category)
                        ->where('status', '1')
                        ->whereIn('proof_type', $proof_types);
        }

        return $model->with(['category'])
                        ->where('status', '1')
                        ->whereIn('proof_type', $proof_types);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return YajraBuilder
     */
    public function html(): YajraBuilder
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax(request()->getRequestUri())
            ->addAction(['width' => '10%'])
            ->parameters([
                'dom'     => 'frtip',
                'order'   => [[1, 'desc']],
                'responsive' => true,
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns(): array
    {
        return [
            [
                'name' => 'image',
                'data' => 'image',
                'title' => 'Image',
                'width' => '10%',
                'searchable' => false,
                'orderable' => false,
            ],
            [
                'name' => 'name',
                'data' => 'name',
                'title' => 'Name',
                'width' => '40%',
            ],
            [
                'name' => 'proof_type',
                'data' => 'proof_type',
                'title' => 'Proof Type',
                'width' => '20%',
            ],
            [
                'name' => 'category.name',
                'data' => 'category.name',
                'title' => 'Category',
                'width' => '20%',
            ],
        ];
    }

}
