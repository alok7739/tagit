<?php

namespace App\Modules\Library\Models;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    public $table = 'library_answers';

    protected $fillable = [
        'question_id',
        'text',
        'type',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    public const CORRECT = 'Correct';
    public const INCORRECT = 'Incorrect';

    public function question()
    {
        return $this->belongsTo(Question::class);
    }
}
