<?php

namespace App\Modules\Library\Models;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    public $table = 'library_questions';

    protected $fillable = [
        'library_id',
        'text',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    public function library()
    {
        return $this->belongsTo(Library::class);
    }

    public function answers()
    {
        return $this->hasMany(Answer::class);
    }
}
