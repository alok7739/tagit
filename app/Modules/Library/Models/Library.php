<?php

namespace App\Modules\Library\Models;

use App\Models\BaseModel;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\SoftDeletes;

class Library extends BaseModel
{
    use SoftDeletes;

    public $table = 'library';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    /**
     * @var array
     */
    public $fillable = [
        'category_id',
        'name',
        'name_ar',
        'image',
        'description',
        'description_ar',
        'link',
        'participants_limit',
        'proof_type',
        'items_count_in_proof',
        'video_duration',
        'location',
        'location_latlng',
        'location_radius',
        'location_bound',
        'status',
    ];

    /**
     * @param $value
     * @return string|null
     */
    public function getImageAttribute($value): ?string
    {
        return $value ? Storage::url(str_replace(env('AWS_URL'), '', $value)) : null;
    }

    /**
     * @return string
     */
    public function getImageWithDefaultAttribute($value): ?string
    {
        return $value ? Storage::url($value) : '/assets/images/default_challenge.svg';
    }

    /**
     * @param $attribute
     */
    public function setImageAttribute($attribute)
    {
        if (null !== $this->image) {
            Storage::delete($this->image);
        }

        $this->attributes['image'] = $attribute;
    }

    public function category()
    {
        return $this->belongsTo(LibraryCategory::class);
    }

    public function questions()
    {
        return $this->hasMany(Question::class);
    }

    public function saveQuestions(array $entities)
    {
        foreach ($entities['questions'] as $index => $question) {
            $question = Question::create([
                'library_id' => $this->id,
                'text' => $question,
            ]);
            Answer::create([
                'question_id' => $question->id,
                'text' => $entities['answers'][$index],
                'type' => Answer::CORRECT,
            ]);
        }
    }

    public function saveTests(array $entities)
    {
        foreach ($entities['questions'] as $qIndex => $question) {
            $question = Question::create([
                'library_id' => $this->id,
                'text' => $question,
            ]);

            foreach ($entities['test_answers'][$qIndex] as $aIndex => $answer) {
                if ($aIndex === 0) {
                    $entity = [
                        'question_id' => $question->id,
                        'text' => $answer,
                        'type' => Answer::CORRECT,
                    ];
                } else {
                    $entity = [
                        'question_id' => $question->id,
                        'text' => $answer,
                        'type' => Answer::INCORRECT,
                    ];
                }

                Answer::create($entity);
            }
        }
    }

    public function saveTrueFalse(array $entities)
    {
        foreach ($entities['questions'] as $index => $question) {
            $question = Question::create([
                'library_id' => $this->id,
                'text' => $question,
            ]);
            Answer::create([
                'question_id' => $question->id,
                'text' => $entities['answers'][$index],
                'type' => Answer::CORRECT,
            ]);
            Answer::create([
                'question_id' => $question->id,
                'text' => $entities['answers'][$index] == 'True' ? 'False' : 'True',
                'type' => Answer::INCORRECT,
            ]);
        }
    }
}
