<?php


namespace App\Modules\Library\Policies;

use App\Modules\Users\Admin\Models\Admin;

class LibraryPolicy
{
    public function view(Admin $admin)
    {
        return true;
    }

    public function edit(Admin $admin)
    {
        return $admin->isAdmin();
    }

    public function create(Admin $admin)
    {
        return $admin->isAdmin();
    }

    public function destroy(Admin $admin)
    {
        return $admin->isAdmin();
    }
}
