<?php

namespace App\Modules\Library\Http\Requests\Admin;

use App\Modules\Challenges\Enums\ProofTypeEnum;
use App\Modules\Challenges\Enums\VideoLengthEnum;
use App\Modules\Challenges\Rules\MinProofItemsCount;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreLibraryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'category_id' => 'required',
            'name' => 'required|string|max:50',
            'name_ar' => 'required|string|max:50',
            'image' => 'required|string|max:255',
            'description' => 'required|string|max:1000',
            'description_ar' => 'required|string|max:1000',
            'link' => 'nullable|url|max:255',
            'participants_limit' => 'int|max:10000',
            'proof_type' => ['required', 'string', 'max:100', Rule::in(ProofTypeEnum::getAll())],
            'items_count_in_proof' => 'required|integer|min:' . MinProofItemsCount::get($this) . '|max:' . config('custom.max_items_in_proof'),
            'video_duration' => ['nullable', 'required_if:proof_type,video', 'integer', Rule::in(VideoLengthEnum::getAll())],
            'questions.*' => ['required', 'string', 'max:300'],
            'test_answers.*.*' => ['required', 'string', 'max:86'],
            'answers.*' => ['required', 'string', 'max:200'],
            'location' => 'nullable|string|max:255',
            'location_latlng' => 'nullable|string|max:100',
            'location_radius' => 'nullable|integer|min:1|max:1000',
        ];
    }

    public function messages()
    {
        return [
            'name_ar.required' => 'The arabic name field is required.',
            'name_ar.max' => 'The arabic name may not be greater than :max characters.',

            'description_ar.required' => 'The arabic description field is required.',
            'description_ar.max' => 'The arabic description may not be greater than :max characters.'
        ];
    }
}
