<?php

namespace App\Modules\Library\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Modules\Library\Datatables\CategoryDataTable;
use App\Modules\Library\Models\LibraryCategory;
use App\Modules\Library\Http\Requests\Admin\StoreCategoryRequest;
use App\Modules\Library\Http\Requests\Admin\UpdateCategoryRequest;
use App\Modules\Library\Models\Library;
use Laracasts\Flash\Flash;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param CategoryDataTable $dataTable
     * @return mixed
     */
    public function index(CategoryDataTable $dataTable)
    {
        return $dataTable->render('libraryCategory.index');
    }

    public function create()
    {
        return view('libraryCategory.create');
    }

    public function store(StoreCategoryRequest $request)
    {
        $category = app()[LibraryCategory::class];
        $category->fill($request->all());
        $category->save();

        Flash::success('Category created successfully.');

        return redirect()->route('category.index');
    }

    public function edit(int $id)
    {
        $category = LibraryCategory::findOrFail($id);

        if (null === $category) {
            flash('No such record');
            return redirect()->route('category.index');
        }

        return view('libraryCategory.edit', [
            'category' => $category
        ]);
    }

    public function update(UpdateCategoryRequest $request, int $id)
    {
        $category = LibraryCategory::findOrFail($id);

        if (null === $category) {
            flash('No such record');
            return redirect()->route('category.index');
        }

        $category->update($request->all());

        Flash::success('Category updated successfully.');

        return redirect()->route('category.index');
    }

    public function destroy(int $id)
    {
        $category = LibraryCategory::findOrFail($id);

        if (null === $category) {
            flash('No such record');
            return redirect()->route('category.index');
        }

        $challenges = Library::where('category_id', $id)->count();

        if($challenges) {
            Flash::error('Can not delete category, it is associated with ' . $challenges . ' library challenge(s).');

            return redirect()->route('category.index');
        }

        $category->name = 'Deleted - ' . $category->name;
        $category->save();

        $category->delete();

        Flash::success('Category deleted successfully.');

        return redirect()->route('category.index');
    }
}
