<?php

namespace App\Modules\Library\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Modules\Library\Datatables\LibraryDataTable;
use App\Modules\Library\Http\Requests\Admin\StoreLibraryRequest;
use App\Modules\Library\Http\Requests\Admin\UpdateLibraryRequest;
use App\Modules\Library\Models\Answer;
use App\Modules\Library\Models\Library;
use App\Modules\Library\Models\Question;
use App\Modules\Challenges\Enums\ProofTypeEnum;
use App\Modules\Challenges\Enums\VideoLengthEnum;
use App\Modules\Challenges\Services\ChallengeService;
use App\Modules\Challenges\DTO\CreateChallengeDTO;
use App\Modules\Companies\Models\Company;
use App\Modules\Library\Datatables\MyLibraryDataTable;
use App\Modules\Library\Datatables\PublicLibraryDataTable;
use App\Modules\Library\Models\LibraryCategory;
use App\Modules\Onboarding\Models\Games;
use App\Modules\Onboarding\Models\GamesChallenges;
use Laracasts\Flash\Flash;
use Illuminate\Http\Request;

class LibraryController extends Controller
{
    public function index(LibraryDataTable $dataTable, Request $request)
    {
        if ($request->category) {
            $dataTable->setCategory($request->category);
        }

        $categories = LibraryCategory::select(['library_category.id', 'library_category.name'])
                                ->join('library', 'library_category.id', '=', 'library.category_id')
                                ->where('library_category.status', 1)
                                ->groupBy('library_category.id')
                                ->get();

        $categories = $categories ? array_pluck($categories, 'name', 'id') : [];
        $route = 'index';

        return $dataTable->render('library.index', compact('categories', 'route'));
    }

    public function create()
    {
        $proof_types = ProofTypeEnum::getNames();
        $video_lengths = array_combine(VideoLengthEnum::getAll(), VideoLengthEnum::getAll());

        $dto = new CreateChallengeDTO([], [], $proof_types, $video_lengths, 0);
        $proof_count = ChallengeService::getProofItemsCount();

        $categories = LibraryCategory::where('status', 1)->get();
        $categories = array_pluck($categories, 'name', 'id');

        return view('library.create', compact('proof_count', 'dto', 'categories'));
    }

    public function store(StoreLibraryRequest $request)
    {
        $inputs = $request->all();

        if($inputs['items_count_in_proof'] > 1) {
            $inputs['proof_type'] = $inputs['proof_type'] == ProofTypeEnum::PHOTO ? ProofTypeEnum::MULTIPLE_PHOTOS : $inputs['proof_type'];
            $inputs['proof_type'] = $inputs['proof_type'] == ProofTypeEnum::VIDEO ? ProofTypeEnum::MULTIPLE_VIDEOS : $inputs['proof_type'];
            $inputs['proof_type'] = $inputs['proof_type'] == ProofTypeEnum::SCREENSHOT ? ProofTypeEnum::MULTIPLE_SCREENSHOTS : $inputs['proof_type'];
        }

        $library = app()[Library::class];
        $library->fill($inputs);
        $library->save();

        if ($request->proof_type === ProofTypeEnum::QUESTIONS) {
            $library->saveQuestions($request->only(['questions', 'answers']));
        }
        elseif ($request->proof_type === ProofTypeEnum::TESTS) {
            $library->saveTests($request->only(['questions', 'test_answers']));
        }
        elseif ($request->proof_type === ProofTypeEnum::TRUE_FALSE) {
            $library->saveTrueFalse($request->only(['questions', 'answers']));
        }

        Flash::success('Library challenge created successfully.');

        return redirect()->route('library.index');
    }

    public function edit(Library $library)
    {
        if($library['items_count_in_proof'] > 1) {
            $library['proof_type'] = $library['proof_type'] == ProofTypeEnum::MULTIPLE_PHOTOS ? ProofTypeEnum::PHOTO : $library['proof_type'];
            $library['proof_type'] = $library['proof_type'] == ProofTypeEnum::MULTIPLE_VIDEOS ? ProofTypeEnum::VIDEO : $library['proof_type'];
            $library['proof_type'] = $library['proof_type'] == ProofTypeEnum::MULTIPLE_SCREENSHOTS ? ProofTypeEnum::SCREENSHOT : $library['proof_type'];
        }

        $qids = $questions = $answers = $test_answers = [];

        if(in_array($library->proof_type,  ['questions', 'tests', 'true_false'])) {
            $cnt = 0;

            $ques = Question::where('library_id', $library->id)->get();

            foreach($ques as $q) {
                $qids[$cnt] = $q->id;
                $questions[$cnt] = $q->text;

                $ans = Answer::where('question_id', $q->id)->get();

                foreach($ans as $a) {
                    if($library->proof_type == 'tests') {
                        $test_answers[$cnt][] = $a ? $a->text : '';
                    }
                    if($library->proof_type == 'true_false') {
                        if(empty($answers[$cnt]) && $a) {
                            $answers[$cnt] = $a->text;
                        }
                    }
                    else {
                        $answers[$cnt] = $a ? $a->text : '';
                    }
                }

                $cnt++;
            }
        }

        $lat_lng = explode(',', $library->location_latlng);

        $proof_types = ProofTypeEnum::getNames();
        $video_lengths = array_combine(VideoLengthEnum::getAll(), VideoLengthEnum::getAll());

        $dto = new CreateChallengeDTO([], [], $proof_types, $video_lengths, 0);
        $proof_count = ChallengeService::getProofItemsCount();

        $categories = LibraryCategory::where('status', 1)->get();
        $categories = array_pluck($categories, 'name', 'id');

        return view('library.edit', compact('library', 'qids', 'questions', 'answers', 'test_answers', 'lat_lng', 'dto', 'proof_count', 'categories'));
    }

    public function update(UpdateLibraryRequest $request, Library $library)
    {
        $inputs = $request->all();

        if($inputs['items_count_in_proof'] > 1) {
            $inputs['proof_type'] = $inputs['proof_type'] == ProofTypeEnum::PHOTO ? ProofTypeEnum::MULTIPLE_PHOTOS : $inputs['proof_type'];
            $inputs['proof_type'] = $inputs['proof_type'] == ProofTypeEnum::VIDEO ? ProofTypeEnum::MULTIPLE_VIDEOS : $inputs['proof_type'];
            $inputs['proof_type'] = $inputs['proof_type'] == ProofTypeEnum::SCREENSHOT ? ProofTypeEnum::MULTIPLE_SCREENSHOTS : $inputs['proof_type'];
        }

        $library->fill($inputs);
        $library->save();

        $current_qids = $request->get('qids') ? explode(',', $request->get('qids')) : [];
        $new_qids = [];

        if ($request->proof_type === ProofTypeEnum::QUESTIONS || $request->proof_type === ProofTypeEnum::TESTS || $request->proof_type === ProofTypeEnum::TRUE_FALSE)
        {
            $questions = $request->get('questions', []);
            $answers = $request->get('answers', []);
            $test_answers = $request->get('test_answers', []);

            foreach($questions as $k => $v)
            {
                $qid = 0;

                if(isset($current_qids[$k]))
                {
                    $ques = Question::where('id', $current_qids[$k])->update(['text' => $v]);
                    $qid = $current_qids[$k];
                }
                else
                {
                    $ques = Question::create([
                        'library_id' => $library->id,
                        'text' => $v,
                    ]);

                    $qid = $ques->id;
                }

                $new_qids[] = $qid;

                if (in_array($request->proof_type, [ProofTypeEnum::QUESTIONS, ProofTypeEnum::TRUE_FALSE])) {
                    $new_ans = $answers;
                }
                elseif($request->proof_type === ProofTypeEnum::TESTS) {
                    $new_ans = $test_answers;
                }

                if(isset($new_ans[$k]))
                {
                    Answer::where('question_id', $qid)->delete();

                    if(!is_array($new_ans[$k])) {
                        $new_ans[$k] = array($new_ans[$k]);
                    }

                    foreach($new_ans[$k] as $k2 => $v2)
                    {
                        $ans_type = $k2 == '0' ? Answer::CORRECT : Answer::INCORRECT;

                        Answer::create([
                            'question_id' => $qid,
                            'type' => $ans_type,
                            'text' => $v2
                        ]);

                        if($request->proof_type === ProofTypeEnum::TRUE_FALSE) {
                            Answer::create([
                                'question_id' => $qid,
                                'type' => Answer::INCORRECT,
                                'text' => $v2 == 'True' ? 'False' : 'True'
                            ]);
                        }
                    }
                }
            }
        }

        if($current_qids)
        {
            foreach($current_qids as $qid)
            {
                if(!in_array($qid, $new_qids))
                {
                    Answer::where('question_id', $qid)->delete();
                    Question::where('id', $qid)->delete();
                }
            }
        }

        Flash::success('Library challenge edited successfully.');

        return redirect()->route('library.index');
    }

    public function destroy(int $id)
    {
        $library = Library::findOrFail($id);

        if (null === $library) {
            flash('No such record');
            return redirect()->route('library.index');
        }

        $games = GamesChallenges::where('library_id', $id)->count();

        if($games) {
            Flash::error('Can not delete this library challenge, it is associated with ' . $games . ' game(s).');

            return redirect()->route('library.index');
        }

        $library->name = 'Deleted - ' . $library->name;
        $library->save();

        $library->delete();

        Flash::success('Library challenge deleted successfully.');

        return redirect()->route('library.index');
    }

    public function view(PublicLibraryDataTable $dataTable, Request $request)
    {
        if ($request->category) {
            $dataTable->setCategory($request->category);
        }

        $proof_types = Company::getAllowedProofTypes($request->companyId);

        $categories = LibraryCategory::select(['library_category.id', 'library_category.name'])
                                ->join('library', 'library_category.id', '=', 'library.category_id')
                                ->whereIN('library.proof_type', $proof_types)
                                ->where('library_category.status', 1)
                                ->groupBy('library_category.id')
                                ->get();

        $categories = $categories ? array_pluck($categories, 'name', 'id') : [];
        $route = 'view';

        return $dataTable->render('library.index', compact('categories', 'route'));
    }

    public function my(MyLibraryDataTable $dataTable, Request $request)
    {
        $route = 'view';

        return $dataTable->render('library.my', compact('route'));
    }
}
