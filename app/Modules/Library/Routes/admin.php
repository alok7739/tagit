<?php
Route::group(['middleware' => 'auth:admin'], function () {
    // Categories
    Route::get('library/category', 'CategoryController@index')->name('category.index');

    Route::get('library/category/create', 'CategoryController@create')->name('category.create')->middleware('can:create,App\Modules\Library\Models\LibraryCategory');
    Route::post('library/category', 'CategoryController@store')->name('category.store')->middleware('can:create,App\Modules\Library\Models\LibraryCategory');

    Route::get('library/category/{category}/edit', 'CategoryController@edit')->name('category.edit')->middleware('can:edit,App\Modules\Library\Models\LibraryCategory');
    Route::match(array('PATCH', 'PUT'), "library/category/{category}", array(
        'uses' => 'CategoryController@update',
        'as' => 'category.update'
    ))->middleware('can:edit,App\Modules\Library\Models\LibraryCategory');

    Route::delete('library/category/{category}', 'CategoryController@destroy')->name('category.destroy')->middleware('can:destroy,App\Modules\Library\Models\LibraryCategory');

    // Library
    Route::get('library', 'LibraryController@index')->name('library.index');

    Route::get('library/create', 'LibraryController@create')->name('library.create')->middleware('can:create,App\Modules\Library\Models\Library');
    Route::post('library', 'LibraryController@store')->name('library.store')->middleware('can:create,App\Modules\Library\Models\Library');

    Route::get('library/{library}/edit', 'LibraryController@edit')->name('library.edit')->middleware('can:edit,App\Modules\Library\Models\Library');
    Route::match(array('PATCH', 'PUT'), "library/{library}", array(
        'uses' => 'LibraryController@update',
        'as' => 'library.update'
    ))->middleware('can:edit,App\Modules\Library\Models\Library');

    Route::delete('library/{library}', 'LibraryController@destroy')->name('library.destroy')->middleware('can:destroy,App\Modules\Library\Models\Library');

    Route::get('library/view', 'LibraryController@view')->name('library.view');
    Route::get('library/my', 'LibraryController@my')->name('library.my');
});
