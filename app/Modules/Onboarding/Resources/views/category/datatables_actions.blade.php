<div class="btn-group" role="group">
    <button id="btnGroupDrop1" type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Actions
    </button>

    <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
        <a href="{{ route('games.index', ['category' => $id]) }}" class='dropdown-item'>View Games</a>

        @can('edit', 'App\Modules\Onboarding\Models\GamesCategory')
        <a href="{{ route('gamescategory.edit', $id) }}" class='dropdown-item'>Edit</a>
        @endcan

        @can('destroy', 'App\Modules\Onboarding\Models\GamesCategory')
            <a href="#" class='dropdown-item' onclick="document.getElementById('delete-{{$id}}-button').click()">
                {!! Form::open(['method'=>'DELETE', 'url'=> route('gamescategory.destroy', $id)]) !!}
                <button style="display:none" id="delete-{{$id}}-button" data-toggle="tooltip" data-placement="top" title="Delete"
                        type="submit" class="dropdown-item"
                        onclick="return confirm('Are you sure you want delete this?');">
                </button>
                {!! Form::close() !!}
                Delete
            </a>
        @endcan
    </div>
</div>
