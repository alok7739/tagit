@extends('layouts.app')
@section('title', 'Games Categories')

@section('content')
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box">
            <div class="box-body">
                <h3 class="new_heading pull-left">Individual Games Categories</h3>
                @can('create', 'App\Modules\Onboarding\Models\GamesCategory')
                    <a href="{{route('gamescategory.create')}}" class="btn btn-primary pull-right create-article">New Category</a>
                @endcan
                <div class="clearfix"></div>

                @include('category.table')
            </div>
        </div>
    </div>
@endsection
