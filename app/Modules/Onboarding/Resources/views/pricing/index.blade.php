@extends('layouts.app-blank')
@section('title', 'Pricing')

@section('content')
<section class="home-body-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-md-offset-3">
                <h2 style="font-family:'Baloo Thambi 2',cursive">Pricing</h2>
            </div>
        </div>
    </div>
</section>
<section class="price-section">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center" style="margin-top: 40px">
                <p class="trial-text">Want to try the system? <a href="{{ route('register') }}" class="btn btn-primary">Start Trial</a></p>
            </div>
            <div class="col-lg-10 col-md-10 col-md-offset-1">
                <div class="owl-carousel">
                    @php $c = 1; @endphp
                    @foreach($packages as $p)
                    <div id="package{{ $p->id }}" class="item">
                        <div class="price-box">
                            <div class="price-name">
                                <h3>{{ $p->name }}</h3>
                            </div>
                            <div class="include-name">
                                <h4>{{ $p->short_desc }}</h4>
                            </div>
                            <div class="package-list">
                                <div class="contentheg">
                                {!! str_replace('<li>', '<li><img src="/build/images/tick.png" /> ', $p->long_desc) !!}
                                </div>
                                <div class="rates">
                                    <span>{{ number_format($p->price) }} SR / user / month</span>
                                </div>
                            </div>
                            <div class="price-footer">
                                <a href="{{ route('register', ['package' => $p->id]) }}" class="price-btn contact-btn">Sign Up</a>
                            </div>
                        </div>
                    </div>
                    @php $c++; @endphp
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>
<section class="looking-for-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-md-offset-3">
                <div class="row">
                    <div class="col-lg-9 col-md-9">
                        <p>Looking for a custom package?</p>
                    </div>
                    <div class="col-lg-3 col-md-3">
                        <a href="{{ route('contactus') }}" class="btn btn-defualt">Contact Us</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
$(document).ready(function(){
    $('.owl-carousel').owlCarousel({
        loop:false,
        nav:true,
        margin:20,
        autoHeight:true,
        responsiveClass: true,
        responsive:{
            0:{
                items:1,
                loop:true
            },
            600: {
                items:2,
                nav:false
            },
            1100:{
                items:3
            }
        }
    })
});
</script>
@endsection
