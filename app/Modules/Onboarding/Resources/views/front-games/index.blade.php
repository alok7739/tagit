@extends('layouts.app-blank')
@section('title', 'Games')

@section('content')
<section class="ecommerce-page">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-12">
                <div class="silder-img">
                    <img src="/build/images/ecommerce-silder.png" class="img-fluid">
                </div>
            </div>
        </div>
    </div>
</section>

<section class="ecommerce-tab-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="ecommerce-tab">
                    <div class="tabbable-panel">
                        <div class="tabbable-line">
                            <div class="row d-flex justify-content-center">
                                <div class="col-md-12">
                                    <p>&nbsp;</p>
                                    <ul class="nav nav-tabs ">
                                        @if(count($categories) > 0)
                                        <li class="{{ request()->get('category') ? '' : 'active' }}">
                                            <a href="{{ route('games') }}">ALL</a>
                                        </li>
                                        @endif
                                        @foreach($categories as $k => $v)
                                        <li class="{{ request()->get('category') == $k ? 'active' : '' }}">
                                            <a href="{{ route('games', ['category' => $k]) }}">{{ $v }}</a>
                                        </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>

                            <div class="tab-content">
                                <div class="tab-pane active" id="all">
                                    <div class="row">
                                        @foreach($games as $g)
                                        <div class="col-lg-3 col-md-3 col-12">
                                            <div class="ecommerce-box">
                                                <figure>
                                                    <a href="{{ route('game.view', ['id' => $g->id]) }}"><img src="{{ $g->image }}" class="img-fluid" /></a>
                                                </figure>
                                                <div class="content-box">
                                                    <a href="{{ route('game.view', ['id' => $g->id]) }}"><h3>{{ $g->name }}</h3></a>
                                                    <h3>SAR {{ str_replace('.00', '', number_format($g->price, 2)) }}</h3>
                                                </div>
                                                <a href="{{ route('game.view', ['id' => $g->id]) }}">
                                                    <div class="cart">
                                                        <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
