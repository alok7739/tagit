@extends('layouts.app')
@section('title', 'Games')

@section('content')
<section class="ecommerce-page" style="margin-top: 0">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-12">
            <div class="row">
                <div class="col-sm-4">
                    <h1>{{ $game->name }}</h1>
                    <div class="price">SAR {{ str_replace('.00', '', number_format($game->price, 2)) }} / user</div>
                    <div class="silder-img images-size">
                        <img src="{{ $game->image}}" class="img-fluid">
                    </div>
                    <p><a href="{{ route('game.buy', ['id' => $game->id]) }}" class="btn btn-primary">Buy Now</a></p>
                </div>

                <div class="col-sm-8">
                    {!! $game->description !!}
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
