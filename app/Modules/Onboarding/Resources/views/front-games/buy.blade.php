@extends('layouts.app-blank')
@section('title', 'Pricing')

@section('content')
<style>.tab-content > .active { border: 0;}</style>
<section class="login-screen step-form h-100">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-8 col-md-offset-2">
                <div class="wizard mt-100">
                    <div class="wizard-inner">
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active">
                                <a><span class="round-tab">1</span></a>
                                <p>Basic Info</p>
                            </li>

                            <li role="presentation" class="disabled">
                                <a><span class="round-tab">2</span></a>
                                <p>Payment</p>
                            </li>
                            <li role="presentation" class="disabled">
                                <a><span class="round-tab">3</span></a>
                                <p>Thank You</p>
                            </li>
                        </ul>
                    </div>

                    <div class="tab-content">
                        <div class="tab-pane active" role="tabpanel" id="step1">
                            @include('flash::message')
                            <h3>Basic <span>Info</span></h3>
                            <form id="step1Frm" name="register" action="" method="POST">
                                @csrf
                                <input type="hidden" name="gid" value="{{ $id }}" />

                                <div id="step2">
                                    <hr />
                                    <div class="row">
                                        <div class="col-lg-4 col-md-4 col-sm-6 col-12">
                                            <div class="row xs-top">
                                                <div class="col-lg-4 col-md-4 col-xs-3">
                                                    <img src="/build/images/no-game-icon.svg" />
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-xs-9">
                                                    <p>Game</p>
                                                    <p><b>{{ $game->name }}</b></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-6 col-12">
                                            <div class="row xs-top">
                                                <div class="col-lg-4 col-md-4 col-xs-3">
                                                    <img src="/build/images/no-game-icon.svg" />
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-xs-9">
                                                    <p>Challenges</p>
                                                    <p><b>{{ $challenges }}</b></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-6 col-12">
                                            <div class="row xs-top">
                                                <div class="col-lg-4 col-md-4 col-xs-3">
                                                    <img src="/build/images/no-of-user-icon.svg" />
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-xs-9">
                                                    <p>Price Per Users</p>
                                                    <p><b>{{ number_format($game->price, 2) }} SR</b></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr />
                                </div>

                                <div class="row">
                                    <div class="col-lg-6 col-md-6">
                                        <div class="input-group">
                                            <span class="input-group-addon"><img src="/build/images/username.svg" /></span>
                                            <input id="name" type="text" class="form-control" name="name" placeholder="Your Name" maxlength="50" value="{{ old('name') ? old('name') : (empty($data['name']) ? '' : $data['name']) }}" />
                                        </div>
                                        @if ($errors->has('name'))
                                        <div class="text-red">{{ $errors->first('name') }}</div>
                                        @endif
                                    </div>
                                    <div class="col-lg-6 col-md-6">
                                        <div class="input-group">
                                            <span class="input-group-addon"><img src="/build/images/your-email.svg" /></span>
                                            <input id="email" type="email" class="form-control" name="email" placeholder="Email Address" maxlength="100" value="{{ old('email') ? old('email') : (empty($data['email']) ? '' : $data['email']) }}" />
                                        </div>
                                        @if ($errors->has('email'))
                                        <div class="text-red">{{ $errors->first('email') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6 col-md-6">
                                        <div class="input-group">
                                            <span class="input-group-addon"><img src="/build/images/phone-number.svg" /></span>
                                            <input id="phone" type="text" class="form-control numberonly" name="phone" placeholder="Phone Number" maxlength="15" value="{{ old('phone') ? old('phone') : (empty($data['phone']) ? '' : $data['phone']) }}" />
                                        </div>
                                        @if ($errors->has('phone'))
                                        <div class="text-red">{{ $errors->first('phone') }}</div>
                                        @endif
                                    </div>
                                    <div class="col-lg-6 col-md-6">
                                        <div class="input-group">
                                            <span class="input-group-addon text-purple"><i class="fa fa-calendar"></i></span>
                                            <input id="start_date" type="text" class="form-control air-datepicker" name="start_date" placeholder="Game Start Date/Time" value="{{ old('start_date') ? old('start_date') : (empty($data['start_date']) ? '' : $data['start_date']) }}" />
                                        </div>
                                        @if ($errors->has('start_date'))
                                        <div class="text-red">{{ $errors->first('start_date') }}</div>
                                        @endif
                                    </div>
                                </div>

                                <hr />
                                <div class="row">
                                    <div class="col-lg-6 col-md-6">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-users"></i></span>
                                            <input id="users" type="text" class="form-control numberonly" name="users" placeholder="Enter number of users" maxlength="4" value="{{ old('users') ? old('users') : (empty($data['users']) ? '' : $data['users']) }}" />
                                        </div>
                                        @if ($errors->has('users'))
                                        <div class="text-red">{{ $errors->first('users') }}</div>
                                        @endif
                                    </div>
                                    <div class="col-lg-6 col-md-6">
                                        <h3 class="price-lbl">
                                            Total Price:
                                            <span id="price" data-p="{{ $game->price }}">0</span>
                                            <span>SR</span>
                                        </h3>
                                    </div>
                                </div>
                                <hr />

                                <div class="row">
                                    <div class="col-lg-12 col-md-12">
                                        <div class="submit-bth">
                                            <button type="submit" class="btn btn-login next-step">Proceed To Payment</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
$(document).ready(function(){
    $('#users').keyup(function(){
        var u = parseInt($(this).val());
        var p = parseFloat($('#price').attr('data-p')).toFixed(2);

        var total = u * p;
        total = total ? total.toFixed(2).replace('.00', '').toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") : 0;

        $('#price').text(total);
    });

    $('#users').trigger('keyup');
})
</script>
@endsection
