@extends('layouts.app-blank')
@section('title', 'Pricing')

@section('content')
<style>
    .tab-content>.active {
        border: 0;
    }
</style>
<section class="login-screen step-form">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-8 col-md-offset-2">
                <div class="wizard mt-100">
                    <div class="wizard-inner">
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="disabled">
                                <a><span class="round-tab">1</span></a>
                                <p>Basic Info</p>
                            </li>

                            <li role="presentation" class="disabled">
                                <a><span class="round-tab">2</span></a>
                                <p>Payment</p>
                            </li>
                            <li role="presentation" class="active">
                                <a><span class="round-tab">3</span></a>
                                <p>Thank You</p>
                            </li>
                        </ul>
                    </div>

                    <div class="tab-content">
                        <div class="tab-pane active" role="tabpanel" id="step3">
                            <div class="row">
                                <div class="col-lg-8 col-md-8 col-md-offset-2">
                                    <img src="/build/images/thank-you-icon.svg" />
                                    <h3>Thank <span>You</span></h3>
                                    <p>You account has been successfully created.</p>
                                    @if($is_trial)
                                    <p>You will shortly receive an email to verify your account.</p>
                                    @else
                                    <p>You can now login and create your games!</p>
                                    @endif
                                </div>
                            </div>
                            @if(!$is_trial)
                            <div class="col-lg-12 col-md-12">
                                <div class="submit-bth">
                                    <a href="{{ route('login') }}" class="btn btn-login next-step">Sign In</a>
                                </div>
                            </div>
                            @endif
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
