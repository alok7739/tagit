@extends('layouts.app')
@section('title', 'Packages')

@section('content')
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box">
            <div class="box-body">
                <h3 class="new_heading pull-left">Manage Packages</h3>
                @can('create', 'App\Modules\Onboarding\Models\Packages')
                    <a href="{{route('packages.create')}}" class="btn btn-primary pull-right create-article">New Package</a>
                @endcan
                <div class="clearfix"></div>
                @include('packages.table')
            </div>
        </div>
    </div>
@endsection
