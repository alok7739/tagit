
<h3 class="new_heading">Create Package</h3>

<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            {{ Form::label('name', 'Name: ', ['class' => 'required']) }}
            {!! Form::text('name', null, ['class' => 'form-control', 'maxlength' => 20]) !!}
            @if ($errors->has('name'))
            <div class="text-red">{{ $errors->first('name') }}</div>
            @endif
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            {{ Form::label('short_desc', 'Short Description:') }}
            {!! Form::text('short_desc', null, ['class' => 'form-control', 'maxlength' => 255]) !!}
            @if ($errors->has('short_desc'))
            <div class="text-red">{{ $errors->first('short_desc') }}</div>
            @endif
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="form-group">
            {{ Form::label('long_desc', 'Description: ') }}
            {!! Form::textarea('long_desc', null, ['id' => 'long_desc_en', 'data-lang' => 'en', 'class' => 'form-control ckeditor', 'maxlength' => 500]) !!}
            @if ($errors->has('long_desc'))
            <div class="text-red">{{ $errors->first('long_desc') }}</div>
            @endif
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-4">
        <div class="form-group">
            {{ Form::label('time_limit', 'Validity: ', ['class' => 'required']) }}
            {!! Form::select('time_limit', [3 => '3 Months', 6 => '6 Months', 12 => '12 Months'], null, ['id' => 'time_limit', 'class' => 'form-control']) !!}
            @if ($errors->has('time_limit'))
            <div class="text-red">{{ $errors->first('time_limit') }}</div>
            @endif
        </div>
    </div>

    <div class="col-sm-4">
        <div class="form-group">
            {{ Form::label('challenges', 'Challenges Per Month: ', ['class' => 'required']) }}
            {!! Form::text('challenges', null, ['id' => 'challenges', 'class' => 'form-control numberonly', 'maxlength' => 3, 'min' => 1]) !!}
            @if ($errors->has('challenges'))
            <div class="text-red">{{ $errors->first('challenges') }}</div>
            @endif
        </div>
    </div>

    <div class="col-sm-4">
        <div class="form-group">
            {{ Form::label('price', 'Price Per User: ', ['class' => 'required']) }}
            {!! Form::text('price', null, ['id' => 'price', 'class' => 'form-control money', 'maxlength' => 5, 'min' => 1]) !!}
            @if ($errors->has('price'))
            <div class="text-red">{{ $errors->first('price') }}</div>
            @endif
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-4">
        <div class="form-group">
            {{ Form::label('sort_order', 'Sort Order: ') }}
            {!! Form::text('sort_order', null, ['id' => 'sort_order', 'class' => 'form-control numberonly', 'maxlength' => 1, 'min' => 1]) !!}
            @if ($errors->has('sort_order'))
            <div class="text-red">{{ $errors->first('sort_order') }}</div>
            @endif
        </div>
    </div>

    <div class="col-sm-4">
        <div class="form-group">
            {{ Form::label('status', 'Status: ', ['class' => 'required']) }}
            {!! Form::select('status', [1=> 'Active', 0 => 'Inactive'], null, ['id' => 'status', 'class' => 'form-control']) !!}
        </div>
    </div>
</div>

<h3 class="new_heading">Allowed Features</h3>

<div class="row">
    <div class="col-sm-3">
        <div class="form-group">
            {{ Form::label('true_false', 'True/False: ', ['class' => 'required']) }}
            {!! Form::select('true_false', [1=> 'Allowed', 0 => 'Not Allowed'], null, ['id' => 'radius', 'class' => 'form-control']) !!}
        </div>
    </div>

    <div class="col-sm-3">
        <div class="form-group">
            {{ Form::label('sc_ques', 'Text Q: ', ['class' => 'required']) }}
            {!! Form::select('sc_ques', [1=> 'Allowed', 0 => 'Not Allowed'], null, ['id' => 'sc_ques', 'class' => 'form-control']) !!}
        </div>
    </div>

    <div class="col-sm-3">
        <div class="form-group">
            {{ Form::label('mc_ques', 'Multiple Choice Q: ', ['class' => 'required']) }}
            {!! Form::select('mc_ques', [1=> 'Allowed', 0 => 'Not Allowed'], null, ['id' => 'mc_ques', 'class' => 'form-control']) !!}
        </div>
    </div>

    <div class="col-sm-3">
        <div class="form-group">
            {{ Form::label('checkin', 'Check In: ', ['class' => 'required']) }}
            {!! Form::select('checkin', [1=> 'Allowed', 0 => 'Not Allowed'], null, ['id' => 'checkin', 'class' => 'form-control']) !!}
        </div>
    </div>

    <div class="col-sm-3">
        <div class="form-group">
            {{ Form::label('qr_code', 'QR Code: ', ['class' => 'required']) }}
            {!! Form::select('qr_code', [1=> 'Allowed', 0 => 'Not Allowed'], null, ['id' => 'qr_code', 'class' => 'form-control']) !!}
        </div>
    </div>

    <div class="col-sm-3">
        <div class="form-group">
            {{ Form::label('images', 'Images: ', ['class' => 'required']) }}
            {!! Form::select('images', [1=> 'Allowed', 0 => 'Not Allowed'], null, ['id' => 'images', 'class' => 'form-control']) !!}
        </div>
    </div>

    <div class="col-sm-3">
        <div class="form-group">
            {{ Form::label('screenshots', 'Screenshots: ', ['class' => 'required']) }}
            {!! Form::select('screenshots', [1=> 'Allowed', 0 => 'Not Allowed'], null, ['id' => 'screenshots', 'class' => 'form-control']) !!}
        </div>
    </div>

    <div class="col-sm-3">
        <div class="form-group">
            {{ Form::label('videos', 'Videos: ', ['class' => 'required']) }}
            {!! Form::select('videos', [1=> 'Allowed', 0 => 'Not Allowed'], null, ['id' => 'videos', 'class' => 'form-control']) !!}
        </div>
    </div>

    <div class="col-sm-3">
        <div class="form-group">
            {{ Form::label('locking', 'Challenge Locking: ', ['class' => 'required']) }}
            {!! Form::select('locking', [1=> 'Allowed', 0 => 'Not Allowed'], null, ['id' => 'locking', 'class' => 'form-control']) !!}
        </div>
    </div>
</div>

<!-- Submit Field -->
<div class="form-group text-right">
    {!! Form::submit('Save', ['class' => 'btn btn-success']) !!}
</div>

<script src="/build/js/ckeditor/ckeditor.js"></script>
<script>
$(function() {
    $('.ckeditor').each(function() {
        var id = $(this).attr('id');
        var lang = $(this).attr('data-lang');

        ClassicEditor.create(document.querySelector('#' + id), {
                toolbar: {
                    items: [
                        'bold',
                        'italic',
                        'underline',
                        '|',
                        'numberedList',
                        'bulletedList',
                        '|',
                        'strikethrough',
                        'subscript',
                        'superscript'
                    ]
                },
                language: lang,
                licenseKey: '',
            })
            .then(editor => {
                window.editor = editor;
            })
            .catch(error => {});
    })
});
</script>
