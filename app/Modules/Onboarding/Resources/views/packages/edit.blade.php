@extends('layouts.app')
@section('title', 'Edit Package: ' . $package->name)

@section('content')
    <section class="content">
        <div class="clearfix"></div>

        @include('flash::message')
        {!! Form::open(['url'=> route('packages.update', ['package' => $package->id]), 'method' => 'PUT']) !!}
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-body">
                        @include('packages.edit_fields')
                    </div>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </section>
@endsection
