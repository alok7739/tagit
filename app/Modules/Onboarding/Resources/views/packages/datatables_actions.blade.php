

<div class="btn-group" role="group">
    <button id="btnGroupDrop1" type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Actions
    </button>

    <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
        <a href="{{ route('packages.edit', $id) }}" class='dropdown-item'>
            <i class="fa fa-edit"></i> Edit
        </a>

        @can('destroy', 'App\Modules\Onboarding\Models\Packages')
            <a href="#" class='dropdown-item' onclick="document.getElementById('delete-{{$id}}-button').click()">
                {!! Form::open(['method'=>'DELETE', 'url'=> route('packages.destroy', $id)]) !!}
                <button style="display:none" id="delete-{{$id}}-button" data-toggle="tooltip" data-placement="top" title="Delete"
                        type="submit" class="dropdown-item"
                        onclick="return confirm('Are you sure you want delete this?');">
                </button>
                {!! Form::close() !!}
                <i class="fa fa-trash"></i> Delete
            </a>
        @endcan
    </div>
</div>
