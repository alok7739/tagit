<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>TagIt</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link href="https://fonts.googleapis.com/css?family=Poppins:400,500,700&display=swap" rel="stylesheet">
</head>
<body style="margin: 0; padding: 0;">
    <table  cellpadding="0" cellspacing="0" width="100%" >
        <tr>
            <td style="padding: 10px 0 30px 0;">
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="padding: 50px;border-collapse: collapse;background-color: #fff;">
                    <tr>
                        <td style="text-align: center">
                            <img src="{{ asset('assets/images/email_header.png') }}" width="100%" alt="TagIt">
                        </td>
                    </tr>
                    <tr>
                        <td bgcolor="#ffffff" style="padding: 40px 30px 0px 30px;">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td style="padding: 0px; font-size: 14px; color: #222; font-family: 'Arial', sans-serif;">
                                        <p style="padding: 0;margin: 0; font-weight:bold;">Hi {{ $name }},</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding: 20px 0 20px 0; color: #222; font-size: 14px; font-family: 'Arial', sans-serif;">
                                        <p style="padding: 0;margin: 0;">We are happy you signed up for TagIt. To start exploring, please verify your email address:</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding: 0px 0 20px 0; color: #222; font-size: 14px; font-family: 'Arial', sans-serif;">
                                        <p style="padding: 0;margin: 0; font-weight:bold;"><a href="{{ route('verify-trial', ['email' => $email, 'code' => $code]) }}">Verify Now</a></p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>
</html>
