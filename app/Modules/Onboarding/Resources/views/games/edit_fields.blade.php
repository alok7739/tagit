
<?php
$challenges = old('challenges') ? old('challenges') : $challenges;
?>
<h3 class="new_heading">Edit Individual Game</h3>

<div class="row">
    <div class="col-sm-8">
        <div class="form-group">
            {{ Form::label('name', 'Name: ', ['class' => 'required']) }}
            {!! Form::text('name', $game->name, ['class' => 'form-control', 'maxlength' => 20]) !!}
            @if ($errors->has('name'))
            <div class="text-red">{{ $errors->first('name') }}</div>
            @endif
        </div>
    </div>

    <div class="col-sm-4">
        <div class="form-group">
            {{ Form::label('category_id', 'Sort Order: ') }}
            {!! Form::select('category_id', $categories, $game->category_id, ['id' => 'status', 'class' => 'form-control select2']) !!}
            @if ($errors->has('category_id'))
            <div class="text-red">{{ $errors->first('category_id') }}</div>
            @endif
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="form-group">
            {{ Form::label('description', 'Description: ') }}
            {!! Form::textarea('description', $game->description, ['id' => 'description_en', 'data-lang' => 'en', 'class' => 'form-control ckeditor']) !!}
            @if ($errors->has('description'))
            <div class="text-red">{{ $errors->first('description') }}</div>
            @endif
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-4">
        <div class="form-group">
            {{ Form::label('price', 'Price Per User: ', ['class' => 'required']) }}
            {!! Form::text('price', $game->price, ['id' => 'price', 'class' => 'form-control money', 'maxlength' => 5]) !!}
            @if ($errors->has('price'))
            <div class="text-red">{{ $errors->first('price') }}</div>
            @endif
        </div>
    </div>

    <div class="col-sm-4">
        <div class="form-group">
            {{ Form::label('sort_order', 'Sort Order: ') }}
            {!! Form::text('sort_order', $game->sort_order, ['id' => 'sort_order', 'class' => 'form-control numberonly', 'maxlength' => 2, 'min' => 1]) !!}
            @if ($errors->has('sort_order'))
            <div class="text-red">{{ $errors->first('sort_order') }}</div>
            @endif
        </div>
    </div>

    <div class="col-sm-4">
        <div class="form-group">
            {{ Form::label('status', 'Status: ', ['class' => 'required']) }}
            {!! Form::select('status', [1=> 'Active', 0 => 'Inactive'], $game->status, ['id' => 'status', 'class' => 'form-control']) !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-4">
        <div class="form-group">
            {!! Form::label('cover-image', 'Cover image', ['class' => 'required']) !!}
        </div>
        <div class="form-group dropzone challenge-logo-dropzone dz-clickable"></div>
        @if ($errors->has('image'))
        <div class="text-red">{{ $errors->first('image') }}</div>
        @endif
        {!! Form::hidden('image', $game->image) !!}
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <label>&nbsp;</label>
        </div>
        @if ($game->image)
        <img class="dashboard-image" src="{{$game->image}}">
        @elseif (old('image'))
        <img class="dashboard-image" src="{{Storage::url(old('image'))}}">
        @else
        <img class="dashboard-image" style="display:none" src="">
        @endif
    </div>
</div>

<h3 class="new_heading">Challenges</h3>

<table id="challenges-tbl" class="table table-bordered">
    <thead>
        <tr>
            <th>Challenge</th>
            <th width="15%">Duration in Hrs<br /><small>(B/w start & end time)</small></th>
            <th width="15%">Is Dependent<br />To Previous?</th>
            <th width="5%"></th>
        </tr>
    </thead>
    <tbody>
        @if($challenges)
            @php $cnt = 0; @endphp
            @foreach($challenges as $c)
            <tr>
                <td>
                    <select name="challenges[{{ $cnt }}][library_id]" class="form-control library select2" required>
                        @foreach($library as $k => $v)
                        <option value="{{ $k }}" {{ $c['library_id'] == $k ? 'selected' : '' }}>{{ $v }}</option>
                        @endforeach
                    </select>
                </td>
                <td><input type="text" name="challenges[{{ $cnt }}][duration]" class="form-control duration money"  maxlength="5"  value="{{ $c['duration'] }}" required /></td>
                <td>
                    <select name="challenges[{{ $cnt }}][dependent]" class="form-control dependent">
                        <option value="0">No</option>
                        <option value="1" {{ $c['dependent'] ? 'selected' : '' }}>Yes</option>
                    </select>
                    <input type="text" class="form-control no-lbl" value="No" readonly />
                </td>
                <td class="text-center">
                    <a href="javascript:void(0)" class="remove-challenge text-danger"><i class="fa fa-times"></i></a></td>
                    <input type="hidden" name="challenges[{{ $cnt }}][id]" value="{{ $c['id'] }}" /></td>
                </td>
            </tr>
            @php $cnt++ @endphp
            @endforeach
        @endif
    </tbody>
    <tfoot>
        <tr>
            <th class="text-right" colspan="4"><a href="javascript:void(0);" class="btn btn-primary add-challenge">Add Another Challenge</a></th>
        </tr>
    </tfoot>
</table>

<!-- Submit Field -->
<div class="form-group text-right">
    {!! Form::submit('Save', ['class' => 'btn btn-success']) !!}
</div>

<script src="https://cdn.ckeditor.com/4.16.0/standard/ckeditor.js"></script>
<script>
var cnt = {{ count($challenges) }};
var library = {!! json_encode($library) !!};
$(function() {
    $.fn.select2.defaults.set("theme", "bootstrap");
    $('.select2').select2();

    $(document).on('click', '.add-challenge', function(){
        var h  = '<tr>';
            h += '  <td>';
            h += '      <select name="challenges[' + cnt+ '][library_id]" class="form-control library select2" required>';
            h += '          <option></option>';
            $.each(library, function(k, v) {
                h += '<option value="' + k + '">' + v + '</option>';
            });

            h += '      </select>';
            h += '  </td>';
            h += '  <td><input type="text" name="challenges[' + cnt+ '][duration]" class="form-control duration money" value=""  maxlength="5" required /></td>';
            h += '  <td>';
            h += '      <select name="challenges[' + cnt+ '][dependent]" class="form-control dependent">';
            h += '          <option value="0">No</option>';
            h += '          <option value="1">Yes</option>';
            h += '      </select>';
            h += '      <input type="text" class="form-control no-lbl" value="No" readonly />';
            h += '  </td>';
            h += '  <td class="text-center">';
            h += '      <a href="javascript:void(0)" class="remove-challenge text-danger"><i class="fa fa-times"></i></a></td>';
            h += '      <input type="hidden" name="challenges[' + cnt+ '][id]" value="" /></td>';
            h += '  </td>';
            h += '</tr>';

        $('#challenges-tbl > tbody').append(h);
        $('.select2').select2({
            placeholder: "Select challenge",
        });
        cnt++;

        dependentFlds();
    });

    if(!$('#challenges-tbl > tbody tr').length) {
        $('.add-challenge').trigger('click');
    }

    dependentFlds();

    $(document).on('click', '.remove-challenge', function(){
        if(confirm('Are you sure you want to remove this?')){
            $(this).parents('tr').remove();

            dependentFlds();
        }
    });

    $("#game-form").on("submit", function(){
        var ids = [];
        var valid = true;

        if($('.library').length > 0) {
            $('.library').each(function(){
                var v = $(this).val()

                if(ids.includes(v)){
                    valid = false;
                } else {
                    ids.push(v);
                }
            });

            if(valid) {
                return true;
            } else {
                alert('You can not select same challenge multiple times.');
                return false;
            }
        } else {
            alert('Please select atleast one challenge.');
            return false;
        }
    })

    function dependentFlds() {
        $('#challenges-tbl > tbody .dependent').show();
        $('#challenges-tbl > tbody .no-lbl').hide();+
        $('#challenges-tbl > tbody').find('tr').first().find('.dependent').val('0');
        $('#challenges-tbl > tbody').find('tr').first().find('.dependent').hide();
        $('#challenges-tbl > tbody').find('tr').first().find('.no-lbl').show();
    }
});
</script>
