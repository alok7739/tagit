@extends('layouts.app')
@section('title', 'Individual Games')

@section('content')
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box">
            <div class="box-body">
                <h3 class="new_heading pull-left">Manage Individual Games</h3>
                @can('create', 'App\Modules\Onboarding\Models\Games')
                    <a href="{{route('games.create')}}" class="btn btn-primary pull-right create-article">New Game</a>
                @endcan
                <div class="clearfix"></div>
                @include('games.table')
            </div>
        </div>
    </div>
@endsection
