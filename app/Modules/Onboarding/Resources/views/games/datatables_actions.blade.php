

<div class="btn-group" role="group">
    <button id="btnGroupDrop1" type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Actions
    </button>

    <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
        @can('edit', 'App\Modules\Onboarding\Models\Games')
            <a href="{{ route('games.edit', $id) }}" class='dropdown-item'>
                <i class="fa fa-edit"></i> Edit
            </a>
        @endcan

        @can('destroy', 'App\Modules\Onboarding\Models\Games')
            <a href="#" class='dropdown-item' onclick="document.getElementById('delete-{{$id}}-button').click()">
                {!! Form::open(['method'=>'DELETE', 'url'=> route('games.destroy', $id)]) !!}
                <button style="display:none" id="delete-{{$id}}-button" data-toggle="tooltip" data-placement="top" title="Delete"
                        type="submit" class="dropdown-item"
                        onclick="return confirm('Are you sure you want delete this?');">
                </button>
                {!! Form::close() !!}
                <i class="fa fa-trash"></i> Delete
            </a>
        @endcan
    </div>
</div>
