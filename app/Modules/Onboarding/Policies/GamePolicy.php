<?php

namespace App\Modules\Onboarding\Policies;

use App\Modules\Users\Admin\Models\Admin;

class GamePolicy
{
    public function view(Admin $admin)
    {
        return $admin->isAdmin();
    }

    public function edit(Admin $admin)
    {
        return $admin->isAdmin();
    }

    public function create(Admin $admin)
    {
        return $admin->isAdmin();
    }

    public function destroy(Admin $admin)
    {
        return $admin->isAdmin();
    }
}
