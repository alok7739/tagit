<?php
Route::get('/pricing', 'PricingController@index')->name('pricing')->middleware('guest:admin');

Route::get('/games', 'GamesController@index')->name('games')->middleware('guest:admin');
Route::get('/game/{id}', 'GamesController@view')->name('game.view')->middleware('guest:admin');

Route::get('/buy-game/thank-you', 'GamesController@thankyou')->middleware('guest:admin');
Route::post('/buy-game/payment-callback', 'GamesController@paymentCallback');

Route::get('/buy-game/{id}', 'GamesController@buy')->name('game.buy')->middleware('guest:admin');
Route::post('/buy-game/{id}', 'GamesController@storeBuy')->middleware('guest:admin');

Route::get('/register', 'RegisterController@step1')->name('register')->middleware('guest:admin');
Route::post('/register', 'RegisterController@storeStep1')->middleware('guest:admin');

Route::get('/payment-callback', 'RegisterController@paymentCallback');
Route::post('/payment-callback', 'RegisterController@paymentCallback');

Route::get('/register/thank-you', 'RegisterController@thankyou')->middleware('guest:admin');

Route::get('/verify-trial', 'RegisterController@verifyTrial')->name('verify-trial')->middleware('guest:admin');
