<?php
Route::group(['middleware' => 'auth:admin'], function () {
    // Company Packages
    Route::get('packages', 'PackagesController@index')->name('packages.index');
    Route::delete('packages/{packages}', 'PackagesController@destroy')->name('packages.destroy')->middleware('can:destroy,App\Modules\Onboarding\Models\Packages');

    Route::get('packages/create', 'PackagesController@create')->name('packages.create')->middleware('can:create,App\Modules\Onboarding\Models\Packages');
    Route::post('packages', 'PackagesController@store')->name('packages.store')->middleware('can:create,App\Modules\Onboarding\Models\Packages');

    Route::get('packages/{packages}/edit', 'PackagesController@edit')->name('packages.edit')->middleware('can:edit,App\Modules\Onboarding\Models\Packages');
    Route::match(array('PATCH', 'PUT'), "packages/{packages}", array(
        'uses' => 'PackagesController@update',
        'as' => 'packages.update'
    ))->middleware('can:edit,App\Modules\Onboarding\Models\Packages');

    // Individual Games
    Route::get('games', 'GamesController@index')->name('games.index');
    Route::delete('games/{games}', 'GamesController@destroy')->name('games.destroy')->middleware('can:destroy,App\Modules\Onboarding\Models\Games');

    Route::get('games/create', 'GamesController@create')->name('games.create')->middleware('can:create,App\Modules\Onboarding\Models\Games');
    Route::post('games', 'GamesController@store')->name('games.store')->middleware('can:create,App\Modules\Onboarding\Models\Games');

    Route::get('games/{games}/edit', 'GamesController@edit')->name('games.edit')->middleware('can:edit,App\Modules\Onboarding\Models\Games');
    Route::match(array('PATCH', 'PUT'), "games/{games}", array(
        'uses' => 'GamesController@update',
        'as' => 'games.update'
    ))->middleware('can:edit,App\Modules\Onboarding\Models\Games');

    // Categories
    Route::get('games/category', 'CategoryController@index')->name('gamescategory.index');

    Route::get('games/category/create', 'CategoryController@create')->name('gamescategory.create')->middleware('can:create,App\Modules\Onboarding\Models\GamesCategory');
    Route::post('games/category', 'CategoryController@store')->name('gamescategory.store')->middleware('can:create,App\Modules\Onboarding\Models\GamesCategory');

    Route::get('games/category/{category}/edit', 'CategoryController@edit')->name('gamescategory.edit')->middleware('can:edit,App\Modules\Onboarding\Models\GamesCategory');
    Route::match(array('PATCH', 'PUT'), "games/category/{category}", array(
        'uses' => 'CategoryController@update',
        'as' => 'gamescategory.update'
    ))->middleware('can:edit,App\Modules\Onboarding\Models\GamesCategory');

    Route::delete('games/category/{category}', 'CategoryController@destroy')->name('gamescategory.destroy')->middleware('can:destroy,App\Modules\Onboarding\Models\GamesCategory');
});
