<?php

namespace App\Modules\Onboarding\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Modules\Challenges\Rules\ChallengeStartDateTimeRule;

class GameBuyRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:50',
            'email' => "required|email|max:100",
            'phone' => 'required|max:20',
            'start_date' => ['required', 'date_format:Y-m-d H:i', new ChallengeStartDateTimeRule],
            'users' => 'required|integer|min:1'
        ];
    }
}
