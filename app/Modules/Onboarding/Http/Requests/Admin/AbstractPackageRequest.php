<?php

namespace App\Modules\Onboarding\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

abstract class AbstractPackageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:50',
            'short_desc' => 'nullable|string|max:255',
            'sort_order' => 'nullable|integer|between:1,99',
            'time_limit' => 'required|integer',
            'challenges' => 'required|integer',
            'price' => 'required|numeric|gt:0',
        ];
    }

    public function attributes()
    {
        return [
            'name_ar' => 'arabic name',
            'short_desc' => 'short description',
            'short_desc_ar' => 'arabic short description',
            'long_desc' => 'description',
            'long_desc_ar' => 'arabic description',
        ];
    }
}
