<?php

namespace App\Modules\Onboarding\Http\Requests\Admin;

class StoreGameRequest extends AbstractGameRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
