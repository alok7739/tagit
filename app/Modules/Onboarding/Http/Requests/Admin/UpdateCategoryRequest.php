<?php

namespace App\Modules\Onboarding\Http\Requests\Admin;

class UpdateCategoryRequest extends AbstractCategoryRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @return array
     */
    public function getNameRule(): array
    {
        return [
            'name' => "required|string|max:50|unique:games_category,name,{$this->category}",
        ];
    }
}
