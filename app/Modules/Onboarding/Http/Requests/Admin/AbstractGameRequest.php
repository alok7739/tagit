<?php

namespace App\Modules\Onboarding\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

abstract class AbstractGameRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'category_id' => 'required',
            'name' => 'required|string|max:150',
            'description' => 'nullable|string',
            'image' => 'required|string|max:250',
            'price' => 'required|numeric|gte:1',
            'sort_order' => 'nullable|integer',
        ];
    }

    public function attributes()
    {
        return [
            'name_ar' => 'arabic name',
            'description_ar' => 'arabic description',
        ];
    }
}
