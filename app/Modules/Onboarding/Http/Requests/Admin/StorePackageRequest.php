<?php

namespace App\Modules\Onboarding\Http\Requests\Admin;

class StorePackageRequest extends AbstractPackageRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
