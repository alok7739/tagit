<?php

namespace App\Modules\Onboarding\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterStep1Request extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:50',
            'company' => 'required|string|max:50',
            'email' => "required|email|max:50|unique:admins,email",
            'phone' => 'required|max:20',
            'password' => 'required|min:8|confirmed',
            'password_confirmation' => 'required|min:8',
            'users' => 'required|integer|min:1'
        ];
    }
}
