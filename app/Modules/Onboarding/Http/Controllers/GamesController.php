<?php

namespace App\Modules\Onboarding\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Companies\Models\Company;
use App\Modules\Onboarding\Http\Requests\GameBuyRequest;
use App\Modules\Onboarding\Models\Cart;
use App\Modules\Onboarding\Models\CompanyPackages;
use App\Modules\Onboarding\Models\Games;
use App\Modules\Onboarding\Models\GamesCategory;
use App\Modules\Onboarding\Models\GamesChallenges;
use App\Modules\Onboarding\Jobs\ProcessGameBrought;
use Illuminate\Http\Request;

class GamesController extends Controller
{
    public function index()
    {
        $category = request()->get('category');

        $games = Games::select(['games.*'])
                        ->where('games.status', '1')
                        ->where('games_category.status', '1')
                        ->join('games_category', 'games.category_id', 'games_category.id')
                        ->orderBy('sort_order', 'ASC')
                        ->get();

        $cids = array_pluck($games, 'category_id', 'category_id');

        if($category) {
            $games = Games::select(['games.*'])
                        ->where('games.status', '1')
                        ->where('games_category.status', '1')
                        ->where('category_id', $category)
                        ->join('games_category', 'games.category_id', 'games_category.id')
                        ->orderBy('sort_order', 'ASC')
                        ->get();
        }

        $categories = GamesCategory::whereIn('id', $cids)->where('status', '1')->get();
        $categories = array_pluck($categories, 'name', 'id');

        return view('front-games.index', [
            'games' => $games,
            'categories' => $categories
        ]);
    }

    public function view($id)
    {
        $game = Games::where('status', '1')
                        ->where('id', $id)
                        ->first();

        if(empty($game)) {
            return redirect()->route('games');
        }

        return view('front-games.view', [
            'game' => $game
        ]);
    }

    public function buy($id)
    {
        $game = Games::where('status', '1')
                        ->where('id', $id)
                        ->first();

        if(empty($game)) {
            return redirect()->route('games');
        }

        $data = request()->session()->get('buy_game');
        $challenges = GamesChallenges::where('game_id', $id)->count();

        return view('front-games.buy', compact(['id', 'game', 'data', 'challenges']));
    }

    public function storeBuy(GameBuyRequest $request)
    {
        $data = $request->except('_token');

        $request->session()->put('buy_game', $data);

        $id = $data['gid'];

        $game = Games::where('status', '1')
                        ->where('id', $id)
                        ->first();

        if(empty($game)) {
            return redirect()->route('games');
        }

        $cart_id = md5(time() . $data['email']);
        $response = [];

        $paymentData = [
            "profile_id"       => env('PAYTAB_ID'),
            "tran_type"        => "sale",
            "tran_class"       => "ecom",
            "cart_description" => $game->name,
            "cart_id"          => $cart_id,
            "cart_currency"    => "SAR",
            "cart_amount"      => $game->price,
            "return"           => url('/buy-game/payment-callback', [], true),
            "hide_shipping"    => true,
            "customer_details" =>  [
                "name"    => $data['name'],
                "email"   => $data['email'],
                "city"    => "Jeddah",
                "country" => "SA",
            ]
        ];

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => env('PAYTAB_URL'),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_SSL_VERIFYPEER => FALSE,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => json_encode($paymentData),
            CURLOPT_HTTPHEADER => array(
                "Authorization: " . env('PAYTAB_SECRET'),
                "content-type: application/json"
            ),
        ));

        $response = curl_exec($curl);
        $curl_err = curl_error($curl);
        curl_close($curl);

        $response = json_decode($response, 1);

        if(empty($response['redirect_url'])) {
            \Flash::error('Unable to process your requested, please try again later.');
            return redirect()->route('game.buy', ['id' => $id]);
        }

        $data['game'] = $game;

        // Insert into cart
        $cart = new Cart;
        $cart->cart_hash = $cart_id;
        $cart->tran_ref  = isset($response['tran_ref']) ? $response['tran_ref'] : '';
        $cart->data      = json_encode($data);
        $cart->save();

        return redirect($response['redirect_url']);
    }

    public function paymentCallback(Request $request)
    {
        $data = $request->except('signature');
        ksort($data);

        $query = http_build_query($data);

        $signature = hash_hmac('sha256', $query, env('PAYTAB_SECRET'));

        if (hash_equals($signature, $request->get("signature")) === TRUE)
        {
            $cart = Cart::where('cart_hash', $request->get('cartId'))->first();

            $data = json_decode($cart->data);

            if($request->get("respStatus") == 'A')
            {
                $games = GamesChallenges::where('game_id', $data->gid)->count();

                $code = explode(' ', $data->name);
                $code = strtolower($code[0]);

                // Insert in companies
                $company = new Company;
                $company->name = $data->name;
                $company->info = json_encode($data);
                $company->type = 'single';
                $company->email_sent = '2';
                $company->save();

                // Insert in company_packages
                $package = new CompanyPackages;
                $package->company_id = $company->id;
                $package->package_id = $data->gid;
                $package->users      = $data->users;
                $package->games      = $games;
                $package->amount     = round($data->game->price * $data->users, 2);
                $package->expiry     = null;
                $package->payment_response = json_encode($request->all());
                $package->save();

                // Update companies package_id & code
                $code = $code . $company->id;
                $code = str_pad($code, 5, 'x', STR_PAD_RIGHT);

                $company->join_code  = $code;
                $company->package_id = $package->id;
                $company->save();

                // Delete cart & session
                $cart->delete();
                request()->session()->put('buy_game', null);

                dispatch(new ProcessGameBrought($company));

                // Redirect
                return redirect('/buy-game/thank-you');
            }
            else
            {
                $id = $data->gid;
                $cart->delete();

                \Flash::error('Payment Error: ' . $request->get("respMessage"));
                return redirect()->route('game.buy', ['id' => $id]);
            }
        }
        else {
            return redirect('/');
        }
    }

    public function thankyou()
    {
        return view('front-games.thankyou');
    }
}
