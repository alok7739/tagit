<?php

namespace App\Modules\Onboarding\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Modules\Onboarding\Datatables\PackagesDataTable;
use App\Modules\Onboarding\Models\Packages;
use App\Modules\Onboarding\Http\Requests\Admin\StorePackageRequest;
use App\Modules\Onboarding\Http\Requests\Admin\UpdatePackageRequest;
use Laracasts\Flash\Flash;

class PackagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param PackagesDataTable $dataTable
     * @return mixed
     */
    public function index(PackagesDataTable $dataTable)
    {
        return $dataTable->render('packages.index');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\View\View
     */
    public function create()
    {
        return view('packages.create');
    }

    /**
     * @param StorePackagesRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StorePackageRequest $request)
    {
        $package = app()[Packages::class];
        $package->fill($request->except('pricing'));
        $package->save();

        Flash::success('Package created successfully.');

        return redirect()->route('packages.index');
    }

    /**
     * @param Packages $package
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(int $id)
    {
        $package = Packages::findOrFail($id);

        if (null === $package) {
            flash('No such record');
            return redirect()->route('packages.index');
        }

        return view('packages.edit', [
            'package' => $package
        ]);
    }

    /**
     * @param UpdatePackagesRequest $request
     * @param Packages $package
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdatePackageRequest $request, int $id)
    {
        $package = Packages::findOrFail($id);

        if (null === $package) {
            flash('No such record');
            return redirect()->route('packages.index');
        }

        $package->update($request->except('pricing'));

        Flash::success('Package updated successfully.');

        return redirect()->route('packages.index');
    }

    /**
     * @param Packages $package
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function destroy(int $id)
    {
        $package = Packages::findOrFail($id);

        if (null === $package) {
            flash('No such record');
            return redirect()->route('packages.index');
        }

        $package->delete();

        Flash::success('Package deleted successfully.');

        return redirect()->route('packages.index');
    }
}
