<?php

namespace App\Modules\Onboarding\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Modules\Library\Models\Library;
use App\Modules\Onboarding\Datatables\GamesDataTable;
use App\Modules\Onboarding\Models\Games;
use App\Modules\Onboarding\Http\Requests\Admin\StoreGameRequest;
use App\Modules\Onboarding\Http\Requests\Admin\UpdateGameRequest;
use App\Modules\Onboarding\Models\GamesCategory;
use App\Modules\Onboarding\Models\GamesChallenges;
use Laracasts\Flash\Flash;
use Illuminate\Http\Request;

class GamesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param GamesDataTable $dataTable
     * @return mixed
     */
    public function index(GamesDataTable $dataTable)
    {
        
        $categoryId = 0;

        if(request()->has("category")){

            $categoryId = (int) request()->category;    
            if($categoryId){
                request()->session()->put('categoryId', $categoryId);
            }

        }

        $dataTable->setCategory($categoryId);
        
        return $dataTable->with('category', $categoryId)->render('games.index');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\View\View
     */
    public function create()
    {
        $categories = GamesCategory::where('status', '1')->get();
        $categories = array_pluck($categories, 'name', 'id');

        $library = Library::where('status', '1')->get();
        $library = array_pluck($library, 'name', 'id');

        return view('games.create', compact('categories', 'library'));
    }

    /**
     * @param StoreGameRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreGameRequest $request)
    {
        $game = app()[Games::class];
        $game->fill($request->except('challenges'));
        $game->save();

        $challenges = $request->get('challenges');

        if($challenges) {
            foreach($challenges as $c) {
                $gc = app()[GamesChallenges::class];
                $gc->game_id    = $game->id;
                $gc->library_id = (int) $c['library_id'];
                $gc->duration   = (float) $c['duration'];
                $gc->dependent  = (int) $c['dependent'];
                $gc->save();
            }
        }

        Flash::success('Game created successfully.');

        return redirect()->route('games.index');
    }

    /**
     * @param Games $game
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(int $id)
    {
        $game = Games::findOrFail($id);

        if (null === $game) {
            flash('No such record');
            return redirect()->route('games.index');
        }

        $challenges = [];
        $cdata = GamesChallenges::where('game_id', $id)->get();

        foreach($cdata as $c)
        {
            $challenges[] = [
                'id' => $c->id,
                'library_id' => $c->library_id,
                'duration' => $c->duration,
                'dependent' => $c->dependent
            ];
        }

        $categories = GamesCategory::where('status', '1')->get();
        $categories = array_pluck($categories, 'name', 'id');

        $library = Library::where('status', '1')->get();
        $library = array_pluck($library, 'name', 'id');

        return view('games.edit', [
            'game' => $game,
            'challenges' => $challenges,
            'categories' => $categories,
            'library' => $library
        ]);
    }

    /**
     * @param UpdateGameRequest $request
     * @param Games $game
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateGameRequest $request, int $id)
    {
        $game = Games::findOrFail($id);

        if (null === $game) {
            flash('No such record');
            return redirect()->route('games.index');
        }

        $game->update($request->except('challenges'));

        $challenges = $request->get('challenges');

        $cids = [0];

        if($challenges) {
            foreach($challenges as $c) {
                $gc = $c['id'] ? GamesChallenges::find($c['id']) : '';
                $gc = $gc ? $gc : app()[GamesChallenges::class];

                $gc->game_id    = $game->id;
                $gc->library_id = (int) $c['library_id'];
                $gc->duration   = (float) $c['duration'];
                $gc->dependent  = (int) $c['dependent'];
                $gc->save();

                $cids[] = $gc->id;
            }
        }

        GamesChallenges::where('game_id', $game->id)
                        ->whereNotIn('id', $cids)
                        ->delete();

        Flash::success('Game updated successfully.');

        return redirect()->route('games.index');
    }

    /**
     * @param Games $game
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function destroy(int $id)
    {
        $game = Games::findOrFail($id);

        if (null === $game) {
            flash('No such record');
            return redirect()->route('games.index');
        }

        $game->delete();

        Flash::success('Game deleted successfully.');

        return redirect()->route('games.index');
    }
}
