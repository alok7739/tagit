<?php

namespace App\Modules\Onboarding\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Modules\Onboarding\Datatables\CategoryDataTable;
use App\Modules\Onboarding\Http\Requests\Admin\StoreCategoryRequest;
use App\Modules\Onboarding\Http\Requests\Admin\UpdateCategoryRequest;
use App\Modules\Onboarding\Models\GamesCategory;
use App\Modules\Onboarding\Models\Games;
use Laracasts\Flash\Flash;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param CategoryDataTable $dataTable
     * @return mixed
     */
    public function index(CategoryDataTable $dataTable)
    {
        return $dataTable->render('category.index');
    }

    public function create()
    {
        return view('category.create');
    }

    public function store(StoreCategoryRequest $request)
    {
        $category = app()[GamesCategory::class];
        $category->fill($request->all());
        $category->save();

        Flash::success('Category created successfully.');

        return redirect()->route('gamescategory.index');
    }

    public function edit(int $id)
    {
        $category = GamesCategory::findOrFail($id);

        if (null === $category) {
            flash('No such record');
            return redirect()->route('gamescategory.index');
        }

        return view('category.edit', [
            'category' => $category
        ]);
    }

    public function update(UpdateCategoryRequest $request, int $id)
    {
        $category = GamesCategory::findOrFail($id);

        if (null === $category) {
            flash('No such record');
            return redirect()->route('gamescategory.index');
        }

        $category->update($request->all());

        Flash::success('Category updated successfully.');

        return redirect()->route('gamescategory.index');
    }

    public function destroy(int $id)
    {
        $category = GamesCategory::findOrFail($id);

        if (null === $category) {
            flash('No such record');
            return redirect()->route('gamescategory.index');
        }

        $games = Games::where('category_id', $id)->count();

        if($games) {
            Flash::error('Can not delete category, it is associated with ' . $games . ' game(s).');

            return redirect()->route('gamescategory.index');
        }

        $category->name = 'Deleted - ' . $category->name;
        $category->save();

        $category->delete();

        Flash::success('Category deleted successfully.');

        return redirect()->route('gamescategory.index');
    }
}
