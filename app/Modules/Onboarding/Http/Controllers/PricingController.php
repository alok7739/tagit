<?php

namespace App\Modules\Onboarding\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Onboarding\Models\Packages;
use App\Modules\Onboarding\Models\PackagesPricing;

class PricingController extends Controller
{
    public $users = [3, 10, 20, 50, 100];
    public $games = [1, 4, 8, 12, 24, 48];

    public function index()
    {
        $packages = Packages::where('status', '1')
                            ->orderBy('sort_order', 'ASC')
                            ->limit(10)
                            ->get();

        $pids = array_pluck($packages, 'id', 'id');

        $pdata = PackagesPricing::whereIn('package_id', $pids)
                                    ->whereNotNull('price')
                                    ->get();

        $pricing = $price_ids = [];

        foreach($pdata as $p)
        {
            $pricing[$p->package_id][$p->games][$p->users] = $p->price;
            $price_ids[$p->package_id][$p->games][$p->users] = $p->id;
        }

        return view('pricing.index', [
            'users' => $this->users,
            'games' => $this->games,
            'pids' => array_values($pids),
            'price_ids' => $price_ids,
            'packages' => $packages,
            'pricing' => $pricing
        ]);
    }
}
