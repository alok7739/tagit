<?php

namespace App\Modules\Onboarding\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\Companies\Models\Company;
use App\Modules\Onboarding\Models\Packages;
use App\Modules\Onboarding\Models\Cart;
use App\Modules\Onboarding\Http\Requests\RegisterStep1Request;
use App\Modules\Onboarding\Models\CompanyPackages;
use App\Modules\Users\Admin\Models\Admin;
use Illuminate\Support\Facades\Mail;

class RegisterController extends Controller
{
    protected $defaultPackage = [
        'name' => 'Free',
        'challenges' => 3,
        'users' => 3,
        'price' => 0,
        'time_limit' => 1,
        'true_false' => 1,
        'sc_ques' => 1,
        'mc_ques' => 1,
        'checkin' => 1,
        'qr_code' => 1,
        'images' => 1,
        'screenshots' => 1,
        'videos' => 1,
        'locking' => 1,
    ];

    public function step1(Request $request)
    {
        $id = $request->get('package', 0);

        if($id) {
            $package = Packages::find($id);
        } else {
            $package = (object) $this->defaultPackage;
        }

        $data = $request->session()->get('reg_step1');

        return view('register.index', compact(['id', 'package', 'data']));
    }

    public function storeStep1(RegisterStep1Request $request)
    {
        $data = $request->except('_token');

        $request->session()->put('reg_step1', $data);

        $id = $request->get('pid', 0);
        $package = Packages::find($id);

        $cart_id = md5(time() . $data['email']);
        $response = [];

        if($package && $package->price > 0)
        {
            $paymentData = [
                "profile_id"       => env('PAYTAB_ID'),
                "tran_type"        => "sale",
                "tran_class"       => "ecom",
                "cart_description" => $package->name . ' - ' . $data['users'] . ' user',
                "cart_id"          => $cart_id,
                "cart_currency"    => "SAR",
                "cart_amount"      => $package->price * $package->time_limit * $data['users'],
                "return"           => url('/payment-callback', [], true),
                "hide_shipping"    => true,
                "customer_details" =>  [
                    "name"    => $data['name'],
                    "email"   => $data['email'],
                    "city"    => "Jeddah",
                    "country" => "SA",
                ]
            ];

            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => env('PAYTAB_URL'),
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_SSL_VERIFYPEER => FALSE,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS => json_encode($paymentData),
                CURLOPT_HTTPHEADER => array(
                    "Authorization: " . env('PAYTAB_SECRET'),
                    "content-type: application/json"
                ),
            ));

            $response = curl_exec($curl);
            $curl_err = curl_error($curl);
            curl_close($curl);

            $response = json_decode($response, 1);

            if(empty($response['redirect_url'])) {
                \Flash::error('Unable to process your requested, please try again later.');
                return redirect('/register?package=' . $id);
            }

            $data['package'] = $package->toArray();
        } else {
            $data['package'] = $this->defaultPackage;
            $data['users']   = $this->defaultPackage['users'];
        }

        // Insert into cart
        $cart = new Cart;
        $cart->cart_hash = $cart_id;
        $cart->tran_ref  = isset($response['tran_ref']) ? $response['tran_ref'] : '';
        $cart->data      = json_encode($data);
        $cart->save();

        if($package && $package->price > 0)
        {
            return redirect($response['redirect_url']);
        }
        else
        {
            $this->processRegister($cart_id, [], 1);

            // Delete session
            request()->session()->put('reg_step1', null);

            // Redirect
            return redirect('/register/thank-you?trial=1');
        }
    }

    public function paymentCallback(Request $request)
    {
        $data = $request->except('signature');
        ksort($data);

        $query = http_build_query($data);

        $signature = hash_hmac('sha256', $query, env('PAYTAB_SECRET'));

        if (hash_equals($signature, $request->get("signature")) === TRUE)
        {
            if($request->get("respStatus") == 'A')
            {
                $this->processRegister($request->get('cartId'), $request->all());
            }
            else
            {
                $cart = Cart::where('cart_hash', $request->get('cartId'))->first();

                $data = json_decode($cart->data);

                $id = $data->pid;
                $cart->delete();

                \Flash::error('Payment Error: ' . $request->get("respMessage"));
                return redirect()->route('register', ['package' => $id]);
            }

            // Redirect
            return redirect('/register/thank-you');
        }
        else {
            return redirect('/register');
        }
    }

    public function thankyou(Request $request)
    {
        $is_trial = $request->trial ? 1 : 0;

        return view('register.thankyou', compact('is_trial'));
    }

    private function processRegister($cart_hash, $payment_response = [], $is_trial = 0)
    {
        $cart = Cart::where('cart_hash', $cart_hash)->first();

        $data = json_decode($cart->data);

        $code = explode(' ', $data->company);
        $code = strtolower($code[0]);

        // Insert in companies
        $company = new Company;
        $company->name = $data->company;
        $company->info = $data->company;
        $company->person_name = $data->name;
        $company->phone = $data->phone;
        $company->type = $is_trial ? 'trial' : 'commercial';
        $company->save();

        // Insert in company_packages
        $package = new CompanyPackages;
        $package->company_id = $company->id;
        $package->package_id = $data->pid;
        $package->users      = $data->users;
        $package->games      = $data->package->challenges;
        $package->amount     = $data->package->price * $data->package->time_limit * $data->users;
        $package->expiry     = $data->package->time_limit > 0 ? date('Y-m-d', strtotime('+' . $data->package->time_limit . ' months')) : null;
        $package->true_false = $data->package->true_false;
        $package->sc_ques    = $data->package->sc_ques;
        $package->mc_ques    = $data->package->mc_ques;
        $package->checkin    = $data->package->checkin;
        $package->qr_code    = $data->package->qr_code;
        $package->images     = $data->package->images;
        $package->screenshots = $data->package->screenshots;
        $package->videos     = $data->package->videos;
        $package->locking    = $data->package->locking;
        $package->payment_response = json_encode($payment_response);
        $package->save();

        // Update companies package_id & code
        if($is_trial) {
            $code = md5(time() . $data->email . $company->id);
        } else {
            $code = $code . $company->id;
            $code = str_pad($code, 5, 'x', STR_PAD_RIGHT);
        }

        $company->join_code  = $code;
        $company->package_id = $package->id;
        $company->save();

        // Insert in users
        $user = new Admin;
        $user->username   = $data->email;
        $user->email      = $data->email;
        $user->password   = $data->password;
        $user->role_id    = '2';
        $user->company_id = $company->id;
        $user->status     = $is_trial ? '0' : '1';
        $user->save();

        // Delete cart & session
        $cart->delete();
        request()->session()->put('reg_step1', null);

        if($is_trial) {
            $inputs = [
                'name' => $data->name,
                'email' => $data->email,
                'code' => $company->join_code
            ];

            Mail::send('emails.trial-account', $inputs, function($message) use($data) {
                $message->to($data->email, $data->name)
                    ->subject('Verify your email address');
            });
        }
    }

    public function verifyTrial(Request $request)
    {
        $code = $request->code;

        $company = Company::where('join_code', $code)->where('type', 'trial')->first();

        if($company) {
            $code = explode(' ', $company->name);
            $code = strtolower($code[0]);
            $code = $code . $company->id;
            $code = str_pad($code, 5, 'x', STR_PAD_RIGHT);

            $company->join_code = $code;
            $company->type = 'commercial';
            $company->save();

            Admin::where('company_id', $company->id)->update(['status' => '1']);

            flash('Your email address is now verified.', 'success');
            return redirect()->route('login');
        } else {
            flash('Invalid or expired verification link.', 'danger');
            return redirect()->route('login');
        }
    }
}
