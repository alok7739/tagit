<?php

namespace App\Modules\Onboarding\Jobs;

use App\Modules\Companies\Models\Company;
use App\Modules\Challenges\Models\Challenge;
use App\Modules\Challenges\Enums\ProofTypeEnum;
use App\Modules\Library\Models\Answer;
use App\Modules\Library\Models\Question;
use App\Modules\Onboarding\Models\CompanyPackages;
use App\Modules\Onboarding\Models\Games;
use App\Modules\Onboarding\Models\GamesChallenges;
use Illuminate\Support\Facades\Mail;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Str;
use Carbon\Carbon;

class ProcessGameBrought implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $company;

    public function __construct(Company $company)
    {
        $this->company = $company;
    }

    public function handle() : void
    {
        $data = json_decode($this->company->info);
        $package = CompanyPackages::find($this->company->package_id);
        $game = Games::find($data->gid);
        $library = GamesChallenges::with(['library'])->where('game_id', $data->gid)->get();
        $last_id = 0;

        foreach($library as $l)
        {
            $hm = explode('.', $l->duration);
            $hr = $hm[0];
            $min = empty($hm[1]) ? 0 : round($hm[1]/100 * 60);

            $end_date = Carbon::createFromFormat('Y-m-d H:i', $data->start_date)->addHours($hr)->addMinutes($min);

            $challenge = app()[Challenge::class];
            $challenge->fill($l->library->toArray());

            $challenge->company_id          = $this->company->id;
            $challenge->participants_limit  = $package->users;
            $challenge->start_date          = $data->start_date;
            $challenge->end_date            = $end_date;
            $challenge->qr_string           = Str::random();
            $challenge->dependent_challenge = $l->dependent ? $last_id : null;
            $challenge->status              = 'waiting';
            $challenge->save();

            $last_id = $challenge->id;

            // Questions data
            if (in_array($challenge->proof_type, [ProofTypeEnum::QUESTIONS, ProofTypeEnum::TESTS, ProofTypeEnum::TRUE_FALSE]))
            {
                $questions = $answers = [];
                $ques = Question::where('library_id', $l->library->id)->get();
                $c = 0;

                foreach($ques as $q)
                {
                    $questions[$c] = $q->text;

                    $ans = Answer::where('question_id', $q->id)->get();

                    foreach($ans as $a)
                    {
                        if($challenge->proof_type == ProofTypeEnum::TESTS) {
                            $answers[$c][] = $a->text;
                        } else {
                            $answers[$c] = $a->text;
                        }
                    }

                    $c++;
                }

                if ($challenge->proof_type === ProofTypeEnum::QUESTIONS) {
                    $challenge->saveQuestions([
                        'questions' => $questions,
                        'answers' => $answers
                    ]);
                }
                elseif ($challenge->proof_type === ProofTypeEnum::TESTS) {
                    $challenge->saveTests([
                        'questions' => $questions,
                        'test_answers' => $answers
                    ]);
                }
                elseif ($challenge->proof_type === ProofTypeEnum::TRUE_FALSE) {
                    $challenge->saveTrueFalse([
                        'questions' => $questions,
                        'answers' => $answers
                    ]);
                }
            }
        }

        $inputs = [
            'name' => $data->name,
            'code' => $this->company->join_code
        ];

        Mail::send('emails.buy-game', $inputs, function($message) use($data) {
            $message->to($data->email, $data->name)
                ->subject('Thank you for your purchase.');
        });

        Company::find($this->company->id)->update(['email_sent', 1]);
    }
}
