<?php
namespace App\Modules\Onboarding\Datatables;

use App\Modules\Onboarding\Models\Games;
use Illuminate\Database\Eloquent\Builder;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Builder as YajraBuilder;
use Yajra\DataTables\Services\DataTable;

class GamesDataTable extends DataTable
{
    protected $category;

    /**
     * Build DataTable class.
     *
     * @param mixed $query Result from query() method.
     * @return EloquentDataTable
     */
    public function dataTable($query): EloquentDataTable
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable
            ->addColumn('action', 'games.datatables_actions')
            ->editColumn('image', function ($query) {
                return ($query->image ? ("<img height='50' src=" . $query->image) . " />" : (''));
            })
            ->editColumn('status', function($query) {
                return $query->status ? 'Active' : 'Inactive';
            })
            ->rawColumns(['image', 'action']);
    }

    public function setCategory(int $category = null): DataTable
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get query source of dataTable.
     *
     * @param Games $model
     * @return Games
     */
    public function query(Games $model): Builder
    {
        if ($this->category) {
            return $model->with(['category'])
                        ->where('category_id', $this->category);
        }

        return $model->with(['category']);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return YajraBuilder
     */
    public function html(): YajraBuilder
    {

        $url = '';
        if ($this->request()->has("category")) {
            $url = $url."?category=".$this->request()->get("category");
        }

        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax(url()->current())
            ->addAction(['width' => '10%'])
            ->ajax($url)
            ->parameters([
                'dom'     => 'frtip',
                'order'   => [[1, 'desc']],
                'responsive' => true,
                'drawCallback' => '
                    function() {
                        $.SweetAlert.init();

                    }',
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns(): array
    {
        return [
            [
                'name' => 'image',
                'data' => 'image',
                'title' => 'Image',
                'width' => '10%',
                'searchable' => false,
                'orderable' => false,
            ],
            [
                'name' => 'name',
                'data' => 'name',
                'title' => 'Name',
                'width' => '65%',
            ],
            [
                'name' => 'category.name',
                'data' => 'category.name',
                'title' => 'Category',
                'width' => '15%',
            ],
            [
                'name' => 'status',
                'data' => 'status',
                'title' => 'Status',
                'width' => '10%',
            ],
        ];
    }
}
