<?php

namespace App\Modules\Onboarding\Models;

use Illuminate\Database\Eloquent\Model as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Packages
 * @package App\Models
 * @version January 12, 2021, 4:00 pm +03
 *
 * @property string name
 * @property string name_ar
 * @property string short_desc
 * @property string short_desc_ar
 * @property string long_desc
 * @property string long_desc_ar
 * @property integer time_limit
 * @property integer true_false
 * @property integer sc_ques
 * @property integer mc_ques
 * @property integer checkin
 * @property integer qr_code
 * @property integer images
 * @property integer videos
 * @property integer locking
 * @property integer sort_order
 * @property integer status
 */
class Packages extends Model
{
    use SoftDeletes;

    public $table = 'packages';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public $fillable = [
        'name',
        'name_ar',
        'short_desc',
        'short_desc_ar',
        'long_desc',
        'long_desc_ar',
        'time_limit',
        'challenges',
        'price',
        'true_false',
        'sc_ques',
        'mc_ques',
        'checkin',
        'qr_code',
        'images',
        'screenshots',
        'videos',
        'locking',
        'sort_order',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'name_ar' => 'string',
        'short_desc' => 'string',
        'short_desc_ar' => 'string',
        'long_desc' => 'string',
        'long_desc_ar' => 'string',
        'time_limit' => 'integer',
        'true_false' => 'integer',
        'sc_ques' => 'integer',
        'mc_ques' => 'integer',
        'checkin' => 'integer',
        'qr_code' => 'integer',
        'images' => 'integer',
        'screenshots' => 'integer',
        'videos' => 'integer',
        'locking' => 'integer',
        'sort_order' => 'integer',
        'status' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'true_false' => 'required',
        'sc_ques' => 'required',
        'mc_ques' => 'required',
        'checkin' => 'required',
        'qr_code' => 'required',
        'images' => 'required',
        'screenshots' => 'required',
        'videos' => 'required',
        'locking' => 'required',
        'status' => 'required'
    ];


}
