<?php

namespace App\Modules\Onboarding\Models;

use Illuminate\Database\Eloquent\Model as Model;

/**
 * Class CompanyPackages
 * @package App\Models
 * @version January 12, 2021, 4:01 pm +03
 */
class CompanyPackages extends Model
{

    public $table = 'company_packages';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public $fillable = [
        'company_id',
        'package_id',
        'users',
        'games',
        'amount',
        'expiry',
        'true_false',
        'sc_ques',
        'mc_ques',
        'checkin',
        'qr_code',
        'images',
        'screenshots',
        'videos',
        'locking',
        'payment_response'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'company_id' => 'required',
        'package_id' => 'required',
        'users' => 'required',
        'games' => 'required',
        'amount' => 'required',
        'true_false' => 'required',
        'sc_ques' => 'required',
        'mc_ques' => 'required',
        'checkin' => 'required',
        'qr_code' => 'required',
        'images' => 'required',
        'screenshots' => 'required',
        'videos' => 'required',
        'locking' => 'required',
    ];


}
