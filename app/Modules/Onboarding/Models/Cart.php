<?php

namespace App\Modules\Onboarding\Models;

use Illuminate\Database\Eloquent\Model as Model;

/**
 * Class Cart
 * @package App\Models
 * @version January 12, 2021, 4:00 pm +03
 */
class Cart extends Model
{
    public $table = 'cart';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public $fillable = [
        'cart_hash',
        'tran_ref',
        'data'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'cart_hash' => 'required',
        'tran_ref' => 'required',
        'data' => 'required'
    ];

    
}
