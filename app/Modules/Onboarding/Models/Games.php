<?php

namespace App\Modules\Onboarding\Models;

use Illuminate\Database\Eloquent\Model as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;

/**
 * Class Games
 * @property string name
 * @property integer status
 */
class Games extends Model
{
    use SoftDeletes;

    public $table = 'games';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public $fillable = [
        'category_id',
        'name',
        'name_ar',
        'description',
        'description_ar',
        'image',
        'price',
        'sort_order',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'category_id' => 'integer',
        'name' => 'string',
        'name_ar' => 'string',
        'description' => 'string',
        'description_ar' => 'string',
        'sort_order' => 'integer',
        'status' => 'integer'
    ];

    /**
     * @param $value
     * @return string|null
     */
    public function getImageAttribute($value): ?string
    {
        return $value ? Storage::url(str_replace(env('AWS_URL'), '', $value)) : null;
    }

    /**
     * @return string
     */
    public function getImageWithDefaultAttribute($value): ?string
    {
        return $value ? Storage::url($value) : '/assets/images/default_challenge.svg';
    }

    /**
     * @param $attribute
     */
    public function setImageAttribute($attribute)
    {
        if (null !== $this->image) {
            Storage::delete($this->image);
        }

        $this->attributes['image'] = $attribute;
    }

    public function category()
    {
        return $this->belongsTo(GamesCategory::class);
    }
}
