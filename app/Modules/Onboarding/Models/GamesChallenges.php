<?php

namespace App\Modules\Onboarding\Models;

use App\Modules\Library\Models\Library;
use Illuminate\Database\Eloquent\Model as Model;

/**
 * Class GamesChallenges
 * @property string name
 * @property integer status
 */
class GamesChallenges extends Model
{
    public $table = 'games_challenges';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public $fillable = [
        'game_id',
        'library_id',
        'duration',
        'dependent'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'game_id' => 'integer',
        'library_id' => 'integer',
        'dependent' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'game_id' => 'required',
        'library_id' => 'required',
        'duration' => 'required',
    ];

    public function library()
    {
        return $this->belongsTo(Library::class);
    }
}
