<?php

namespace App\Modules\Onboarding\Models;

use Illuminate\Database\Eloquent\Model as Model;

/**
 * Class PackagesPricing
 * @package App\Models
 * @version January 12, 2021, 4:01 pm +03
 *
 * @property integer package_id
 * @property integer users
 * @property integer games
 * @property number price
 */
class PackagesPricing extends Model
{

    public $table = 'packages_pricing';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';




    public $fillable = [
        'package_id',
        'users',
        'games',
        'price'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'package_id' => 'integer',
        'users' => 'integer',
        'games' => 'integer',
        'price' => 'float'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'package_id' => 'required',
        'users' => 'required',
        'games' => 'required',
        'price' => 'required'
    ];

    
}
