<?php


namespace App\Modules\Challenges\Policies;

use App\Modules\Users\Admin\Models\Admin;

class ReportsPolicy
{
    public function view(Admin $admin)
    {
        return $admin->isAdmin();
    }

    public function destroy(Admin $admin)
    {
        return $admin->isAdmin();
    }
}
