<?php


namespace App\Modules\Challenges\Policies;


use App\Modules\Challenges\Models\Challenge;
use App\Modules\Users\Admin\Models\Admin;

class ChallengesPolicy
{
    public function view(Admin $admin, Challenge $challenge)
    {
        if ($admin->isNotAdmin() && $admin->company_id !== $challenge->company_id) {
            abort(403, 'Sorry, you are not allowed to enter other challenges');
        }

        return true;
    }

    public function edit(Admin $admin)
    {
//        if ($admin->isNotAdmin() && $admin->company_id !== $challenge->company_id) {
//            abort(403, 'Sorry, you are not allowed to enter other challenges');
//        }

        return true;
    }

    public function create(Admin $admin)
    {
//        return $user->isAdmin();
        return true;
    }

    public function notification(Admin $admin)
    {
        return true;//$admin->isAdmin();
    }
}
