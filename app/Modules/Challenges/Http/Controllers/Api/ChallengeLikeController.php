<?php

namespace App\Modules\Challenges\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Modules\Challenges\Http\Requests\LikeRequest;
use App\Modules\Challenges\Models\Challenge;
use App\Modules\Challenges\Models\Likes;
use App\Services\ResponseBuilder\CustomResponseBuilder;
use Illuminate\Support\Facades\Auth;

class ChallengeLikeController extends Controller
{
    /**
     * @param Challenge $challenge
     * @param LikeRequest $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function store(LikeRequest $request, Challenge $challenge)
    {
        $like = (bool)$request->get('like', false);
        $feed = (int)$request->get('feed_id', 0);

        Likes::where([
            'challenge_id' => $challenge->id,
            'feed_id'      => $feed,
            'user_id'      => Auth::id()
        ])->delete();

        if (!$like) {    
            return CustomResponseBuilder::success();
        }

        Likes::insert([
            'challenge_id' => $challenge->id,
            'feed_id'      => $feed,
            'user_id'      => Auth::id(),
            'created_at'   => \Carbon::now(),
            'updated_at'   => \Carbon::now(),
        ]);

        return CustomResponseBuilder::success();
    }
}
