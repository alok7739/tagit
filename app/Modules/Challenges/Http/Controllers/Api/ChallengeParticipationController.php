<?php
/**
 * Created by PhpStorm.
 * User: artem.petrov
 * Date: 2019-03-01
 * Time: 17:07
 */

namespace App\Modules\Challenges\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Modules\Challenges\Enums\ProofTypeEnum;
use App\Modules\Challenges\Http\Requests\ParticipantsRequest;
use App\Modules\Challenges\Http\Requests\ParticipateRequest;
use App\Modules\Challenges\Models\Answer;
use App\Modules\Challenges\Models\Challenge;
use App\Modules\Users\User\Models\User;
use App\Services\ResponseBuilder\ApiCode;
use App\Services\ResponseBuilder\CustomResponseBuilder;
use Illuminate\Support\Facades\Auth;

class ChallengeParticipationController extends Controller
{
    /**
     * @param ParticipantsRequest $request
     * @param Challenge $challenge
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(ParticipantsRequest $request, Challenge $challenge)
    {
        $limit = (int)$request->get('limit') ?? config('custom.participants_count_per_page');

        $participants = $challenge->participants()->orderByDesc('full_name')->paginate($limit);

        return CustomResponseBuilder::success($participants);
    }

    /**
     * @param Challenge $challenge
     * @param ParticipateRequest $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function store(ParticipateRequest $request, Challenge $challenge)
    {
        /** @var User $user */
        $user = Auth::user();
        $participate = (bool)$request->get('participate', false);

        if (!$participate) {
            $challenge->participants()->detach(Auth::id());
            return CustomResponseBuilder::success();
        }

        if ($challenge->participants->contains($user->id)) {
            return CustomResponseBuilder::error(ApiCode::USER_IS_ALREADY_PARTICIPATING);
        }

        if (!$challenge->enoughFreePlaces()) {
            return CustomResponseBuilder::error(ApiCode::PARTICIPANTS_LIMIT_EXCEEDED);
        }

        $challenge->participants()->attach(Auth::id());

        return CustomResponseBuilder::success(['coins' => $user->coins]);
    }
}
