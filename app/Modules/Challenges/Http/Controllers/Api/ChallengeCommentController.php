<?php

namespace App\Modules\Challenges\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Modules\Challenges\Http\Requests\CommentRequest;
use App\Modules\Challenges\Http\Requests\CommentsRequest;
use App\Modules\Challenges\Models\Challenge;
use App\Modules\Challenges\Models\Comments;
use App\Services\ResponseBuilder\CustomResponseBuilder;
use App\Services\ResponseBuilder\ApiCode;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class ChallengeCommentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index(CommentsRequest $request, Challenge $challenge, $feed_id = 0)
    {
        $limit = config('custom.comments_count_per_page');
        $page = (int)$request->get('page') ?? 1;
        $offset = ($page - 1) * $limit;

        $comments = Comments::where('challenge_id', $challenge->id)
                    ->where('feed_id', (int) $feed_id)
                    ->orderByDesc('id')
                    ->limit($limit)
                    ->offset($offset)
                    ->get();

        $messages = [];

        foreach($comments as $c) {
            $messages[] = $this->prepareMessage($c);
        }

        $messages = array_reverse($messages);

        return CustomResponseBuilder::success(['cid' => 'comments'.$challenge->id, 'messages' => $messages]);
    }

    /**
     * @param Challenge $challenge
     * @param LikeRequest $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function store(CommentRequest $request, Challenge $challenge, $feed_id = 0)
    {
        $message = $request->get('message');
        
        $user = Auth::user();

        $comments = app(Comments::class)->create([
            'challenge_id' => $challenge->id,
            'user_id' => $user->id,
            'feed_id' => (int) $feed_id,
            'message' => $message,
        ]);

        return CustomResponseBuilder::success($this->prepareMessage($comments));
    }

    public function destroy(Challenge $challenge, Comments $comment) : Response
    {
        $user = Auth::user();

        if ($comment->user_id !== $user->id) {
            return CustomResponseBuilder::error(ApiCode::UNAUTHENTICATED);
        }
        elseif ($comment->challenge_id !== $challenge->id) {
            return CustomResponseBuilder::error(ApiCode::COMMENT_DOES_NOT_BELONG_TO_THIS_CHALLENGE);
        }
        
        $comment->delete();

        return CustomResponseBuilder::success();
    }

    private function prepareMessage($row) {
        $user = Auth::user();

        $name = $row->user->full_name;
        $avatar = $row->user->avatar;
        $avatar = $avatar ?? 'https://i.ibb.co/HDHsj5q/default-avatar.png';

        $msg = [
            "id"      => $row->id,
            "time"    => strtotime($row->created_at),
            "message" => $row->message,
            "user_id" => $row->user_id,
            "name"    => $name,
            "avatar"  => $avatar
        ];

        return $msg;
    }
}
