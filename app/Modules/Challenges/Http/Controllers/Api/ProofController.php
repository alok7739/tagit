<?php
/**
 * Created by Maksym Ignatchenko, Appus Studio LP on 29.03.19
 *
 */

namespace App\Modules\Challenges\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Modules\Challenges\Enums\ProofStatusEnum;
use App\Modules\Challenges\Enums\ProofTypeEnum;
use App\Modules\Challenges\Http\Requests\SendProofRequest;
use App\Modules\Challenges\Http\Requests\SingleUploadRequest;
use App\Modules\Challenges\Models\Challenge;
use App\Modules\Challenges\Models\Proof;
use App\Modules\Challenges\Services\ChallengeService;
use App\Modules\Challenges\Services\Counter\CacheCounter;
use App\Services\ResponseBuilder\ApiCode;
use App\Services\ResponseBuilder\CustomResponseBuilder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\Response;

class ProofController extends Controller
{

    /**
     * @param Challenge $challenge
     * @param Proof $proof
     * @return Response
     */
    public function show(Challenge $challenge, Proof $proof) : Response
    {
        if ($proof->challenge_id !== $challenge->id) {
            return CustomResponseBuilder::error(ApiCode::PROOF_DOES_NOT_BELONG_TO_THIS_CHALLENGE);
        }
        return CustomResponseBuilder::success($proof);
    }

    /**
     * @param SendProofRequest $request
     * @return Response
     */
    public function store(SendProofRequest $request) : Response
    {
        $challenge = $request->challenge;
        $user = Auth::user();
        if (!$challenge->checkForActiveStatus()) {
            return CustomResponseBuilder::error(ApiCode::CHALLENGE_NOT_ACTIVE);
        }
        if (!$challenge->is_participated) {
            return CustomResponseBuilder::error(ApiCode::USER_NOT_PARTICIPATING);
        }
        if ($user->isAbleToSendProof($challenge)) {
            return CustomResponseBuilder::error(ApiCode::USER_HAS_PENDING_OR_ACCEPTED_PROOF);
        }

        // $counter = app(CacheCounter::class, ['key' => 'proof_' . Auth::user()->id . '_' . $challenge->id]);

        // if ($counter->checkLimit(ChallengeService::PROOF_ADDING_LIMIT)) {
        //     return CustomResponseBuilder::error(ApiCode::PROOF_ADDING_LIMIT_EXCEEDED);
        // }

        // $counter->increment();

        $proof = app()[Proof::class];
        $proof->fill($request->all());
        $proof->fillFiles($request->items);
        $proof->type = $proof->type ? $proof->type : $challenge->getRequiredProofsType();
        $proof->challenge_id = $challenge->id;
        $proof->user_id = $user->id;
        $proof->status = ProofStatusEnum::PENDING;
        $proof->handleAttemptDurationTime($challenge);
        $proof->save();

        return CustomResponseBuilder::success([
            'id' => $proof->id,
            'challenge_id' => $proof->challenge_id,
            'status' => $proof->status,
        ]);
    }

    public function storeQuestionsProof(Request $request, Challenge $challenge)
    {
        $user = Auth::user();

        if (!$challenge->checkForActiveStatus()) {
            return CustomResponseBuilder::error(ApiCode::CHALLENGE_NOT_ACTIVE);
        }

        if (!$challenge->is_participated) {
            return CustomResponseBuilder::error(ApiCode::USER_NOT_PARTICIPATING);
        }

        if ($user->isAbleToSendProof($challenge)) {
            return CustomResponseBuilder::error(ApiCode::USER_HAS_PENDING_OR_ACCEPTED_PROOF);
        }

        $proof = app(Proof::class);
        $proof->challenge_id = $challenge->id;
        $proof->user_id = $user->id;
        $proof->type = ProofTypeEnum::QUESTIONS;
        $proof->status = ProofStatusEnum::PENDING;
        $proof->items = [];
        $proof->save();

        $proof->update(['status' => ProofStatusEnum::ACCEPTED]);

        return CustomResponseBuilder::success([
            'id' => $proof->id,
            'challenge_id' => $proof->challenge_id,
            'status' => $proof->status,
        ]);
    }

    public function storeQrProof(Request $request, Challenge $challenge)
    {
        $user = Auth::user();

        if (!$challenge->checkForActiveStatus()) {
            return CustomResponseBuilder::error(ApiCode::CHALLENGE_NOT_ACTIVE);
        }

        if (!$challenge->is_participated) {
            return CustomResponseBuilder::error(ApiCode::USER_NOT_PARTICIPATING);
        }

        if ($user->isAbleToSendProof($challenge)) {
            return CustomResponseBuilder::error(ApiCode::USER_HAS_PENDING_OR_ACCEPTED_PROOF);
        }

        $proof = app(Proof::class);
        $proof->challenge_id = $challenge->id;
        $proof->user_id = $user->id;
        $proof->type = ProofTypeEnum::QRCODE;
        $proof->status = ProofStatusEnum::PENDING;
        $proof->items = [];
        $proof->save();

        $proof->update(['status' => ProofStatusEnum::ACCEPTED]);

        return CustomResponseBuilder::success([
            'id' => $proof->id,
            'challenge_id' => $proof->challenge_id,
            'status' => $proof->status,
        ]);
    }

    public function storeLocationProof(Request $request, Challenge $challenge)
    {
        $user = Auth::user();

        if (!$challenge->checkForActiveStatus()) {
            return CustomResponseBuilder::error(ApiCode::CHALLENGE_NOT_ACTIVE);
        }

        if (!$challenge->is_participated) {
            return CustomResponseBuilder::error(ApiCode::USER_NOT_PARTICIPATING);
        }

        if ($user->isAbleToSendProof($challenge)) {
            return CustomResponseBuilder::error(ApiCode::USER_HAS_PENDING_OR_ACCEPTED_PROOF);
        }

        $proof = app(Proof::class);
        $proof->challenge_id = $challenge->id;
        $proof->user_id = $user->id;
        $proof->type = ProofTypeEnum::CHECKIN;
        $proof->status = ProofStatusEnum::PENDING;
        $proof->items = [];
        $proof->save();

        $proof->update(['status' => ProofStatusEnum::ACCEPTED]);

        return CustomResponseBuilder::success([
            'id' => $proof->id,
            'challenge_id' => $proof->challenge_id,
            'status' => $proof->status,
        ]);
    }

    /**
     * @param Challenge $challenge
     * @param Proof $proof
     * @return Response
     * @throws \Exception
     */
    public function destroy(Challenge $challenge, Proof $proof) : Response
    {
        $this->authorize('destroy', $proof);
        if ($proof->challenge_id !== $challenge->id) {
            return CustomResponseBuilder::error(ApiCode::PROOF_DOES_NOT_BELONG_TO_THIS_CHALLENGE);
        }
        $proof->delete();
        return CustomResponseBuilder::success();
    }

    public function upload(SingleUploadRequest $request) : Response
    {
        $user = Auth::user();
        $challenge = $request->challenge;
        $required_count = $challenge->getRequiredProofsCount();

        if (!$challenge->checkForActiveStatus()) {
            return CustomResponseBuilder::error(ApiCode::CHALLENGE_NOT_ACTIVE);
        }
        if (!$challenge->is_participated) {
            return CustomResponseBuilder::error(ApiCode::USER_NOT_PARTICIPATING);
        }

        if ($user->isAbleToSendProof($challenge)) {
            return CustomResponseBuilder::error(ApiCode::USER_HAS_PENDING_OR_ACCEPTED_PROOF);
        }

        $proof = Proof::where('challenge_id', $challenge->id)
                        ->where('user_id', $user->id)
                        ->first();

        if(!$proof) {
            $proof = app()[Proof::class];
            $proof->challenge_id = $challenge->id;
            $proof->user_id = $user->id;
            $proof->type = $challenge->getRequiredProofsType();
            $proof->status = ProofStatusEnum::UPLOADING;
            $proof->handleAttemptDurationTime($challenge);

            $items = [];
        } else {
            $items = $proof->items;
        }

        if(count($items) < $required_count) {
            $file = $request->file('item');
            $file = $proof->saveItem($file);
            $items[] = Storage::url($file);
            $proof->status = ProofStatusEnum::UPLOADING;
        }

        if(count($items) >= $required_count) {
            $proof->status = ProofStatusEnum::PENDING;

            if(count($items) > $required_count) {
                array_splice($items, 0, $required_count);
            }
        }

        $proof->items = $items;
        $proof->save();

        return CustomResponseBuilder::success([
            'id' => 1,
            'challenge_id' => $challenge->id,
            'status' => $proof->status,
        ]);
    }
}
