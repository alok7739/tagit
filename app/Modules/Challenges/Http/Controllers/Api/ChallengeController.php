<?php
/**
 * Created by Maksym Ignatchenko, Appus Studio LP on 25.02.19
 *
 */

namespace App\Modules\Challenges\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Modules\Challenges\Enums\ProofTypeEnum;
use App\Modules\Challenges\Models\Answer;
use App\Modules\Challenges\Models\Challenge;
use App\Modules\Challenges\Models\Proof;
use App\Modules\Challenges\Models\Reports;
use App\Modules\Challenges\Services\ChallengeService;
use App\Modules\Users\Services\UserEnvironmentService\Interfaces\UserEnvironmentServiceInterface;
use App\Modules\Users\User\Http\Requests\IndexRequest;
use App\Modules\Challenges\Http\Requests\ReportRequest;
use App\Modules\Companies\Models\Company;
use App\Services\ResponseBuilder\ApiCode;
use App\Services\ResponseBuilder\CustomResponseBuilder;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\Request;

class ChallengeController extends Controller
{
    /**
     * @param UserEnvironmentServiceInterface $userEnvironmentService
     * @param IndexRequest $request
     * @return Response
     */
    public function index(UserEnvironmentServiceInterface $userEnvironmentService, IndexRequest $request): Response
    {
        $search = $request->get('search');
        $limit = 0;//(int)$request->get('limit');
        $challenges = $userEnvironmentService->getChallengesList($search, $limit);

        foreach ($challenges as $challenge) {
            $challenge = ChallengeService::getUserProofStatus($challenge);
            unset($challenge->participants);
        }

        return CustomResponseBuilder::success($challenges);
    }

    /**
     * @param Challenge $challenge
     * @return Response
     */
    public function show(Challenge $challenge): Response
    {
        $challenge->completed_count = $challenge->getCompletedProofsCount();
        $challenge = ChallengeService::getUserProofStatus($challenge);

        return CustomResponseBuilder::success($challenge->load(['participants']));
    }

    public function getQuestions(Challenge $challenge)
    {
        $isExistProof = Proof::where('challenge_id', $challenge->id)
            ->where('user_id', Auth::id())
            ->exists();

        if ($isExistProof) {
            return CustomResponseBuilder::error(ApiCode::USER_HAS_PENDING_OR_ACCEPTED_PROOF);
        }

        if ($challenge->proof_type !== ProofTypeEnum::QUESTIONS) {
            return CustomResponseBuilder::success(null);
        }

        $questions = $challenge->questions;
        $answers = Answer::whereIn('question_id', $questions->pluck('id')->toArray())->get();

        $response = [
            'questions' => $questions->toArray(),
            'answers' => $answers->toArray(),
        ];

        foreach($response['answers'] as $k => $v) {
            $text = array_map('trim', explode(',', $v['text']));
            $text = implode(',', $text);
            $response['answers'][$k]['text'] = $text;
        }

        return CustomResponseBuilder::success($response);
    }

    public function getMultipleQuestions(Challenge $challenge)
    {
        $isExistProof = Proof::where('challenge_id', $challenge->id)
            ->where('user_id', Auth::id())
            ->exists();

        if ($isExistProof) {
            return CustomResponseBuilder::error(ApiCode::USER_HAS_PENDING_OR_ACCEPTED_PROOF);
        }

        if (!in_array($challenge->proof_type, [ProofTypeEnum::TESTS, ProofTypeEnum::TRUE_FALSE])) {
            return CustomResponseBuilder::success([]);
        }

        $questions = $challenge->questions;
        $answers = Answer::whereIn('question_id', $questions->pluck('id')->toArray())->get();

        $response = [
            'questions' => $questions->toArray(),
            'answers' => $answers->toArray(),
        ];

        return CustomResponseBuilder::success($response);
    }

    public function report(ReportRequest $request)
    {
        $challenge_id = $request->get('challenge_id');
        $reason = $request->get('reason');

        $user = Auth::user();

        app(Reports::class)->updateOrCreate(
            [
                'challenge_id' => $challenge_id,
                'user_id' => $user->id,
            ],
            [
                'challenge_id' => $challenge_id,
                'user_id' => $user->id,
                'reason' => $reason
            ]
        );

        return CustomResponseBuilder::success();
    }

    public function verifyCode(Request $request)
    {
        $code = $request->get('code');

        if($code) {
            $isExist = Company::where('join_code', $code)->first();

            if(!$isExist) {
                return CustomResponseBuilder::error(ApiCode::NO_SUCH_ITEM);
            }
        }

        return CustomResponseBuilder::success();
    }
}
