<?php

namespace App\Modules\Challenges\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Modules\Challenges\Datatables\ReportsDataTable;
use App\Modules\Challenges\Models\Reports;

class ReportsController extends Controller
{
    /**
     * @param ResultDataTable $dataTable
     * @return mixed
     */
    public function index(ReportsDataTable $dataTable)
    {
        return $dataTable->render('reports.index');
    }

    public function destroy(int $id)
    {
        $report = Reports::findOrFail($id);

        if (null === $report) {
            flash('No such record');
            return redirect()->route('reports.index');
        }

        $report->delete();

        flash('Record has been deleted successfully');

        return redirect()->route('reports.index');
    }
}