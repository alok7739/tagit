<?php
/**
 * Created by Maksym Ignatchenko, Appus Studio LP on 17.09.19
 *
 */

namespace App\Modules\Challenges\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Modules\Challenges\Models\Challenge;
use App\Modules\Notifications\DTO\EntityRelatedManualMessage;
use App\Modules\Notifications\Events\MessageSentEvent;
use App\Modules\Notifications\Requests\ManualMessageRequest;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

class NotificationController extends Controller
{
    /**
     * @param Challenge $challenge
     * @return View
     */
    public function showGroupOfUsersNotificationForm(Challenge $challenge): View
    {
        $dto = new EntityRelatedManualMessage('challenge.users.notification-send', 'Send messages to users related to the challenge', $challenge);
        return view('message', ['dto' => $dto]);
    }

    public function sendGroupNotification(ManualMessageRequest $request, Challenge $challenge): RedirectResponse
    {
        $users = $challenge->users;
        event(new MessageSentEvent($users, $request->message, $request->title));
        flash('Message has been sent');
        return redirect()->route('challenge.index');
    }
}
