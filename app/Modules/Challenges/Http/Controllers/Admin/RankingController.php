<?php
/**
 * Created by Vitaliy Shabunin, Appus Studio LP on 02.06.2020
 */

namespace App\Modules\Challenges\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Modules\Challenges\Datatables\RankingDataTable;
use App\Modules\Companies\Models\Company;
use App\Modules\Users\User\Models\User;
use App\Modules\Companies\Enums\CompanyTypeEnum;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RankingController extends Controller
{

    /**
     * @param RankingDataTable $dataTable
     * @param Request $request
     * @return mixed
     */
    public function index(RankingDataTable $dataTable, Request $request)
    {
        $companyIds = Company::select(['id', 'name'])->where('type', CompanyTypeEnum::COMMERCIAL)->get()->pluck('name', 'id')->toArray();
        $companyId = Auth::user()->getCompanyId($request);

        return $dataTable->setCompanyId($companyId)
            ->render('ranking.index', compact('companyIds', 'companyId'));
    }

    public function live(Request $request)
    {
        $companyId = Auth::user()->getCompanyId($request);
        $company = Company::where('id', $companyId)->first();

        return view('ranking.live', compact('company'));
    }

    public function liveAjax($company)
    {
        $company = Company::where('join_code', $company)->first();

        if($company) {
            $userModel = new User();
            $userModel->setCompanyIdAttribute($company->id);
            $rating = $userModel->getLiveRating()->get();

            return view('ranking.live-ajax', compact('rating', 'company'));
        }
    }

    /**
     * @param RankingDataTable $dataTable
     * @param Request $request
     * @return mixed
     */
    public function download(RankingDataTable $dataTable, Request $request)
    {
        $companyId = Auth::user()->getCompanyId($request);

        return $dataTable->setFilter($request->get('filter', []))
            ->setCompanyId($companyId)
            ->setOrder($request->get('order_field'), $request->get('order_dir'))
            ->csv();
    }

}
