<?php
/**
 * Created by Maksym Ignatchenko, Appus Studio LP on 25.02.19
 *
 */

namespace App\Modules\Challenges\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Modules\Challenges\Datatables\ArchivedChallengesDataTable;
use App\Modules\Challenges\Datatables\ChallengeDataTable;
use App\Modules\Challenges\DTO\CreateChallengeDTO;
use App\Modules\Challenges\DTO\EditChallengeDto;
use App\Modules\Challenges\DTO\ShowChallengeDTO;
use App\Modules\Challenges\Enums\ChallengeStatusEnum;
use App\Modules\Challenges\Enums\CountryEnum;
use App\Modules\Challenges\Enums\ProofTypeEnum;
use App\Modules\Challenges\Enums\VideoLengthEnum;
use App\Modules\Challenges\Http\Requests\Admin\CreateChallengeRequest;
use App\Modules\Challenges\Http\Requests\Admin\StoreChallengeRequest;
use App\Modules\Challenges\Http\Requests\Admin\UpdateChallengeRequest;
use App\Modules\Challenges\Models\Answer;
use App\Modules\Challenges\Models\Challenge;
use App\Modules\Challenges\Models\Question;
use App\Modules\Challenges\Services\ChallengeService;
use App\Modules\Companies\Models\Company;
use App\Modules\Library\Models\Library;
use App\Modules\Library\Models\Question As LibraryQuestion;
use App\Modules\Library\Models\Answer As LibraryAnswer;
use App\Modules\Onboarding\Models\CompanyPackages;
use App\Modules\Companies\Enums\CompanyTypeEnum;
use App\Services\CountriesRepository;
use Illuminate\Http\Request;
use Laracasts\Flash\Flash;
use Illuminate\Support\Str;

class ChallengeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param ChallengeDataTable $dataTable
     * @return mixed
     */
    public function index(ChallengeDataTable $dataTable, Request $request)
    {
        if(\Auth::user()->company_id !== null) {
            return redirect()->route('company.show', ['company' => \Auth::user()->company_id]);
        }

        $countries = CountriesRepository::make()->all();
        $companies = Company::select(['id', 'name'])->where('type', CompanyTypeEnum::COMMERCIAL)->get();

        if ($request->country) {
            $dataTable->setCountry($request->country);
        } elseif ($request->company_id) {
            $dataTable->setCompanyId($request->company_id);
        }

        return $dataTable->render('challenge.index', compact('countries', 'companies'));
    }

    /**
     * Display the specified resource.
     *
     * @param Challenge $challenge
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Challenge $challenge)
    {
        $dto = new ShowChallengeDTO($challenge);
        return view('challenge.show', ['dto' => $dto, 'challenge' => $challenge]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param CreateChallengeRequest $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(CreateChallengeRequest $request)
    {
        $locking = false;

        if(\Auth::user()->company_id !== null) {
            $company = Company::find(\Auth::user()->company_id);

            if(\Auth::user()->status != '1' || !$company->canCreateChallenge()) {
                Flash::error('Can not create challenge, limit exceeded.');
                return redirect()->route('company.show', ['company' => \Auth::user()->company_id]);
            }

            $package = CompanyPackages::find($company->package_id);
            $locking = $package && $package->locking ? true : false;
        }

        $libraryId = $request->query('libraryId');
        $challengeId = $request->query('challengeId');
        $challenge = new Challenge;
        $qids = $questions = $answers = $test_answers = [];

        if($libraryId) {
            $library = Library::where('status', '1')->where('id', $libraryId)->first();

            if($library) {
                $challenge->fill($library->toArray());

                if(in_array($library->proof_type,  ['questions', 'tests', 'true_false'])) {
                    $cnt = 0;

                    $ques = LibraryQuestion::where('library_id', $library->id)->get();

                    foreach($ques as $q) {
                        $qids[$cnt] = $q->id;
                        $questions[$cnt] = $q->text;

                        $ans = LibraryAnswer::where('question_id', $q->id)->get();

                        foreach($ans as $a) {
                            if($library->proof_type == 'tests') {
                                $test_answers[$cnt][] = $a ? $a->text : '';
                            }
                            if($library->proof_type == 'true_false') {
                                if(empty($answers[$cnt]) && $a) {
                                    $answers[$cnt] = $a->text;
                                }
                            }
                            else {
                                $answers[$cnt] = $a ? $a->text : '';
                            }
                        }

                        $cnt++;
                    }
                }
            }
        }
        elseif($challengeId) {
            $challenge = Challenge::find($challengeId);

            if(in_array($challenge->proof_type,  ['questions', 'tests', 'true_false'])) {
                $cnt = 0;

                $ques = Question::where('challenge_id', $challenge->id)->get();

                foreach($ques as $q) {
                    $qids[$cnt] = $q->id;
                    $questions[$cnt] = $q->text;

                    $ans = Answer::where('question_id', $q->id)->get();

                    foreach($ans as $a) {
                        if($challenge->proof_type == 'tests') {
                            $test_answers[$cnt][] = $a ? $a->text : '';
                        }
                        if($challenge->proof_type == 'true_false') {
                            if(empty($answers[$cnt]) && $a) {
                                $answers[$cnt] = $a->text;
                            }
                        }
                        else {
                            $answers[$cnt] = $a ? $a->text : '';
                        }
                    }

                    $cnt++;
                }
            }
        }

        if($challenge['items_count_in_proof'] > 1) {
            $challenge['proof_type'] = $challenge['proof_type'] == ProofTypeEnum::MULTIPLE_PHOTOS ? ProofTypeEnum::PHOTO : $challenge['proof_type'];
            $challenge['proof_type'] = $challenge['proof_type'] == ProofTypeEnum::MULTIPLE_VIDEOS ? ProofTypeEnum::VIDEO : $challenge['proof_type'];
            $challenge['proof_type'] = $challenge['proof_type'] == ProofTypeEnum::MULTIPLE_SCREENSHOTS ? ProofTypeEnum::SCREENSHOT : $challenge['proof_type'];
        }

        $challenge->company_id = $request->query('companyId');
        $dto = $this->getEditDto($challenge);

        return view('challenge.create')
            ->with([
                'dto' => $dto,
                'proofCount' => ChallengeService::getProofItemsCount(),
                'qrString' => Str::random(),
                'locking' => $locking,
                'companyId' => $request->query('companyId'),
                'qids' => $qids,
                'questions' => $questions,
                'answers' => $answers,
                'test_answers' => $test_answers,
                'lat_lng' => explode(',', $challenge->location_latlng),
            ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreChallengeRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreChallengeRequest $request)
    {
        $inputs = $request->all();

        if($inputs['company_id']) {
            $inputs['country'] = $inputs['city'] = '';

            $company = Company::find($inputs['company_id']);

            if(!$company->canCreateChallenge()) {
                Flash::error('Can not create challenge for this company, challenge limit exceeded or is expired.');
                return redirect()->route('challenge.create')->withInput($inputs);
            }

            if(!$company->isChallengeTypeAllowed($inputs['proof_type'])) {
                Flash::error('Selected proof type is not allowed for this company.');
                return redirect()->route('challenge.create')->withInput($inputs);
            }
        }

        if($inputs['items_count_in_proof'] > 1) {
            $inputs['proof_type'] = $inputs['proof_type'] == ProofTypeEnum::PHOTO ? ProofTypeEnum::MULTIPLE_PHOTOS : $inputs['proof_type'];
            $inputs['proof_type'] = $inputs['proof_type'] == ProofTypeEnum::VIDEO ? ProofTypeEnum::MULTIPLE_VIDEOS : $inputs['proof_type'];
            $inputs['proof_type'] = $inputs['proof_type'] == ProofTypeEnum::SCREENSHOT ? ProofTypeEnum::MULTIPLE_SCREENSHOTS : $inputs['proof_type'];
        }

        $challenge = app()[Challenge::class];
        $challenge->fill($inputs);
        $challenge->save();

        if ($request->proof_type === ProofTypeEnum::QUESTIONS) {
            $challenge->saveQuestions($request->only(['questions', 'answers']));
        }
        elseif ($request->proof_type === ProofTypeEnum::TESTS) {
            $challenge->saveTests($request->only(['questions', 'test_answers']));
        }
        elseif ($request->proof_type === ProofTypeEnum::TRUE_FALSE) {
            $challenge->saveTrueFalse($request->only(['questions', 'answers']));
        }

        Flash::success('Challenge created successfully.');

        $company_id = $challenge->company_id ? $challenge->company_id : request()->session()->get('company_id', 0);
        $company_id = $company_id ? $company_id : (\Auth::user()->company_id !== null ? \Auth::user()->company_id : 0);

        if($company_id) {
            return redirect()->route('company.show', ['company' => $company_id]);
        } else {
            return redirect()->route('challenge.index');
        }
    }

    /**
     * @param Challenge $challenge
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Challenge $challenge)
    {
        if(\Auth::user()->company_id !== null && \Auth::user()->status != '1') {
            return redirect()->route('company.show', ['company' => \Auth::user()->company_id]);
        }

        if($challenge['items_count_in_proof'] > 1) {
            $challenge['proof_type'] = $challenge['proof_type'] == ProofTypeEnum::MULTIPLE_PHOTOS ? ProofTypeEnum::PHOTO : $challenge['proof_type'];
            $challenge['proof_type'] = $challenge['proof_type'] == ProofTypeEnum::MULTIPLE_VIDEOS ? ProofTypeEnum::VIDEO : $challenge['proof_type'];
            $challenge['proof_type'] = $challenge['proof_type'] == ProofTypeEnum::MULTIPLE_SCREENSHOTS ? ProofTypeEnum::SCREENSHOT : $challenge['proof_type'];
        }

        $dto = $this->getEditDto($challenge);

        if(\Auth::user()->company_id !== null && $challenge->company_id != \Auth::user()->company_id) {
            return redirect()->route('company.show', ['company' => \Auth::user()->company_id]);
        }

        $qids = $questions = $answers = $test_answers = [];

        if(in_array($challenge->proof_type,  ['questions', 'tests', 'true_false'])) {
            $cnt = 0;

            $ques = Question::where('challenge_id', $challenge->id)->get();

            foreach($ques as $q) {
                $qids[$cnt] = $q->id;
                $questions[$cnt] = $q->text;

                $ans = Answer::where('question_id', $q->id)->get();

                foreach($ans as $a) {
                    if($challenge->proof_type == 'tests') {
                        $test_answers[$cnt][] = $a ? $a->text : '';
                    }
                    if($challenge->proof_type == 'true_false') {
                        if(empty($answers[$cnt]) && $a) {
                            $answers[$cnt] = $a->text;
                        }
                    }
                    else {
                        $answers[$cnt] = $a ? $a->text : '';
                    }
                }

                $cnt++;
            }
        }

        $locking = false;

        if(\Auth::user()->company_id !== null) {
            $company = Company::find(\Auth::user()->company_id);
            $package = CompanyPackages::find($company->package_id);
            $locking = $package && $package->locking ? true : false;
        }

        return view('challenge.edit')
            ->with([
                'dto' => $dto,
                'proofCount' => ChallengeService::getProofItemsCount(),
                'qids' => $qids,
                'questions' => $questions,
                'answers' => $answers,
                'test_answers' => $test_answers,
                'lat_lng' => explode(',', $challenge->location_latlng),
                'locking' => $locking,
                'companyId' => $challenge->company_id
            ]);
    }

    /**
     * @param UpdateChallengeRequest $request
     * @param Challenge $challenge
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateChallengeRequest $request, Challenge $challenge)
    {
        if(\Auth::user()->company_id !== null && $challenge->company_id != \Auth::user()->company_id) {
            return redirect()->route('company.show', ['company' => \Auth::user()->company_id]);
        }

        $inputs = $request->all();

        if($inputs['items_count_in_proof'] > 1) {
            $inputs['proof_type'] = $inputs['proof_type'] == ProofTypeEnum::PHOTO ? ProofTypeEnum::MULTIPLE_PHOTOS : $inputs['proof_type'];
            $inputs['proof_type'] = $inputs['proof_type'] == ProofTypeEnum::VIDEO ? ProofTypeEnum::MULTIPLE_VIDEOS : $inputs['proof_type'];
            $inputs['proof_type'] = $inputs['proof_type'] == ProofTypeEnum::SCREENSHOT ? ProofTypeEnum::MULTIPLE_SCREENSHOTS : $inputs['proof_type'];
        }

        if($inputs['company_id']) {
            $inputs['country'] = $inputs['city'] = '';
        }

        $challenge->fill($inputs);
        $challenge->save();

        $current_qids = $request->get('qids') ? explode(',', $request->get('qids')) : [];
        $new_qids = [];

        if ($request->proof_type === ProofTypeEnum::QUESTIONS || $request->proof_type === ProofTypeEnum::TESTS || $request->proof_type === ProofTypeEnum::TRUE_FALSE)
        {
            $questions = $request->get('questions', []);
            $answers = $request->get('answers', []);
            $test_answers = $request->get('test_answers', []);

            foreach($questions as $k => $v)
            {
                $qid = 0;

                if(isset($current_qids[$k]))
                {
                    $ques = Question::where('id', $current_qids[$k])->update(['text' => $v]);
                    $qid = $current_qids[$k];
                }
                else
                {
                    $ques = Question::create([
                        'challenge_id' => $challenge->id,
                        'text' => $v,
                    ]);

                    $qid = $ques->id;
                }

                $new_qids[] = $qid;

                if (in_array($request->proof_type, [ProofTypeEnum::QUESTIONS, ProofTypeEnum::TRUE_FALSE])) {
                    $new_ans = $answers;
                }
                elseif($request->proof_type === ProofTypeEnum::TESTS) {
                    $new_ans = $test_answers;
                }

                if(isset($new_ans[$k]))
                {
                    Answer::where('question_id', $qid)->delete();

                    if(!is_array($new_ans[$k])) {
                        $new_ans[$k] = array($new_ans[$k]);
                    }

                    foreach($new_ans[$k] as $k2 => $v2)
                    {
                        $ans_type = $k2 == '0' ? Answer::CORRECT : Answer::INCORRECT;

                        Answer::create([
                            'question_id' => $qid,
                            'type' => $ans_type,
                            'text' => $v2
                        ]);

                        if($request->proof_type === ProofTypeEnum::TRUE_FALSE) {
                            Answer::create([
                                'question_id' => $qid,
                                'type' => Answer::INCORRECT,
                                'text' => $v2 == 'True' ? 'False' : 'True'
                            ]);
                        }
                    }
                }
            }
        }

        if($current_qids)
        {
            foreach($current_qids as $qid)
            {
                if(!in_array($qid, $new_qids))
                {
                    Answer::where('question_id', $qid)->delete();
                    Question::where('id', $qid)->delete();
                }
            }
        }

        Flash::success('Challenge edited successfully.');

        $company_id = $challenge->company_id ? $challenge->company_id : request()->session()->get('company_id', 0);
        $company_id = $company_id ? $company_id : (\Auth::user()->company_id !== null ? \Auth::user()->company_id : 0);

        if($company_id) {
            return redirect()->route('company.show', ['company' => $company_id]);
        } else {
            return redirect()->route('challenge.index');
        }
    }

    public function archive(Challenge $challenge)
    {
        $challenge->update(['status' => ChallengeStatusEnum::ARCHIVE]);

        Flash::success('Challenge archived successfully.');

        $company_id = $challenge->company_id ? $challenge->company_id : request()->session()->get('company_id', 0);
        $company_id = $company_id ? $company_id : (\Auth::user()->company_id !== null ? \Auth::user()->company_id : 0);

        if($company_id) {
            return redirect()->route('company.show', ['company' => $company_id]);
        } else {
            return redirect()->route('challenge.index');
        }
    }

    public function getArchived(ArchivedChallengesDataTable $dataTable, Request $request)
    {
        $countries = CountriesRepository::make()->all();
        $companies = Company::select(['id', 'name'])->where('type', CompanyTypeEnum::COMMERCIAL)->get();

        if ($request->country) {
            $dataTable->setCountry($request->country);
        } elseif ($request->company_id) {
            $dataTable->setCompanyId($request->company_id);
        }

        return $dataTable->render('challenge.index', compact('countries', 'companies'));
        // return $dataTable->render('challenge.index');
    }

    /**
     * @param array $options
     * @return CreateChallengeDTO
     */
    private function getCreateDto(array $options = []) : CreateChallengeDTO
    {
        if (auth()->user()->isAdmin()) {
            $companies = Company::all()->pluck('name', 'id')->toArray();
            $selectedCompanyId = $options['companyId'];
        } else {
            $companies = Company::where('id', auth()->user()->company_id)->pluck('name', 'id')->toArray();
            $selectedCompanyId = array_key_first($companies);
        }

        $countries = CountriesRepository::make()->all()->toArray();
        $proofTypes = ProofTypeEnum::getNames();
        $videoLengthTypes = array_combine(VideoLengthEnum::getAll(), VideoLengthEnum::getAll());

        return new CreateChallengeDTO($companies, $countries, $proofTypes, $videoLengthTypes, $selectedCompanyId);
    }

    /**
     * @param Challenge $challenge
     * @return CreateChallengeDTO
     */
    private function getEditDto(Challenge $challenge) : CreateChallengeDTO
    {
        $companies = Company::select(['id', 'name'])->where('type', CompanyTypeEnum::COMMERCIAL)->get()->pluck('name', 'id')->toArray();
        $countries = CountriesRepository::make()->all()->toArray();
        $proofTypes = ProofTypeEnum::getNames();
        $videoLengthTypes = array_combine(VideoLengthEnum::getAll(), VideoLengthEnum::getAll());
        return new EditChallengeDto($companies, $countries, $proofTypes, $videoLengthTypes, $challenge);
    }

    public function getAllowedFeatures(Request $request)
    {
        $id = $request->get('id');
        $company = $request->get('company');
        $country = $request->get('country');
        $current = $request->get('current_type');
        $locking = true;
        $types   = ProofTypeEnum::getNames();

        $cids = Challenge::where('dependent_challenge', $id)->get();
        $cids = $cids ? array_pluck($cids, 'id', 'id') : [0];

        if($company) {
            $company = Company::find($company);
            $package = CompanyPackages::find($company->package_id);

            if($package) {
                $locking = $package->locking ? true : false;

                $current_lbl = $current ? $types[$current] : '';

                if(!$package->true_false) {
                   unset($types['true_false']);
                }

                if(!$package->sc_ques) {
                    unset($types['questions']);
                }

                if(!$package->mc_ques) {
                    unset($types['tests']);
                }

                if(!$package->checkin) {
                    unset($types['checkin']);
                }

                if(!$package->qr_code) {
                    unset($types['qrcode']);
                }

                if(!$package->images) {
                    unset($types['photo']);
                }

                if(!$package->screenshots) {
                    unset($types['screenshot']);
                }

                if(!$package->videos) {
                    unset($types['video']);
                }

                if($current) {
                    $types[$current] = $current_lbl;
                }
            }

            if($locking) {
                $dependent = Challenge::where('company_id', $company->id)
                                        ->whereIn('status', ['active', 'waiting'])
                                        ->where('id', '!=', $id)
                                        ->whereNotIn('id', $cids)
                                        ->get();
            } else {
                $dependent = [];
            }
        } else {
            $dependent = Challenge::where('country', $country)
                                        ->whereIn('status', ['active', 'waiting'])
                                        ->where('id', '!=', $id)
                                        ->whereNotIn('id', $cids)
                                        ->get();
        }

        $dependent = array_pluck($dependent, 'name', 'id');

        $result = [
            'types'     => $types,
            'dependent' => $dependent,
            'locking'   => $locking
        ];

        echo json_encode($result);
        exit;
    }

    public function addToLibrary(Challenge $challenge)
    {
        $challenge->update(['is_library' => '1']);

        Flash::success('Challenge added to library successfully.');

        $company_id = $challenge->company_id ? $challenge->company_id : request()->session()->get('company_id', 0);
        $company_id = $company_id ? $company_id : (\Auth::user()->company_id !== null ? \Auth::user()->company_id : 0);

        if($company_id) {
            return redirect()->route('company.show', ['company' => $company_id]);
        } else {
            return redirect()->route('challenge.index');
        }
    }

    public function removeFromLibrary(Challenge $challenge)
    {
        $challenge->update(['is_library' => '0']);

        Flash::success('Challenge removed from library successfully.');

        return redirect()->route('library.my', ['companyId' => $challenge->company_id]);
    }
}
