<?php
/**
 * Created by Maksym Ignatchenko, Appus Studio LP on 26.02.19
 *
 */

namespace App\Modules\Challenges\Http\Requests\Admin;

use App\Modules\Challenges\Enums\CountryEnum;
use App\Modules\Challenges\Enums\ProofTypeEnum;
use App\Modules\Challenges\Enums\VideoLengthEnum;
use App\Modules\Challenges\Rules\ChallengeStartDateTimeRule;
use App\Modules\Challenges\Rules\MinProofItemsCount;
use App\Modules\Challenges\Rules\ParticipantLimitRule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreChallengeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'company_id' => 'nullable|exists:companies,id',
            'name' => 'required|string|max:50',
            'name_ar' => 'required|string|max:50',
            'image' => 'required|string|max:255',
            'description' => 'required|string|max:1000',
            'description_ar' => 'required|string|max:1000',
            'country' => ['nullable', 'string', 'max:100'],
            'link' => 'nullable|url|max:255',
            'city' => 'nullable|string|max:100',
            'participants_limit' => ['int', new ParticipantLimitRule],
            'proof_type' => ['required', 'string', 'max:100', Rule::in(ProofTypeEnum::getAll())],
            'items_count_in_proof' => 'required|integer|min:' . MinProofItemsCount::get($this) . '|max:' . config('custom.max_items_in_proof'),
            'video_duration' => ['nullable', 'required_if:proof_type,video', 'integer', Rule::in(VideoLengthEnum::getAll())],
            'start_date' => ['required', 'date_format:Y-m-d H:i', new ChallengeStartDateTimeRule],
            'end_date' => 'required|date_format:Y-m-d H:i|after:start_date',
            'questions.*' => ['required', 'string', 'max:300'],
            'test_answers.*.*' => ['required', 'string', 'max:86'],
            'answers.*' => ['required', 'string', 'max:200'],
            'qr_string' => 'nullable|string|max:50',
            'qr_url' => 'nullable|url|max:255',
            'location' => 'nullable|string|max:255',
            'location_latlng' => 'nullable|string|max:100',
            'location_radius' => 'nullable|integer|min:1|max:1000',
        ];
    }

    public function messages()
    {
        return [
            'name_ar.required' => 'The arabic name field is required.',
            'name_ar.max' => 'The arabic name may not be greater than :max characters.',

            'description_ar.required' => 'The arabic description field is required.',
            'description_ar.max' => 'The arabic description may not be greater than :max characters.'
        ];
    }
}
