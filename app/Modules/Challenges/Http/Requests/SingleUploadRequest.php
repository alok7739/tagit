<?php

namespace App\Modules\Challenges\Http\Requests;

use App\Services\ResponseBuilder\ValidationErrorsApiMessagesTrait;
use Illuminate\Foundation\Http\FormRequest;

class SingleUploadRequest extends FormRequest
{
    use ValidationErrorsApiMessagesTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $challenge = $this->route('challenge');
        return [
            'item' => ['required', 'file', 'mimes:' . implode(',', $challenge->getAvailableProofItemsMimeType()), 'max:' . $challenge->getMaxSizeProofItemsMimeType()],
        ];
    }
}
