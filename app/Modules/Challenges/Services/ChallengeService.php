<?php
/**
 * Created by Vitaliy Shabunin, Appus Studio LP on 19.06.2020
 */

namespace App\Modules\Challenges\Services;

use App\Modules\Challenges\Enums\ProofStatusEnum;
use App\Modules\Challenges\Models\Challenge;
use Illuminate\Support\Facades\Auth;

class ChallengeService
{

    const PROOF_ADDING_LIMIT = 3;

    /**
     * @param Challenge $challenge
     * @return Challenge
     */
    public static function getUserProofStatus(Challenge $challenge): Challenge
    {
        if ($challenge->participants->count() > 0 && $challenge->participants->where('id', Auth::user()->id)->count() > 0) {
            $status = $challenge->status == 'waiting' ? 'joined' : 'submission';
        }

        if ($challenge->my_proof) {
            $status = ProofStatusEnum::getName($challenge->my_proof->status);
        }

        $challenge->user_proof_status = $status ?? null;

        return $challenge;
    }

    /**
     * @return array|int[]
     */
    public static function getProofItemsCount(): array
    {
        return [
            1 => 1,
            2 => 2,
            3 => 3,
            4 => 4,
            5 => 5,
        ];
    }

}
