<?php
/**
 * Created by Vitaliy Shabunin, Appus Studio LP on 20.06.2020
 */

namespace App\Modules\Challenges\Services\Counter;

abstract class CounterAbstract implements CounterInterface
{

    protected $key;

    /**
     * CounterAbstract constructor.
     *
     * @param string $key
     */
    public function __construct(string $key)
    {
        $this->key = $key;
    }

    /**
     * @param int $limit
     * @return bool
     */
    public function checkLimit(int $limit): bool
    {
        $currentCount = $this->get();

        return $currentCount >= $limit;
    }

    public function increment(): void
    {
        $currentCount = $this->get();
        $this->set(++$currentCount);
    }

    /**
     * @return int
     */
    abstract public function get(): int;

    /**
     * @param int $count
     */
    abstract public function set(int $count): void;

}
