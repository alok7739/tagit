<?php
/**
 * Created by Vitaliy Shabunin, Appus Studio LP on 20.06.2020
 */

namespace App\Modules\Challenges\Services\Counter;

use Illuminate\Support\Facades\Cache;

class CacheCounter extends CounterAbstract
{

    /**
     * @return int
     */
    public function get(): int
    {
        return Cache::get($this->key, 0);
    }

    /**
     * @param int $count
     */
    public function set(int $count): void
    {
        Cache::forever($this->key, $count);
    }

}
