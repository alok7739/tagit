<?php
/**
 * Created by Vitaliy Shabunin, Appus Studio LP on 20.06.2020
 */

namespace App\Modules\Challenges\Services\Counter;

interface CounterInterface
{

    /**
     * @param int $limit
     * @return bool
     */
    public function checkLimit(int $limit): bool;

    public function increment(): void;

}
