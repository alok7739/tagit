<?php
/**
 * Created by Maksym Ignatchenko, Appus Studio LP on 12.04.19
 *
 */

namespace App\Modules\Challenges\Jobs;

use App\Modules\Challenges\Models\Challenge;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class HandleChallengesStatusJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public function handle() : void
    {
        $challenge = app()[Challenge::class];
        $challenge->handleStatuses();
    }
}
