<?php
/**
 * Created by Maksym Ignatchenko, Appus Studio LP on 25.02.19
 *
 */

Route::group(['middleware' => 'auth:admin'], function () {
    Route::post('challenge', 'ChallengeController@store')->name('challenge.store')->middleware('can:create,App\Modules\Challenges\Models\Challenge');
    Route::get('challenge', 'ChallengeController@index')->name('challenge.index');
    Route::get('challenge/create', 'ChallengeController@create')->name('challenge.create')->middleware('can:create,App\Modules\Challenges\Models\Challenge');
    Route::get('challenge/{challenge}', 'ChallengeController@show')->name('challenge.show')->middleware('can:view,challenge');
    Route::get('challenge/{challenge}/edit', 'ChallengeController@edit')->name('challenge.edit')->middleware('can:edit,challenge');
    Route::patch('challenge/{challenge}/archive', 'ChallengeController@archive')->name('challenge.archive')->middleware('can:edit,challenge');
    Route::get('challenges/archived', 'ChallengeController@getArchived')->name('challenges.archived');
    Route::post('challenges/{challenge}/add-to-library', 'ChallengeController@addToLibrary')->name('challenge.add-to-library');
    Route::post('challenges/{challenge}/remove-from-library', 'ChallengeController@removeFromLibrary')->name('challenge.remove-from-library');
    Route::post('get-allowed-features', 'ChallengeController@getAllowedFeatures')->name('get-allowed-features');

    Route::match(array('PATCH', 'PUT'), "challenge/{challenge}", array(
        'uses' => 'ChallengeController@update',
        'as' => 'challenge.update'
    ))->middleware('can:edit,challenge');

    Route::get('challenge/{challenge}/result', 'ResultController@index')->name('result')->middleware('can:view,challenge');

    Route::get('challenge/{challenge}/proof', 'ProofController@index')->name('challenge.proof.index')->middleware('can:view,challenge');
    Route::get('challenge/{challenge}/proof/{proof}', 'ProofController@show')->name('challenge.proof.show')->middleware('can:view,challenge');
    Route::match(array('PATCH', 'PUT'), "challenge/{challenge}/proof/{proof}", array(
        'uses' => 'ProofController@update',
        'as' => 'challenge.proof.update'
    ))->middleware('can:edit,App\Modules\Challenges\Models\Challenge');


    Route::get('challenge/{challenge}/check', 'ProofController@check')->name('check')->middleware('can:view,challenge');
    Route::get('challenge/{challenge}/users/notification', 'NotificationController@showGroupOfUsersNotificationForm')->name('challenge.users.notification-form')->middleware('can:notification,App\Modules\Challenges\Models\Challenge');
    Route::post('challenge/{challenge}/users/notification', 'NotificationController@sendGroupNotification')->name('challenge.users.notification-send')->middleware('can:notification,App\Modules\Challenges\Models\Challenge');

    Route::get('ranking', 'RankingController@index')
        ->name('ranking.index');

    Route::get('ranking/live', 'RankingController@live')
        ->name('ranking.live');

    Route::get('ranking/download', 'RankingController@download')
        ->name('ranking.download');

    Route::get('reports', 'ReportsController@index')->name('reports.index');
    Route::delete('reports/{id}', 'ReportsController@destroy')->name('reports.destroy');

    Route::get('qr-code/{code}', function ($code) {
        $image = QrCode::format('png')
                        ->size(500)
                        ->errorCorrection('H')
                        ->generate($code);

        return response($image)->header('Content-type','image/png');
    });
});

Route::get('live-ranking/{company}', 'RankingController@liveAjax');
