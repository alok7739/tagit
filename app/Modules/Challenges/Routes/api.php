<?php
/**
 * Created by Maksym Ignatchenko, Appus Studio LP on 25.02.19
 *
 */

Route::group(['middleware' => ['api', 'auth:api', 'auth.active']], function () {
    Route::get('challenge/', 'ChallengeController@index');
    Route::post('challenge/report', 'ChallengeController@report');
    Route::get('challenge/{challenge}', 'ChallengeController@show');
    Route::get('challenge/{challenge}/questions', 'ChallengeController@getQuestions');
    Route::get('challenge/{challenge}/multiple-choice-questions', 'ChallengeController@getMultipleQuestions');
    Route::get('challenge/{challenge}/result', 'ResultController@index');

    Route::get('challenge/{challenge}/participation', 'ChallengeParticipationController@index');
    Route::post('challenge/{challenge}/participation', 'ChallengeParticipationController@store');
    Route::resource('challenge.proof', 'ProofController', [
        'names' => [
            'show' => 'api.proof.show',
            'store' => 'api.proof.store',
            'destroy' => 'api.proof.destroy'
        ]])->only(['show', 'store', 'destroy']);

    Route::post('challenge/{challenge}/questions-proof', 'ProofController@storeQuestionsProof');
    Route::post('challenge/{challenge}/qr-proof', 'ProofController@storeQrProof');
    Route::post('challenge/{challenge}/location-proof', 'ProofController@storeLocationProof');
    Route::post('challenge/{challenge}/upload-proof', 'ProofController@upload');
    Route::post('verify-code', 'ChallengeController@verifyCode');

    Route::post('challenge/{challenge}/like', 'ChallengeLikeController@store');

    Route::get('challenge/{challenge}/comments', 'ChallengeCommentController@index');
    Route::get('challenge/{challenge}/comments/{feed}', 'ChallengeCommentController@index');
    Route::post('challenge/{challenge}/comments', 'ChallengeCommentController@store');
    Route::post('challenge/{challenge}/comments/{feed}', 'ChallengeCommentController@store');
    Route::delete('challenge/{challenge}/comments/{comment}', 'ChallengeCommentController@destroy');
});
