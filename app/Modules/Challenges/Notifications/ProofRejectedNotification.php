<?php
/**
 * Created by Maksym Ignatchenko, Appus Studio LP on 16.09.19
 *
 */

namespace App\Modules\Challenges\Notifications;

use App\Modules\Challenges\Models\Challenge;
use App\Modules\Notifications\Channels\FcmChannel;
use Edujugon\PushNotification\Messages\PushMessage;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Notification;

class ProofRejectedNotification extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * @var Challenge
     */
    private $challenge;

    /**
     * @var string
     */
    private $message;

    /**
     * @var string
     */
    private $title;

    /**
     * @var int
     */
    private $challengeId;

    /**
     * @var string
     */
    private $extraType;

    private $extraVariables;

    public function __construct(Challenge $challenge)
    {
        $this->challenge = $challenge;
        $this->message = 'Your proof related to ' . $this->challenge->name . ' challenge has been rejected. You can try again!';
        $this->title = 'Your proof has been rejected';
        $this->challengeId = $challenge->id;
        $this->extraType = 'proofRejected';
        $this->extraVariables = [
            'challengeName' => $this->challenge->name,
        ];
    }

    /**
     * Get the notification channels.
     *
     * @param  mixed  $notifiable
     * @return array|string
     */
    public function via($notifiable)
    {
        return [
            FcmChannel::class,
            'database',
        ];
    }

    /**
     * @param $notifiable
     * @return PushMessage
     */
    public function toFcm($notifiable)
    {
        $message = new PushMessage($this->message);
        $message->title($this->title);
        $message->extra = [
            'type' => $this->extraType,
            'challengeId' =>$this->challengeId,
            'variables' => $this->extraVariables,
        ];
        return $message;
    }

    /**
     * @param $notifiable
     * @return array
     */
    public function toArray($notifiable): array
    {
        return [
            'title' => $this->title,
            'message' => $this->message,
            'data' => [
                'type' => $this->extraType,
                'challengeId' => $this->challengeId,
                'variables' => $this->extraVariables,
            ],
        ];
    }
}
