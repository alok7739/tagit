<?php
/**
 * Created by Maksym Ignatchenko, Appus Studio LP on 27.06.19
 *
 */

namespace App\Modules\Challenges\Datatables;

use Yajra\DataTables\Contracts\DataTableScope;

class CompanyChallengeDataTableScope implements DataTableScope
{
    /**
     * @var int
     */
    private $companyId;

    /**
     * @var string
     */
    private $status;

    /**
     * CompanyChallengeDataTableScope constructor.
     * @param int $companyId
     * @param string|null $status
     */
    public function __construct(int $companyId, string $status = null)
    {
        $this->companyId = $companyId;
        $this->status = $status;
    }

    /**
     * @param $query
     * @return mixed
     */
    public function apply($query)
    {
        $query = $query->where('company_id', $this->companyId);

        $this->byStatus($query);

        return $query;
    }

    /**
     * @param $query
     * @return mixed
     */
    private function byStatus($query)
    {
        if (null !== $this->status) {
            return $query->where('status', $this->status);
        }

        return $query->where('status', '<>', 'archive');
    }

}
