<?php
/**
 * Created by Vitaliy Shabunin, Appus Studio LP on 02.06.2020
 */

namespace App\Modules\Challenges\Datatables;

use App\Modules\Users\User\DataTables\UserDataTable;
use App\Modules\Users\User\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Yajra\DataTables\Html\Builder as DataTablesBuilder;
use Yajra\DataTables\Services\DataTable;

class RankingDataTable extends UserDataTable
{

    protected $companyId;
    protected $params;
    protected $orderField;
    protected $orderDirection;

    /**
     * @param int|null $companyId
     * @return $this|DataTable
     */
    public function setCompanyId(int $companyId = null): DataTable
    {
        $this->companyId = $companyId;

        return $this;
    }

    /**
     * @param array $params
     * @return $this|DataTable
     */
    public function setFilter(array $params = []): DataTable
    {
        $this->params = $params;

        return $this;
    }

    /**
     * @param int $indexField
     * @param string $direction
     * @return $this|DataTable
     */
    public function setOrder(int $indexField, string $direction = 'desc'): DataTable
    {
        $this->orderField = $this->getColumnName($indexField);
        $this->orderDirection = $direction;

        return $this;
    }

    /**
     * Get query source of dataTable.
     *
     * @param User $model
     * @return Builder
     */
    public function query(User $model): Builder
    {
        $model->setCompanyIdAttribute($this->companyId);

        return $model->getLiveRating()
                    ->order($this->orderField, $this->orderDirection)
                    ->newQuery();

        // return $model->where('company_id', $this->companyId)
        //     ->filter($this->params)
        //     ->order($this->orderField, $this->orderDirection)
        //     ->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return DataTablesBuilder
     */
    public function html(): DataTablesBuilder
    {
        return parent::html()
            ->minifiedAjax(request()->getRequestUri())
            ->removeColumn('action')
            ->parameters(['order' => [[4, 'desc']]]);
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename(): string
    {
        return 'rankingdatatable_' . time();
    }

    /**
     * @param int $index
     * @return string|null
     */
    protected function getColumnName(int $index): ?string
    {
        return $this->getColumns()[$index]['name'] ?? null;
    }

}
