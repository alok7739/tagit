<?php
/**
 * Created by Maksym Ignatchenko, Appus Studio LP on 26.02.19
 *
 */

namespace App\Modules\Challenges\Datatables;

use App\Modules\Challenges\Models\Reports;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Builder as YajraBuilder;
use Yajra\DataTables\Services\DataTable;

class ReportsDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Result from query() method.
     * @return EloquentDataTable
     */
    public function dataTable($query): EloquentDataTable
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable
            ->addColumn('action', 'reports.datatables_actions')
            ->editColumn('challenge.image', function ($query) {
                return ($query->challenge->image ? ("<img height='50' src=" . $query->challenge->image) . " />" : (''));
            })
            ->editColumn('user.full_name', function ($query) {
                return "<a href='" . route('users.show', $query->user->id) . "' target='_blank'>" . $query->user->full_name . "</a>";
            })
            ->editColumn('challenge.name', function ($query) {
                return "<a href='" . route('challenge.show', $query->challenge->id) . "' target='_blank'>" . $query->challenge->name . "</a>";
            })
            ->editColumn('created_at', function($query) {
                return Carbon::parse($query->created_at)->format('Y-m-d H:i:s');
            })
            ->rawColumns(['challenge.image', 'user.full_name', 'challenge.name', 'action']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param Reports $model
     * @return Builder
     */
    public function query(Reports $model): Builder
    {
        return $model->with(['challenge', 'user']);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return YajraBuilder
     */
    public function html(): YajraBuilder
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax(request()->getRequestUri())
            ->addAction(['width' => '10%'])
            ->parameters([
                'dom'     => 'frtip',
                'order'   => [[0, 'desc']],
                'responsive' => true,
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns(): array
    {
        return [
            [
                'name' => 'challenge.image',
                'data' => 'challenge.image',
                'title' => 'Image',
                'width' => '10%',
                'searchable' => false,
                'orderable' => false,
            ],
            [
                'name' => 'challenge.name',
                'data' => 'challenge.name',
                'title' => 'Challenge',
                'width' => '20%',
            ],
            [
                'name' => 'user.full_name',
                'data' => 'user.full_name',
                'title' => 'User',
                'width' => '20%',
            ],
            [
                'name' => 'reason',
                'data' => 'reason',
                'title' => 'Message/Reason',
                'width' => '30%',
            ],
            [
                'name' => 'created_at',
                'data' => 'created_at',
                'title' => 'Reported at',
                'width' => '20%',
            ]
        ];
    }

}
