<?php
/**
 * Created by Maksym Ignatchenko, Appus Studio LP on 26.02.19
 *
 */

namespace App\Modules\Challenges\Datatables;

use App\Helpers\PrettyNameHelper;
use App\Modules\Challenges\Enums\ChallengeStatusEnum;
use App\Modules\Challenges\Enums\ProofStatusEnum;
use App\Modules\Challenges\Helpers\ChallengeStatusClassHelper;
use App\Modules\Challenges\Models\Challenge;
use App\Modules\Companies\Helpers\CompanyViewHelper;
use App\Services\CountriesRepository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Builder as YajraBuilder;
use Yajra\DataTables\Services\DataTable;

class ChallengeDataTable extends DataTable
{
    protected $companyId;
    protected $country;

    /**
     * Build DataTable class.
     *
     * @param mixed $query Result from query() method.
     * @return EloquentDataTable
     */
    public function dataTable($query): EloquentDataTable
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable
            ->addColumn('action', 'challenge.datatables_actions')
            ->editColumn('image', function ($query) {
                return ($query->image ? ("<img height='50' src=" . $query->image) . " />" : (''));
            })
            ->editColumn('company.name', function ($query) {
                $badgeClass = CompanyViewHelper::getTypeContainerClass($query->company->type);
                $companyName = substr($query->company->name, 0, 50);
                return $companyName;
                // return "<span class='badge " . $badgeClass . "'>$companyName</span>";
            })
            ->editColumn('status', function($query) {
                $className = ChallengeStatusClassHelper::getClassName($query->status);
                $name = ChallengeStatusClassHelper::getName($query->status);
                return "<span class='" . $className . "'>$name</span>";
            })
            ->editColumn('start_date', function($query) {
                return Carbon::parse($query->start_date)->format('Y-m-d H:i');
            })
            ->editColumn('end_date', function($query) {
                return Carbon::parse($query->end_date)->format('Y-m-d H:i');
            })
            ->rawColumns(['image', 'company.name', 'action', 'status']);
    }

    /**
     * @param int|null $companyId
     * @return $this|DataTable
     */
    public function setCompanyId(int $companyId = null): DataTable
    {
        $this->companyId = $companyId;

        return $this;
    }

    /**
     * @param string|null $countryName
     * @return $this|DataTable
     */
    public function setCountry(string $countryName = null): DataTable
    {
        $this->country = $countryName;

        return $this;
    }

    /**
     * Get query source of dataTable.
     *
     * @param Challenge $model
     * @return Builder
     */
    public function query(Challenge $model): Builder
    {

        if (auth()->user()->isNotAdmin()) {
            return $this->queryForAdmin($model);
        }
        return $this->queryForSuperAdmin($model);
    }

    protected function queryForAdmin(Challenge $model)
    {
        $authUser = auth()->user();

        if ($authUser->country_id) {
            $country = CountriesRepository::make()->find($authUser->country_id);

            return $model->where('status', '!=', ChallengeStatusEnum::ARCHIVE)
                ->where('country', $country->name)
                ->whereNull('company_id')
                ->withCount(['proofs as pending_proofs' => function ($query) {
                    $query->where('status', ProofStatusEnum::PENDING);
                }])->with('company');
        }

        return $model->where('status', '!=', ChallengeStatusEnum::ARCHIVE)
            ->where('company_id', $authUser->company_id)
            ->withCount(['proofs as pending_proofs' => function ($query) {
                $query->where('status', ProofStatusEnum::PENDING);
            }])->with('company');

    }

    protected function queryForSuperAdmin(Challenge $model)
    {
        if ($this->country) {
            return $model->where('status', '!=', ChallengeStatusEnum::ARCHIVE)
                ->where('country', $this->country)
                ->withCount(['proofs as pending_proofs' => function ($query) {
                    $query->where('status', ProofStatusEnum::PENDING);
                }])->with('company');

        } elseif ($this->companyId) {
            return $model->where('status', '!=', ChallengeStatusEnum::ARCHIVE)
                ->where('company_id', $this->companyId)
                ->withCount(['proofs as pending_proofs' => function ($query) {
                    $query->where('status', ProofStatusEnum::PENDING);
                }])->with('company');

        } else {
            return $model->where('status', '!=', ChallengeStatusEnum::ARCHIVE)
                ->withCount(['proofs as pending_proofs' => function ($query) {
                    $query->where('status', ProofStatusEnum::PENDING);
                }])->with('company');
        }
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return YajraBuilder
     */
    public function html(): YajraBuilder
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax(request()->getRequestUri())
            ->addAction(['width' => '10%'])
            ->parameters([
                'dom'     => 'frtip',
                'order'   => [[0, 'desc']],
                'responsive' => true,
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns(): array
    {
        return [
            [
                'name' => 'name',
                'data' => 'name',
                'title' => 'Name',
                'width' => '15%',
            ],
            [
                'data' => 'pending_proofs',
                'title' => 'Pending proofs',
                'width' => '10%',
                'searchable' => false,
            ],
            [
                'name' => 'company.name',
                'data' => 'company.name',
                'title' => 'Company',
                'width' => '10%',
            ],
            [
                'name' => 'country',
                'data' => 'country',
                'title' => 'Country',
                'width' => '15%',
            ],
            [
                'name' => 'image',
                'data' => 'image',
                'title' => 'Image',
                'width' => '15%',
                'searchable' => false,
                'orderable' => false,
            ],
            [
                'name' => 'start_date',
                'data' => 'start_date',
                'title' => 'Start date',
                'width' => '10%',
            ],
            [
                'name' => 'status',
                'data' => 'status',
                'title' => 'Status',
                'width' => '10%',
            ],
            // [
            //     'name' => 'participants_limit',
            //     'data' => 'participants_limit',
            //     'title' => 'Participants limit',
            //     'width' => '10%',
            // ],
            // [
            //     'name' => 'end_date',
            //     'data' => 'end_date',
            //     'title' => 'End date',
            //     'width' => '10%',
            // ],
        ];
    }

}
