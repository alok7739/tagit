<?php


namespace App\Modules\Challenges\Datatables;


use App\Helpers\PrettyNameHelper;
use App\Modules\Challenges\Enums\ChallengeStatusEnum;
use App\Modules\Challenges\Enums\ProofStatusEnum;
use App\Modules\Challenges\Helpers\ChallengeStatusClassHelper;
use App\Modules\Challenges\Models\Challenge;
use App\Modules\Companies\Helpers\CompanyViewHelper;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Yajra\DataTables\EloquentDataTable;

class ArchivedChallengesDataTable extends ChallengeDataTable
{
    /**
     * Get query source of dataTable.
     *
     * @param Challenge $model
     * @return Builder
     */
    public function query(Challenge $model): Builder
    {
        if (auth()->user()->isNotAdmin()) {
            return $model->where('status', ChallengeStatusEnum::ARCHIVE)
                ->where('company_id', auth()->user()->company_id)
                ->withCount(['proofs as pending_proofs' => function ($query) {
                    $query->where('status', ProofStatusEnum::PENDING);
                }])->with('company');
        }

        if ($this->country) {
            return $model->where('status', ChallengeStatusEnum::ARCHIVE)
                ->where('country', $this->country)
                ->withCount(['proofs as pending_proofs' => function ($query) {
                    $query->where('status', ProofStatusEnum::PENDING);
                }])->with('company');

        } elseif ($this->companyId) {
            return $model->where('status', ChallengeStatusEnum::ARCHIVE)
                ->where('company_id', $this->companyId)
                ->withCount(['proofs as pending_proofs' => function ($query) {
                    $query->where('status', ProofStatusEnum::PENDING);
                }])->with('company');

        } else {
            return $model->where('status', ChallengeStatusEnum::ARCHIVE)
                ->withCount(['proofs as pending_proofs' => function ($query) {
                    $query->where('status', ProofStatusEnum::PENDING);
                }])->with('company');
        }
    }
}
