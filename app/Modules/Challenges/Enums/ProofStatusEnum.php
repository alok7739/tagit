<?php
/**
 * Created by Maksym Ignatchenko, Appus Studio LP on 30.03.19
 *
 */

namespace App\Modules\Challenges\Enums;

class ProofStatusEnum
{
    public const UPLOADING = 'uploading';
    public const PENDING = 'pending';
    public const IN_REVIEW = 'in_review';
    public const ACCEPTED = 'accepted';
    public const REJECTED = 'rejected';

    /**
     * @return array
     */
    public static function getAll() : array
    {
        return [
            self::PENDING,
            self::IN_REVIEW,
            self::ACCEPTED,
            self::REJECTED,
        ];
    }

    /**
     * @return array
     */
    public static function getAbleForDeletionStatuses() : array
    {
        return [
            self::REJECTED,
            self::PENDING,
        ];
    }

    /**
     * @param string|null $name
     * @return string|null
     */
    public static function getName(string $name = null): ?string
    {
        $statuses = [
            self::PENDING => 'in_review',
            self::IN_REVIEW => 'in_review',
            self::ACCEPTED => 'approved',
            self::REJECTED => 'rejected',
        ];

        return $statuses[$name] ?? null;
    }

}
