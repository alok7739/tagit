<?php
/**
 * Created by Maksym Ignatchenko, Appus Studio LP on 26.02.19
 *
 */

namespace App\Modules\Challenges\Enums;

class ProofTypeEnum
{
    public const PHOTO = 'photo';
    public const MULTIPLE_PHOTOS = 'multiple_photos';
    public const VIDEO = 'video';
    public const MULTIPLE_VIDEOS = 'multiple_videos';
    public const SCREENSHOT = 'screenshot';
    public const MULTIPLE_SCREENSHOTS = 'multiple_screenshots';
    public const QUESTIONS = 'questions';
    public const TRUE_FALSE = 'true_false';
    public const TESTS = 'tests';
    public const QRCODE = 'qrcode';
    public const CHECKIN = 'checkin';

    /**
     * @return array
     */
    public static function getAll() : array
    {
        return [
            self::PHOTO,
            self::MULTIPLE_PHOTOS,
            self::VIDEO,
            self::MULTIPLE_VIDEOS,
            self::SCREENSHOT,
            self::MULTIPLE_SCREENSHOTS,
            self::QUESTIONS,
            self::TRUE_FALSE,
            self::TESTS,
            self::QRCODE,
            self::CHECKIN,
        ];
    }

    /**
     * @return array
     */
    public static function getMultipleTypes() : array
    {
        return [
            self::MULTIPLE_PHOTOS,
            self::MULTIPLE_VIDEOS,
            self::MULTIPLE_SCREENSHOTS
        ];
    }

    /**
     * @return array
     */
    public function getSingleItemTypes() : array
    {
        return [
            self::PHOTO,
            self::SCREENSHOT,
            self::VIDEO,
        ];
    }

    /**
     * @return array
     */
    public static function getImageTypes() : array
    {
        return [
            self::PHOTO,
            self::SCREENSHOT,
            self::MULTIPLE_PHOTOS,
            self::MULTIPLE_SCREENSHOTS
        ];
    }

    /**
     * @return array
     */
    public static function getNames() : array
    {
        return [
            self::PHOTO => 'Photos',
            // self::MULTIPLE_PHOTOS => 'Multiple photo',
            self::VIDEO => 'Videos',
            // self::MULTIPLE_VIDEOS => 'Multiple video',
            self::SCREENSHOT => 'Screenshots',
            // self::MULTIPLE_SCREENSHOTS => 'Multiple screenshot',
            self::QUESTIONS => 'Text Questions',
            self::TESTS => 'Multiple Choice Questions',
            self::TRUE_FALSE => 'True/False Questions',
            self::QRCODE => 'QR Code',
            self::CHECKIN => 'Check In',
        ];
    }

}
