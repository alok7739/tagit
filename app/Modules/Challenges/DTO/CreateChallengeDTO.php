<?php
/**
 * Created by Maksym Ignatchenko, Appus Studio LP on 26.02.19
 *
 */

namespace App\Modules\Challenges\DTO;


use App\Helpers\PrettyNameHelper;
use App\Modules\Companies\Models\Company;
use App\Modules\Onboarding\Models\CompanyPackages;

class CreateChallengeDTO
{
    /**
     * @var array
     */
    private $companies;

    /**
     * @var array
     */
    private $countries;

    /**
     * @var array
     */
    private $proofTypes;

    /**
     * @var array
     */
    private $videoLengthTypes;

    /**
     * @var int|null
     */
    private $selectedCompanyId;

    /**
     * CreateChallengeDTO constructor.
     * @param array $companies
     * @param array $countries
     * @param array $proofTypes
     * @param array $videoLengthTypes
     * @param int|null $selectedCompanyId
     */
    public function __construct(array $companies, array $countries, array $proofTypes, array $videoLengthTypes, ?int $selectedCompanyId = null, int $selectedRadius = 50)
    {
        $this->companies = $companies;
        $this->countries = $countries;
        $this->proofTypes = $proofTypes;
        $this->videoLengthTypes = $videoLengthTypes;
        $this->selectedCompanyId = $selectedCompanyId;
        $this->selectedRadius = $selectedRadius;
    }

    /**
     * @return array
     */
    public function getCompanies(): array
    {
        return $this->companies;
    }

    /**
     * @return array
     */
    public function getCountries(): array
    {
        return $this->countries;
    }

    /**
     * @return array
     */
    public function getProofTypes($compnayId = '', $current = ''): array
    {
        $types = array_map(function($proofName) {
            return PrettyNameHelper::transform($proofName);
        }, $this->proofTypes);

        $compnayId = $compnayId ? $compnayId : \Auth::user()->company_id;

        if($compnayId) {
            $company = Company::find($compnayId);
            $package = CompanyPackages::find($company->package_id);
        } else {
            return $types;
        }

        $current_lbl = $current ? $types[$current] : '';

        if($package) {
            if(!$package->true_false) {
                unset($types['true_false']);
            }

            if(!$package->sc_ques) {
                unset($types['questions']);
            }

            if(!$package->mc_ques) {
                unset($types['tests']);
            }

            if(!$package->checkin) {
                unset($types['checkin']);
            }

            if(!$package->qr_code) {
                unset($types['qrcode']);
            }

            if(!$package->images) {
                unset($types['photo']);
            }

            if(!$package->screenshots) {
                unset($types['screenshot']);
            }

            if(!$package->videos) {
                unset($types['video']);
            }
        }

        if($current) {
            $types[$current] = $current_lbl;
        }

        return $types;
    }

    /**
     * @return array
     */
    public function getVideoLengthTypes(): array
    {
        return $this->videoLengthTypes;
    }

    public function getRadiuses(): array
    {
        return [50 => '50 meters', 100 => '100 meters', 150 => '150 meters', 200 => '200 meters', 250 => '250 meters'];
    }

    /**
     * @return int|null
     */
    public function getSelectedCompanyId() : ?int
    {
        return $this->selectedCompanyId;
    }

    public function getSelectedRadius() : ?int
    {
        return $this->selectedRadius;
    }
}
