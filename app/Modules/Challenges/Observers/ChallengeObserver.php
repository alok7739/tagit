<?php
/**
 * Created by Maksym Ignatchenko, Appus Studio LP on 30.03.19
 *
 */

namespace App\Modules\Challenges\Observers;

use App\Modules\Challenges\Enums\ChallengeStatusEnum;
use App\Modules\Challenges\Events\ChallengeEndedEvent;
use App\Modules\Challenges\Events\ChallengeStartedEvent;
use App\Modules\Challenges\Models\Challenge;
use App\Modules\Feeds\Events\ChallengeEndedEventListener;
use App\Modules\Feeds\Events\ChallengeStartedEventListener;
use App\Modules\Feeds\Models\Feed;
use Illuminate\Support\Facades\Storage;

class ChallengeObserver
{
    public function updating(Challenge $challenge) : void
    {
        if ($challenge->isDirty('status')) {

            switch ($challenge->status) {
                case ChallengeStatusEnum::ACTIVE :
                    event(new ChallengeStartedEventListener($challenge));
                    event(new ChallengeStartedEvent($challenge));
                    break;
                case ChallengeStatusEnum::END :
                    event(new ChallengeEndedEventListener($challenge));
                    event(new ChallengeEndedEvent($challenge));
                    break;
            }
        }

        if ($challenge->isDirty('image')) {
            $oldFileName = $challenge->getOriginal('image');
            Storage::delete($oldFileName);
        }

        if ($challenge->isDirty('feed_visible')) {
            Feed::where('challenge_id', $challenge->id)->update(['visible' => $challenge->feed_visible]);

            Feed::whereIn('proof_id', function($query) use($challenge) {
                $query->select('id')
                ->from('proofs')
                ->where('challenge_id', $challenge->id);
            })->update(['visible' => $challenge->feed_visible]);
        }
    }
}
