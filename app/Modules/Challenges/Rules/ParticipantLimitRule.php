<?php

namespace App\Modules\Challenges\Rules;

use App\Modules\Companies\Models\Company;
use App\Modules\Onboarding\Models\CompanyPackages;
use Illuminate\Contracts\Validation\Rule;

class ParticipantLimitRule implements Rule
{
    private $limit = 10000;

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $company_id = request()->get('company_id', 0);

        if($company_id)
        {
            $company = Company::find($company_id);

            if($company->package_id) {
                $package = CompanyPackages::find($company->package_id);
                $this->limit = $package ? $package->users : 0;
            } else {
                $this->limit = 0;
            }
        }

        return (int) $value <= $this->limit;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->limit ? 'The participants limit may not be greater than ' . $this->limit . '.' : 'User limit not defined for this company';
    }
}
