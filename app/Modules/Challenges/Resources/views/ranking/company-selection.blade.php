@if(\Auth::user()->isSuperAdmin() && !empty($companyIds))
    <form method="GET">
        <select
            class="form-control select2"
            name="company_id"
            onchange="submit();"
        >
            <option value="">Select a company</option>
            @foreach ($companyIds as $id => $name)
                <option
                    value="{{ $id }}"
                    @if($companyId === $id)
                    selected
                    @endif
                >
                    {{ $name }}
                </option>
            @endforeach
        </select>
    </form>

    <script>
    $(document).ready(function(){
        $('.select2').select2({
            theme: 'bootstrap'
        });
    });
    </script>
@endif
