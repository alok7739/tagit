@extends('layouts.app')
@section('title', 'Ranking')

@section('content')
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="row">
            <div class="col-sm-6">
                @include('ranking.company-selection')
            </div>
            <div class="col-sm-6">
                @if (null !== $companyId)
                @include('ranking.buttons')
                @endif
            </div>
        </div>
        
        <hr />

        @if (null !== $companyId)
            @include('ranking.table')
        @elseif(\Auth::user()->isSuperAdmin())
            <div class="alert alert-info" style="margin-top: 20px;">Please select a company</div>
        @endif
    </div>

    <script>

        $('body').on('click', '.export-csv', function (e) {
            e.preventDefault();

            let url = $(this).attr('href');
            const search = $('#dataTableBuilder_wrapper input[type="search"]').val();
            url += '&filter[search]=' + search;
            const orderElement = $('#dataTableBuilder_wrapper thead th[aria-sort]');

            if (orderElement.length > 0) {
                const fieldIndex = orderElement.index();
                let direction = orderElement.attr('aria-sort');
                if ('ascending' === direction) {
                    direction = 'asc';
                } else {
                    direction = 'desc';
                }
                url += '&order_field=' + fieldIndex;
                url += '&order_dir=' + direction;
            }

            location.href = url;

        });

    </script>

@endsection






