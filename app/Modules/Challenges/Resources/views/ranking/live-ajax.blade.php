<link href="https://fonts.googleapis.com/css2?family=Baloo+Thambi+2:wght@400;500;600;700;800&display=swap" rel="stylesheet" />
<style>
.main-div {
    min-width: 300px;
    padding-bottom: 20px;
    margin: 0px auto;
    max-width: 600px;
}
h1 {
    text-align: center;
    color: #693988;
    font-family: "Baloo Thambi 2", cursive;
    font-size: 26px;
    font-weight: 600;
    margin: 15px auto 25px;
}
.clearfix::after {
  content: "";
  clear: both;
  display: table;
}
.item { 
    padding: 15px 10px;
    border-bottom: 1px solid #aaa;  
    font-family: "Baloo Thambi 2", cursive;
    background-color: #f7f7f7;
    color: #222;
    font-size: 15px;
}
.item .img {
    float: left;
    padding-right: 15px;
}
.item .name {
    float: left;
    margin-top: 12px;
}
.item .position, 
.item .points {
    float: right;
    margin-left: 15px;
    width: 30px;
    text-align: center;
    margin-top: 10px;
}
.item .img > img {
    width: 50px;
    height: 50px;
    border-radius: 50px;
}
.item .position {
    background: #f8ae4f;
    padding: 4px;
    border-radius: 5px;
    font-weight: bold;
}
.item .points {
    margin-top: 13px;
}
</style>

<div class="main-div">
    <h1>Live Ranking Of {{ $company->name }}</h1>

    <div class="scroll-div">
        @foreach($rating as $r)
        <div class="item">
            <div class="img"><img src="{!! $r->avatar !!}" /></div>
            <div class="name">{{ $r->full_name }}</div>
            <div class="points">{{ $r->total_reward }}</div>
            <div class="position"><span>{{ $r->position }}</span></div>
            <div class="clearfix"></div>
        </div>
        @endforeach
    </div>
</div>
