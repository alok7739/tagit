<a
    class="btn btn-primary pull-right export-csv"
    href="{{ route('ranking.download', ['company_id' => $companyId]) }}"
>
    Export to CSV
</a>
