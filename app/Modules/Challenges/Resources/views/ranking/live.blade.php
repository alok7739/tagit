@extends('layouts.app')
@section('title', 'Ranking')

@section('content')
    <div class="content">
        <div class="clearfix"></div>

        <div class="row">
            <div class="col-sm-7">
                <div id="ranking-div"></div>
            </div>
            <div class="col-sm-5">
                <h1>Embed Code</h1>
                <textarea id="embedcode" rows="5" class="form-control" readonly>&lt;iframe src="{{ url('admin/live-ranking/' . $company->join_code) }}" style="border:0px #ffffff none;" name="live-ranking" scrolling="yes" frameborder="1" marginheight="0px" marginwidth="0px" height="600px" width="100%" allowfullscreen&gt;&lt;/iframe&gt;</textarea>
                <p class="text-center">
                    <button id="copyBtn" onclick="copyToClip()" class="btn btn-primary" style="margin-top: 20px">Copy to clipboard</button>
                </p>
            </div>
        </div>
        
    </div>

    <script>
    setInterval(function(){
        loadRanking();
    }, 10000);

    function loadRanking() {
        $.ajax({
            url: "{{ url('admin/live-ranking/' . $company->join_code) }}", 
            success: function(result) {
                $("#ranking-div").html(result);
            }
        });
    }

    loadRanking();

    function copyToClip() {
        var copyText = document.getElementById("embedcode");
        copyText.select();
        copyText.setSelectionRange(0, 99999);
        document.execCommand("copy");
        
        var copyBtn = document.getElementById("copyBtn");
        copyBtn.disabled = true;
        copyBtn.innerHTML = "Copied!";

        setTimeout(function(){
            copyBtn.disabled = false;
            copyBtn.innerHTML = "Copy to clipboard";
        }, 3000);
    }
    </script>
@endsection
