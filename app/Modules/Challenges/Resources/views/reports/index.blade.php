@extends('layouts.app')
@section('title', 'Challenges Reports')

@section('content')
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box">
            <div class="box-body">
                <h3 class="new_heading">Reported Challenges</h3>
                @include('reports.table')
            </div>
        </div>
    </div>
@endsection
