<div class="row">
    <div class="col-md-2" style="margin-bottom: 20px;">
        <a class="image-popup-no-margins" target="_blank" href="{{ $dto->getImageUrl() }}">
            {!!  $dto->getImageUrl() ? ("<img class='dashboard-image rounded-circle' src=" . $dto->getImageUrl()) . " />" : ('') !!}
        </a>
    </div>
    <div class="col-md-10 text-right">
        <a href="{{ route('check', $dto->getChallengeId()) }}" class="btn btn-success">Check proofs</a>

        @if(Auth::user()->status == '1')
            <a href="{{ route('challenge.edit', $dto->getChallengeId()) }}" class="btn btn-success">Edit</a>
        @endif

        <a href="{{ route('challenge.proof.index', ['challenge' => $dto->getChallengeId()]) }}" class="btn btn-primary" role="button">Show proofs</a>

        @if($dto->mayHaveResults())
        <a href="{{ route('result', ['challenge' => $dto->getChallengeId()]) }}" class="btn btn-warning" role="button">Results</a>
        @endif

        <a href="{{ route('challenge.users.notification-form', ['challenge' => $dto->getChallengeId()]) }}" class="btn btn-primary">Send message</a>

        @if($dto->getStatusVal() !== \App\Modules\Challenges\Enums\ChallengeStatusEnum::ARCHIVE)
            <form action="{{ route('challenge.archive', ['challenge' => $dto->getChallengeId()]) }}" method="post">
                @csrf @method('patch')
                <button class="btn btn-danger" >Archive challenge</button>
            </form>
        @endif
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="card m-b-20 card-inverse text-white">
            <div class="card-block">
                <h3 class="card-title= mt-0">Base info</h3>
            </div>
            <div class="card-block">
                <table class="table table-striped">
                    <tr>
                        <th width="30%">Name</th>
                        <td>{{ $dto->getName() }}</td>
                    </tr>
                    <tr>
                        <th>Link</th>
                        <td>{{ $dto->getLink() ? Html::link($dto->getLink(), 'Click here') : '-' }}</td>
                    </tr>
                    @if($dto->getCompanyName())
                        <tr>
                            <th>Company</th>
                            <td>
                                <span class="badge {{ CompanyViewHelper::getTypeContainerClass($dto->getCompanyType()) }} ">
                                    <a href="{{ route('company.show', $dto->getCompanyId()) }}" class="company-type-label">
                                        {{ $dto->getCompanyName() }}
                                    </a>
                                </span>
                            </td>
                        </tr>
                    @endif
                    <tr>
                        <th>Status</th>
                        <td>{{ $dto->getStatus() }}</td>
                    </tr>
                    <tr>
                        <th>Start date <span class="fa fa-calendar"></span></th>
                        <td>{{ $dto->getStartDate() }}</td>
                    </tr>
                    <tr>
                        <th>End date <span class="fa fa-calendar"></span></th>
                        <td>{{ $dto->getEndDate() }}</td>
                    </tr>
                    <tr>
                        <th>Visible In Feed</th>
                        <td>{{ $challenge->feed_visible ? 'Yes' : 'No' }}</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="card m-b-20 card-inverse text-white">
            <div class="card-block">
                <h3 class="card-title mt-0">Proofs</h3>
            </div>
            <div class="card-block">
                <table class="table table-striped">
                    <tr>
                        <th width="30%">Proof type</th>
                        <td>{{ $dto->getProofType() }}</td>
                    </tr>
                    <tr>
                        <th>Required proof items count</th>
                        <td>{{ $dto->getRequiredProofItems() }}</td>
                    </tr>
                    @if($dto->getVideoDuration())
                    <tr>
                        <th>Video duration</th>
                        <td>{{ $dto->getVideoDuration() }}sec</td>
                    </tr>
                    @endif
                </table>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="card m-b-20 card-inverse text-white">
            <div class="card-block">
                <h3 class="card-title mt-0">Participants</h3>
            </div>
            <div class="card-block">
                <table class="table table-striped">
                    <tr>
                        <th width="30%">Participants limit</th>
                        <td>{{ $dto->getParticipantsLimit() }}</td>
                    </tr>
                    <tr>
                        <th>Participants now</th>
                        <td>{{ $dto->getParticipantsCount() }}</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="card m-b-20 card-inverse text-white">
            <div class="card-block">
                <h3 class="card-title mt-0">Location</h3>
            </div>
            <div class="card-block">
                <table class="table table-striped">
                    <tr>
                        <th width="30%">Country</th>
                        <td>{{ $dto->getCountry() ?? 'Empty' }}</td>
                    </tr>
                    <tr>
                        <th>City</th>
                        <td>{{ $dto->getCity() ?? 'Empty' }}</td>
                    </tr>
                    <tr>
                        <th>Location Bound?</th>
                        <td>{{ $challenge->location_bound ? 'Yes' : 'No' }}</td>
                    </tr>
                    @if($challenge->location_bound)
                    <tr>
                        <th>Location</th>
                        <td>{{ $challenge->location }}</td>
                    </tr>
                    <tr>
                        <th>Radius</th>
                        <td>{{ $challenge->location_radius }} meters</td>
                    </tr>
                    @endif
                </table>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="card m-b-20 card-inverse text-white">
            <div class="card-block">
                <h3 class="card-title mt-0">Description</h3>
            </div>
            <div class="card-block">
                <p>{!! $dto->getDescription() !!}</p>
            </div>
        </div>
    </div>
</div>


