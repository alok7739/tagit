@extends('layouts.app')
@section('title', 'Challenges')

@section('content')
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box">
            <div class="box-body">
                <h3 class="new_heading">Manage Challenges</h3>
                <div class="buttons-group">
                    @if(auth()->user()->isAdmin())
                        <div>
                            <a style="margin-bottom: 10px" href="{{route('challenge.create')}}" class="btn btn-primary pull-right create-article">
                                New challenge
                            </a>
                        </div>
                        <div>
                            <a style="margin-bottom: 10px" href="{{route('reports.index')}}" class="btn btn-danger pull-right">
                                Reported challenges
                            </a>
                        </div>
                    @endif
                    <div>
                        <a style="margin-bottom: 10px" href="{{ route('ranking.index') }}" class="btn btn-primary pull-right">
                            View Ranking
                        </a>
                    </div>
                    @if(url()->current() == route('challenge.index'))
                        <div>
                            <a href="{{ route('challenges.archived') }}" class="btn btn-primary pull-right">
                                Archived challenges list
                            </a>
                        </div>
                    @else
                        <div>
                            <a href="{{ route('challenge.index') }}" class="btn btn-primary pull-right">
                                All challenges list
                            </a>
                        </div>
                    @endif
                </div>
                @include('challenge.table')
            </div>
        </div>
    </div>
@endsection
