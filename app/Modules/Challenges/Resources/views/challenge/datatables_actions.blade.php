<div class="btn-group" role="group">
    <button id="btnGroupDrop1" type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Actions
    </button>

    @php $challenge = \App\Modules\Challenges\Models\Challenge::find($id) @endphp

    <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
        <a class="dropdown-item" href="{{ route('check', $id) }}">Check proofs</a>
        <a class="dropdown-item" href="{{ route('challenge.show', $id) }}">Show</a>
        @if(Auth::user()->status == '1')
            @can('edit', $challenge)
                <a class="dropdown-item" href="{{ route('challenge.edit', $id) }}">Edit</a>
            @endcan
        @endif
        <a class="dropdown-item" href="{{ route('challenge.proof.index', ['challenge' => $id]) }}">Show proofs</a>
        <a class="dropdown-item" href="{{ route('result', ['challenge' => $id]) }}">Results</a>
        @can('edit', $challenge)
            <a class="dropdown-item" href="{{ route('challenge.users.notification-form', ['challenge' => $id]) }}">Send message</a>
        @endcan
        @can('edit', $challenge)
            @if($challenge->status !== \App\Modules\Challenges\Enums\ChallengeStatusEnum::ARCHIVE)
                <form action="{{ route('challenge.archive', ['challenge' => $id]) }}" method="post">
                    @csrf @method('patch')
                    <button class="dropdown-item">Archive challenge</button>
                </form>
            @endif
        @endcan

        @if($challenge->company_id && !$challenge->is_library)
        <form action="{{ route('challenge.add-to-library', ['challenge' => $id]) }}" method="post">
            @csrf
            <button class="dropdown-item">Add To My Library</button>
        </form>
        @endif
    </div>
</div>
