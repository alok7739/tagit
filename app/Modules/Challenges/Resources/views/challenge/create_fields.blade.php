@php
$lat_lng = old('location_latlng') ? explode(',', old('location_latlng')) : $lat_lng;
$current_type = $dto->getCurrentProofType() ? $dto->getCurrentProofType() : old('proof_type');
@endphp

<h3 class="new_heading">Create Challenge</h3>

<div class="row">
    <div class="col-sm-6">
        <ul class="nav nav-tabs">
            <li><a href="#tab_en" class="active" data-toggle="tab"><img src="{{ asset('assets/images/en.png') }}" /> English</a></li>
        </ul>

        <div class="tab-content">
            <div id="tab_en" class="tab-pane fade in active">
                <div class="form-group">
                    {{ Form::label('name', 'Name: ', ['class' => 'required']) }}
                    {!! Form::text('name', $dto->getCurrentName(), ['class' => 'form-control', 'maxlength' => 50]) !!}
                    @if ($errors->has('name'))
                    <div class="text-red">{{ $errors->first('name') }}</div>
                    @endif
                </div>

                <div class="form-group">
                    {{ Form::label('description', 'Description: ', ['class' => 'required']) }}
                    {!! Form::textarea('description', $dto->getCurrentDescription(), ['id' => 'desc_en', 'data-lang' => 'en', 'class' => 'form-control ckeditor', 'maxlength' => 1000]) !!}
                    @if ($errors->has('description'))
                    <div class="text-red">{{ $errors->first('description') }}</div>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <div class="col-sm-6">
        <ul class="nav nav-tabs">
            <li><a href="#tab_ar" class="active" data-toggle="tab"><img src="{{ asset('assets/images/ar.png') }}" /> Arabic</a></li>
        </ul>

        <div class="tab-content">
            <div id="tab_ar" class="tab-pane fade in rtl active">
                <div class="form-group">
                    {{ Form::label('name_ar', 'Arabic Name: ', ['class' => 'required']) }}
                    {!! Form::text('name_ar', $dto->getCurrentNameAr(), ['class' => 'form-control', 'maxlength' => 50]) !!}
                    @if ($errors->has('name_ar'))
                    <div class="text-red">{{ $errors->first('name_ar') }}</div>
                    @endif
                </div>

                <div class="form-group">
                    {{ Form::label('description_ar', 'Arabic Description: ', ['class' => 'required']) }}
                    {!! Form::textarea('description_ar', $dto->getCurrentDescriptionAr(), ['id' => 'desc_ar', 'data-lang' => 'ar', 'class' => 'form-control ckeditor', 'maxlength' => 1000]) !!}
                    @if ($errors->has('description_ar'))
                    <div class="text-red">{{ $errors->first('description_ar') }}</div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div @if(auth()->user()->isNotAdmin() || request()->query('companyId')) style="display: none" @endif class="col-md-4">
        <div class="form-group">
            {{ Form::label('company_id', 'Company: ') }}
            {!! Form::select('company_id', $dto->getCompanies(), $dto->getCurrentCompany(), ['id' => 'company_id', 'placeholder' => 'Select company', 'class' => 'form-control select2']) !!}
            @if ($errors->has('company_id'))
            <div class="text-red">{{ $errors->first('company_id') }}</div>
            @endif
        </div>
    </div>

    <div class="col-sm-4" id="country-component">
        <div class="form-group">
            <label for="country_id">Country:</label>
            <select name="country" class="form-control select2" id="country_id">
                <option disabled>Select a country</option>
                @foreach($dto->getCountries() as $country)
                <option value="{{ $country->name }}" {{ $dto->getCurrentCountry() === $country->name ? 'selected' : '' }}>{{ $country->name }}</option>
                @endforeach
            </select>
            @if ($errors->has('country'))
            <div class="text-red">{{ $errors->first('country') }}</div>
            @endif
        </div>
    </div>

    <div class="col-sm-4" id="city-component">
        <div class="form-group">
            {{ Form::label('city', 'City: ') }}
            {!! Form::text('city', $dto->getCurrentCity(), ['class' => 'form-control', 'maxlength' => 50]) !!}
            @if ($errors->has('city'))
            <div class="text-red">{{ $errors->first('city') }}</div>
            @endif
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-8">
        <div class="form-group">
            {{ Form::label('link', 'Link: ') }}
            {!! Form::text('link', $dto->getCurrentLink(), ['class' => 'form-control', 'maxlength' => 100]) !!}
            @if ($errors->has('link'))
            <div class="text-red">{{ $errors->first('link') }}</div>
            @endif
        </div>
    </div>
    <div class="col-sm-4" id="dependent_challenge_div">
        <div class="form-group">
            {{ Form::label('dependent_challenge', 'Prerequisite Challenge: ') }}
            {!! Form::select('dependent_challenge', [], null, ['id' => 'dependent_challenge', 'placeholder' => 'None', 'class' => 'form-control', 'data-val' => $dto->getCurrentDependent()]) !!}
            @if ($errors->has('dependent_challenge'))
            <div class="text-red">{{ $errors->first('dependent_challenge') }}</div>
            @endif
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-3">
        <div class="form-group">
            {!! Html::decode(Form::label('start_date', 'Start date <span class="fa fa-calendar"></span>', ['class' => 'required'])) !!}
            {!! Form::text('start_date', $dto->getCurrentStartDate(), ['class' => 'air-datepicker form-control', 'readonly' => 'true']) !!}
            @if ($errors->has('start_date'))
            <div class="text-red">{{ $errors->first('start_date') }}</div>
            @endif
        </div>
    </div>

    <div class="col-sm-3">
        <div class="form-group">
            {!! Html::decode(Form::label('end_date', 'End date <span class="fa fa-calendar"></span>', ['class' => 'required'])) !!}
            {!! Form::text('end_date', $dto->getCurrentEndDate(), ['class' => 'air-datepicker form-control', 'readonly' => 'true']) !!}
            @if ($errors->has('end_date'))
            <div class="text-red">{{ $errors->first('end_date') }}</div>
            @endif
        </div>
    </div>

    <div class="col-sm-3">
        <div class="form-group">
            {{ Form::label('participants_limit', 'Participants limit: ', ['class' => 'required']) }}
            {!! Form::text('participants_limit', $dto->getCurrentParticipantsLimit(), ['class' => 'form-control', 'maxlength' => 10]) !!}
            @if ($errors->has('participants_limit'))
            <div class="text-red">{{ $errors->first('participants_limit') }}</div>
            @endif
        </div>
    </div>

    <div class="col-sm-3">
        <div class="form-group">
            {{ Form::label('feed_visible', 'Visible In Feed: ', ['class' => 'required']) }}
            {!! Form::select('feed_visible', [1 => 'Yes', 0 => 'No'], $dto->getCurrentFeedVisible(), ['class' => 'form-control']) !!}
            @if ($errors->has('feed_visible'))
            <div class="text-red">{{ $errors->first('feed_visible') }}</div>
            @endif
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-4">
        <div class="form-group">
            {{ Form::label('proof_type', 'Proof type: ', ['class' => 'required']) }}
            {!! Form::select('proof_type', [], $current_type, ['id' => 'proof_type', 'class' => 'form-control', 'placeholder' => 'Select proof type', 'data-val' => $current_type]) !!}
            @if ($errors->has('proof_type'))
            <div class="text-red">{{ $errors->first('proof_type') }}</div>
            @endif
        </div>
    </div>

    <div class="col-sm-4" id="item_count_div">
        <div class="form-group">
            {{ Form::label('items_count_in_proof', 'Required count of items for proof: ', ['class' => 'required']) }}
            {!! Form::select('items_count_in_proof', $proofCount, $dto->getCurrentItemsCount(), ['id' => 'item_count_fld',  'class' => 'form-control', 'placeholder' => 'Select proof items count']) !!}
            @if ($errors->has('items_count_in_proof'))
            <div class="text-red">{{ $errors->first('items_count_in_proof') }}</div>
            @endif
        </div>
    </div>

    <div class="col-sm-4" id="location_bound_div">
        <div class="form-group">
            {{ Form::label('location_bound', 'Location Bound? ', ['class' => 'required']) }}
            {!! Form::select('location_bound', [0 => 'No', 1 => 'Yes'], $dto->getCurrentLocationBound(), ['class' => 'form-control']) !!}
            @if ($errors->has('location_bound'))
            <div class="text-red">{{ $errors->first('location_bound') }}</div>
            @endif
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div id="text-proofs-block" data-questions="{{ json_encode(old('questions') ? old('questions') : $questions) }}" data-answers="{{ json_encode(old('answers') ? old('answers') : $answers) }}" data-tests="{{ json_encode(old('test_answers') ? old('test_answers') : $test_answers) }}"></div>
        <input type="hidden" name="qids" value="{{ implode(',', $qids) }}" />
    </div>
    @if ($errors->has('questions.*') || $errors->has('answers.*') || $errors->has('test_answers.*.*'))
    <div class="col-md-12 text-red form-group">Answers and questions fields are required. (Question max length - 200. Answer max length - 200)</div>
    @endif
</div>

<div class="row" id="video-duration-section">
    <div class="col-sm-4">
        <div class="form-group">
            {{ Form::label('video_duration', 'Video duration: ', ['class' => 'required']) }}
            {!! Form::select('video_duration', ['15' => '15 seconds', '30' => '30 seconds'], $dto->getCurrentVideoDuration(), ['class' => 'form-control', 'placeholder' => 'Select video duration']) !!}
            @if ($errors->has('video_duration'))
            <div class="text-red">{{ $errors->first('video_duration') }}</div>
            @endif
        </div>
    </div>
</div>

<div class="row" id="qr_row" style="display: none;">
    <div class="col-sm-4">
        <a href="{{ url('admin/qr-code/' . $qrString) }}" target="_blank" style="margin-bottom: 20px;">
            <img src="{{ url('admin/qr-code/' . $qrString) }}" style="max-width: 75px;" />
            <br />
            View Large
        </a>
        {!! Form::hidden('qr_string', $qrString) !!}
    </div>
</div>

<div class="row" id="map_row" style="display: none;">
    <div class="col-sm-4">
        <div class="form-group">
            {{ Form::label('location_latlng', 'Search Address: ') }}
            <input id="autocomplete" name="location" placeholder="Enter address to search" type="text" class="form-control" value="{{ $dto->getCurrentLocation() }}" />
            {!! Form::hidden('location_latlng', $dto->getCurrentLatLng(), ['id' => 'location_latlng']) !!}
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            {{ Form::label('location_radius', 'Radius: ', ['class' => 'required']) }}
            {!! Form::select('location_radius', $dto->getRadiuses(), $dto->getCurrentRadius(), ['id' => 'radius', 'class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-sm-12">
        <div id="map_canvas" style="width:100%; height:250px;"></div>
    </div>
</div>

<div class="row">
    <div class="col-sm-4">
        <div class="form-group">
            {!! Form::label('cover-image', 'Cover image', ['class' => 'required']) !!}
        </div>
        <div class="form-group dropzone challenge-logo-dropzone dz-clickable"></div>
        @if ($errors->has('image'))
        <div class="text-red">{{ $errors->first('image') }}</div>
        @endif
        {!! Form::hidden('image', $dto->getCurrentImage()) !!}
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <label>&nbsp;</label>
        </div>
        @if ($dto->getCurrentImage())
        <img class="dashboard-image" src="{{$dto->getCurrentImage()}}">
        @elseif (old('image'))
        <img class="dashboard-image" src="{{Storage::url(str_replace(env('AWS_URL'), '', old('image')))}}">
        @else
        <img class="dashboard-image" style="display:none" src="">
        @endif
    </div>
</div>

<!-- Submit Field -->
<div class="form-group text-right">
    {!! Form::submit('Save', ['class' => 'btn btn-success']) !!}
</div>

<script src="/build/js/ckeditor/ckeditor.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDc0H6iXPd9ta8HUzURxqHyLD4Slhv50DI&libraries=places&callback=initialize" async defer></script>
<script>
var lat = {{ empty($lat_lng[0]) ? 21.5582185 : $lat_lng[0] }},
    lng = {{ empty($lat_lng[1]) ? 39.1486315 : $lat_lng[1] }};
var map, ac, marker, circle;

function initialize() {
    map = new google.maps.Map(document.getElementById("map_canvas"), {
        center: new google.maps.LatLng(lat, lng),
        zoom: 17,
        disableDefaultUI: false,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    ac = new google.maps.places.Autocomplete((document.getElementById('autocomplete')));

    marker = new google.maps.Marker({
        position: new google.maps.LatLng(lat, lng),
        map: map,
        draggable: true
    });

    circle = new google.maps.Circle({
        strokeColor: "#FF0000",
        strokeOpacity: 0.8,
        strokeWeight: 1,
        fillColor: "#FF0000",
        fillOpacity: 0.35,
        map,
        center: new google.maps.LatLng(lat, lng),
        radius: parseInt($('#radius').val()),
    });

    google.maps.event.addListener(ac, 'place_changed', function() {
        var place = ac.getPlace();

        lat = place.geometry.location.lat();
        lng = place.geometry.location.lng();

        var latlng = new google.maps.LatLng(lat, lng);
        marker.setPosition(latlng);
        map.setCenter(latlng);
        circle.setCenter(latlng);

        $('#location_latlng').val(lat + ',' + lng);
    });

    google.maps.event.addListener(marker, 'dragend', function() {
        var position = marker.getPosition();

        lat = position.lat();
        lng = position.lng();

        var latlng = new google.maps.LatLng(lat, lng);
        circle.setCenter(latlng);

        $('#location_latlng').val(lat + ',' + lng);
    });

    $('#location_latlng').val(lat + ',' + lng);
}

$(function() {
    $('.select2').select2({
        theme: 'bootstrap',
        width: '100%'
    });

    $('#item_count_fld, #proof_type').on('change', function() {
        if($.inArray($('#proof_type').val(), ['questions', 'tests', 'true_false']) !== -1) {
            addQuestionProofFields($('#item_count_fld').val());
        }
    });

    $('#proof_type').trigger('change');

    $('#radius').change(function(){
        circle.setRadius(parseInt($(this).val()));
    });

    $(document).on('change', '#company_id, #country_id', function(){
        fetchAllowedFeatures();
    });

    fetchAllowedFeatures();

    function fetchAllowedFeatures() {
        var company = $('#company_id').val();
        var country = $('#country_id').val();
        var proof_type = $('#proof_type');
        var dependent = $('#dependent_challenge');

        $.ajax({
            url: '{{ route('get-allowed-features') }}',
            type: 'POST',
            dataType: 'JSON',
            data: {
                id: 0,
                company: company,
                country: country,
                current_type: '{{ $current_type }}',
                _token: '{{ csrf_token() }}'
            },
            success: function(result){
                proof_type.empty();
                dependent.empty();

                proof_type.append($('<option>', {value:'', text:'Select proof type'}));
                dependent.append($('<option>', {value:'', text:'None'}));

                $.each(result.types, function(i, v){
                    proof_type.append($('<option>', {value:i, text:v}));
                })

                $.each(result.dependent, function(i, v){
                    dependent.append($('<option>', {value:i, text:v}));
                })

                proof_type.val(proof_type.attr('data-val'));
                dependent.val(dependent.attr('data-val'));

                proof_type.trigger('change');

                if(result.locking) {
                    $('#dependent_challenge_div').show();
                } else {
                    $('#dependent_challenge_div').hide();
                }
            }
        });
    }

    function addQuestionProofFields(quantity) {
        let additionalHtml = '';
        let textProofsBlock = $('#text-proofs-block');
        let oldQuestions = textProofsBlock.data('questions');
        let oldAnswers = textProofsBlock.data('answers');
        let oldTests = textProofsBlock.data('tests');
        let selectedType = $('#proof_type').val();

        for (let i = 0; i < quantity; i++) {
            if (oldAnswers || oldQuestions || oldTests) {
                if (selectedType === 'tests') {
                    additionalHtml += formTestingProofFields(
                        i,
                        oldQuestions[i] ? oldQuestions[i] : '',
                        oldTests !== null ? oldTests[i] ? oldTests[i] : ['', '', '', ''] : ['', '', '', '']
                    )
                } else if (selectedType === 'questions') {
                    additionalHtml += formQuestionProofFields(
                        i,
                        oldQuestions[i] ? oldQuestions[i] : '',
                        oldAnswers !== null ? oldAnswers[i] ? oldAnswers[i] : '' : ''
                    )
                } else if (selectedType === 'true_false') {
                    additionalHtml += formTrueFalseProofFields(
                        i,
                        oldQuestions[i] ? oldQuestions[i] : '',
                        oldAnswers !== null ? oldAnswers[i] ? oldAnswers[i] : '' : ''
                    )
                }
            } else {
                if (selectedType === 'tests') {
                    additionalHtml += formTestingProofFields(i)
                } else if (selectedType === 'questions') {
                    additionalHtml += formQuestionProofFields(i)
                } else if (selectedType === 'true_false') {
                    additionalHtml += formTrueFalseProofFields(i)
                }
            }
        }

        if(additionalHtml) {
            additionalHtml += '<hr />';
        }

        textProofsBlock.html(additionalHtml)
    }

    function formQuestionProofFields(index, oldQuestion = '', oldAnswer = '') {
        return "\n" +
            "<hr />\n" +
            "<div class=\"row\">\n" +
            "    <div class=\"col-md-4\">\n" +
            "        <div class=\"form-group\">\n" +
            "            <textarea class=\"form-control\" name=\"questions[" + index + "]\" type=\"text\" placeholder=\"Enter question\" rows=\"2\">" + oldQuestion + "</textarea>\n" +
            "        </div>\n" +
            "    </div>\n" +
            "    <div class=\"col-md-4\">\n" +
            "        <div class=\"form-group\">\n" +
            "            <textarea class=\"form-control\" name=\"answers[" + index + "]\" type=\"text\" placeholder=\"Enter answers (comma seperated)\" rows=\"2\">" + oldAnswer + "</textarea>\n" +
            "        </div>\n" +
            "    </div>\n" +
            "</div>\n" +
            "\n";
    }

    function formTestingProofFields(index, oldQuestion = '', oldAnswer = ['', '', '', '']) {
        let answer1 = oldAnswer[0] === null ? '' : oldAnswer[0];
        let answer2 = oldAnswer[1] === null ? '' : oldAnswer[1];
        let answer3 = oldAnswer[2] === null ? '' : oldAnswer[2];
        let answer4 = oldAnswer[3] === null ? '' : oldAnswer[3];

        return "\n" +
            "<hr />\n" +
            "<div class=\"row\">\n" +
            "    <div class=\"col-md-4\">\n" +
            "        <div class=\"form-group\">\n" +
            "            <textarea rows='3' class=\"form-control\" name=\"questions[" + index + "]\" type=\"text\" placeholder=\"Enter question\">" + oldQuestion + "</textarea>\n" +
            "        </div>\n" +
            "    </div>\n" +
            "    <div class=\"col-md-8\">\n" +
            "        <div class=\"row\">\n" +
            "            <div class=\"col-md-4\">\n" +
            "                <div class=\"form-group\">\n" +
            "                    <input class=\"form-control\" value=\"" + answer1 + "\" name=\"test_answers[" + index + "][0]\" type=\"text\" placeholder=\"Enter correct answer\">\n" +
            "                </div>\n" +
            "            </div>\n" +
            "        </div>\n" +
            "        <div class=\"row\">\n" +
            "            <div class=\"col-md-4\">\n" +
            "                <div class=\"form-group\">\n" +
            "                    <input class=\"form-control\" value=\"" + answer2 + "\" name=\"test_answers[" + index + "][1]\" type=\"text\" placeholder=\"Enter incorrect answer\">\n" +
            "                </div>\n" +
            "            </div>\n" +
            "            <div class=\"col-md-4\">\n" +
            "                <div class=\"form-group\">\n" +
            "                    <input class=\"form-control\" value=\"" + answer3 + "\" name=\"test_answers[" + index + "][2]\" type=\"text\" placeholder=\"Enter incorrect answer\">\n" +
            "                </div>\n" +
            "            </div>\n" +
            "            <div class=\"col-md-4\">\n" +
            "                <div class=\"form-group\">\n" +
            "                    <input class=\"form-control\" value=\"" + answer4 + "\" name=\"test_answers[" + index + "][3]\" type=\"text\" placeholder=\"Enter incorrect answer\">\n" +
            "                </div>\n" +
            "            </div>\n" +
            "        </div>\n" +
            "    </div>\n" +
            "</div>\n" +
            "\n";
    }

    function formTrueFalseProofFields(index, oldQuestion = '', oldAnswer = '') {
        return "\n" +
            "<hr />\n" +
            "<div class=\"row\">\n" +
            "    <div class=\"col-md-4\">\n" +
            "        <div class=\"form-group\">\n" +
            "            <textarea class=\"form-control\" name=\"questions[" + index + "]\" type=\"text\" placeholder=\"Enter question\">" + oldQuestion + "</textarea>\n" +
            "        </div>\n" +
            "    </div>\n" +
            "    <div class=\"col-md-2\">\n" +
            "        <div class=\"form-group\">\n" +
            "            <select class=\"form-control\" name=\"answers[" + index + "]\">" +
            "                <option value=\"True\">True</option>\n" +
            "                <option value=\"False\" " + (oldAnswer == 'False' ? 'selected' : '') + ">False</option>\n" +
            "            </select>\n" +
            "        </div>\n" +
            "    </div>\n" +
            "</div>\n" +
            "\n";
    }

    $('.ckeditor').each(function() {
        var id = $(this).attr('id');
        var lang = $(this).attr('data-lang');

        ClassicEditor.create(document.querySelector('#' + id), {
                toolbar: {
                    items: [
                        'bold',
                        'italic',
                        'underline',
                        '|',
                        'numberedList',
                        'bulletedList',
                        '|',
                        'strikethrough',
                        'subscript',
                        'superscript'
                    ]
                },
                language: lang,
                licenseKey: '',
            })
            .then(editor => {
                window.editor = editor;
            })
            .catch(error => {});
    })
})
</script>
