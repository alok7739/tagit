@if(auth()->user()->isSuperAdmin())
    <hr />
    <div class="row">
        <div class="col-sm-4">
            <form method="GET">
                <select style="width: 100%" class="form-control bd-10 select2" name="company_id" onchange="submit()">
                    <option value="">Filter by companies</option>
                    @foreach($companies as $company)
                    <option value="{{ $company->id }}" {{ request()->get('company_id') == $company->id ? 'selected' : '' }}>{{ $company->name }}</option>
                    @endforeach
                </select>
            </form>
        </div>
        <div class="col-sm-4">
            <form method="GET">
                <select style="width: 100%" class="form-control bd-10 select2" name="country" onchange="submit()">
                    <option value="">Filter by countries</option>
                    @foreach($countries as $country)
                    <option value="{{ $country->name }}" {{ request()->get('country') == $country->name ? 'selected' : '' }}>{{ $country->name }}</option>
                    @endforeach
                </select>
            </form>
        </div>
        <div class="col-sm-4">
            @if(request()->get('company_id') || request()->get('country'))
            <a class="btn btn-default" href="/admin/challenge">Clear Filters</a>
            @endif
        </div>
    </div>
    <hr />
@endif
{!! $dataTable->table(['width' => '100%']) !!}

@section('script')
<script>
    $(document).ready(function(){
        $('.select2').select2({
            theme: 'bootstrap'
        });
    });
</script>
    {!! $dataTable->scripts() !!}
@endsection
