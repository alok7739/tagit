<?php


namespace App\Modules\Challenges\Models;

use App\Modules\Users\User\Models\User;
use App\Modules\Challenges\Models\Challenge;
use Illuminate\Database\Eloquent\Model;

class Reports extends Model
{
    protected $fillable = [
        'user_id',
        'challenge_id',
        'reason'
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function challenge()
    {
        return $this->belongsTo(Challenge::class);
    }
}
