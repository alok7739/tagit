<?php
/**
 * Created by Maksym Ignatchenko, Appus Studio LP on 25.02.19
 *
 */

namespace App\Modules\Challenges\Models;

use App\Models\BaseModel;
use App\Modules\Challenges\Enums\ChallengeStatusEnum;
use App\Modules\Challenges\Enums\ProofTypeEnum;
use App\Modules\Challenges\Helpers\AvailableMimeTypeForProofItemHelper;
use App\Modules\Challenges\Helpers\MaxSizeProofItemHelper;
use App\Modules\Challenges\Interfaces\AbleToContainProofs;
use App\Modules\Companies\Models\Company;
use App\Modules\Users\Services\UserEnvironmentService\Enums\UserEnvironmentEnum;
use App\Modules\Users\Services\UserEnvironmentService\Interfaces\EnvironmentAble;
use App\Modules\Users\User\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class Challenge extends BaseModel implements AbleToContainProofs
{
    public const PARTICIPATION_COST = 10;
    protected const DEFAULT_LIMIT = 10;

    /**
     * @var array
     */
    public $fillable = [
        'company_id',
        'name',
        'name_ar',
        'image',
        'description',
        'description_ar',
        'link',
        'country',
        'city',
        'participants_limit',
        'proof_type',
        'items_count_in_proof',
        'video_duration',
        'start_date',
        'end_date',
        'status',
        'qr_string',
        'qr_url',
        'location',
        'location_latlng',
        'location_radius',
        'location_bound',
        'feed_visible',
        'dependent_challenge',
        'is_library'
    ];

    /**
     * @var array
     */
    protected $appends = [
        'participants_count',
        'is_participated',
        'participation_cost',
        'likes_count',
        'is_liked',
        'comments_count',
        'my_proof',
    ];

    /**
     * @var array
     */
    protected $casts = [
        'start_date' => 'datetime:U',
        'end_date' => 'datetime:U',
        'created_at' => 'datetime:U',
        'updated_at' => 'datetime:U',
        'location_bound' => 'boolean'
    ];

    /**
     * @param $value
     * @return string|null
     */
    public function getImageAttribute($value): ?string
    {
        return $value ? Storage::url(str_replace(env('AWS_URL'), '', $value)) : null;
    }

    /**
     * @return string
     */
    public function getImageWithDefaultAttribute($value): ?string
    {
        return $value ? Storage::url($value) : '/assets/images/default_challenge.svg';
    }

    /**
     * @param $attribute
     */
    public function setImageAttribute($attribute)
    {
        if (null !== $this->image) {
            Storage::delete($this->image);
        }

        $this->attributes['image'] = $attribute;
    }

    /**
     * @param $attribute
     */
    public function setStartDateAttribute($attribute)
    {
        $this->attributes['start_date'] = Carbon::parse($attribute)->format('Y-m-d H:i:s');
    }

    /**
     * @param $attribute
     */
    public function setEndDateAttribute($attribute)
    {
        $this->attributes['end_date'] = Carbon::parse($attribute)->format('Y-m-d H:i:s');
    }

    /**
     * @return int
     */
    public function getParticipantsCountAttribute(): int
    {
        return $this->participants()->count();
    }

    /**
     * @return int
     */
    public function getParticipationCostAttribute(): int
    {
        return self::PARTICIPATION_COST;
    }

    public function getLikesCountAttribute(): int
    {
        return $this->likes()->count();
    }

    public function getCommentsCountAttribute(): int
    {
        return $this->comments()->count();
    }

    // public function getProofTypeAttribute($value)
    // {
    //     return $value == ProofTypeEnum::TRUE_FALSE ? ProofTypeEnum::TESTS : $value;
    // }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function participants()
    {
        return $this->belongsToMany(User::class, 'challenge_user', 'challenge_id', 'user_id')->withTimestamps();
    }

    public function likes()
    {
        return $this->belongsToMany(User::class, 'challenge_like', 'challenge_id', 'user_id')->withTimestamps();
    }

    public function comments()
    {
        return $this->belongsToMany(User::class, 'challenge_comment', 'challenge_id', 'user_id')->withTimestamps();
    }

    public function questions()
    {
        return $this->hasMany(Question::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function proofs()
    {
        return $this->hasMany(Proof::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function company()
    {
        return $this->belongsTo(Company::class)
            ->withDefault([
                'name' => null,
                'type' => 'none',
            ]);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    /**
     * @return bool
     */
    public function getIsParticipatedAttribute(): bool
    {
        return $this->participants()
            ->wherePivot('user_id', Auth::id())
            ->get()
            ->isNotEmpty();
    }

    public function getIsLikedAttribute(): bool
    {
        return $this->likes()
            ->wherePivot('user_id', Auth::id())
            ->get()
            ->isNotEmpty();
    }

    /**
     * @return Proof|null
     */
    public function getMyProofAttribute(): ?Proof
    {
        $latestProof = $this->
            proofs()
            ->where('user_id', Auth::id())
            ->orderBy('created_at', 'desc')
            ->first();
        return $latestProof ?? null;
    }

    /**
     * @param User $user
     * @param string|null $search
     * @param int|null $limit
     * @return iterable
     */
    public static function searchForCountryEnvironment(User $user, ?string $search, ?int $limit): iterable
    {
        $ids = self::getAvailableChallengeIds($user);

        $query = self::orderBy('start_date', 'DESC')
            ->whereIn('id', $ids)
            ->where('country', '=', $user->country)
            ->where('status', '!=', 'archive')
            ->withCompletedProofs()
            ->withMyParticipation();

        if ($search) {
            $query = $query::where('name', 'like', "%{$search}%");
        }

        return $query->paginate($limit ?? self::DEFAULT_LIMIT);
    }

    /**
     * @param User $user
     * @param string|null $search
     * @param int|null $limit
     * @return iterable
     */
    public static function searchForCompanyEnvironment(User $user, ?string $search, ?int $limit): iterable
    {
        $ids = self::getAvailableChallengeIds($user);

        $query = self::orderBy('start_date', 'DESC')
            ->whereIn('id', $ids)
            ->where('company_id', '=', $user->company_id)
            ->where('status', '!=', 'archive')
            ->withCompletedProofs()
            ->withMyParticipation();

        if ($search) {
            $query = $query::where('name', 'like', "%{$search}%");
        }

        return $query->paginate($limit ?? self::DEFAULT_LIMIT);
    }

    public static function getAvailableChallengeIds(User $user)
    {
        $wk = $wv = '';
        if($user->company_id) {
            $wk = 'challenges.company_id';
            $wv = $user->company_id;
        } else {
            $wk = 'challenges.country';
            $wv = $user->country;
        }

        $ids1 = self::select(['id'])
                    ->where($wk, '=', $wv)
                    ->where('status', '!=', 'archive')
                    ->whereNull('dependent_challenge')
                    ->get();

        $ids2 = self::select(['challenges.id'])
                    ->join('proofs', 'proofs.challenge_id', 'challenges.dependent_challenge')
                    ->where('proofs.user_id', '=', $user->id)
                    ->where('proofs.status', '=', 'accepted')
                    ->where($wk, '=', $wv)
                    ->where('challenges.status', '!=', 'archive')
                    ->where('challenges.dependent_challenge', '>', '0')
                    ->get();

        $ids1 = array_pluck($ids1, 'id', 'id');
        $ids2 = array_pluck($ids2, 'id', 'id');

        $ids = array_merge($ids1, $ids2);
        $ids = array_unique($ids);

        return $ids;
    }

    /**
     * @param $query
     */
    public function scopeWithCompletedProofs($query)
    {
        $query->withCount(['proofs as completed_count' => function ($q) {
            $q->where('status', 'accepted');
        }]);
    }

    /**
     * @param $query
     */
    public function scopeWithMyParticipation($query)
    {
        $query->with(['participants' => function ($q) {
            $q->where('user_id', Auth::id());
        }]);
    }

    /**
     * @return int
     */
    public function getCompletedProofsCount(): int
    {
        return $this->proofs->where('status', 'accepted')->count();
    }

    /**
     * @return bool
     */
    public function enoughFreePlaces(): bool
    {
        return ($this->participants_limit === 0) || ($this->participants_limit > $this->participants_count);
    }

    /**
     * @return string
     */
    public function getRequiredProofsType(): string
    {
        return $this->proof_type;
    }

    /**
     * @return int
     */
    public function getRequiredProofsCount(): int
    {
        return $this->items_count_in_proof;
    }

    /**
     * @return int|null
     */
    public function getRequiredVideoDuration(): ?int
    {
        return $this->video_duration;
    }

    /**
     * @return array
     */
    public function getAvailableProofItemsMimeType() : array
    {
        return AvailableMimeTypeForProofItemHelper::get($this->proof_type);
    }

    /**
     * @return int
     */
    public function getMaxSizeProofItemsMimeType() : int
    {
        return MaxSizeProofItemHelper::get($this->proof_type);
    }

    /**
     * @return int
     */
    public function getCurrentProofsCountAttribute() : int
    {
        return $this->proofs()->where('challenge_id', $this->id)->count();

    }

    /**
     * @return bool
     */
    public function checkForActiveStatus() : bool
    {
        return ChallengeStatusEnum::ACTIVE === $this->status;
    }

    /**
     * @param $query
     * @return Builder
     */
    public function scopeShouldBeActivated($query) : Builder
    {
        $now = Carbon::now();
        return $query->where('start_date', '<=', $now)->where('end_date', '>', $now)->where('status', '<>', ChallengeStatusEnum::ACTIVE);
    }

    /**
     * @param $query
     * @return Builder
     */
    public function scopeShouldBeEnded($query) : Builder
    {
        $now = Carbon::now();
        return $query->where('end_date', '<=', $now)->where('status', '<>', ChallengeStatusEnum::END);
    }

    /**
     * @param $query
     * @param EnvironmentAble $user
     * @return Builder
     */
    public function scopeEnvironment($query, EnvironmentAble $user) : Builder
    {
        switch ($user->getCurrentEnvironment()) {
            case UserEnvironmentEnum::COUNTRY:
                return $query->where('country', $user->country)->whereNull('company_id');
            case UserEnvironmentEnum::COMPANY:
                return $query->where('company_id', $user->company_id);
        }

    }

    public function handleStatuses() : void
    {
        $this->changeStatuses($this->shouldBeActivated()->get(), ChallengeStatusEnum::ACTIVE);
        $this->changeStatuses($this->shouldBeEnded()->get(), ChallengeStatusEnum::END);
    }

    /**
     * @param Collection $challenges
     * @param string $newStatus
     */
    private function changeStatuses(Collection $challenges, string $newStatus) : void
    {
        // Sorry for db queries in loop.
        // This makes it possible to use challenge observer to create feeds.
        // Also, this will not cause problems with DB performance due to the low intensity of this operation.
        foreach($challenges as $challenge) {
            if ($challenge->status === ChallengeStatusEnum::ARCHIVE) {
                continue;
            }

            $challenge->status = $newStatus;
            $challenge->save();
        }
    }

    /**
     * @param int $newLimit
     * @return bool
     */
    public function isAbleToChangeParticipantsLimit(int $newLimit) : bool
    {
        return $this->participants_count <= $newLimit;
    }

    /**
     * @return bool
     */
    public function isAbleToChangeProofType() : bool
    {
        return $this->current_proofs_count === 0;
    }

    /**
     * @return bool
     */
    public function isAbleToChangeProofsCount() : bool
    {
        return $this->current_proofs_count === 0;
    }

    /**
     * @return bool
     */
    public function isAbleToChangeVideoDuration() : bool
    {
        return $this->current_proofs_count === 0;
    }

    /**
     * @return bool
     */
    public function isActive() : bool
    {
        return ChallengeStatusEnum::ACTIVE === $this->status;
    }

    /**
     * @return bool
     */
    public function isEnded() : bool
    {
        return ChallengeStatusEnum::END === $this->status;
    }

    /**
     * @return bool
     */
    public function mayHaveResults() : bool
    {
        return in_array(
            $this->status,
            [
                ChallengeStatusEnum::ACTIVE,
                ChallengeStatusEnum::END,
            ]
        );
    }

    /**
     * @param int|null $fromPosition
     * @param int|null $limit
     * @return Collection
     */
    public function getAcceptedProofs(int $fromPosition = null, int $limit = null) : Collection
    {
        $fromPosition = $fromPosition ? $fromPosition - 1 : 0;
        $limit = $limit ? $limit : config('custom.results_count_per_page');
        return $this
            ->proofs()
            ->accepted()
            ->with('user')
            ->orderBy('position')
            ->offset($fromPosition)
            ->limit($limit)
            ->get();
    }

    /**
     * @return Proof|null
     */
    public function getMyAcceptedProof() : ?Proof
    {
        return $this->proofs()->accepted()->my()->with('user')->first();
    }

    public function saveQuestions(array $entities)
    {
        foreach ($entities['questions'] as $index => $question) {
            $question = Question::create([
                'challenge_id' => $this->id,
                'text' => $question,
            ]);
            Answer::create([
                'question_id' => $question->id,
                'text' => $entities['answers'][$index],
                'type' => Answer::CORRECT,
            ]);
        }
    }

    public function saveTests(array $entities)
    {
        foreach ($entities['questions'] as $qIndex => $question) {
            $question = Question::create([
                'challenge_id' => $this->id,
                'text' => $question,
            ]);

            foreach ($entities['test_answers'][$qIndex] as $aIndex => $answer) {
                if ($aIndex === 0) {
                    $entity = [
                        'question_id' => $question->id,
                        'text' => $answer,
                        'type' => Answer::CORRECT,
                    ];
                } else {
                    $entity = [
                        'question_id' => $question->id,
                        'text' => $answer,
                        'type' => Answer::INCORRECT,
                    ];
                }

                Answer::create($entity);
            }
        }
    }

    public function saveTrueFalse(array $entities)
    {
        foreach ($entities['questions'] as $index => $question) {
            $question = Question::create([
                'challenge_id' => $this->id,
                'text' => $question,
            ]);
            Answer::create([
                'question_id' => $question->id,
                'text' => $entities['answers'][$index],
                'type' => Answer::CORRECT,
            ]);
            Answer::create([
                'question_id' => $question->id,
                'text' => $entities['answers'][$index] == 'True' ? 'False' : 'True',
                'type' => Answer::INCORRECT,
            ]);
        }
    }
}
