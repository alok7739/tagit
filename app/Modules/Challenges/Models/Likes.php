<?php


namespace App\Modules\Challenges\Models;

use App\Modules\Users\User\Models\User;
use App\Modules\Challenges\Models\Challenge;
use App\Modules\Feeds\Models\Feed;
use Illuminate\Database\Eloquent\Model;

class Likes extends Model
{
    public $table = 'challenge_like';

    protected $fillable = [
        'user_id',
        'challenge_id',
        'feed_id'
    ];

    protected $hidden = [
        'user_id',
        'challenge_id',
        'feed_id',
        'updated_at',
    ];

    protected $casts = [
        'created_at' => 'datetime:U',
        'updated_at' => 'datetime:U',
    ];

    protected $with = [
        'user'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function challenge()
    {
        return $this->belongsTo(Challenge::class);
    }

    public function feed()
    {
        return $this->belongsTo(Feed::class);
    }
}
