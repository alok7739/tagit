<?php
/**
 * Created by Maksym Ignatchenko, Appus Studio LP on 17.04.19
 *
 */

namespace App\Modules\Challenges\Models;

use Illuminate\Support\Facades\Auth;

class ChallengeWithoutAppends extends Challenge
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'challenges';

    /**
     * @var array
     */
    protected $appends = [
        'is_participated',
        'likes_count',
        'is_liked',
        'comments_count',
    ];

    public function getLikesCountAttribute(): int
    {
        return $this->likes()->count();
    }

    public function getCommentsCountAttribute(): int
    {
        return $this->comments()->count();
    }

    public function getIsLikedAttribute(): bool
    {
        return $this->likes()
            ->wherePivot('user_id', Auth::id())
            ->get()
            ->isNotEmpty();
    }
}