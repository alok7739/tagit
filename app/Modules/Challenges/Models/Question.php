<?php


namespace App\Modules\Challenges\Models;


use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $fillable = [
        'challenge_id',
        'text',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    protected static function boot()
    {
        parent::boot(); // TODO: Change the autogenerated stub
    }

    public function challenge()
    {
        return $this->belongsTo(Challenge::class);
    }

    public function answers()
    {
        return $this->hasMany(Answer::class);
    }
}
