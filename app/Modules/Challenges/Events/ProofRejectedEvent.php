<?php
/**
 * Created by Maksym Ignatchenko, Appus Studio LP on 16.09.19
 *
 */

namespace App\Modules\Challenges\Events;

use App\Modules\Challenges\Models\Challenge;
use App\Modules\Challenges\Notifications\ProofRejectedNotification;
use App\Modules\Notifications\Events\Interfaces\NotifiableEventInterface;
use Illuminate\Support\Collection;

class ProofRejectedEvent implements NotifiableEventInterface
{
    /**
     * @var Collection
     */
    private $users;

    /**
     * @var Challenge
     */
    private $challenge;

    /**
     * ProofRejectedEvent constructor.
     * @param Collection $users
     * @param Challenge $challenge
     */
    public function __construct(Collection $users, Challenge $challenge)
    {
        $this->users = $users;
        $this->challenge = $challenge;
    }

    /**
     * @return Collection
     */
    public function getNotifiables(): Collection
    {
        return $this->users;
    }

    public function getNotification()
    {
        return new ProofRejectedNotification($this->challenge);
    }

}
