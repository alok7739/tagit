<?php
/**
 * Created by Maksym Ignatchenko, Appus Studio LP on 16.09.19
 *
 */

namespace App\Modules\Challenges\Events;

use App\Modules\Challenges\Models\Challenge;
use App\Modules\Challenges\Notifications\ChallengeStartedNotification;
use App\Modules\Notifications\Events\Interfaces\NotifiableEventInterface;
use Illuminate\Support\Collection;

class ChallengeStartedEvent implements NotifiableEventInterface
{
    /**
     * @var Challenge
     */
    private $challenge;

    /**
     * ProofRejectedEvent constructor.
     * @param Collection $users
     * @param Challenge $challenge
     */
    public function __construct(Challenge $challenge)
    {
        $this->challenge = $challenge;
    }

    /**
     * @return Collection
     */
    public function getNotifiables(): Collection
    {
        return $this->challenge->users;
    }

    public function getNotification()
    {
        return new ChallengeStartedNotification($this->challenge);
    }

}
