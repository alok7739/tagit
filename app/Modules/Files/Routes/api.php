<?php

Route::group(['middleware' => ['api', 'auth:api', 'auth.active']], function () {
    Route::get('files', 'FilesController@index');
});
