<?php

namespace App\Modules\Files\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Modules\Challenges\Models\Proof;
use App\Services\ResponseBuilder\CustomResponseBuilder;
use Symfony\Component\HttpFoundation\Response;

class FilesController extends Controller
{
    public function index() : Response
    {
        $user = \Auth::user();
        $limit = request()->get('limit') ?? config('custom.files_count_per_page');

        $company_id = $country_id = null;

        if($user->company_id) {
            $company_id = $user->company_id;
        } else {
            $country_id = $user->country;
        }

        $files = Proof::select(['proofs.id AS proof_id', 'proofs.challenge_id', 'challenges.name', 'challenges.name_ar', 'proofs.type', 'proofs.status', 'proofs.items'])
                        ->join('challenges', 'challenges.id', 'proofs.challenge_id')
                        ->where('proofs.user_id', $user->id)
                        ->whereJsonLength('proofs.items', '>', 0);

        if($user->company_id) {
            $files->where('challenges.company_id', $user->company_id);
        } else {
            $files->where('challenges.country', $user->country);
        }

        $files = $files->orderBy('proofs.id', 'DESC')
                    ->limit($limit)
                    ->get();

        return CustomResponseBuilder::success($files);
    }
}
