<?php

namespace App\Modules\Misc\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Misc\Http\Requests\ContactRequest;
use App\Modules\Misc\Models\Contact;
use Illuminate\Support\Facades\Mail;

class ContactController extends Controller
{
    public function index()
    {
        return view('contact.index');
    }

    public function store(ContactRequest $request)
    {
        $inputs = $request->only(['name', 'email', 'message']);

        $contact  = new Contact;
        $contact->fill($inputs);
        $contact->save();

        $contact_email = env('CONTACTUS_ADMIN_EMAIL');

        if($contact_email) {
            $inputs['msg'] = $inputs['message'];

            Mail::send('contact.emails.admin', $inputs, function($message) use($contact_email, $inputs) {
                $message->to($contact_email, '')
                    ->replyTo($inputs['email'], $inputs['name'])
                    ->subject('Contact Us Query');
            });
        }

        return redirect('/contact-us/thankyou');
    }

    public function thankyou()
    {
        return view('contact.thankyou');
    }
}
