<?php

namespace App\Modules\Misc\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Modules\Misc\Datatables\ContactDataTable;
use App\Modules\Misc\Models\Contact;
use Laracasts\Flash\Flash;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param ContactDataTable $dataTable
     * @return mixed
     */
    public function index(ContactDataTable $dataTable)
    {
        return $dataTable->render('contact.admin.index');
    }

    /**
     * @param Contact $contact
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function view(int $id)
    {
        $contact = Contact::findOrFail($id);

        if (null === $contact) {
            flash('No such record');
            return redirect()->route('contactus.index');
        }

        return view('contact.admin.view', [
            'contact' => $contact
        ]);
    }

    /**
     * @param Contact $contact
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function destroy(int $id)
    {
        $contact = Contact::findOrFail($id);

        if (null === $contact) {
            flash('No such record');
            return redirect()->route('contactus.index');
        }

        $contact->delete();

        Flash::success('Contact us query deleted successfully.');

        return redirect()->route('contactus.index');
    }
}
