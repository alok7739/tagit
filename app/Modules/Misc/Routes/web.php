<?php
Route::get('/contact-us', 'ContactController@index')->name('contactus');
Route::post('/contact-us', 'ContactController@store');
Route::get('/contact-us/thankyou', 'ContactController@thankyou');
