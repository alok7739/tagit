<?php
Route::group(['middleware' => 'auth:admin'], function () {
    Route::get('contactus', 'ContactController@index')->name('contactus.index');
    Route::get('contactus/{contactus}', 'ContactController@view')->name('contactus.view')->middleware('can:view,App\Modules\Misc\Models\Contact');
    Route::delete('contactus/{contactus}', 'ContactController@destroy')->name('contactus.destroy')->middleware('can:destroy,App\Modules\Misc\Models\Contact');
});