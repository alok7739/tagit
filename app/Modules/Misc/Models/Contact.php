<?php

namespace App\Modules\Misc\Models;

use Illuminate\Database\Eloquent\Model as Model;

/**
 * Class Contact
 * @package App\Models
 * @version January 12, 2021, 4:00 pm +03
 *
 * @property string name
 * @property string email
 * @property string message
 */
class Contact extends Model
{
    public $table = 'contact_us';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public $fillable = [
        'name',
        'email',
        'message'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'email' => 'string',
        'message' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'email' => 'required',
        'message' => 'required'
    ];

    
}
