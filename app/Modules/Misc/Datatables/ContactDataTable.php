<?php
namespace App\Modules\Misc\Datatables;

use App\Modules\Misc\Models\Contact;
use Illuminate\Database\Eloquent\Builder;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Builder as YajraBuilder;
use Yajra\DataTables\Services\DataTable;

class ContactDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Result from query() method.
     * @return EloquentDataTable
     */
    public function dataTable($query): EloquentDataTable
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable
            ->addColumn('action', 'contact.admin.datatables_actions')
            ->editColumn('message', function ($query) {
                return strlen($query->message) > 50 ? (substr($query->message, 0, 50) . '...') : $query->message;
            })
            ->editColumn('created_at', function($query) {
                return \Carbon::parse($query->created_at)->format('Y-m-d H:i');
            })
            ->rawColumns(['name', 'email', 'message', 'created_at', 'action']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param Packages $model
     * @return Packages
     */
    public function query(Contact $model): Builder
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return YajraBuilder
     */
    public function html(): YajraBuilder
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax(url()->current())
            ->addAction(['width' => '10%'])
            ->parameters([
                'dom'     => 'frtip',
                'order'   => [[3, 'desc']],
                'responsive' => true,
                'drawCallback' => '
                    function() {
                        $.SweetAlert.init();

                    }',
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns(): array
    {
        return [
            [
                'name' => 'name',
                'data' => 'name',
                'title' => 'Name',
                'width' => '20%',
            ],
            [
                'name' => 'email',
                'data' => 'email',
                'title' => 'Email',
                'width' => '20%',
            ],
            [
                'name' => 'message',
                'data' => 'message',
                'title' => 'Message',
            ],
            [
                'name' => 'created_at',
                'data' => 'created_at',
                'title' => 'Received At',
                'width' => '15%',
            ],
        ];
    }
}
