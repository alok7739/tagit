@extends('layouts.app-blank')
@section('title', 'Contact Us')

@section('content')
<section class="login-screen new-game-screen h-120">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-8 col-md-offset-2">
                <div class="login-white-div mt-180">
                    <h2><span>Thank you!</span></h2>
                    <p class="text-center">Your message has been successfully sent. We will contact you very soon!</p>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection