@extends('layouts.app')
@section('title', 'View Contact Us Query')

@section('content')
    <section class="content">
        <div class="clearfix"></div>

        @include('flash::message')
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-body">
                        <h3 class="new_heading">Contact Us Query</h3>

                        <table class="table table-bordered table-striped">
                            <tbody>
                                <tr>
                                    <th width="20%">Name</th>
                                    <td>{{ $contact->name }}</td>
                                </tr>
                                <tr>
                                    <th>Email Address</th>
                                    <td><a href="mailto:{{ $contact->email }}">{{ $contact->email }}</a></td>
                                </tr>
                                <tr>
                                    <th>Received At</th>
                                    <td>{{ date('d/M/Y h:i a', strtotime($contact->created_at)) }}</td>
                                </tr>
                                <tr>
                                    <th>Message</th>
                                    <td>{{ $contact->message }}</td>
                                </tr>
                            </tbody>
                        </table>

                        <a href="{{ route('contactus.index') }}" class='btn btn-primary'>Back</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
