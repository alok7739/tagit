@extends('layouts.app')
@section('title', 'Contact Us Queries')

@section('content')
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box">
            <div class="box-body">
                <h3 class="new_heading">Contact Us Queries</h3>
                @include('contact.admin.table')
            </div>
        </div>
    </div>
@endsection
