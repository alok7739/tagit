<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td style="padding: 0px;text-align: left;font-size: 14px; color: #6f6f6f;font-family: 'Arial', sans-serif;">
            <p style="padding: 0;margin: 0;">Dear Admin,</p>
        </td>
    </tr>
    <tr>
        <td style="padding: 20px 0 20px 0; color: #6f6f6f;font-size: 14px;font-family: 'Arial', sans-serif;">
            <p style="padding: 0;margin: 0;">A new contact us query is received, details are as follow:</p>
        </td>
    </tr>
    <tr>
        <td style="padding: 0; color: #6f6f6f;font-size: 14px;font-family: 'Arial', sans-serif;">
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td style="padding-bottom: 10px; padding-right: 10px" width="20%">Name:</td>
                    <td style="padding-bottom: 10px; padding-right: 10px">{{ $name }}</td>
                </tr>
                <tr>
                    <td style="padding-bottom: 10px; padding-right: 10px">Email:</td>
                    <td style="padding-bottom: 10px; padding-right: 10px">{{ $email }}</td>
                </tr>
                <tr>
                    <td style="padding-bottom: 10px; padding-right: 10px">Message:</td>
                    <td style="padding-bottom: 10px; padding-right: 10px">{{ $msg }}</td>
                </tr>
            </table>
        </td>
    </tr>
</table>
