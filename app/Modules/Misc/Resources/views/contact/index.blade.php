@extends('layouts.app-blank')
@section('title', 'Contact Us')

@section('content')
<section class="login-screen new-game-screen h-120">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-8 col-md-offset-2">
                <div class="login-white-div mt-180">
                    <h2><span>Contact Us</span></h2>
                    <form id="contactFrm" name="contact" action="" method="POST">
                        @csrf
                        <div class="row">
                            <div class="col-lg-6 col-md-6">
                                <div class="form-group">
                                    <input id="name" type="text" class="form-control" name="name" placeholder="Your Name" maxlength="50" />
                                    @if ($errors->has('name'))
                                    <div class="text-red">{{ $errors->first('name') }}</div>
                                    @endif
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="form-group">
                                    <input id="email" type="email" class="form-control" name="email" placeholder="Email Address" maxlength="150" />
                                    @if ($errors->has('email'))
                                    <div class="text-red">{{ $errors->first('email') }}</div>
                                    @endif
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12">
                                <div class="form-group">
                                    <textarea class="form-control" id="messages" name="message" rows="5" placeholder="Message" maxlength="1000"></textarea>
                                    @if ($errors->has('message'))
                                    <div class="text-red">{{ $errors->first('message') }}</div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 text-center">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection