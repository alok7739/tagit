<?php


namespace App\Modules\Misc\Policies;

use App\Modules\Users\Admin\Models\Admin;

class ContactPolicy
{
    public function view(Admin $admin)
    {
        return $admin->isAdmin();
    }

    public function destroy(Admin $admin)
    {
        return $admin->isAdmin();
    }
}
