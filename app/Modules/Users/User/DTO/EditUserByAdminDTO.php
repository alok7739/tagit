<?php
/**
 * Created by Maksym Ignatchenko, Appus Studio LP on 03.09.19
 *
 */

namespace App\Modules\Users\User\DTO;

use App\Modules\Challenges\Enums\CountryEnum;
use App\Modules\Users\User\Models\User;
use DB;

class EditUserByAdminDTO
{
    /**
     * @var User
     */
    protected $user;

    /**
     * @var array
     */
    protected $companies;

    public function __construct(User $user, array $companies)
    {
        $this->user = $user;
        $this->companies = $companies;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->user->id;
    }

    /**
     * @return string|null
     */
    public function getFullName(): ?string
    {
        return $this->user->full_name;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->user->email;
    }

    /**
     * @return string|null
     */
    public function getBirthday(): ?string
    {
        return $this->user->getBirthdaySimpleFormat();
    }

    /**
     * @return string|null
     */
    public function getSex(): ?string
    {
        return $this->user->sex;
    }

    /**
     * @return array
     */
    public function getAvailableGenderOptions(): array
    {
        return [
            '' => '',
            'Male' => 'Male',
            'Female' => 'Female',
        ];
    }

    /**
     * @return string|null
     */
    public function getCountry(): ?string
    {
        return $this->user->country;
    }

    /**
     * @return array
     */
    public function getAvailableCountries(): array
    {
        $countries = DB::table('countries')
                        ->orderBy('sort_order', 'desc')
                        ->orderBy('name', 'asc')
                        ->get(['id', 'name']);

        return ['' => ''] + array_pluck($countries, 'name', 'name');
    }

    /**
     * @return string|null
     */
    public function getCity(): ?string
    {
        return $this->user->city;
    }

    /**
     * @return string|null
     */
    public function getReferralCode(): ?string
    {
        return $this->user->referral_code;
    }

    /**
     * @return int|null
     */
    public function getUserCompanyId(): ?int
    {
        return $this->user->getUserCompanyId();
    }

    /**
     * @return array
     */
    public function getAllCommercialCompanies(): array
    {
        return $this->companies;
    }

    /**
     * @return string|null
     */
    public function getAvatar(): ?string
    {
        return $this->user->avatar;
    }

    /**
     * @return string
     */
    public function getUserDefaultAvatarUrl(): string
    {
        return $this->user->avatar ?? $this->user->avatar_with_default;
    }

    /**
     * @return string|null
     */
    public function getAvatarPath(): ?string
    {
        return $this->user->getAvatarPath();
    }

    /**
     * @return string|null
     */
    public function getDefaultAvatar(): ?string
    {
        return $this->user->default_avatar;
    }
}
