<?php
/**
 * Created by Vitaliy Shabunin, Appus Studio LP on 01.04.2020
 */

namespace App\Modules\Users\User\Rules;

use App\Modules\Users\User\Models\User;
use App\Services\ResponseBuilder\ValidationErrorCode;
use Illuminate\Contracts\Validation\Rule;

class ExistsEmailRule implements Rule
{

    /**
     * Determine if the validation rule passes.
     *
     * @param  string $attribute
     * @param  mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $user = User::getUserCaseNotSensitive($value);

        if (null === $user) {
            return false;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        if (request()->wantsJson()) {
            return ValidationErrorCode::EXISTS;
        }

        return trans('validation.exists');
    }

}
