<?php
/**
 * Created by Vitaliy Shabunin, Appus Studio LP on 01.04.2020
 */

namespace App\Modules\Users\User\Rules;

use App\Modules\Users\User\Models\User;
use App\Services\ResponseBuilder\ValidationErrorCode;
use Illuminate\Contracts\Validation\Rule;

class UniqueEmailRule implements Rule
{

    protected $ignoreId;
    protected $where;

    public function __construct(int $ignoreId = null)
    {
        $this->ignoreId = $ignoreId;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string $attribute
     * @param  mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $query = User::where('email', 'ilike', $value);

        if (null !== $this->where) {
            $callback = $this->where;
            $callback($query);
        }

        $user = $query->first();

        if (null !== $user && $user->id !== $this->ignoreId) {
            return false;
        }

        return true;
    }

    public function where(\Closure $callback)
    {
        $this->where = $callback;

        return $this;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        if (request()->wantsJson()) {
            return ValidationErrorCode::UNIQUE;
        }

        return trans('validation.email_exists');
    }

}
