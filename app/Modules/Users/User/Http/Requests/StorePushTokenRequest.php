<?php
/**
 * Created by Maksym Ignatchenko, Appus Studio LP on 09.09.19
 *
 */

namespace App\Modules\Users\User\Http\Requests;

use App\Modules\Users\User\Enums\DeviceTypeEnum;
use App\Services\ResponseBuilder\ValidationErrorsApiMessagesTrait;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StorePushTokenRequest extends FormRequest
{
    use ValidationErrorsApiMessagesTrait;

    /**
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'device_id' => 'required|string|min:10|max:255',
            'token' => 'required|string|min:10|max:255',
            'type' => [
                'required',
                'min:1',
                'max:255',
                Rule::in(DeviceTypeEnum::toArray())
            ],
        ];
    }
}
