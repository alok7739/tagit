<?php
/**
 * Created by Maksym Ignatchenko, Appus Studio LP on 04.09.19
 *
 */

namespace App\Modules\Users\User\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AbstractUpdateUserProfileRequest extends FormRequest
{
    /**
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'avatar' => 'nullable|string|max:100',
            'full_name' => 'nullable|string|max:100',
            'birthday' => 'nullable',
            'sex' => 'nullable|string|max:50',
            'country' => 'nullable|string|max:50',
            'city' => 'nullable|string|max:50',
        ];
    }
}
