<?php
/**
 * Created by Maksym Ignatchenko, Appus Studio LP on 04.09.19
 *
 */

namespace App\Modules\Users\User\Http\Requests;

class CreateUserByAdminRequest extends AbstractUpdateUserProfileRequest
{
    /**
     * @return array
     */
    public function rules(): array
    {
        $rules = [
            'email' => "nullable|email|max:50|unique:users,email",
            'country_code' => 'required|string|max:3',
            'phone_number' => 'required|string|max:10|unique_with:users,country_code',
            'password' => [
                'required',
                'min:8',
                'max:50',
                'confirmed'
            ],
        ];
        return array_merge(parent::rules(), $rules);
    }
}
