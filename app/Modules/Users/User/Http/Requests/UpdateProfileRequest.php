<?php
/**
 * Created by PhpStorm.
 * User: artem.petrov
 * Date: 2019-02-19
 * Time: 14:49
 */

namespace App\Modules\Users\User\Http\Requests;

use App\Modules\Users\User\Rules\UniqueEmailRule;
use App\Services\ResponseBuilder\ValidationErrorsApiMessagesTrait;

class UpdateProfileRequest extends AbstractUpdateUserProfileRequest
{
    use ValidationErrorsApiMessagesTrait;

    /**
     * @return array
     */
    public function rules(): array
    {
        $rules = [
            'email' => [
                'nullable',
                'email',
                'max:50',
                new UniqueEmailRule($this->user()->id ?? null),
            ],
            'avatar' => 'file|max:' . config('custom.user_avatar_max_size'),
        ];
        return array_merge(parent::rules(), $rules);
    }
}
