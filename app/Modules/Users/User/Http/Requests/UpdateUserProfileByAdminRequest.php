<?php
/**
 * Created by Maksym Ignatchenko, Appus Studio LP on 04.09.19
 *
 */

namespace App\Modules\Users\User\Http\Requests;

class UpdateUserProfileByAdminRequest extends AbstractUpdateUserProfileRequest
{
    /**
     * @return array
     */
    public function rules(): array
    {
        $rules = [
            'email' => "nullable|email|max:50|unique:users,email,{$this->user->id}",
            'referral_code' => [
                'required',
                'string',
                'size:' . config('custom.referral_code_length'),
                "unique:users,referral_code,{$this->user->id}",
            ]
        ];
        return array_merge(parent::rules(), $rules);
    }
}
