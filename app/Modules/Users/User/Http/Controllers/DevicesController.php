<?php
/**
 * Created by Maksym Ignatchenko, Appus Studio LP on 09.09.19
 *
 */

namespace App\Modules\Users\User\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Users\User\Http\Requests\StorePushTokenRequest;
use App\Modules\Users\User\Models\Device;
use App\Services\ResponseBuilder\ApiCode;
use App\Services\ResponseBuilder\CustomResponseBuilder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class DevicesController extends Controller
{
    public function store(StorePushTokenRequest $request)
    {
        $user = $request->user();
        app(Device::class)->updateOrCreate(
            ['device_id' => $request->device_id],
            [
                'token' => $request->token,
                'type' => $request->type,
                'user_id' => $user->id,
            ]
        );
        return CustomResponseBuilder::success();
    }

    public function destroy(Request $request, Device $device)
    {
        if (Gate::forUser($request->user())->allows('destroy-device', $device)) {
            $device->delete();
            return CustomResponseBuilder::success();
        }
        return CustomResponseBuilder::error(ApiCode::UNAUTHORISED_ACTION);
    }
}
