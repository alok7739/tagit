<?php

namespace App\Modules\Users\User\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Companies\Models\Company;
use App\Modules\Users\Services\UserEnvironmentService\Interfaces\UserEnvironmentServiceInterface;
use App\Services\ResponseBuilder\CustomResponseBuilder;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\Response;
use Johntaa\Arabic\I18N_Arabic;
use Image;

class RatingController extends Controller
{
    /**
     * @param UserEnvironmentServiceInterface $userEnvironmentService
     * @return Response
     */
    public function index(UserEnvironmentServiceInterface $userEnvironmentService): Response
    {
        return CustomResponseBuilder::success($userEnvironmentService->getRating());
    }

    public function certificate(UserEnvironmentServiceInterface $userEnvironmentService)
    {
        $user = \Auth::user();
        $rating = $userEnvironmentService->getRating();

        $company = $user->country;

        if($user->company_id > 0) {
            $company = Company::find($user->company_id);
            $company = $company ? $company->name : $user->country;
        }

        $arabic = new I18N_Arabic('Glyphs');
        $name = $user->full_name ? $arabic->utf8Glyphs($user->full_name) : '';
        $rank = $rating['my_rating']['position'];
        $points = $rating['my_rating']['total_reward'];
        $company = $company ? $arabic->utf8Glyphs($company) : '';

        $fileame_en = "certificates/" . $user->id . "/" . time() . "_en.png";
        $fileame_ar = "certificates/" . $user->id . "/" . time() . "_ar.png";

        // English Certificate
        $img = Image::make(public_path('certificate_template/en.png'));

        $img->text($name, 375, 130, function ($font) {
            $font->file(public_path('certificate_template/Amiri.ttf'));
            $font->size(30);
            $font->color('#000000');
            $font->align('center');
            $font->valign('top');
            $font->angle(0);
        });

        $img->text($rank, 375, 265, function ($font) {
            $font->file(public_path('certificate_template/Amiri.ttf'));
            $font->size(40);
            $font->color('#ffffff');
            $font->align('center');
            $font->valign('top');
            $font->angle(0);
        });

        $img->text($points, 375, 473, function ($font) {
            $font->file(public_path('certificate_template/Amiri.ttf'));
            $font->size(10);
            $font->color('#000000');
            $font->align('center');
            $font->valign('top');
            $font->angle(0);
        });

        $img->text($company, 375, 546, function ($font) {
            $font->file(public_path('certificate_template/Amiri.ttf'));
            $font->size(20);
            $font->color('#000000');
            $font->align('center');
            $font->valign('top');
            $font->angle(0);
        });

        Storage::disk('s3')->put($fileame_en, $img->stream());
        $en = Storage::disk('s3')->url($fileame_en);

        // Arabic Certificate
        $img = Image::make(public_path('certificate_template/ar.png'));

        $img->text($name, 375, 123, function ($font) {
            $font->file(public_path('certificate_template/Amiri.ttf'));
            $font->size(30);
            $font->color('#000000');
            $font->align('center');
            $font->valign('top');
            $font->angle(0);
        });

        $img->text($rank, 375, 265, function ($font) {
            $font->file(public_path('certificate_template/Amiri.ttf'));
            $font->size(40);
            $font->color('#ffffff');
            $font->align('center');
            $font->valign('top');
            $font->angle(0);
        });

        $img->text($points, 375, 472, function ($font) {
            $font->file(public_path('certificate_template/Amiri.ttf'));
            $font->size(10);
            $font->color('#000000');
            $font->align('center');
            $font->valign('top');
            $font->angle(0);
        });

        $img->text($company, 375, 548, function ($font) {
            $font->file(public_path('certificate_template/Amiri.ttf'));
            $font->size(20);
            $font->color('#000000');
            $font->align('center');
            $font->valign('top');
            $font->angle(0);
        });

        Storage::disk('s3')->put($fileame_ar, $img->stream());
        $ar = Storage::disk('s3')->url($fileame_ar);

        return CustomResponseBuilder::success([
            'image_ar' => $ar,
            'image_en' => $en,
            'subject_en' => '',
            'subject_ar' => '',
            'text_en' => '',
            'text_ar' => '',
        ]);
    }
}
