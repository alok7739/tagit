<?php
/**
 * Created by Artem Petrov, Appus Studio LP on 10.11.2017
 */

namespace App\Modules\Users\User\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Modules\Users\User\Models\User;
use App\Modules\Users\User\Rules\ExistsEmailRule;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;

class ResetPasswordController extends Controller
{
    use ResetsPasswords;

    /**
     * @return string
     */
    public function redirectTo()
    {
        return route('password.success');
    }

    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Reset the given user's password.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    public function reset(Request $request)
    {
        $request->validate($this->rules(), $this->validationErrorMessages());

        $params = $request->only('email', 'password', 'token');

        $tokenReset = \DB::table(config('auth.passwords.users.table'))
            ->where('email', 'ilike', $params['email'])
            ->first();

        $response = Password::INVALID_TOKEN;

        if (Hash::check($params['token'], $tokenReset->token)) {
            $user = User::getUserCaseNotSensitive($params['email']);
            $this->resetPassword($user, $params['password']);
            $response = Password::PASSWORD_RESET;
            \DB::table(config('auth.passwords.users.table'))
                ->where('email', 'ilike', $params['email'])
                ->delete();
        }

        // If the password was successfully reset, we will redirect the user back to
        // the application's home authenticated view. If there is an error we can
        // redirect them back to where they came from with their error message.
        return $response == Password::PASSWORD_RESET
            ? $this->sendResetResponse($request, $response)
            : $this->sendResetFailedResponse($request, $response);
    }

    /**
     * Get the password reset validation rules.
     *
     * @return array
     */
    protected function rules()
    {
        return [
            'token' => 'required',
            'email' => [
                'required',
                'email',
                'max:100',
                new ExistsEmailRule(),
            ],
            'password' => [
                'required',
                'confirmed',
                'min:8',
                'max:50',
            ]
        ];
    }

    /**
     * Reset the given user's password.
     *
     * @param  \Illuminate\Contracts\Auth\CanResetPassword $user
     * @param  string $password
     *
     * @return void
     */
    protected function resetPassword($user, $password)
    {
        $user->password = $password;
        $user->save();

        event(new PasswordReset($user));

        $this->guard()->login($user);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function success()
    {
        return view('auth.passwords.success');
    }
}
