<?php
/**
 * Created by Maksym Ignatchenko, Appus Studio LP on 25.06.19
 *
 */

namespace App\Modules\Users\User\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Companies\Models\Company;
use App\Modules\Onboarding\Models\CompanyPackages;
use App\Modules\Users\User\Http\Requests\JoinCompanyRequest;
use App\Modules\Users\User\Models\UserCodes;
use App\Services\ResponseBuilder\ApiCode;
use App\Services\ResponseBuilder\CustomResponseBuilder;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use DB;

class CompanyParticipationController extends Controller
{
    public function index(JoinCompanyRequest $request)
    {
        $companyJoinCode = $request->get('company_join_code');
        $company = $companyJoinCode ? app(Company::class)->getCompanyByJoinCode($companyJoinCode) : '';

        if($companyJoinCode && !$company->id) {
            return CustomResponseBuilder::error(ApiCode::VALIDATION_ERRORS);
        }

        if($company) {
            $package = CompanyPackages::find($company->package_id);

            // if($package->users && $package->users >= $company->users_count) {
            //     // $already_join = DB::table('challenge_user')->where('', $->get();

            //     return CustomResponseBuilder::error(ApiCode::PARTICIPANTS_LIMIT_EXCEEDED);
            // }
        }

        $user = Auth::user();
        $user->company_id = $company ? $company->id : null;
        $user->save();

        $youtube = null;
        $company_name = $companyJoinCode;

        if($company) {
            if($company->youtube_link) {
                $already_join = UserCodes::where(['company_id' => $company->id, 'user_id' => $user->id])->first();
                $youtube = $already_join ? $youtube : $company->youtube_link;
            }

            if($company->id) {
                UserCodes::updateOrCreate(
                    ['company_id' => $company->id, 'user_id' => $user->id],
                    ['company_id' => $company->id, 'user_id' => $user->id]
                );
            }

            $company_name = $company->name;
        }

        $all_codes = UserCodes::where('user_id', $user->id)->get();
        $codes = [];

        foreach($all_codes as $c) {
            $codes[] = $c->company->join_code;
        }

        return CustomResponseBuilder::success([
            'company' => $company_name,
            'youtube' => $youtube,
            'codes' => $codes
        ]);
    }

    public function removeCode(Request $request)
    {
        $joinCode = $request->get('join_code');
        $company = app(Company::class)->getCompanyByJoinCode($joinCode);

        if($joinCode && $company->id) {
            $user = Auth::user();
            UserCodes::where(['company_id' => $company->id, 'user_id' => $user->id])->delete();
        }

        return CustomResponseBuilder::success([]);
    }
}
