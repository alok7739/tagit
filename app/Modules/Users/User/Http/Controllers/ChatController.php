<?php

namespace App\Modules\Users\User\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Users\Admin\Models\Countries;
use App\Modules\Users\User\Http\Requests\ChatMessagesRequest;
use App\Modules\Users\User\Models\ChatMessages;
use App\Services\ResponseBuilder\CustomResponseBuilder;
use App\Services\ResponseBuilder\ApiCode;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class ChatController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index()
    {
        $user = Auth::user();
        $limit = config('custom.chat_messages_count_per_page');
        
        $company_id = $country_id = null;
        $chat = $messages = [];

        if($user->company_id) {
            $company_id = $user->company_id;
        } else {
            $country_id = Countries::where('name', $user->country)->first();
            $country_id = $country_id ? $country_id->id : null;
        }

        $cid = 0;
        if($country_id) {
            $chat = ChatMessages::where('country_id', $country_id)
                        ->orderByDesc('id')
                        ->limit($limit)
                        ->get();
            
            $cid = 'country' . $country_id;
        }
        elseif($company_id) {
            $chat = ChatMessages::where('company_id', $company_id)
                        ->orderByDesc('id')
                        ->limit($limit)
                        ->get();
            
            $cid = 'company' . $company_id;
        }

        foreach($chat as $c) {
            $messages[] = $this->prepareMessage($c);
        }

        $messages = array_reverse($messages);

        return CustomResponseBuilder::success(['cid' => $cid, 'messages' => $messages]);
    }

    public function store(ChatMessagesRequest $request)
    {
        /** @var User $user */
        $user = Auth::user();
        
        $message = $request->get('message');

        $company_id = $country_id = null;

        if($user->company_id) {
            $company_id = $user->company_id;
        } else {
            $country_id = Countries::where('name', $user->country)->first();
            $country_id = $country_id ? $country_id->id : null;            
        }

        $user_id = $user ? $user->id : null;
        $admin_id = null;
        
        $challege = app(ChatMessages::class)->create([
            'company_id' => $company_id,
            'country_id' => $country_id,
            'user_id' => $user_id,
            'admin_id' => $admin_id,
            'message' => $message,
        ]);

        return CustomResponseBuilder::success($this->prepareMessage($challege));
    }

    public function destroy(ChatMessages $chat) : Response
    {
        $user = Auth::user();
        
        if ($chat->user_id && $chat->user_id !== $user->id) {
            return CustomResponseBuilder::error(ApiCode::UNAUTHENTICATED);
        }

        $chat->delete();

        return CustomResponseBuilder::success();
    }

    private function prepareMessage($row) {
        $user = Auth::user();

        if($row->user_id) {
            $name = $row->user->full_name;
            $avatar = $row->user->avatar;
        } else {
            if($row->admin->company_id) {
                $name = $row->admin->company->name;
                $name = $row->admin->company->logo;
            } else {
                $name  = 'Admin';
                $avatar = '';
            }
        }

        $avatar = $avatar ?? 'https://i.ibb.co/HDHsj5q/default-avatar.png';
        $avatar = $row->admin_id ? 'https://i.ibb.co/HDHsj5q/default-avatar.png' : $avatar;

        $msg = [
            "id"       => $row->id,
            "time"     => strtotime($row->created_at),
            "message"  => $row->message,
            "user_id"  => $row->user_id ? $row->user_id : $row->admin_id,
            "is_admin" => $row->admin_id ? true : false,
            "name"     => $name,
            "avatar"   => $avatar
        ];

        return $msg;
    }
}
