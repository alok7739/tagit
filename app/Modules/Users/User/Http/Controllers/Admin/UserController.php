<?php
/**
 * Created by Andrei Podgornyi, Appus Studio LP on 08.10.2018
 */

namespace App\Modules\Users\User\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Modules\Companies\Models\Company;
use App\Modules\Notifications\DTO\EntityRelatedManualMessage;
use App\Modules\Notifications\DTO\SimpleMessageWithFiltersDTO;
use App\Modules\Notifications\Events\MessageSentEvent;
use App\Modules\Notifications\Requests\ManualMessageRequest;
use App\Modules\Users\User\DataTables\UserDataTable;
use App\Modules\Users\User\DTO\EditUserByAdminDTO;
use App\Modules\Users\User\Export\UserExportFactory;
use App\Modules\Users\User\Http\Requests\CreateUserByAdminRequest;
use App\Modules\Users\User\Http\Requests\UpdateUserProfileByAdminRequest;
use App\Modules\Users\User\Jobs\ResetPublicChallengesCoins;
use App\Modules\Users\User\Models\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class UserController extends Controller
{
    /**
     * Display a listing of the Customer.
     *
     * @param AdminDataTable $userDataTable
     * @return mixed
     */
    public function index(UserDataTable $userDataTable)
    {
        return $userDataTable->render('index');
    }

    /**
     * Display the specified Customer.
     *
     * @param User $user
     * @return \Illuminate\Contracts\View\Factory|View
     */
    public function show(User $user)
    {
        return view('show')->with('user', $user);
    }

    public function export(Request $request)
    {
        $columns = [
            'points' => 'coins',
            'country' => 'country',
            'phone' => DB::raw("CONCAT(country_code,phone_number) AS phone"),
            'email' =>'email',
            'gender' => 'sex',
            'name' => 'full_name',
        ];
        list($file, $name, $headers) = UserExportFactory::get($request->get('type', 'csv'))->prepareExport($columns);
        return response()->download($file, $name, $headers);
    }
    /**
     * @param User $user
     * @return \Illuminate\Contracts\View\Factory|View
     */
    public function edit(User $user)
    {
        $companiesAssocArray = ['' => '']
            + Company::commercial()
            ->get(['id', 'name'])
            ->pluck('name', 'id')
            ->toArray();
        $dto = new EditUserByAdminDTO($user, $companiesAssocArray);
        return view('edit')->with('dto', $dto);
    }

    public function store(CreateUserByAdminRequest $request) : RedirectResponse
    {
        User::create($request->all() + ['is_registration_completed' => true]);
        flash("User have been created");

        return redirect()->route('users.index');
    }

    public function create()
    {
        return view('create');
    }

    public function update(UpdateUserProfileByAdminRequest $request, User $user) : RedirectResponse
    {
        $user->fill($request->except('avatar'));
        $user->setAvatarPath($request->avatar);
        $user->save();
        flash("User $user->full_name have been updated");
        return redirect()->route('users.show', $user->id);
    }

    /**
     * @param User $user
     * @return RedirectResponse
     */
    public function resetCoinsAndRating(User $user) : RedirectResponse
    {
        $user->resetPublicCoinsAndRating();

        flash('Points and rating have been reset');
        return redirect()->back();
    }

    /**
     * @return RedirectResponse
     */
    public function resetCoinsForPublicChallenges() : RedirectResponse
    {
        // app(User::class)->resetCoinsForPublicChallenges();
        dispatch(new ResetPublicChallengesCoins(0));

        flash('Points and rating for public challenges have been queued to reset');

        return redirect()->back();
    }

    public function resetAllCoins(): RedirectResponse
    {
        app(User::class)->resetAllCoins();
        flash('All points and rating have been reset');

        return redirect()->back();
    }

    /**
     * @param User $user
     * @return \Illuminate\Contracts\View\Factory|View
     */
    public function showUserNotificationForm(User $user) : View
    {
        $dto = new EntityRelatedManualMessage('direct-user-send-notification', 'Send message to the user', $user);
        return view('message', ['dto' => $dto]);
    }

    /**
     * @param ManualMessageRequest $request
     * @param User $user
     * @return RedirectResponse
     */
    public function sendNotification(ManualMessageRequest $request, User $user) : RedirectResponse
    {
        event(new MessageSentEvent(collect([$user]), $request->message, $request->title));
        flash('Message has been sent');
        return redirect()->route('users.index');
    }

    /**
     * @return View
     */
    public function showGroupOfUsersNotificationForm() : View
    {
        $dto = new SimpleMessageWithFiltersDTO('all-users-send-notification', 'Send message to all users');
        return view('message', ['dto' => $dto]);
    }

    /**
     * @param ManualMessageRequest $request
     * @return RedirectResponse
     */
    public function sendGroupNotification(ManualMessageRequest $request) : RedirectResponse
    {
        $usersQuery = User::query();
        if($request->country) {
            $usersQuery = $usersQuery->country($request->country);
        }
        $users = $usersQuery->get();
        event(new MessageSentEvent($users, $request->message, $request->title));
        flash('Message has been sent');
        return redirect()->route('users.index');
    }

    public function suspend(int $user_id)
    {
        $user = User::findOrFail($user_id);

        if (null === $user) {
            flash('No such user detected');
            return redirect()->route('users.index');
        }

        $user->status = '2';
        $user->save();

        flash('User has been suspended successfully');

        return redirect()->route('users.index');

    }

    public function enable(int $user_id)
    {
        $user = User::findOrFail($user_id);

        if (null === $user) {
            flash('No such user detected');
            return redirect()->route('users.index');
        }

        $user->status = '1';
        $user->save();

        flash('User has been enabled successfully');

        return redirect()->route('users.index');

    }
}
