<?php


namespace App\Modules\Users\User\Http\Controllers;


use App\Http\Controllers\Controller;
use App\Services\ResponseBuilder\CustomResponseBuilder;
use Illuminate\Support\Facades\DB;

class CountriesController extends Controller
{
    public function list()
    {
        $countries = DB::table('countries')
                        ->orderBy('sort_order', 'desc')
                        ->orderBy('name', 'asc')
                        ->get(['id', 'name']);

        return CustomResponseBuilder::success($countries);
    }
}
