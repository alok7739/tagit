<?php

namespace App\Modules\Users\User\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Companies\Models\Company;
use App\Modules\Users\Admin\Models\Admin;
use App\Modules\Users\User\Models\User;
use App\Modules\Users\User\Models\UserCodes;
use App\Services\JwtService;
use App\Services\ResponseBuilder\ApiCode;
use App\Services\ResponseBuilder\CustomResponseBuilder;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Laravel\Socialite\Facades\Socialite;
use Symfony\Component\HttpFoundation\Response;
use App\Modules\Users\Services\UserEnvironmentService\Interfaces\UserEnvironmentServiceInterface;

class AuthController extends Controller
{
    use AuthResponseTrait, ThrottlesLogins;

    public $maxAttempts = 5;
    public $decayMinutes = 5;

    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login', 'loginVia']]);
    }

    public function login(Request $request)
    {
        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        $credentials = request(['phone_number', 'password', 'country_code']);

        $user = User::where('phone_number', $credentials['phone_number'])
                        ->where('country_code', $credentials['country_code'])
                        ->first();

        if ($user && \Hash::check($credentials['password'], $user->password)) {
            if($user->status == '2') {
                return response()->json([
                    'success' => false,
                    'code' => 401,
                ], 401);
            } else {
                $credentials['status'] = '1';

                if ($token = auth()->attempt($credentials)) {
                    return CustomResponseBuilder::success($this->getTokenStructure($token));
                }
            }
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return CustomResponseBuilder::error(ApiCode::NO_SUCH_USER);
    }

    public function loginVia(Request $request, string $provider)
    {
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        if ($provider === 'apple') {
            $this->validate($request, ['token' => 'required']);
            $service = new JwtService($request->token);
            $email = $service->getEmail();
        } else {
            $providerUser = Socialite::driver($provider)->userFromToken($request->token);
            $email = $providerUser->email;
        }

        $user = User::where(['email' => $email])->first();

        if (!$user) {
            $user = User::create(['email' => $email]);
        }

        if ($token = auth()->login($user)) {
            return CustomResponseBuilder::success($this->getTokenStructure($token));
        }

        $this->incrementLoginAttempts($request);

        return CustomResponseBuilder::error(ApiCode::NO_SUCH_USER);
    }

    protected function sendLockoutResponse($request)
    {
        $seconds = $this->limiter()->availableIn(
            $this->throttleKey($request)
        );

        return CustomResponseBuilder::error(14, null, Lang::get('auth.throttle', ['seconds' => $seconds]), 429);
    }
    protected function username()
    {
        return 'phone_number';
    }
    /**
     * Get the authenticated User.
     *
     * @return Response
     */
    public function me(UserEnvironmentServiceInterface $userEnvironmentService)
    {
        $user = User::with('company')->find(auth()->id());

        $code = '';

        if($user->company) {
            $code = Company::where('id', $user->company->id)->first()->makeVisible('join_code');
            $code = $code ? $code->join_code : '';
        }

        $user->join_code = $code;

        $all_codes = UserCodes::where('user_id', $user->id)->get();
        $codes = [];

        foreach($all_codes as $c) {
            $codes[] = $c->company->join_code;
        }

        $user->codes = $codes;

        $rating = $userEnvironmentService->getRating();
        $user->coins = $rating['my_rating']['total_reward'];

        return CustomResponseBuilder::success($user);
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return Response
     */
    public function logout()
    {
        auth()->logout();

        return CustomResponseBuilder::success();
    }

    /**
     * Refresh a token.
     *
     * @return Response
     */
    public function refresh()
    {
        return CustomResponseBuilder::success(
            $this->getTokenStructure(auth()->refresh())
        );
    }
}
