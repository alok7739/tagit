<?php


namespace App\Modules\Users\User\Services\UserVerification;


use App\Modules\Users\User\Services\Message\MessageServiceInterface;
use App\Modules\Users\User\Services\UserVerification\Code\VerificationCodeRepositoryInterface;

interface UserVerificationServiceInterface
{

    public function parsePhone(string $countryCode, string $phone): string;

    public function sendVerificationCode(string $phone);

    public function checkVerificationCode(string $phone, string $code);

    public function getMessageService() : MessageServiceInterface;

    public function setMessageService(MessageServiceInterface $service);

    public function getCodeRepository() : VerificationCodeRepositoryInterface;

    public function setCodeRepository(VerificationCodeRepositoryInterface $codeRepository);
}
