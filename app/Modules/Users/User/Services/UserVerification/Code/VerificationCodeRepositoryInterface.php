<?php


namespace App\Modules\Users\User\Services\UserVerification\Code;


interface VerificationCodeRepositoryInterface
{
    public function add($key, $value);

    public function generate($key);

    public function get($key);

    public function delete($key);

}
