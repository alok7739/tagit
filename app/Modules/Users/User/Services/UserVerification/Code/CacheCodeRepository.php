<?php


namespace App\Modules\Users\User\Services\UserVerification\Code;


use Illuminate\Support\Facades\Cache;

class CacheCodeRepository implements VerificationCodeRepositoryInterface
{

    public function add($key, $value)
    {
        Cache::put($key, $value, config('auth.sms_code_ttl'));
    }

    public function generate($key)
    {
        $code = $this->generateCode();
        $this->add($key, $code);

        return $code;
    }

    public function get($key)
    {
        return Cache::get($key);
    }

    protected function generateCode() : string
    {
        return '1234';
        $firstNumber = rand(0, 9);

        return $firstNumber . rand(0, 9) . $firstNumber . rand(0, 9);
    }

    public function delete($key)
    {
        Cache::forget($key);
    }
}
