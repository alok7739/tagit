<?php
/**
 * Created by Vitaliy Shabunin, Appus Studio LP on 26.05.2020
 */

namespace App\Modules\Users\User\Services\UserVerification\Code;

use App\Modules\Users\User\Models\PhoneCode;

class DbCodeRepository implements VerificationCodeRepositoryInterface
{

    public function add($key, $value)
    {
        $this->delete($key);

        PhoneCode::create([
            'phone' => $key,
            'code' => $value,
            'expire_at' => date('Y-m-d H:i:s', strtotime('+ ' . config('auth.sms_code_ttl') . ' minutes')),
        ]);
    }

    public function generate($key)
    {
        $code = $this->generateCode();
        $this->add($key, $code);

        return $code;
    }

    public function get($key)
    {
        $res = PhoneCode::getByPhone($key);

        if (null === $res) {
            return null;
        }

        if ($this->isExpired($res)) {
            $this->delete($key);
            return null;
        }

        return $res->code;
    }

    public function delete($key)
    {
        PhoneCode::deleteByPhone($key);
    }

    /**
     * @param PhoneCode $code
     * @return bool
     */
    protected function isExpired(PhoneCode $code): bool
    {
        return time() > strtotime($code->expire_at);
    }

    protected function generateCode() : string
    {
        return '1234';
        $firstNumber = rand(0, 9);

        return $firstNumber . rand(0, 9) . $firstNumber . rand(0, 9);
    }

}
