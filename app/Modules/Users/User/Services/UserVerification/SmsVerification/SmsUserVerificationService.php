<?php


namespace App\Modules\Users\User\Services\UserVerification\SmsVerification;


use App\Modules\Users\User\Services\Message\MessageServiceInterface;
use App\Modules\Users\User\Services\UserVerification\UserVerificationServiceInterface;
use App\Modules\Users\User\Services\UserVerification\Code\VerificationCodeRepositoryInterface;

class SmsUserVerificationService implements UserVerificationServiceInterface
{

    /**
     * @var MessageServiceInterface
     */
    protected $messageService;

    /**
     * @var VerificationCodeRepositoryInterface
     */
    protected $codeRepository;

    protected $repositoryKeyPrefix = 'user.verification';

    /**
     * SmsUserVerificationService constructor.
     * @param MessageServiceInterface $messageService
     * @param VerificationCodeRepositoryInterface $codeRepository
     */
    public function __construct(MessageServiceInterface $messageService, VerificationCodeRepositoryInterface $codeRepository)
    {
        $this->messageService = $messageService;
        $this->codeRepository = $codeRepository;
    }


    public function sendVerificationCode($phone)
    {
        $code = $this->getCodeRepository()->generate($this->repositoryKeyPrefix . $phone);
        $message = __('auth.sms_verification_code', ['code' => $code]);

        $this->getMessageService()->send($phone, $message);

        return $code;
    }

    public function checkVerificationCode($phone, $code)
    {
        $res = $this->getCodeRepository()->get($this->repositoryKeyPrefix . $phone) === $code;

        if ($res) {
            $this->getCodeRepository()->delete($this->repositoryKeyPrefix . $phone);
        }

        return $res;
    }

    public function getMessageService() : MessageServiceInterface
    {
        return $this->messageService;
    }

    public function setMessageService(MessageServiceInterface $service)
    {
        $this->messageService = $service;
    }

    public function getCodeRepository() : VerificationCodeRepositoryInterface
    {
        return $this->codeRepository;
    }

    public function setCodeRepository(VerificationCodeRepositoryInterface $codeRepository)
    {
        $this->codeRepository = $codeRepository;
    }

    /**
     * @param string $countryCode
     * @param string $phone
     * @return string
     */
    public function parsePhone(string $countryCode, string $phone): string
    {
        if ('966' === $countryCode) {
            if ($this->checkStartNumber($phone)) {
                $phone = substr($phone, 1);
            }
        }
        return $countryCode . $phone;
    }

    /**
     * @param string $phone
     * @return bool
     */
    private function checkStartNumber(string $phone): bool
    {
        return '0' === substr($phone, 0, 1);
    }

}
