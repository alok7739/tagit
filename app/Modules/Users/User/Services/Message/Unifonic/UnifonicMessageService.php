<?php


namespace App\Modules\Users\User\Services\Message\Unifonic;


use App\Modules\Users\User\Services\Message\MessageServiceInterface;
use Multicaret\Unifonic\UnifonicFacade as Unifonic;

class UnifonicMessageService implements MessageServiceInterface
{

    public function send(string $phone, string $message)
    {
        return Unifonic::send($phone, $message);
    }
}
