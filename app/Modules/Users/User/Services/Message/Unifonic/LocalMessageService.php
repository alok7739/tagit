<?php


namespace App\Modules\Users\User\Services\Message\Unifonic;


use App\Modules\Users\User\Services\Message\MessageServiceInterface;
use Illuminate\Support\Facades\Cache;

class LocalMessageService implements MessageServiceInterface
{

    protected $key = 'fake_messages';

    public function send(string $phone, string $message)
    {
        Cache::put($this->key . $phone, $message, 5);
    }

    public function getMessage(string $phone)
    {
        return Cache::get($this->key . $phone);
    }
}
