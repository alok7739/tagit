<?php


namespace App\Modules\Users\User\Services\Message;


interface MessageServiceInterface
{
    public function send(string $phone, string $message);
}
