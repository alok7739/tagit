<?php
/**
 * Created by Vitaliy Shabunin, Appus Studio LP on 23.10.2019
 */

namespace App\Modules\Users\User\Models;

use Illuminate\Database\Eloquent\Model;

class PhoneCode extends Model
{

    protected $fillable = [
        'phone',
        'code',
        'expire_at',
    ];

    /**
     * @param string $phone
     * @return mixed
     */
    public static function getByPhone(string $phone)
    {
        return self::where('phone', $phone)->first();
    }

    /**
     * @param string $phone
     * @return mixed
     */
    public static function deleteByPhone(string $phone)
    {
        return self::where('phone', $phone)->delete();
    }

}
