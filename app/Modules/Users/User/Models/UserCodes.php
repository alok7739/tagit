<?php


namespace App\Modules\Users\User\Models;

use App\Modules\Companies\Models\Company;
use App\Modules\Users\User\Models\User;
use Illuminate\Database\Eloquent\Model;

class UserCodes extends Model
{
    public $table = 'user_codes';

    protected $fillable = [
        'user_id',
        'company_id'
    ];

    protected $hidden = [
        'user_id',
        'company_id',
        'updated_at',
    ];

    protected $casts = [
        'created_at' => 'datetime:U',
        'updated_at' => 'datetime:U',
    ];

    protected $with = [
        'company'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function company()
    {
        return $this->belongsTo(Company::class);
    }
}
