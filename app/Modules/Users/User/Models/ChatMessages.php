<?php


namespace App\Modules\Users\User\Models;

use App\Modules\Users\User\Models\User;
use App\Modules\Users\Admin\Models\Admin;
use App\Modules\Users\Admin\Models\Countries;
use Illuminate\Database\Eloquent\Model;

class ChatMessages extends Model
{
    protected $fillable = [
        'company_id',
        'country_id',
        'user_id',
        'admin_id',
        'message'
    ];

    protected $hidden = [
        'updated_at',
    ];

    public function company()
    {
        return $this->hasOne(Company::class, 'id', 'company_id');
    }

    public function country()
    {
        return $this->hasOne(Countries::class, 'id', 'company_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }

    
}
