<?php

namespace App\Modules\Users\User\Models;

use App\Modules\Challenges\Enums\CountryEnum;
use App\Modules\Challenges\Enums\ProofStatusEnum;
use App\Modules\Challenges\Models\Challenge;
use App\Modules\Companies\Models\Company;
use App\Modules\Challenges\Models\Proof;
use App\Modules\Files\Services\ImageService;
use App\Modules\Notifications\Traits\Notifiable;
use App\Modules\Users\Services\ApiRatingData\Rankable;
use App\Modules\Users\Services\ReferralCodeService\ReferralAble;
use App\Modules\Users\Services\UserEnvironmentService\Enums\UserEnvironmentEnum;
use App\Modules\Users\Services\UserEnvironmentService\Interfaces\EnvironmentAble;
use App\Modules\Users\User\Mails\ResetPasswordMail;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Query\Builder as QueryBuilder;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Pagination\AbstractPaginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Tymon\JWTAuth\Facades\JWTAuth;

class User extends Authenticatable implements JWTSubject, ReferralAble, CanGenerateJwtToken, Rankable, EnvironmentAble
{
    use Notifiable;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * @var integer
     */
    protected const DEFAULT_COINS_AMOUNT = 0;

    /**
     * @var null|integer
     */
    protected $total_reward;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'birthday',
    ];

    /**
     * @var array
     */
    protected $with = [
        'company',
    ];

    protected $attributes = [
        'coins' => self::DEFAULT_COINS_AMOUNT,
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'full_name',
        'email',
        'password',
        'phone_number',
        'country_code',
        'is_registration_completed',
        'avatar',
        'birthday',
        'sex',
        'country',
        'city',
        'coins',
        'referral_code',
        'company_id',
        'status'
    ];

    protected $casts = [
        'is_registration_completed' => 'boolean',
        'birthday' => 'date:U',
        'created_at' => 'date:U',
        'updated_at' => 'date:U',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'facebook_id',
        'email_verified_at',
        'pivot',
        'company_id',
    ];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    /**
     * @param string $password
     */
    public function setPasswordAttribute(string $password): void
    {
        $this->attributes['password'] = Hash::make($password);
    }

    /**
     * Sends the password reset notification.
     *
     * @param string $token
     *
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordMail($token, $this));
    }

    /**
     * @return string
     */
    public function getAvatarAttribute($value): ?string
    {
        return $value ? Storage::url($value) : asset('/assets/images/default_avatar.png');
    }

    /**
     * @return string
     */
    public function getAvatarWithDefaultAttribute($value): ?string
    {
        return $value ? Storage::url($value) : asset('/assets/images/default_avatar.png');
    }

    /**
     * @return string|null
     */
    public function getDefaultAvatarAttribute(): ?string
    {
        return asset('/assets/images/default_avatar.png');
    }

    /**
     * @return string
     */
    public function getFullPhoneNumberAttribute(): string
    {
        return $this->country_code . $this->phone_number;
    }

    /**
     * @param $attribute
     */
    public function setAvatarAttribute($attribute)
    {
        $this->deleteAvatarFile();
        $path = 'avatars';
        $imageService = new ImageService($attribute);
        $image = $imageService->orientate();
        $fileName = $path . $attribute->hashName();
        Storage::put($fileName, $image);

        $this->attributes['avatar'] = $fileName;
    }

    protected function deleteAvatarFile(): void
    {
        if (null !== $this->getAvatarPath()) {
            Storage::delete($this->getAvatarPath());
        }
    }

    /**
     * @param $attribute
     */
    public function setCompanyIdAttribute($attribute)
    {
        $this->attributes['company_id'] = $attribute;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function challenges()
    {
        return $this->belongsToMany(Challenge::class)->withTimestamps();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function proofs()
    {
        return $this->hasMany(Proof::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function points()
    {
        return $this->hasMany(Point::class);
    }

    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function devices()
    {
        return $this->hasMany(Device::class);
    }

    /**
     * @return bool
     */
    public function enoughCoinsToParticipateChallenge(): bool
    {
        return $this->coins >= Challenge::PARTICIPATION_COST;
    }

    /**
     * @param Challenge $challenge
     * @return bool
     */
    public function isAbleToSendProof(Challenge $challenge): bool
    {
        return Proof::where('challenge_id', $challenge->id)
            ->where('user_id', $this->id)
            ->whereIn('status', [ProofStatusEnum::PENDING, ProofStatusEnum::ACCEPTED])
            ->count();

    }

    /**
     * @return string
     */
    public function getCountry(): string
    {
        return $this->country ?? CountryEnum::SAUDI_ARABIA;
    }

    /**
     * @return User
     */
    public function hideForPublic(): self
    {
        return $this->makeHidden([
            'updated_at_at',
            'phone_number',
            'country_code',
            'is_registration_completed',
            // 'coins',
        ]);
    }

    public function chargeReward(int $reward): void
    {
        $this->coins += $reward;
    }

    public function chargeRewardToReferralUser(): void
    {
        // TODO implement reward referral user after adding gold coins functionality
        // $referralUser = $this->where('referral_code', $this->referral_code)->first();
    }

    public function calculateReferralCode(): void
    {
        do {
            $randomCode = str_random(config('custom.referral_code_length'));
        } while ($this->where('referral_code', $randomCode)->first());
        $this->attributes['referral_code'] = str_random(config('custom.referral_code_length'));
    }

    /**
     * @return string
     */
    public function generateToken(): string
    {
        return JWTAuth::fromUser($this);
    }

    public function resetPublicCoinsAndRating(): void
    {
        //$this->points()->withoutCompanyPoints()->delete(); //todo: implement points
        $this->challenges()->each(function (Challenge $challenge) {
            $totalRewards = $challenge->proofs()->sum('reward');
            $this->coins = $this->coins - $totalRewards;
            $this->save();
            $challenge->proofs()->update(['reward' => 0]);
        });

        $this->update(['coins' => 0]);
    }

    public function resetPrivateCoinsAndRating(int $companyId): void
    {
        $this->company->challenges->each(function (Challenge $challenge) {
            $totalRewards = $challenge->proofs()->sum('reward');
            $this->coins = $this->coins - $totalRewards;
            $this->save();
            $challenge->proofs()->update(['reward' => 0]);
        });

        //$this->points()->forCompany($companyId)->delete(); //todo: implement points
        $this->save();
    }

    /**
     * @return AbstractPaginator
     */
    public function getRating(): AbstractPaginator
    {
        return $this->prepareRatingQuery()->paginate(config('custom.rating_results_count_per_page'));
    }

    protected function prepareRatingQuery(): Builder
    {
        $sub = $this->getRewardsSubqueryForEnvironment();

        return $this::joinSub($sub, 'rewards', function($join) {
            $join->on('users.id', '=', 'rewards.user_id');
        })
            ->select([
                'id',
                'full_name',
                'avatar',
                'rewards.total_reward',
                DB::raw('ROW_NUMBER() OVER(ORDER BY rewards.total_reward DESC, rewards.total_attempt_duration ASC) AS Position, rewards.total_reward'),
                'company_id'
            ]);
    }

    public function getLiveRating()
    {
        return $this->prepareLiveRatingQuery();
    }

    protected function prepareLiveRatingQuery(): Builder
    {
        $sub = $this->getRewardsSubqueryForEnvironment();

        return $this::joinSub($sub, 'rewards', function($join) {
            $join->on('users.id', '=', 'rewards.user_id');
        })
            ->select([
                'id',
                'full_name',
                'email',
                'phone_number',
                'country',
                'avatar',
                'rewards.total_reward',
                DB::raw('ROW_NUMBER() OVER(ORDER BY rewards.total_reward DESC, rewards.total_attempt_duration ASC) AS Position, rewards.total_reward'),
                'company_id'
            ]);
    }

    /**
     * @return int
     */
    public function getCurrentPosition(): int
    {
        $sub = $this->prepareRatingQuery();

        return DB::table($this->table . ' AS current_user')->where('current_user.id', Auth::id())->joinSub($sub, 'rewards', function($join) {
            $join->on('current_user.id', '=', 'rewards.id');
        })->first(['position'])->position;
    }

    /**
     * @return Builder
     */
    protected function getRewardsSubqueryForEnvironment(): QueryBuilder
    {
        // Query for fetch all users who belong to the current environment but don't have approved proofs
        $usersWithoutProofsSubQuery = $this->select([
            DB::raw('0 as total_reward'),
            DB::raw('0 as total_attempt_duration'),
            'id',
        ])->myEnvironment()->whereDoesntHave('proofs', function($query) {
            $query->environment($this)->accepted();
        });

        // Query gor fetch all users who belong to the current environment and have approved proofs
        $usersWithProofsSubQuery = DB::query()->select([
            DB::raw('sum(proofs.reward) as total_reward'),
            DB::raw('sum(proofs.attempt_duration) as total_attempt_duration'),
            'proofs.user_id',
        ])
            ->from('challenges')
            ->where('proofs.status', ProofStatusEnum::ACCEPTED);

        switch ($this->getCurrentEnvironment()) {
            case UserEnvironmentEnum::COUNTRY:
                $usersWithProofsSubQuery->where('challenges.country', $this->country)->whereNull('company_id');
                break;
            case UserEnvironmentEnum::COMPANY:
                $usersWithProofsSubQuery->where('challenges.company_id', $this->company_id);
                break;
        }
        $usersWithProofsSubQuery->join('proofs', 'challenges.id', '=', 'proofs.challenge_id')
            ->groupBy('proofs.user_id');

        return $usersWithProofsSubQuery->union($usersWithoutProofsSubQuery);
    }

    /**
     * @return array
     */
    public function getMyPositionFormattedData(): array
    {
        $code = '';

        if($this->company) {
            $code = Company::where('id', $this->company->id)->first();
            $code = $code ? $code->join_code : '';
        }

        return [
            'id' => $this->id,
            'full_name' => $this->full_name,
            'position' => $this->getCurrentPosition(),
            'total_reward' => $this->getCurrentUserTotalReward(),
            'avatar' => $this->avatar,
            'game_code' => $code,
            'company' => $this->company,
        ];
    }

    /**
     * @param $query
     * @return Builder
     */
    public function scopeCompletedRegistration($query): Builder
    {
        return $query->where('is_registration_completed', true);
    }

    /**
     * @param $query
     * @return Builder
     */
    public function scopeMyCountry($query): Builder
    {
        return $query->where('country', $this->country)->whereNull('company_id');
    }

    /**
     * @param $query
     * @param string $country
     * @return Builder
     */
    public function scopeCountry($query, string $country): Builder
    {
        return $query->where('country', 'like', "%{$country}%");
    }

    /**
     * @param $query
     * @return Builder
     */
    public function scopeMyCompany($query): Builder
    {
        return $query->where('company_id', $this->company_id);
    }

    /**
     * @param $query
     * @return Builder
     */
    public function scopeMyEnvironment($query): Builder
    {
        switch ($this->getCurrentEnvironment()) {
            case UserEnvironmentEnum::COUNTRY:
                return $query->myCountry();
            case UserEnvironmentEnum::COMPANY:
                return $query->myCompany();
        }
    }

    /**
     * @param $query
     * @param int $companyId
     * @return Builder
     */
    public function scopeHasCompanyChallenge($query, int $companyId): Builder
    {
        return $query->whereHas('challenges', function($query) use ($companyId) {
            $query->where('company_id', $companyId);
        });
    }

    public function resetCoinsForPublicChallenges() : void
    {
        try {
            DB::beginTransaction();

            DB::table('users')->orderBy('id')->chunk(100, function ($users) {
                foreach($users as $user) {
                    User::find($user->id)->resetPublicCoinsAndRating();
                }
            });
        } catch (Exception $exception) {
            DB::rollBack();
            flash('Something went wrong..');
        }

        DB::commit();
    }

    public function resetAllCoins()
    {
        try {
            DB::beginTransaction();

            DB::table('users')->update(['coins' => self::DEFAULT_COINS_AMOUNT]);
            DB::table('proofs')->update(['reward' => 0]); //todo: ???
            //DB::table('points')->truncate(); //todo: implement points
        } catch (Exception $exception) {
            DB::rollBack();
            flash('Something went wrong..');
        }

        DB::commit();
    }

    /**
     * @return string
     */
    public function getCurrentEnvironment(): string
    {
        if ($this->company_id) {
            return UserEnvironmentEnum::COMPANY;
        }
        return UserEnvironmentEnum::COUNTRY;
    }

    /**
     * @return int
     */
    public function getCurrentUserTotalReward(): int
    {
        if (!$this->total_reward) {
            $query = $this->proofs();

            switch ($this->getCurrentEnvironment()) {
                case UserEnvironmentEnum::COUNTRY:
                    $query->whereHas('challenge', function($query){
                        $query->where('country', $this->country)->whereNull('company_id');
                    });
                    break;
                case UserEnvironmentEnum::COMPANY:
                    $query->whereHas('challenge', function($query){
                        $query->where('company_id', $this->company_id);
                    });
                    break;
            }
            $this->total_reward = $query->accepted()->sum('reward');
        }

        return $this->total_reward;
    }

    /**
     * @return string|null
     */
    public function getBirthdaySimpleFormat(): ?string
    {
        if($this->birthday) {
            return $this->birthday->format('Y-m-d');
        }
        return null;
    }

    /**
     * @return int|null
     */
    public function getUserCompanyId(): ?int
    {
        if ($company = $this->company) {
            return $company->id;
        }
        return null;
    }

    /**
     * @param string|null $avatarPath
     */
    public function setAvatarPath(?string $avatarPath): void
    {
        if ($avatarPath === $this->attributes['avatar']) {
            return;
        }
        $this->deleteAvatarFile();
        $this->attributes['avatar'] = $avatarPath;
    }

    /**
     * @return string|null
     */
    public function getAvatarPath(): ?string
    {
        return $this->getOriginal('avatar');
    }

    /**
     * Route notifications for the Apn channel.
     *
     * @return string|array
     */
    public function routeNotificationForFcm()
    {
        return $this->getDeviceTokens();
    }

    public function getDeviceTokens()
    {
        return $this->devices->all();
    }

    /**
     * @param $query
     * @param $params
     * @return mixed
     */
    public function scopeFilter($query, $params)
    {
        if (!empty($params)) {
            if (!empty($params['search'])) {
                $query->where('full_name', 'ilike', "%{$params['search']}%")
                    ->orWhere('email', 'ilike', "%{$params['search']}%")
                    ->orWhere('phone_number', 'ilike', "%{$params['search']}%")
                    ->orWhere('country', 'ilike', "%{$params['search']}%");
            }
        }
        return $query;
    }

    /**
     * @param $query
     * @param string|null $field
     * @param string|null $direction
     * @return mixed
     */
    public function scopeOrder($query, string $field = null, string $direction = null)
    {
        if (null === $field || null === $direction) {
            return $query;
        }

        return $query->orderBy($field, $direction);
    }

    /**
     * @param string $email
     * @return mixed
     */
    public static function getUserCaseNotSensitive(string $email = null)
    {
        return self::where('email', 'ilike', $email)->first();
    }

    public static function getCountriesList()
    {
        $countries = DB::table('countries')
                ->orderBy('sort_order', 'desc')
                ->orderBy('name', 'asc')
                ->get(['id', 'name']);

        return ['' => ''] + array_pluck($countries, 'name', 'name');
    }
}
