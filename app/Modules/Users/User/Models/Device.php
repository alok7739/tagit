<?php
/**
 * Created by Maksym Ignatchenko, Appus Studio LP on 06.09.19
 *
 */

namespace App\Modules\Users\User\Models;

use App\Models\BaseModel;
use Doctrine\DBAL\Query\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class Device extends BaseModel
{
    protected $primaryKey = 'device_id';

    public $incrementing = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'device_id',
        'token',
        'user_id',
        'type',
    ];

    public function resolveRouteBinding($value)
    {
        try {
            $model = $this->where($this->getRouteKeyName(), $value)->firstOrFail();
        } catch (ModelNotFoundException $exception) {
            throw $exception;
        } catch (QueryException $exception) {
            $exception = new ModelNotFoundException();
            $exception->setModel(class_basename(static::class));
            throw $exception;
        }
        return $model;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
