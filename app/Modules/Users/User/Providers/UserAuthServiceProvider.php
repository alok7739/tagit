<?php
/**
 * Created by Maksym Ignatchenko, Appus Studio LP on 09.09.19
 *
 */

namespace App\Modules\Users\User\Providers;

use App\Modules\Users\User\Services\Message\MessageServiceInterface;
use App\Modules\Users\User\Services\Message\Unifonic\LocalMessageService;
use App\Modules\Users\User\Services\Message\Unifonic\UnifonicMessageService;
use App\Modules\Users\User\Services\UserVerification\Code\CacheCodeRepository;
use App\Modules\Users\User\Services\UserVerification\Code\DbCodeRepository;
use App\Modules\Users\User\Services\UserVerification\Code\VerificationCodeRepositoryInterface;
use App\Modules\Users\User\Services\UserVerification\SmsVerification\SmsUserVerificationService;
use App\Modules\Users\User\Services\UserVerification\UserVerificationServiceInterface;
use App\Providers\AuthServiceProvider;
use Illuminate\Support\Facades\Gate;

class UserAuthServiceProvider extends AuthServiceProvider
{
    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('destroy-device', function ($user, $device) {
            return $user->id == $device->user_id;
        });
        $this->app->singleton(VerificationCodeRepositoryInterface::class, function($app){
            return new DbCodeRepository();
        });
        $this->app->singleton(MessageServiceInterface::class, function($app){
            return $this->app->isLocal()
                ? new LocalMessageService()
                : new UnifonicMessageService();
        });
        $this->app->singleton(UserVerificationServiceInterface::class, function($app){
            return new SmsUserVerificationService($app->make(MessageServiceInterface::class), $app->make(VerificationCodeRepositoryInterface::class));
        });
    }

}
