<?php
/**
 * Created by Maksym Ignatchenko, Appus Studio LP on 09.09.19
 *
 */

namespace App\Modules\Users\User\Enums;

class DeviceTypeEnum
{
    public const IOS = 'ios';
    public const ANDROID = 'android';

    /**
     * @return array
     */
    public static function toArray(): array
    {
        return [
            self::IOS,
            self::ANDROID,
        ];
    }
}
