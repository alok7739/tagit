<?php


namespace App\Modules\Users\User\Export\Type;


use App\Modules\Users\User\Export\UserExport;
use App\Modules\Users\User\Models\User;

class CsvUserExport implements UserExport
{

    /**
     * @inheritDoc
     */
    public function prepareExport(array $columns): array
    {
        $table = User::select(array_values($columns))->get()->toArray();
        $filename = "users.csv";
        $handle = fopen($filename, 'w+');
        fputcsv($handle, array_keys($columns));

        foreach($table as $row) {
            fputcsv($handle, $row);
        }

        fclose($handle);

        $headers = array(
            'Content-Type' => 'text/csv',
        );

        return [$filename, 'users.csv', $headers];
    }
}
