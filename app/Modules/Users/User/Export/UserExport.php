<?php


namespace App\Modules\Users\User\Export;


interface UserExport
{
    /**
     * @param array $columns columns to export. supports eloquent select syntax
     * @return array
     *      $file
     *      $name
     *      $headers
     */
    public function prepareExport(array $columns) : array;
}
