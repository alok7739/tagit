<?php


namespace App\Modules\Users\User\Export;


use App\Modules\Users\User\Export\Type\CsvUserExport;

class UserExportFactory
{
    protected static $exports = [
        'csv' => CsvUserExport::class,
    ];

    /**
     * @param string $type
     * @return UserExport
     * @throws \Exception
     */
    public static function get(string $type) : UserExport
    {
        if(!array_key_exists($type, self::$exports)) {
            throw new \Exception('undefined export type'); //TODO add custom exception
        }
        return new self::$exports[$type]();
    }
}
