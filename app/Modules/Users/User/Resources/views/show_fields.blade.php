<div class="row">
    <div class="col-sm-2">
        <div class="media">
            <img class="d-flex mr-3 rounded-circle img-thumbnail thumb-lg" src="{{$user->avatar ?? $user->avatar_with_default}}" alt="Generic placeholder image">
        </div>
        <p>&nbsp;</p>
        <!-- Edit Button -->
        <div class="form-group text-left">
            <a href="{{ route('users.edit', $user->id) }}" class="btn btn-primary waves-effect waves-light">Go to edit user</a>
        </div>
    </div>
    <div class="col-sm-6">
        <table class="table table-striped">
            <tr>
                <th width='30%'>Full name</th>
                <td>{{$user->full_name}}</td>
            </tr>
            <tr>
                <th>Email</th>
                <td>{{$user->email}}</td>
            </tr>
            <tr>
                <th>Phone number</th>
                <td><span class="{{$user->full_phone_number ? '' : 'text-danger'}}">{{ $user->full_phone_number ?? 'Empty' }}</span></td>
            </tr>
            <tr>
                <th>Country</th>
                <td><span class="{{$user->country ? '' : 'text-danger'}}">{{ $user->country ?? 'Empty' }}</span></td>
            </tr>
            <tr>
                <th>Date of Birth</th>
                <td><span class="{{$user->birthday ? '' : 'text-danger'}}">{{ $user->birthday ? $user->birthday->format('y-m-d') : 'Empty' }}</span></td>
            </tr>
            <tr>
                <th>Sex</th>
                <td><span class="{{$user->sex ? '' : 'text-danger'}}">{{ $user->sex ?? 'Empty' }}</span></td>
            </tr>
            <tr>
                <th>City</th>
                <td><span class="{{$user->city ? '' : 'text-danger'}}">{{ $user->city ?? 'Empty' }}</span></td>
            </tr>
            <tr>
                <th>Company</th>
                <td>
                    @if($user->company)
                        <a href="{{ route('company.show', $user->company->id) }}">
                            {{ $user->company->name }}
                        </a>
                    @else
                    <span class="text-danger">
                        Empty
                    </span>
                    @endif
                </td>
            </tr>
        </table>

    </div>
</div>
