<div class="btn-group" role="group">
    <button id="btnGroupDrop1" type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Actions
    </button>
    <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
        <a href="{{ route('users.show', $id) }}" class='dropdown-item'>
            Show
        </a>
        <a href="{{ route('users.edit', $id) }}" class='dropdown-item'>
            Edit
        </a>
        <a href="{{ route('direct-user-send-notification', $id) }}" class='dropdown-item'>
            Send message
        </a>
        <a href="#" class='dropdown-item'
           onclick="document.getElementById('reset-{{$id}}-button').click()">
            {!! Form::open(['method'=>'PUT', 'url'=> route('user-reset-coins', $id)]) !!}
            <button style="display: none;" id="reset-{{$id}}-button" data-toggle="tooltip" data-placement="top" title="Reset points and rating"
                    type="submit" class="dropdown-item"
                    onclick="return confirm('Are you sure you want reset user\'s points?');">
            </button>
            {!! Form::close() !!}
            Reset points and rating
        </a>
        @php $user = \App\Modules\Users\User\Models\User::find($id) @endphp
        @if($user->status == '1')
        <a href="#" class='dropdown-item'
        onclick="document.getElementById('suspend-{{$id}}-button').click()">
            {!! Form::open(['method'=>'POST', 'url'=> route('users.suspend', $id)]) !!}
            <button style="display: none;" id="suspend-{{$id}}-button" data-toggle="tooltip" data-placement="top" title="Suspend"
                    type="submit" class="dropdown-item"
                    onclick="return confirm('Are you sure you want suspend this reset user?');">
            </button>
            {!! Form::close() !!}
            Suspend
        </a>
        @endif

        @if($user->status == '2')
        <a href="#" class='dropdown-item'
        onclick="document.getElementById('enable-{{$id}}-button').click()">
            {!! Form::open(['method'=>'POST', 'url'=> route('users.enable', $id)]) !!}
            <button style="display:none" id="enable-{{$id}}-button" data-toggle="tooltip" data-placement="top" title="Enable"
                    type="submit" class="dropdown-item"
                    onclick="return confirm('Are you sure you want to enable this user?');">
            </button>
            {!! Form::close() !!}
            Enable
        </a>
        @endif
    </div>
</div>
