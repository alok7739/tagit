@extends('layouts.app')
@section('title', $dto->getPageTitle())
@section('content')

    <div class="content">

        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>

        {!! Form::open(['url'=> $dto->getFormAction(), 'method' => 'POST']) !!}
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-body">
                        <!-- Title Field -->
                        <div class="form-group">
                            {!! Form::label('title', 'Title ') !!}
                            {!! Form::text('title', $dto->getDefaultTitle(), ['class' => 'form-control', 'maxlength' => $dto->getTitleLimitSize()]) !!}
                            @if ($errors->has('title'))
                                <div class="alert alert-danger" role="alert"><strong>{{ $errors->first('title') }}</strong></div>
                            @endif
                        </div>

                        <!-- Message Field -->
                        <div class="form-group">
                            {!! Form::label('message', 'Message ') !!}
                            {!! Form::textarea('message', null, ['class' => 'form-control', 'id' => 'message', 'maxlength' => $dto->getMessageLimitSize()]) !!}
                            @if ($errors->has('message'))
                                <div class="alert alert-danger" role="alert"><strong>{{ $errors->first('message') }}</strong></div>
                            @endif
                        </div>

                        @if($dto->isNeedFilters())
                            <!-- Country Field -->
                                <div class="form-group">
                                    {!! Form::label('country', 'Country ') !!}
                                    {!! Form::select('country', App\Modules\Users\User\Models\User::getCountriesList(), old('country'), ['class' => 'form-control']) !!}
                                    @if ($errors->has('country'))
                                        <div class="alert alert-danger" role="alert"><strong>{{ $errors->first('country') }}</strong></div>
                                    @endif
                                </div>
                        @endif

                        <!-- Submit Field -->
                        <div class="form-group text-left">
                            {!! Form::submit('Send', ['class' => 'btn btn-primary waves-effect waves-light']) !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {!! Form::close() !!}

    </div>
@endsection
