@extends('layouts.app')
@section('title', 'Create user')
@section('content')

    <div class="content">
        <div class="clearfix"></div>
        @include('flash::message')
        <div class="clearfix"></div>
        {!! Form::open(['url'=> route('users.store'), 'method' => 'POST', 'files' => true]) !!}
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-body">
                        <h3 class="new_heading">Create User</h3>
                        @include('create_fields')
                    </div>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
@endsection

