<div class="row form-justify-container">
    <div class="col-md-6">
        <div class="box">
            <div class="box-body">
                <!-- Name Field -->
                <div class="form-group">
                    <p>
                        {{ Form::label('full_name', 'Full name: ') }}
                        {!! Form::text('full_name', null, ['class' => 'form-control', 'maxlength' => 100]) !!}
                    </p>
                    @if ($errors->has('full_name'))
                        <div class="text-red">{{ $errors->first('full_name') }}</div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="box">
            <div class="box-body">
                <!-- Email Field -->
                <div class="form-group">
                    <p>
                        {{ Form::label('email', 'Email: ') }}
                        {!! Form::text('email', null, ['class' => 'form-control', 'maxlength' => 50]) !!}
                    </p>
                    @if ($errors->has('email'))
                        <div class="text-red">{{ $errors->first('email') }}</div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row form-justify-container">
    <div class="col-md-6">
        <div class="box">
            <div class="box-body">
                <!-- Country Field -->
                <div class="form-group">
                    <p>
                        {{ Form::label('country_code', 'Country code: ',  ['class' => 'required']) }}
                        {!! Form::text('country_code', null, ['class' => 'form-control']) !!}
                    </p>
                    @if ($errors->has('country_code'))
                        <div class="text-red">{{ $errors->first('country_code') }}</div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="box">
            <div class="box-body">
                <!-- Country Field -->
                <div class="form-group">
                    <p>
                        {{ Form::label('phone_number', 'Phone number: ',  ['class' => 'required']) }}
                        {!! Form::text('phone_number', null, ['class' => 'form-control']) !!}
                    </p>
                    @if ($errors->has('phone_number'))
                        <div class="text-red">{{ $errors->first('phone_number') }}</div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row form-justify-container">
    <div class="col-md-6">
        <div class="box">
            <div class="box-body">
                <!-- Country Field -->
                <div class="form-group">
                    <p>
                        {{ Form::label('country', 'Country: ') }}
                        {!! Form::select('country', App\Modules\Users\User\Models\User::getCountriesList(), null, ['class' => 'form-control select2']) !!}
                    </p>
                    @if ($errors->has('country'))
                        <div class="text-red">{{ $errors->first('country') }}</div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row form-justify-container">
    <div class="col-md-6">
        <div class="box">
            <div class="box-body">
                <!-- Country Field -->
                <div class="form-group">
                    <p>
                        {{ Form::label('password', 'Password: ',  ['class' => 'required']) }}
                        {!! Form::password('password', ['class' => 'form-control']) !!}
                    </p>
                    @if ($errors->has('password'))
                        <div class="text-red">{{ $errors->first('password') }}</div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="box">
            <div class="box-body">
                <!-- Country Field -->
                <div class="form-group">
                    <p>
                        {{ Form::label('password_confirmation', 'Password confirm: ',  ['class' => 'required']) }}
                        {!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
                    </p>
                    @if ($errors->has('password_confirmation'))
                        <div class="text-red">{{ $errors->first('password_confirmation') }}</div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Submit Field -->
<div class="form-group text-left">
    {!! Form::submit('Create', ['class' => 'btn btn-primary waves-effect waves-light']) !!}
</div>

<script>
$(document).ready(function(){
    $('.select2').select2({
        theme: 'bootstrap',
        width: '100%'
    });
});
</script>
