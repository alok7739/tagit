@extends('layouts.app')
@section('title', 'Users')

@section('content')
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box">
            <div class="box-body">
                    <h3 class="new_heading">Manage Users</h3>
                    <div class="buttons-group">
                        <div>
                            <a href="#" class='btn btn-danger create-article'
                               onclick="document.getElementById('reset-coins').click()">
                                {!! Form::open(['method'=>'PUT', 'url'=> route('reset-coins')]) !!}
                                <button hidden id="reset-coins" data-toggle="tooltip" data-placement="top" title="Reset coins for all users"
                                        type="submit" class="dropdown-item"
                                        onclick="return confirm('Are you sure you want reset points for all users?');">
                                </button>
                                {!! Form::close() !!}
                                Reset all points
                            </a>
                        </div>
                        <div>
                            <a href="#" class='btn btn-warning create-article'
                               onclick="document.getElementById('reset-coins-public-challenges').click()">
                                {!! Form::open(['method'=>'PUT', 'url'=> route('reset-coins-public-challenges')]) !!}
                                <button hidden id="reset-coins-public-challenges" data-toggle="tooltip" data-placement="top"
                                        title="Reset coins for all public challenges"
                                        type="submit" class="dropdown-item"
                                        onclick="return confirm('Are you sure you want reset points for all public challenges?');">
                                </button>
                                {!! Form::close() !!}
                                Reset points for public challenges
                            </a>
                        </div>
                        <div>
                            <a href="{{ route('all-users-notification-form') }}" class='btn btn-primary'>
                                Send message to all users
                            </a>
                        </div>
                        <div>
                            <a href="{{ route('users.export', ['type' => 'csv']) }}" class='btn btn-primary'>
                                Export to csv
                            </a>
                        </div>
                        <div>
                            <a href="{{ route('users.create') }}" class='btn btn-primary'>
                                Create user
                            </a>
                        </div>
                    </div>
                @include('table')
            </div>
        </div>
    </div>
@endsection
