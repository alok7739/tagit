<div class="row form-justify-container">
    <div class="col-md-6">
{{--        @php dd($dto->getUserDefaultAvatarUrl()); @endphp--}}
        <div class="box">
            <div class="box-body">
                <!-- Avatar Image -->
                <div class="form-group">
                    {!! Form::label('avatar', 'Avatar') !!}
                </div>
                <div class="form-group">
                    @if ($dto->getAvatar())
                        <div class="user-avatar-container">
                            <img class="dashboard-image" id="avatar-image" src="{{$dto->getUserDefaultAvatarUrl()}}">
                            <button type="button" class="btn btn-danger waves-effect waves-light user-avatar-delete-btn" id="delete-avatar-btn">Delete</button>
                        </div>
                    @elseif (old('avatar'))
                        <img class="dashboard-image" id="avatar-image" src="{{Storage::url(old('avatar'))}}">
                    @else
                        <img class="dashboard-image" id="avatar-image" src="{{$dto->getUserDefaultAvatarUrl()}}">
                    @endif
                        <input type="hidden" id="prev-avatar" value="{{ $dto->getUserDefaultAvatarUrl() }}">
                        <input type="hidden" id="prev-avatar-path" value="{{ $dto->getAvatarPath() }}">
                        <input type="hidden" id="default-avatar" value="{{ $dto->getDefaultAvatar() }}">
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="box">
            <div class="box-body">
                <!-- Avatar Field -->
                <div class="form-group">
                    {!! Form::label('avatar', 'Upload new avatar') !!}
                </div>
                <div class="form-group dropzone user-avatar-dropzone dz-clickable">
                </div>
                @if ($errors->has('avatar'))
                    <div class="alert alert-danger" role="alert">
                        <strong>{{ $errors->first('avatar') }}</strong></div>
                @endif
                <div class="form-group" hidden>
                    {!! Form::text('avatar', $dto->getAvatarPath(), ['class' => 'form-control']) !!}
                </div>
            </div>
        </div>
    </div>
</div>


<div class="row form-justify-container">
    <div class="col-md-6">
        <div class="box">
            <div class="box-body">
                <!-- Name Field -->
                <div class="form-group">
                    <p>
                        {{ Form::label('full_name', 'Full name: ') }}
                        {!! Form::text('full_name', $dto->getFullName(), ['class' => 'form-control', 'maxlength' => 100]) !!}
                    </p>
                    @if ($errors->has('full_name'))
                        <div class="text-red">{{ $errors->first('full_name') }}</div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="box">
            <div class="box-body">
                <!-- Email Field -->
                <div class="form-group">
                    <p>
                        {{ Form::label('email', 'Email: ') }}
                        {!! Form::text('email', $dto->getEmail(), ['class' => 'form-control', 'maxlength' => 50]) !!}
                    </p>
                    @if ($errors->has('email'))
                        <div class="text-red">{{ $errors->first('email') }}</div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row form-justify-container">
    <div class="col-md-6">
        <div class="box">
            <div class="box-body">
                <!-- Birthday Field -->
                <div class="form-group">
                    <p>
                        {{ Form::label('birthday', 'Birthday: ') }}
                        {!! Html::decode(Form::label('start_date', '<span class="fa fa-calendar"></span>', ['class' => 'text-muted'])) !!}
                        <input name="birthday"
                               class="birthday-datepicker form-control"
                               data-date-format='yyyy-mm-dd'
                               data-timepicker="false"
                               data-position="bottom left"
                               value="{{ $dto->getBirthday() }}"
                               readonly />
                    </p>
                    @if ($errors->has('birthday'))
                        <div class="text-red">{{ $errors->first('birthday') }}</div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="box">
            <div class="box-body">
                <!-- Sex Field -->
                <div class="form-group">
                    <p>
                        {{ Form::label('sex', 'Gender: ') }}
                        {!! Form::select('sex', $dto->getAvailableGenderOptions(), $dto->getSex(), ['class' => 'form-control']) !!}
                    </p>
                    @if ($errors->has('sex'))
                        <div class="text-red">{{ $errors->first('sex') }}</div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row form-justify-container">
    <div class="col-md-6">
        <div class="box">
            <div class="box-body">
                <!-- Country Field -->
                <div class="form-group">
                    <p>
                        {{ Form::label('country', 'Country: ') }}
                        {!! Form::select('country', $dto->getAvailableCountries(), $dto->getCountry(), ['class' => 'form-control select2']) !!}
                    </p>
                    @if ($errors->has('country'))
                        <div class="text-red">{{ $errors->first('country') }}</div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="box">
            <div class="box-body">
                <!-- City Field -->
                <div class="form-group">
                    <p>
                        {{ Form::label('city', 'City: ') }}
                        {!! Form::text('city', $dto->getCity(), ['class' => 'form-control', 'maxlength' => 50]) !!}
                    </p>
                    @if ($errors->has('city'))
                        <div class="text-red">{{ $errors->first('city') }}</div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row form-justify-container">
    <div class="col-md-6">
        <div class="box">
            <div class="box-body">
                <!-- Referral code -->
                <div class="form-group">
                    <p>
                        {{ Form::label('referral_code', 'Referral code: ') }}
                        {!! Form::text('referral_code', $dto->getReferralCode(), ['class' => 'form-control', 'size' => '', 'maxlength' => config('custom.referral_code_length')]) !!}
                    </p>
                    @if ($errors->has('referral_code'))
                        <div class="text-red">{{ $errors->first('referral_code') }}</div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="box">
            <div class="box-body">
                <!-- Company Field -->
                <div class="form-group">
                    <p>
                        {{ Form::label('company_id', 'Company: ') }}
                        {!! Form::select('company_id', $dto->getAllCommercialCompanies(), $dto->getUserCompanyId(), ['class' => 'form-control select2']) !!}
                    </p>
                    @if ($errors->has('company_id'))
                        <div class="text-red">{{ $errors->first('company_id') }}</div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Submit Field -->
<div class="form-group text-left">
    {!! Form::submit('Save', ['class' => 'btn btn-primary waves-effect waves-light']) !!}
</div>

<script>
$(document).ready(function(){
    $('.select2').select2({
        theme: 'bootstrap',
        width: '100%'
    });

    $('.birthday-datepicker').datepicker({
        maxDate: new Date()
    });
});
</script>
