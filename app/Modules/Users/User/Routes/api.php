<?php
/**
 * Created by Artem Petrov, Appus Studio on 11/1/17.
 */

Route::group([
    'prefix' => 'auth',
    'middleware' => ['api', 'auth.active'],
], function () {

    Route::post('login', 'AuthController@login');
    Route::post('via/{provider}/callback', 'AuthController@loginVia');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::get('me', 'AuthController@me');

    Route::resource('devices', 'DevicesController')->only(['store', 'destroy'])->middleware('auth:api');

    Route::post('start', 'RegisterController@startVerification');
    Route::post('verify', 'RegisterController@verifyCode');
    Route::post('profile', 'RegisterController@updateProfile')->middleware('auth:api');
    Route::post('me/company', 'CompanyParticipationController@index')->middleware('auth:api');
    Route::delete('me/remove-code', 'CompanyParticipationController@removeCode')->middleware('auth:api');
});

Route::get('user/{user}', 'UserController@show')->middleware('auth:api');

$this->group([
    'prefix' => 'password',
], function () {

    Route::post('change', 'ChangePasswordController@change')->name('password.change');
    Route::post('email', 'ForgotPasswordController@sendResetLinkEmail')->name('password.email');
});

Route::group(['middleware' => ['api', 'auth:api', 'auth.active']], function () {
    Route::get('rating', 'RatingController@index')->middleware('auth:api');
    Route::get('rating/certificate', 'RatingController@certificate')->middleware('auth:api');

    Route::get('chat', 'ChatController@index');
    Route::post('chat', 'ChatController@store');
    Route::delete('chat/{chat}', 'ChatController@destroy');
});

Route::get('countries', 'CountriesController@list');
