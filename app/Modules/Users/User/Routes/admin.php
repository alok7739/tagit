<?php
/**
 * Created by Maksym Ignatchenko, Appus Studio LP on 25.02.19
 *
 */

Route::group(['middleware' => 'auth:admin'], function () {
    Route::get('users/export', 'UserController@export')->name('users.export')->middleware('can:export,App\Modules\Users\User\Models\User');

    Route::post('users', 'UserController@store')->name('users.store')->middleware('can:create,App\Modules\Users\User\Models\User');
    Route::get('users', 'UserController@index')->name('users.index')->middleware('can:view,App\Modules\Users\User\Models\User');
    Route::get('users/create', 'UserController@create')->name('users.create')->middleware('can:create,App\Modules\Users\Admin\Models\Admin');
    Route::get('users/{user}/edit', 'UserController@edit')->name('users.edit')->middleware('can:edit,App\Modules\Users\User\Models\User');
    Route::get('users/{user}', 'UserController@show')->name('users.show')->middleware('can:view,App\Modules\Users\User\Models\User');

    Route::match(array('PATCH', 'PUT'), "users/{user}", array(
        'uses' => 'UserController@update',
        'as' => 'users.update'
    ))->middleware('can:edit,App\Modules\Users\User\Models\User');

    Route::put('reset-coins', 'UserController@resetAllCoins')
        ->name('reset-coins')
        ->middleware('can:resetTickets,App\Modules\Users\User\Models\User');

    Route::put('reset-coins-public-challenges', 'UserController@resetCoinsForPublicChallenges')
        ->name('reset-coins-public-challenges')
        ->middleware('can:resetTickets,App\Modules\Users\User\Models\User');

    Route::put('user-reset-coins/{user}', 'UserController@resetCoinsAndRating')
        ->name('user-reset-coins')
        ->middleware('can:resetTickets,App\Modules\Users\User\Models\User');

    Route::get('direct-user-send-notification/{user}', 'UserController@showUserNotificationForm')
        ->name('direct-user-notification-form')
        ->middleware('can:sendMessege,App\Modules\Users\User\Models\User');

    Route::post('direct-user-send-notification/{user}', 'UserController@sendNotification')
        ->name('direct-user-send-notification')
        ->middleware('can:sendMessege,App\Modules\Users\User\Models\User');

    Route::get('all-users-send-notification', 'UserController@showGroupOfUsersNotificationForm')
        ->name('all-users-notification-form')
        ->middleware('can:sendMessege,App\Modules\Users\User\Models\User');

    Route::post('all-users-send-notification', 'UserController@sendGroupNotification')
        ->name('all-users-send-notification')
        ->middleware('can:sendMessege,App\Modules\Users\User\Models\User');

    Route::post('users/{user_id}/suspend', 'UserController@suspend')->name('users.suspend')->middleware('can:suspend,App\Modules\Users\User\Models\User');
    Route::post('users/{user_id}/enable', 'UserController@enable')->name('users.enable')->middleware('can:enable,App\Modules\Users\User\Models\User');
});
