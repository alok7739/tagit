<?php

namespace App\Modules\Users\User\Jobs;

use App\Modules\Challenges\Models\Challenge;
use App\Modules\Users\User\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ResetPublicChallengesCoins implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $id;

    public function __construct($id)
    {
        $this->id = $id;
    }

    public function handle()
    {
        $lastId = 0;

        $users = User::with(['challenges'])
                        ->where('id', '>', $this->id)
                        ->orderBy('id')
                        ->limit(100)
                        ->get();

        foreach($users as $user) {
            $user->challenges()->each(function (Challenge $challenge) use(&$user) {
                if(!$challenge->company_id) {
                    $totalRewards = $challenge->proofs()->sum('reward');
                    $user->coins = $user->coins - $totalRewards;
                    $user->save();
                    $challenge->proofs()->update(['reward' => 0]);
                }
            });

            $lastId = $user->id;
        }

        $count = User::where('id', '>', $lastId)->count();

        if($count) {
            dispatch(new ResetPublicChallengesCoins($lastId));
        }
    }
}
