<?php

namespace App\Modules\Users\Admin\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{

    const SUPER_ADMIN = 1;
    public const ADMIN = 1;
    public const JUDGE = 2;

    public static function getRolesAdmin(bool $show_super_admin = false): array
    {
        $roles = self::get();

        $roles_list = [];
        foreach ($roles as $rol) {
            if($show_super_admin === false && $rol->id === self::ADMIN) continue;

            $roles_list[$rol->id] = $rol->name;
        }

        return $roles_list;
    }

    public function admin()
    {
        return $this->belongsTo(Admin::class, 'role_id');
    }

}
