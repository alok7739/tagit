<?php
/**
 * Created by Artem Petrov, Appus Studio LP on 10.11.2017
 */

namespace App\Modules\Users\Admin\Models;

use App\Modules\Companies\Models\Company;
use  App\Modules\Users\Admin\Mails\ResetPasswordMail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\CanResetPassword;

class Admin extends Authenticatable implements CanResetPassword
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username',
        'email',
        'password',
        'role_id',
        'company_id',
        'country_id',
        'status'
    ];

    /**
     * Sends the password reset notification.
     *
     * @param string $token
     *
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordMail($token, $this));
    }

    /**
     * @param string $password
     */
    public function setPasswordAttribute($password): void
    {
        if($password) {
            $this->attributes['password'] = Hash::make($password);
        }
    }

    public function setRoleIdAttribute($role_id)
    {
        if((int)$role_id === Role::ADMIN && !\Auth::user()->isAdmin()) {
            $role_id = Role::JUDGE;
        }

        $this->attributes['role_id'] = $role_id;
    }

    /**
     * @return bool
     */
    public function isSuperAdmin() : bool
    {
        return $this->role_id === Role::SUPER_ADMIN;
    }

    public function isAdmin() : bool
    {
        return $this->role_id === Role::ADMIN;
    }

    public function isNotAdmin() : bool
    {
        return $this->role_id !== Role::ADMIN;
    }

    public function isJudge() : bool
    {
        return $this->role_id === Role::JUDGE;
    }

    public function role()
    {
        return $this->hasOne(Role::class, 'id', 'role_id');
    }

    /**
     * @param Request $request
     * @return int|null
     */
    public function getCompanyId(Request $request): ?int
    {
        if ($this->isSuperAdmin()) {
            return $request->get('company_id');
        }

        return $this->company_id;
    }

    public function company()
    {
        return $this->hasOne(Company::class, 'id', 'company_id');
    }

}
