<?php
/**
 * Created by Artem Petrov, Appus Studio on 11/1/17.
 */

Route::group(['middleware' => ['auth:admin']], function () {
    Route::get('/', 'DashboardController@index')->name('admin');
    Route::get('/index', 'DashboardController@index');
    Route::post('/logout', 'LoginController@logout')->name('logout');

    Route::get('/chat', 'ChatController@index')->name('chat');;
    Route::delete('/chat/clear', 'ChatController@clear');

    Route::get('/comments', 'CommentsController@index')->name('comments');
    Route::delete('/comments/clear', 'CommentsController@clear');
});

Route::group(['prefix' => 'api', 'middleware' => ['auth:adminws']], function () {
    Route::get('chat/{type}/{id}', 'ChatController@list');
    Route::post('chat/{type}/{id}', 'ChatController@store');
    Route::delete('chat/del/{chat}', 'ChatController@destroy');

    Route::get('comments', 'CommentsController@list');
    Route::get('comments/{id}', 'CommentsController@list');
    Route::delete('comments/{comment}', 'CommentsController@destroy');
});

Route::middleware('auth:admin')->group(function () {
    Route::post('administration', 'AdminController@store')->name('administration.store')->middleware('can:create,App\Modules\Users\Admin\Models\Admin');
    Route::get('administration', 'AdminController@index')->name('administration.index')->middleware('can:view,App\Modules\Users\Admin\Models\Admin');
    Route::get('administration/create', 'AdminController@create')->name('administration.create')->middleware('can:create,App\Modules\Users\Admin\Models\Admin');
    Route::delete('administration/{admin_id}', 'AdminController@destroy')->name('administration.destroy')->middleware('can:destroy,App\Modules\Users\Admin\Models\Admin');
    Route::get('administration/{admin_id}/edit', 'AdminController@edit')->name('administration.edit')->middleware('can:edit,App\Modules\Users\Admin\Models\Admin');
    Route::post('administration/{admin_id}/suspend', 'AdminController@suspend')->name('administration.suspend')->middleware('can:suspend,App\Modules\Users\Admin\Models\Admin');
    Route::post('administration/{admin_id}/enable', 'AdminController@enable')->name('administration.enable')->middleware('can:enable,App\Modules\Users\Admin\Models\Admin');

    Route::match(array('PATCH', 'PUT'), "administration/{admin_id}", array(
        'uses' => 'AdminController@update',
        'as' => 'administration.update'
    ))->middleware('can:edit,App\Modules\Users\Admin\Models\Admin');
});

Route::get('/login', 'LoginController@showLoginForm')->middleware('guest:admin')->name('login');
Route::post('/login', 'LoginController@login');

Route::get('/forgot-password', 'PasswordController@forgotPassword')->middleware('guest')->name('auth.password.request');
Route::post('/forgot-password', 'PasswordController@processForgotPassword')->middleware('guest')->name('auth.password.email');

Route::get('/reset-password/{token}','PasswordController@resetPassword')->middleware('guest')->name('auth.password.reset');
Route::post('/reset-password', 'PasswordController@processResetPassword')->middleware('guest')->name('auth.password.update');
