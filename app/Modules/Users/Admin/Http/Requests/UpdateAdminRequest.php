<?php


namespace App\Modules\Users\Admin\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;

class UpdateAdminRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'username' => 'sometimes|string|max:255|unique:admins,username,' . $this->admin_id,
            'email' => 'sometimes|email|unique:admins,email,' . $this->admin_id,
            'role_id' => 'required|numeric|exists:roles,id',
            'password' => [
                'required_if:password_confirmation,!=,null',
                'min:8',
                'max:50',
                'confirmed'
            ],
            'password_confirmation' => [
                'nullable',
                'min:8',
                'max:50'
            ],
        ];
    }
}
