<?php


namespace App\Modules\Users\Admin\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;

class CreateAdminRequest extends FormRequest
{
    /**
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'email' => "email|max:50|unique:admins,email",
            'username' => 'required|string|min:3|max:50|unique:admins,username',
            'role_id' => 'required|integer|exists:roles,id',
            'password' => [
                'required',
                'min:8',
                'max:50',
                'confirmed'
            ],
        ];
    }
}
