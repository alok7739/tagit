<?php

namespace App\Modules\Users\Admin\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Users\User\Models\User;
use App\Modules\Companies\Models\Company;
use App\Modules\Challenges\Models\Challenge;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function index()
    {
        if (!Auth::user()->isSuperAdmin()) {
            return $this->redirectAdmin();
        }

        $users = User::count();
        $companies = Company::count();
        $live_challenges = Challenge::where('status', 'active')->count();
        $ended_challenges = Challenge::where('status', 'end')->count();
        $archived_challenges = Challenge::where('status', 'archive')->count();

        $challenge_users = DB::table('challenge_user')
                                ->select([DB::raw('COUNT(user_id) AS cnt')])
                                ->whereIn('challenge_id', function ($query) {
                                    $query->select(['id'])
                                            ->from('challenges')
                                            ->where('status', 'active');
                                })
                                ->first();

        $challenge_users = $challenge_users ? $challenge_users->cnt : 0;

        return view('home', compact('users', 'companies', 'live_challenges', 'ended_challenges', 'archived_challenges', 'challenge_users'));
    }

    private function redirectAdmin()
    {
        if (Auth::user()->company_id) {
            return redirect()
                ->route('company.show', [
                    'company_id' => Auth::user()->company_id,
                ]);
        }

        return redirect()->route('challenge.index');
    }

}
