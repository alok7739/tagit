<?php

namespace App\Modules\Users\Admin\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Challenges\Models\Challenge;
use App\Modules\Challenges\Models\Comments;
use App\Modules\Users\Admin\Models\AdminWs;
use Illuminate\Support\Facades\Auth;
use App\Modules\Users\User\Http\Requests\ChatMessagesRequest;
use App\Modules\Users\User\Models\ChatMessages;
use App\Services\ResponseBuilder\CustomResponseBuilder;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class CommentsController extends Controller
{
    public function index(Request $request)
    {
        $user = AdminWs::find(Auth::user()->id);
        $token = auth('adminws')->login($user);
        
        $id = $request->get('id', 0);

        if (Auth::user()->isSuperAdmin()) {
            $challenges = array_pluck(Challenge::all(['id', 'name']), 'name', 'id');
        } else {
            $challenges = array_pluck(Challenge::select(['id', 'name'])->where('company_id', $user->company_id)->get(), 'name', 'id');
        }

        return view('comments', compact('token', 'id', 'challenges'));
    }

    public function list(Request $request, $id = 0)
    {
        $limit = config('custom.comments_count_per_page');
        $page = (int)$request->get('page') ?? 1;
        $offset = ($page - 1) * $limit;

        if (Auth::user()->isSuperAdmin()) {
            if($id) {
                $comments = Comments::where('challenge_id', $id)
                                ->orderByDesc('id')
                                ->limit($limit)
                                ->offset($offset)
                                ->get();
            } else {
                $comments = Comments::orderByDesc('id')
                                ->limit($limit)
                                ->offset($offset)
                                ->get();
            }
        } else {
            $company_id = Auth::user()->company_id;

            if($id) {
                $comments = Comments::where('challenge_id', $id)
                                ->join('challenges', function ($join) use($company_id) {
                                    $join->on('challenges.id', '=', 'challenge_comment.challenge_id')
                                        ->where('challenges.company_id', $company_id);
                                })
                                ->orderByDesc('challenge_comment.id')
                                ->limit($limit)
                                ->offset($offset)
                                ->get();
            } else {
                $comments = Comments::join('challenges', function ($join) use($company_id) {
                                    $join->on('challenges.id', '=', 'challenge_comment.challenge_id')
                                        ->where('challenges.company_id', $company_id);
                                })
                                ->orderByDesc('challenge_comment.id')
                                ->limit($limit)
                                ->offset($offset)
                                ->get();
            }
        }
        
        
        $messages = [];

        foreach($comments as $c) {
            $messages[] = $this->prepareMessage($c);
        }

        $messages = array_reverse($messages);

        return CustomResponseBuilder::success($messages);
    }

    public function destroy(Comments $comment) : Response
    {
        $comment->delete();

        return CustomResponseBuilder::success();
    }

    public function clear(Request $request)
    {
        $id = $request->get('id');

        if($id) {
            Comments::where('challenge_id', $id)->delete();
        } else {
            Comments::query()->truncate();
        }

        return redirect(url('admin/comments?id=' . $id));
    }
    
    private function redirectAdmin()
    {
        if (Auth::user()->company_id) {
            return redirect()
                ->route('company.show', [
                    'company_id' => Auth::user()->company_id,
                ]);
        }

        return redirect()->route('challenge.index');
    }

    private function prepareMessage($row) {
        $user = Auth::user();

        if($row->user_id) {
            $name = $row->user->full_name;
            $avatar = $row->user->avatar;
        } else {
            if($row->admin->company_id) {
                $name = $row->admin->company->name;
                $name = $row->admin->company->logo;
            } else {
                $name  = 'Admin';
                $avatar = '';
            }
        }

        $avatar = $avatar ? $avatar : 'https://i.ibb.co/HDHsj5q/default-avatar.png';

        $msg = [
            "id"        => $row->id,
            "time"      => strtotime($row->created_at),
            "message"   => $row->message,
            "user_id"   => $row->user_id ? $row->user_id : 'admin.' . $row->admin_id,
            "name"      => $name,
            "avatar"    => $avatar,
            "challenge" => $row->challenge->name
        ];

        return $msg;
    }
}
