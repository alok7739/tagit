<?php


namespace App\Modules\Users\Admin\Http\Controllers;


use App\Http\Controllers\Controller;
use App\Modules\Companies\Models\Company;
use App\Modules\Users\Admin\DataTables\AdminDataTable;
use App\Modules\Users\Admin\Http\Requests\CreateAdminRequest;
use App\Modules\Users\Admin\Http\Requests\UpdateAdminRequest;
use App\Modules\Companies\Enums\CompanyTypeEnum;
use App\Modules\Users\Admin\Models\Admin;
use App\Modules\Users\Admin\Models\Role;
use App\Services\CountriesRepository;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    public function index(AdminDataTable $userDataTable)
    {
        return $userDataTable->render('admin.index');
    }

    public function edit(int $admin_id)
    {
        $admin = Admin::findOrFail($admin_id);
        $role = Role::getRolesAdmin(Auth::user()->isAdmin());
        $companies = Company::select(['id', 'name'])->where('type', CompanyTypeEnum::COMMERCIAL)->orderBy('name')->get();

        return view('admin.edit', compact('role', 'admin', 'companies'));
    }

    public function create()
    {
        $role = Role::getRolesAdmin(Auth::user()->isAdmin());
        $companies = Company::select(['id', 'name'])->where('type', CompanyTypeEnum::COMMERCIAL)->orderBy('name')->get();
        $countries = CountriesRepository::make()->all('name');

        return view('admin.create', compact('role', 'companies', 'countries'));
    }

    public function store(CreateAdminRequest $request)
    {
        $data = $request->all();

        if ($data['role_id'] === '1') {
            $data['company_id'] = null;
            $data['country_id'] = null;
        }

        if(isset($data['relevant'])) {
            if ($data['relevant'] === 'company') {
                $data['country_id'] = null;
            } elseif ($data['relevant'] === 'country') {
                $data['company_id'] = null;
            }
        }

        Admin::create($data);

        flash("User have been created");

        return redirect()->route('administration.index');
    }

    public function update(UpdateAdminRequest $request, int $admin_id)
    {
        $data = $request->all();

        if ($data['role_id'] === '1') {
            $data['company_id'] = null;
            $data['country_id'] = null;
        }

        $admin = Admin::findOrFail($admin_id);
        $admin->update($data);

        flash("User have been updated");

        return redirect(route('administration.index'));
    }

    public function destroy(int $admin_id)
    {
        $admin = Admin::findOrFail($admin_id);

        if (null === $admin) {
            flash('No such user detected');
            return redirect()->route('administration.index');
        }

        $identityId = Auth::id();

        if ($identityId === $admin_id) {
            flash('Sorry! You cannot delete yourself');
            return redirect()->route('administration.index');
        }

        $admin->delete();

        flash('User has been deleted successfully');

        return redirect()->route('administration.index');

    }

    public function suspend(int $admin_id)
    {
        $admin = Admin::findOrFail($admin_id);

        if (null === $admin) {
            flash('No such user detected');
            return redirect()->route('administration.index');
        }

        $admin->status = '2';
        $admin->save();

        flash('User has been suspended successfully');

        return redirect()->route('administration.index');

    }

    public function enable(int $admin_id)
    {
        $admin = Admin::findOrFail($admin_id);

        if (null === $admin) {
            flash('No such user detected');
            return redirect()->route('administration.index');
        }

        $admin->status = '1';
        $admin->save();

        flash('User has been enabled successfully');

        return redirect()->route('administration.index');

    }
}
