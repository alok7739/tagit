<?php

namespace App\Modules\Users\Admin\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Users\Admin\Models\Admin;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin';

    /**
     * @return mixed
     */
    protected function guard()
    {
        return Auth::guard('admin');
    }

    /**
     * The user has logged out of the application.
     *
     * @param  \Illuminate\Http\Request $request
     * @return mixed
     */
    protected function loggedOut(Request $request)
    {
        return redirect('/admin/login');
    }

    /**
     * @return string
     */
    protected function username()
    {
        return 'username';
    }

    public function login(Request $request)
    {
        $input = $request->all();

        $this->validate($request, [
            'username' => 'required',
            'password' => 'required',
        ]);

        $fieldType = filter_var($request->username, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';
        $user = Admin::where($fieldType, $input['username'])->first();

        if ($user && \Hash::check($input['password'], $user->password)) {
            if($user->status == '0') {
                flash('Your account is disabled.', 'danger');
                return redirect()->route('login');
            } else {
                auth('admin')->attempt(array($fieldType => $input['username'], 'password' => $input['password']));
                return redirect()->route('admin');
            }
        } else {
            flash('These credentials do not match our records.', 'danger');
            return redirect()->route('login');
        }
    }
}
