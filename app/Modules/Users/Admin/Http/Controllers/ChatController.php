<?php

namespace App\Modules\Users\Admin\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Companies\Models\Company;
use App\Modules\Users\Admin\Models\AdminWs;
use App\Modules\Users\Admin\Models\Countries;
use Illuminate\Support\Facades\Auth;
use App\Modules\Users\User\Http\Requests\ChatMessagesRequest;
use App\Modules\Users\User\Models\ChatMessages;
use App\Services\ResponseBuilder\CustomResponseBuilder;
use App\Modules\Companies\Enums\CompanyTypeEnum;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class ChatController extends Controller
{
    public function index(Request $request)
    {
        $user = AdminWs::find(Auth::user()->id);
        $token = auth('adminws')->login($user);

        if (Auth::user()->isSuperAdmin()) {
            $countryId = $request->get('country', 0);
            $companyId = $request->get('company', 0);

            $countries = array_pluck(Countries::all(['id', 'name']), 'name', 'id');
            $companies = Company::select(['id', 'name'])->where('type', CompanyTypeEnum::COMMERCIAL)->get()->pluck('name', 'id')->toArray();
        } else {
            $countryId = 0;
            $companyId = $user->company_id;

            $countries = $companies = [];
        }

        $id = $countryId ? $countryId : $companyId;
        $type = $companyId ? 'company' : 'country';

        return view('chats', compact('token', 'type', 'id', 'countries', 'companies', 'countryId', 'companyId'));
    }

    public function list($type = 'company', $id = 0)
    {
        $limit = config('custom.chat_messages_count_per_page');

        $company_id = $country_id = null;
        $chat = $messages = [];

        if($type == 'company') {
            $company_id = $id;
        }
        elseif($type == 'country') {
            $country_id = $id;
        }

        if($country_id) {
            $chat = ChatMessages::where('country_id', $country_id)
                        ->orderByDesc('id')
                        ->limit($limit)
                        ->get();
        }
        elseif($company_id) {
            $chat = ChatMessages::where('company_id', $company_id)
                        ->orderByDesc('id')
                        ->limit($limit)
                        ->get();
        }

        foreach($chat as $c) {
            $messages[] = $this->prepareMessage($c);
        }

        $messages = array_reverse($messages);

        return CustomResponseBuilder::success($messages);
    }

    public function store(ChatMessagesRequest $request, $type = 'company', $id = 0)
    {
        /** @var User $user */
        $user = Auth::user();

        $message = $request->get('message');

        $company_id = $country_id = null;

        if($type == 'company') {
            $company_id = $id;
        }
        elseif($type == 'country') {
            $country_id = $id;
        }

        $user_id = null;
        $admin_id = $user ? $user->id : null;

        $challege = app(ChatMessages::class)->create([
            'company_id' => $company_id,
            'country_id' => $country_id,
            'user_id' => $user_id,
            'admin_id' => $admin_id,
            'message' => $message,
        ]);

        return CustomResponseBuilder::success($this->prepareMessage($challege));
    }

    public function destroy(ChatMessages $chat) : Response
    {
        $chat->delete();

        return CustomResponseBuilder::success();
    }

    public function clear(Request $request)
    {
        if (Auth::user()->isSuperAdmin()) {
            $type = $request->get('type');
            $id = $request->get('id');
        } else {
            $type = 'company';
            $id = Auth::user()->company_id;
        }

        $company_id = $country_id = null;

        if($type == 'company') {
            $company_id = $id;
        }
        elseif($type == 'country') {
            $country_id = $id;
        }

        if($country_id) {
            ChatMessages::where('country_id', $country_id)->delete();
        }
        elseif($company_id) {
            ChatMessages::where('company_id', $company_id)->delete();
        }

        return redirect(url('admin/chat?' . $type . '=' . $id));
    }

    private function prepareMessage($row) {
        $user = Auth::user();

        if($row->user_id) {
            $name = $row->user->full_name;
            $avatar = $row->user->avatar;
        } else {
            if($row->admin->company_id) {
                $name = $row->admin->company->name;
                $avatar = $row->admin->company->logo;
            } else {
                $name  = 'Admin';
                $avatar = '';
            }
        }

        $avatar = $avatar ? $avatar : 'https://i.ibb.co/HDHsj5q/default-avatar.png';

        $msg = [
            "id"      => $row->id,
            "time"    => strtotime($row->created_at),
            "message" => $row->message,
            "user_id" => $row->user_id ? $row->user_id : 'admin.' . $row->admin_id,
            "name"    => $name,
            "avatar"  => $avatar
        ];

        return $msg;
    }
}
