@extends('layouts.app')
@section('title', 'Dashboard')

@section('content')
    @include('flash::message')

    <div class="row">
        <div class="col-md-2 col-sm-4">
            <div class="dashstatsbox">
                <b><?= number_format($users, 0) ?></b>
                <span>Number of<br />Users</span>
            </div>
        </div>

        <div class="col-md-2 col-sm-4">
            <div class="dashstatsbox">
                <b><?= number_format($companies, 0) ?></b>
                <span>Number of<br />Companies</span>
            </div>
        </div>

        <div class="col-md-2 col-sm-4 col-xs-2">
            <div class="dashstatsbox">
                <b><?= number_format($live_challenges, 0) ?></b>
                <span>Live<br />Challenges</span>
            </div>
        </div>

        <div class="col-md-2 col-sm-4">
            <div class="dashstatsbox">
                <b><?= number_format($ended_challenges, 0) ?></b>
                <span>Ended<br />Challenges</span>
            </div>
        </div>

        <div class="col-md-2 col-sm-4">
            <div class="dashstatsbox">
                <b><?= number_format($archived_challenges, 0) ?></b>
                <span>Archived<br />Challenges</span>
            </div>
        </div>

        <div class="col-md-2 col-sm-4">
            <div class="dashstatsbox">
                <b><?= number_format($challenge_users, 0) ?></b>
                <span>Users in<br />Live Challenges</span>
            </div>
        </div>
    </div>
    
    <hr />

    <div class="row">
        <div class="col-sm-6 col-md-3">
            <a class="dashbox" href="{{ url('admin/challenge') }}">
                <i class="fa fa-globe fa-2x"></i>
                <div>Challenges</div>
            </a>
        </div>

        <div class="col-sm-6 col-md-3">
            <a class="dashbox" href="{{ url('admin/company') }}">
                <i class="fa fa-building fa-2x"></i>
                <div>Companies</div>
            </a>
        </div>

        <div class="col-sm-6 col-md-3">
            <a class="dashbox" href="{{ url('admin/chat') }}">
                <i class="fa fa-comments fa-2x"></i>
                <div>Chats</div>
            </a>
        </div>

        <div class="col-sm-6 col-md-3">
            <a class="dashbox" href="{{ url('admin/comments') }}">
                <i class="fa fa-comment fa-2x"></i>
                <div>Comments</div>
            </a>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-6 col-md-3">
            <a class="dashbox" href="{{ url('admin/reports') }}">
                <i class="fa fa-exclamation-circle fa-2x"></i>
                <div>Reported Challenges</div>
            </a>
        </div>

        <div class="col-sm-6 col-md-3">
            <a class="dashbox" href="{{ url('admin/ranking') }}">
                <i class="fa fa-star fa-2x"></i>
                <div>Ranking</div>
            </a>
        </div>

        <div class="col-sm-6 col-md-3">
            <a class="dashbox" href="{{ url('admin/users') }}">
                <i class="fa fa-user fa-2x"></i>
                <div>Users</div>
            </a>
        </div>

        <div class="col-sm-6 col-md-3">
            <a class="dashbox" href="{{ url('admin/administration') }}">
                <i class="fa fa-user-circle-o fa-2x"></i>
                <div>Administration</div>
            </a>
        </div>
    </div>

    <style>
        .dashbox {
            display: block;
            border: 1px solid #ccc;
            border-radius: 10px;
            padding: 20px;
            margin-bottom: 20px;
            box-shadow: 2px 2px 4px #ddd;
            text-align: center;
            color: #693988;
            font-family: "Baloo Thambi 2", cursive;
            font-size: 22px;
            font-weight: 600;
        }

        .dashbox:hover {
            text-decoration: none;
            color: #F9AC50;
        }

        .dashstatsbox {
            text-align: center;
            background: #693988;
            padding: 10px;
            margin-bottom: 20px;
            border-radius: 6px;
            color: #fff;
        }
        .dashstatsbox b {
            display: block;
            font-size: 28px;
        }
        .dashstatsbox span {
            display: block;
        }
    </style>
@endsection
