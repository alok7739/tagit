@extends('layouts.auth')

@section('content')
    <h2>@lang('auth.sign_in')</h2>

    @include('flash::message')

    <div class="p-3">
        <form class="form-horizontal m-t-20" method="post" action="{{ route('login') }}">
            @csrf

            @if ($errors->has('username') || $errors->has('password'))
            <div class="alert alert-danger">
                @if ($errors->has('username'))
                <div>{{ $errors->first('username') }}</div>
                @endif
                @if ($errors->has('password'))
                <div>{{ $errors->first('password') }}</div>
                @endif
            </div>
            @endif

            <div class="input-group">
                <span class="input-group-addon"><img src="/build/images/username.svg" /></span>
                <input id="username" type="text" name="username" class="form-control" required autofocus
                    placeholder="@lang('auth.username_placeholder')" value="{{ old('username') }}">
            </div>

            <div class="input-group">
                <span class="input-group-addon"><img src="/build/images/password.svg" /></span>
                <input id="password" class="form-control" type="password" name="password" required
                    placeholder="@lang('auth.password_placeholder')">
            </div>

            <div class="forget-password">
                <div class="custom-control custom-checkbox mt-10">
                    <input type="checkbox" class="custom-control-input" id="customCheck1" name="remember" {{ old('remember') ? 'checked' : '' }}>
                    <label class="custom-control-label" for="customCheck1">@lang('auth.remember_me')</label>
                </div>
            </div>

            <div class="submit-bth">
                <button class="btn btn-login" type="submit">@lang('auth.login')</button>

                {{-- <div class="text-center">Don't have an account? <a href="{{ url('/') }}" style="color:#f6ac4e;">Sign Up</a></div> --}}
                <div class="text-center"><a href="{{ route('auth.password.request') }}" style="color:#f6ac4e;">Forgot Password?</a></div>
            </div>
        </form>
    </div>
@endsection
