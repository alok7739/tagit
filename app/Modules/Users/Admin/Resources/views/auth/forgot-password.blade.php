@extends('layouts.auth')

@section('content')
    <h2>Forgot <span>Password</span></h2>

    @include('flash::message')

    <div class="p-3">
        <form class="form-horizontal m-t-20" method="post" action="{{ route('auth.password.email') }}">
            @csrf

            @if ($errors->has('email'))
            <div class="alert alert-danger">
                @if ($errors->has('email'))
                <div>{{ $errors->first('email') }}</div>
                @endif
            </div>
            @endif

            <div class="input-group">
                <span class="input-group-addon"><img src="/build/images/username.svg" /></span>
                <input id="email" type="text" name="email" class="form-control" required autofocus placeholder="Email Address" value="{{ old('email') }}" />
            </div>

            <div class="submit-bth">
                <button class="btn btn-login" type="submit">Submit</button>

                <div class="text-center"><a href="{{ route('login') }}" style="color:#f6ac4e;">Back To Login</a></div>
            </div>
        </form>
    </div>
@endsection
