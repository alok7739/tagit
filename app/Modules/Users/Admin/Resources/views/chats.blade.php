@extends('layouts.app')
@section('title', 'Chat')

@section('content')
@include('flash::message')

<div class="message-chat">
    @if(Auth::user()->isSuperAdmin())
    <div class="row">
        <div class="col-sm-4">
            <form method="GET">
                <label>Country:</label>
                <select class="form-control select2" name="country" onchange="submit();">
                    <option value="">Select Country</option>
                    @foreach ($countries as $cid => $name)
                    <option value="{{ $cid }}" {{ $countryId == $cid ? 'selected' : '' }}>{{ $name }}</option>
                    @endforeach
                </select>
            </form>
        </div>

        <div class="col-sm-4">
            <form method="GET">
                <label>Company:</label>
                <select class="form-control select2" name="company" onchange="submit();">
                    <option value="">Select Company</option>
                    @foreach ($companies as $cid => $name)
                    <option value="{{ $cid }}" {{ $companyId == $cid ? 'selected' : '' }}>{{ $name }}</option>
                    @endforeach
                </select>
            </form>
        </div>

        @if($id)
        <div class="col-sm-4 text-right clear-div">
            <label>&nbsp;</label><br />
            {!! Form::open(['method'=>'DELETE', 'url'=> url('admin/chat/clear')]) !!}
                <input type="hidden" name="type" value="{{ $type }}" />
                <input type="hidden" name="id" value="{{ $id }}" />
                <button title="Delete" type="submit" class="btn btn-danger" onclick="return confirm('Are you sure you want clear chat?');">Clear Chat</button>
            {!! Form::close() !!}
        </div>
        @endif
    </div>
    @else
    <div class="row">
        <div class="col-sm-12 text-right clear-div">
            {!! Form::open(['method'=>'DELETE', 'url'=> url('admin/chat/clear')]) !!}
                <input type="hidden" name="type" value="company" />
                <input type="hidden" name="id" value="{{ $id }}" />
                <button title="Delete" type="submit" class="btn btn-danger" onclick="return confirm('Are you sure you want clear chat?');">Clear Chat</button>
            {!! Form::close() !!}
        </div>
    </div>
    @endif

    @if($id)

    @if(Auth::user()->isSuperAdmin())
    <hr />
    @endif

    <div id="chat-list" class="chat-body"></div>

    <div class="chat-footer">
        <input id="send-text" type="text" class="send-message-text" maxlength="200" />
        <button id="send-btn" type="button" class="send-message-button btn-info"> <i class="fa fa-send"></i> </button>
    </div>
    @else
    <div style="text-align:center; font-size: 20px; margin: 50px 0">Please first select country or company...</div>
    @endif
</div>

<script>
$(document).ready(function() {
    $('.clear-div').hide();
    $('.select2').select2();

    @if($id)
    if (!("WebSocket" in window)) {
        $('#chat-list').html('<p>Oh no, you need a browser that supports WebSockets. How about <a href="http://www.google.com/chrome">Google Chrome</a>?</p>')
    } else {
        var socket;
        var host = "wss://{{ env('WS_ADMIN_DOMAIN') }}/chat/{{ $token }}/{{ $type }}/{{ $id }}";

        try {
            var socket = new WebSocket(host);

            socket.onopen = function() {}

            socket.onmessage = function(msg) {
                var res = JSON.parse(msg.data)

                if(res.type == 'messages' && res.data.length) {
                    res.data.map(function(msg, i){
                        renderMsg(msg)
                    });

                    $('#chat-list').scrollTop($('#chat-list')[0].scrollHeight);+
                    $(".timeago").timeago();
                    $('.clear-div').show();
                }
                else if(res.type == 'delete' && res.data) {
                    $('#cmsg' + res.data).remove();
                }
            }

            socket.onclose = function() {}

            socket.onerror = function() {
                console.log(socket);
            }
        } catch (exception) {
            console.log(exception)
        }

        function sendMsg() {
            var text = $.trim($('#send-text').val());

            if (text == "") {
                $('#send-text').val('');
                return;
            }

            try {
                socket.send(text);
            } catch (exception) {
            }

            $('#send-text').val("");
        }

        function renderMsg(msg) {
            msg.user_id += "";
            msg.time = new Date(msg.time * 1000);

            var c = msg.user_id.indexOf("admin") > -1 ? 'my-message' : '';

            var h  = '<div id="cmsg' + msg.id + '" class="message ' + c + '">';
                h += '  <img alt="" class="img-circle medium-image" src="' + msg.avatar + '">';
                h += '  <div class="message-body">';
                h += '    <div class="message-info">';
                h += '      <i class="fa fa-trash text-danger btn-remove" data-id="' + msg.id + '"></i>';
                h += '      <h4>' + msg.name + '</h4>';
                h += '      <h5> <i class="fa fa-clock-o"></i> <time class="timeago" datetime="' + msg.time + '"></time> </h5>';
                h += '    </div>';
                h += '    <hr>';
                h += '    <div class="message-text">' + msg.message + '</div>';
                h += '  </div>';
                h += '  <br>';

                h += '</div>';

            $('#chat-list').append(h);
        }

        $('#send-text').keypress(function(event) {
            if (event.keyCode == '13') {
                sendMsg();
            }
        });

        $('#send-btn').click(function(){
            sendMsg();
        });

        $(document).on('click', '.btn-remove', function(){
            $(this).parents('.message').remove();
            socket.send('DELETE::' + $(this).attr('data-id'));
        });
    }
    @endif
});
</script>
@endsection
