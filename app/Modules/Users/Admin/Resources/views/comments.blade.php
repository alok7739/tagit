@extends('layouts.app')
@section('title', 'Comments')

@section('content')
@include('flash::message')

<div class="message-chat">
    <div class="row">
        <div class="col-sm-4">
            <form method="GET">
                <label>Challenge:</label>
                <select class="form-control select2" name="id" onchange="submit();">
                    <option value="">All Challenges</option>
                    @foreach ($challenges as $cid => $name)
                    <option value="{{ $cid }}" {{ $id == $cid ? 'selected' : '' }}>{{ $name }}</option>
                    @endforeach
                </select>
            </form>
        </div>

        <div class="col-sm-8 text-right clear-div">
            {!! Form::open(['method'=>'DELETE', 'url'=> url('admin/comments/clear')]) !!}
                <input type="hidden" name="id" value="{{ $id }}" />
                <button title="Delete" type="submit" class="btn btn-danger" onclick="return confirm('Are you sure you want clear comments?');">Clear Comments</button>
            {!! Form::close() !!}
        </div>
    </div>

    <hr />

    <div id="chat-list" class="chat-body"></div>
</div>

<script>
$(document).ready(function() {
    $('.clear-div').hide();
    $('.select2').select2();

    if (!("WebSocket" in window)) {
        $('#chat-list').html('<p>Oh no, you need a browser that supports WebSockets. How about <a href="http://www.google.com/chrome">Google Chrome</a>?</p>')
    } else {
        var socket;
        var host = "wss://{{ env('WS_ADMIN_DOMAIN') }}/comments/{{ $token }}/{{ $id }}";

        try {
            var socket = new WebSocket(host);

            socket.onopen = function() {}

            socket.onmessage = function(msg) {
                var res = JSON.parse(msg.data)

                if(res.type == 'messages' && res.data.length) {
                    res.data.map(function(msg, i){
                        renderMsg(msg)
                    });

                    $('#chat-list').scrollTop($('#chat-list')[0].scrollHeight);+
                    $(".timeago").timeago();
                    $('.clear-div').show();
                }
                else if(res.type == 'delete' && res.data) {
                    $('#cmsg' + res.data).remove();
                }
            }

            socket.onclose = function() {}

            socket.onerror = function() {
                console.log(socket);
            }
        } catch (exception) {
            console.log(exception)
        }

        function renderMsg(msg) {
            msg.user_id += "";
            msg.time = new Date(msg.time * 1000);

            var c = msg.user_id.indexOf("admin") > -1 ? 'my-message' : '';

            var h  = '<div id="cmsg' + msg.id + '" class="message ' + c + '">';
                h += '  <img alt="" class="img-circle medium-image" src="' + msg.avatar + '">';
                h += '  <div class="message-body">';
                h += '    <div class="message-info">';
                h += '      <i class="fa fa-trash text-danger btn-remove" data-id="' + msg.id + '"></i>';
                h += '      <h4>' + msg.name + '</h4>';
                h += '      <h5> <i class="fa fa-clock-o"></i> <time class="timeago" datetime="' + msg.time + '"></time> </h5>';
                h += '    </div>';
                h += '    <hr>';
                h += '    <div class="message-text">' + msg.message + '</div>';

                @if(!$id)
                h += '    <hr /> <div class="message-challenge">' + msg.challenge + '</div>';
                @endif

                h += '  </div>';
                h += '  <br>';
                
                h += '</div>';

            $('#chat-list').append(h);
        }

        $(document).on('click', '.btn-remove', function(){
            $(this).parents('.message').remove();
            socket.send('DELETE::' + $(this).attr('data-id'));
        });
    }
});
</script>
@endsection