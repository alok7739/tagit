@extends('layouts.app')
@section('title', 'Edit user')
@section('content')

    <div class="content">
        <div class="clearfix"></div>
        @include('flash::message')
        <div class="clearfix"></div>
        {!! Form::open(['url'=> route('administration.update', ['administration' => $admin->id]), 'method' => 'PUT']) !!}
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-body">
                        <h3 class="new_heading">Edit Admin</h3>
                        @include('admin.edit_fields')
                    </div>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
@endsection

