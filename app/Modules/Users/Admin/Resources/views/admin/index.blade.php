@extends('layouts.app')
@section('title', 'Administrations')

@section('content')
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box">
            <div class="box-body">
                <h3 class="new_heading pull-left">Manage Admins</h3>
                <div class="buttons-group pull-right">
                    <div>
                        <a href="{{ route('administration.create') }}" class='btn btn-primary'>
                            Create Admin
                        </a>
                    </div>
                </div>
                <div class="clearfix"></div>
                @include('admin.table')
            </div>
        </div>
    </div>
@endsection
