<div class="btn-group" role="group">
    <button id="btnGroupDrop1" type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Actions
    </button>
    <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
        @php $admin = \App\Modules\Users\Admin\Models\Admin::find($id) @endphp
        @if($admin->role_id == '2')
            @if($admin->status == '1')
            <a href="#" class='dropdown-item'
            onclick="document.getElementById('suspend-{{$id}}-button').click()">
                {!! Form::open(['method'=>'POST', 'url'=> route('administration.suspend', $id)]) !!}
                <button style="display:none" id="suspend-{{$id}}-button" data-toggle="tooltip" data-placement="top" title="Suspend"
                        type="submit" class="dropdown-item"
                        onclick="return confirm('Are you sure you want to suspend this user?');">
                </button>
                {!! Form::close() !!}
                Suspend
            </a>
            @endif

            @if($admin->status == '2')
            <a href="#" class='dropdown-item'
            onclick="document.getElementById('enable-{{$id}}-button').click()">
                {!! Form::open(['method'=>'POST', 'url'=> route('administration.enable', $id)]) !!}
                <button style="display:none" id="enable-{{$id}}-button" data-toggle="tooltip" data-placement="top" title="Enable"
                        type="submit" class="dropdown-item"
                        onclick="return confirm('Are you sure you want to enable this user?');">
                </button>
                {!! Form::close() !!}
                Enable
            </a>
            @endif
        @endif
        <a href="{{ route('administration.edit', $id) }}" class='dropdown-item'>
            Edit
        </a>
        <a href="#" class='dropdown-item'
           onclick="document.getElementById('reset-{{$id}}-button').click()">
            {!! Form::open(['method'=>'DELETE', 'url'=> route('administration.destroy', $id)]) !!}
            <button style="display:none" id="reset-{{$id}}-button" data-toggle="tooltip" data-placement="top" title="Delete"
                    type="submit" class="dropdown-item"
                    onclick="return confirm('Are you sure you want delete user?');">
            </button>
            {!! Form::close() !!}
            Delete
        </a>
    </div>
</div>
