<div class="row form-justify-container">
    <div class="col-md-6">
        <div class="box">
            <div class="box-body">
                <!-- Name Field -->
                <div class="form-group">
                    <p>
                        {{ Form::label('username', 'Username: ',  ['class' => 'required']) }}
                        {!! Form::text('username', $admin->username, ['class' => 'form-control', 'maxlength' => 100]) !!}
                    </p>
                    @if ($errors->has('username'))
                        <div class="text-red">{{ $errors->first('username') }}</div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="box">
            <div class="box-body">
                <!-- Email Field -->
                <div class="form-group">
                    <p>
                        {{ Form::label('email', 'Email: ',  ['class' => 'required']) }}
                        {!! Form::text('email', $admin->email, ['class' => 'form-control', 'autocomplete' => 'false', 'maxlength' => 50]) !!}
                    </p>
                    @if ($errors->has('email'))
                        <div class="text-red">{{ $errors->first('email') }}</div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row form-justify-container">
    <div class="col-md-6">
        <div class="box">
            <div class="box-body">
                <!-- Country Field -->
                <div class="form-group">
                    <p>
                        {{ Form::label('role_id', 'Role: ',  ['class' => 'required']) }}
                        {!! Form::select('role_id', $role, $admin->role_id, ['class' => 'form-control']) !!}
                    </p>
                    @if ($errors->has('role_id'))
                        <div class="text-red">{{ $errors->first('role_id') }}</div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div style="display: none" class="col-md-6" id="company-container">
        <div class="box">
            <div class="box-body">
                <!-- Country Field -->
                <div class="form-group">
                    <label for="company_id">Company:</label>
                    <select name="company_id" class="form-control select2" id="company_id">
                        @foreach($companies as $company)
                            <option @if($company->id === $admin->company_id) selected @endif value="{{ $company->id }}">
                                {{ $company->name }}
                            </option>
                        @endforeach
                    </select>
                    @if ($errors->has('company_id'))
                        <div class="text-red">{{ $errors->first('company_id') }}</div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
<hr />
<div class="row form-justify-container">
    <div class="col-md-6">
        <div class="box">
            <div class="box-body">
                <!-- Password Field -->
                <div class="form-group">
                    <p>
                        {{ Form::label('password', 'New Password: ') }}
                        {!! Form::password('password', ['class' => 'form-control', 'maxlength' => 20]) !!}
                    </p>
                    @if ($errors->has('password'))
                        <div class="text-red">{{ $errors->first('password') }}</div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="box">
            <div class="box-body">
                <!-- Confirm Password Field -->
                <div class="form-group">
                    <p>
                        {{ Form::label('password_confirmation', 'Confirm Password: ') }}
                        {!! Form::password('password_confirmation', ['class' => 'form-control', 'maxlength' => 20]) !!}
                    </p>
                    @if ($errors->has('password_confirmation'))
                        <div class="text-red">{{ $errors->first('password_confirmation') }}</div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Submit Field -->
<div class="form-group text-left">
    {!! Form::submit('Update', ['class' => 'btn btn-primary waves-effect waves-light']) !!}
</div>

<script>
    $(function () {
        $('.select2').select2({
            theme: 'bootstrap',
            width: '100%'
        });

        function toggleCompany() {
            if ($('#role_id option:selected').val() === '2') {
                $('#company-container').css('display', 'block')
            } else {
                $('#company-container').css('display', 'none')
            }
        }
        toggleCompany();

        $('#role_id').on('change', function () {
            toggleCompany();
        })
    })
</script>
