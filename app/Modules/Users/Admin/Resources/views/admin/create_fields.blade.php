<div class="row form-justify-container">
    <div class="col-md-6">
        <div class="box">
            <div class="box-body">
                <!-- Name Field -->
                <div class="form-group">
                    <p>
                        {{ Form::label('username', 'Username: ',  ['class' => 'required']) }}
                        {!! Form::text('username', null, ['class' => 'form-control', 'maxlength' => 100]) !!}
                    </p>
                    @if ($errors->has('username'))
                        <div class="text-red">{{ $errors->first('username') }}</div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="box">
            <div class="box-body">
                <!-- Email Field -->
                <div class="form-group">
                    <p>
                        {{ Form::label('email', 'Email: ',  ['class' => 'required']) }}
                        {!! Form::text('email', null, ['class' => 'form-control', 'autocomplete' => 'false', 'maxlength' => 50]) !!}
                    </p>
                    @if ($errors->has('email'))
                        <div class="text-red">{{ $errors->first('email') }}</div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row form-justify-container">
    <div class="col-md-6">
        <div class="box">
            <div class="box-body">
                <!-- Country Field -->
                <div class="form-group">
                    <label for="role_id" class="required">Role:</label>
                    <select name="role_id" class="form-control" id="role_id">
                        @foreach($role as $index => $item)
                            <option value="{{ $index }}">{{ $item }}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('role_id'))
                        <div class="text-red">{{ $errors->first('role_id') }}</div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div style="display: none" class="col-md-6" id="relevant-container">
        <div class="box">
            <div class="box-body">
                <div class="form-group">
                    <label for="relevant">Relevant for:</label>

                    <div class="form-control">
                        <input type="radio" name="relevant" value="company"> Company
                        <input type="radio" name="relevant" value="country"> Country
                    </div>
                </div>
            </div>
        </div>
        <div class="box">
            <div class="box-body">
                <!-- Country Field -->
                <div class="form-group" id="company-container" style="display: none">
                    <select name="company_id" class="form-control select2" id="company">
                        <option>Select a company</option>
                        @foreach($companies as $company)
                            <option value="{{ $company->id }}">{{ $company->name }}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('company_id'))
                        <div class="text-red">{{ $errors->first('company_id') }}</div>
                    @endif
                </div>
                <div class="form-group" id="country-container" style="display: none">
                        <select name="country_id" class="form-control select2" id="country">
                            <option>Select a country</option>
                            @foreach($countries as $country)
                                <option value="{{ $country->id }}">{{ $country->name }}</option>
                            @endforeach
                        </select>
                    @if ($errors->has('country'))
                        <div class="text-red">{{ $errors->first('password') }}</div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row form-justify-container">
    <div class="col-md-6">
        <div class="box">
            <div class="box-body">
                <!-- Country Field -->
                <div class="form-group">
                    <p>
                        {{ Form::label('password', 'Password: ',  ['class' => 'required']) }}
                        {!! Form::password('password', ['class' => 'form-control', 'autocomplete' => 'new-password']) !!}
                    </p>
                    @if ($errors->has('password'))
                        <div class="text-red">{{ $errors->first('password') }}</div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="box">
            <div class="box-body">
                <!-- Country Field -->
                <div class="form-group">
                    <p>
                        {{ Form::label('password_confirmation', 'Password confirm: ',  ['class' => 'required']) }}
                        {!! Form::password('password_confirmation', ['class' => 'form-control', 'autocomplete' => 'new-password']) !!}
                    </p>
                    @if ($errors->has('password_confirmation'))
                        <div class="text-red">{{ $errors->first('password_confirmation') }}</div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Submit Field -->
<div class="form-group text-left">
    {!! Form::submit('Create', ['class' => 'btn btn-primary waves-effect waves-light']) !!}
</div>

<script>
    $(function () {
        $('.select2').select2({
            theme: 'bootstrap',
            width: '100%'
        });

        $('#role_id').on('change', function () {
            if ($('#role_id option:selected').val() === '2') {
                $('#relevant-container').css('display', 'block')
            } else {
                $('#relevant-container').css('display', 'none')
                $('#company option:selected').prop("selected", false)
                $('#country option:selected').prop("selected", false)
            }
        })

        $('input[name="relevant"]').on('change', function () {
            if ($('input[name="relevant"]:checked').val() === 'company') {
                $('#company-container').css('display', 'block')
                $('#country-container').css('display', 'none')
                $('#country option:selected').prop("selected", false)

            } else if ($('input[name="relevant"]:checked').val() === 'country') {
                $('#company-container').css('display', 'none')
                $('#country-container').css('display', 'block')
                $('#company option:selected').prop("selected", false)
            }
        })
    })
</script>

