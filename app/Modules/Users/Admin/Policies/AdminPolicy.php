<?php


namespace App\Modules\Users\Admin\Policies;


use App\Modules\Users\Admin\Models\Admin;

class AdminPolicy
{
    public function view(Admin $user)
    {
        return $user->isAdmin();
    }

    public function edit(Admin $user)
    {
        return $user->isAdmin();
    }

    public function create(Admin $user)
    {
        return $user->isAdmin();
    }

    public function destroy(Admin $user)
    {
        return $user->isAdmin();
    }

    public function suspend(Admin $user)
    {
        return $user->isAdmin();
    }

    public function enable(Admin $user)
    {
        return $user->isAdmin();
    }
}
