<?php

namespace App\Modules\Users\Admin\Mails;

use App\Modules\Users\Admin\Models\Admin;
use Illuminate\Auth\Notifications\ResetPassword;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\Lang;

class ResetPasswordMail extends ResetPassword
{
    /** @var User */
    protected $user;

    /**
     * ResetPasswordMail constructor.
     * @param string $token
     * @param User $user
     */
    public function __construct(string $token, Admin $user)
    {
        parent::__construct($token);
        $this->user = $user;
    }

    /**
     * Build the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $appName = config('app.name');

        return (new MailMessage)
            ->subject(Lang::getFromJson('Reset Password Notification'))
            ->greeting("Hello!")
            ->line(Lang::getFromJson('You are receiving this email because we received a password reset request for your account.'))
            ->action(Lang::getFromJson('Reset Password'), url(config('app.url').route('auth.password.reset', $this->token, false)))
            ->line(Lang::getFromJson('This password reset link will expire in :count minutes.', ['count' => config('auth.passwords.admin.expire')]))
            ->line(Lang::getFromJson('If you did not request a password reset, no further action is required.'));
    }
}
