<?php
/**
 * Created by Andrei Podgornyi, Appus Studio LP on 08.10.2018
 */

namespace App\Modules\Users\Admin\DataTables;

use App\Modules\Users\Admin\Models\Admin;
use App\Services\CountriesRepository;
use Illuminate\Database\Eloquent\Builder;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\Html\Builder as DataTablesBuilder;

class AdminDataTable extends DataTable
{

    /**
     * Build DataTable class.
     *
     * @param mixed $query Result from query() method.
     * @return EloquentDataTable
     */
    public function dataTable($query): EloquentDataTable
    {
        $dataTable = new EloquentDataTable($query);

        $dataTable->editColumn('role_id', function ($user) {
            return $user->role->name ?? '';
        });
        $dataTable->editColumn('company_id', function ($user) {
            return $user->company->name ?? '';
        });
        $dataTable->editColumn('country_id', function ($user) {
            $country = \App\Services\CountriesRepository::make()->find($user->country_id);

            return $country ? $country->name : '';
        });
        $dataTable->editColumn('status', function ($user) {
            return $user->status == '1' ? "<span class='label label-success'>Active</span>" : "<span class='label label-danger'>Suspended</span>";
        });
        $dataTable->editColumn('created_at', function ($query) {
            return date('d-M-Y', strtotime($query->created_at));
        });
        return $dataTable->addColumn('action', 'admin.datatables_actions')
            ->rawColumns(['username', 'email', 'company_id', 'country_id', 'role_id', 'action', 'created_at', 'status']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param Admin $model
     * @return Builder
     */
    public function query(Admin $model): Builder
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return DataTablesBuilder
     */
    public function html(): DataTablesBuilder
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax(url()->current())
            ->addAction(['width' => '80px'])
            ->parameters([
                'dom' => 'frtip',
                'order' => [[0, 'desc']],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns(): array
    {
        return [
            [
                'name' => 'username',
                'data' => 'username',
                'title' => 'Username'
            ],
            [
                'name' => 'email',
                'data' => 'email',
                'title' => 'Email'
            ],
            [
                'name' => 'company_id',
                'data' => 'company_id',
                'title' => 'Company'
            ],
            // [
            //     'name' => 'country_id',
            //     'data' => 'country_id',
            //     'title' => 'Country'
            // ],
            [
                'name' => 'role_id',
                'data' => 'role_id',
                'title' => 'Role'
            ],
            [
                'name' => 'status',
                'data' => 'status',
                'title' => 'Status'
            ],
            [
                'name' => 'created_at',
                'data' => 'created_at',
                'title' => 'Registered On',
            ],
        ];
    }

}
