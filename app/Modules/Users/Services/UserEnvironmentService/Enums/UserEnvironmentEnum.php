<?php
/**
 * Created by Maksym Ignatchenko, Appus Studio LP on 01.07.19
 *
 */

namespace App\Modules\Users\Services\UserEnvironmentService\Enums;

class UserEnvironmentEnum
{
    public const COUNTRY = 'country';
    public const COMPANY = 'company';
}
