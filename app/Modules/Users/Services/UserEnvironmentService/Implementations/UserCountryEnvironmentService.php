<?php
/**
 * Created by Maksym Ignatchenko, Appus Studio LP on 01.07.19
 *
 */

namespace App\Modules\Users\Services\UserEnvironmentService\Implementations;

use Illuminate\Pagination\AbstractPaginator;

class UserCountryEnvironmentService extends AbstractUserEnvironmentService
{
    /**
     * @param int $limit
     * @return AbstractPaginator
     */
    public function getFeedsList(int $limit): AbstractPaginator
    {
        return $this->feedModel
            ->select(['feeds.*'])
            ->country($this->user->country)
            ->visible()
            ->dependent()
            // ->finishedChallenges()
            ->orderBy('feeds.created_at', 'DESC')
            ->paginate($limit);
    }

    /**
     * @param null|string $search
     * @param int|null $limit
     * @return AbstractPaginator
     */
    public function getChallengesList(?string $search, ?int $limit): AbstractPaginator
    {
        return $this->challengeModel->searchForCountryEnvironment($this->user, $search, $limit);
    }
}
