<?php
/**
 * Created by Maksym Ignatchenko, Appus Studio LP on 13.09.19
 *
 */

namespace App\Modules\Notifications\ConcreteNotifications;

use App\Modules\Notifications\Channels\FcmChannel;
use Edujugon\PushNotification\Messages\PushMessage;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Notification;

class ManualMessageNotification extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * @var string
     */
    private $message;

    /**
     * @var string
     */
    private $title;

    /**
     * ManualMessageNotification constructor.
     * @param string $message
     */
    public function __construct(string $message, string $title)
    {
        $this->message = $this->preparePushMessageString($message);
        $this->title = $this->preparePushMessageString($title);
    }

    /**
     * Get the notification channels.
     *
     * @param  mixed  $notifiable
     * @return array|string
     */
    public function via($notifiable)
    {
        return [
            FcmChannel::class,
            'database',
        ];
    }

    /**
     * @param $notifiable
     * @return PushMessage
     */
    public function toFcm($notifiable): PushMessage
    {
        $message = new PushMessage($this->message);
        $message->title($this->title);
        $message->extra = [
            'click_action' => 'FLUTTER_NOTIFICATION_CLICK'
        ];
        return $message;
    }

    /**
     * @param string $string
     * @return string
     */
    public function preparePushMessageString(string $string = ''): string
    {
        $string = preg_replace('#<br\s*?/?>#i',PHP_EOL, $string);
        $string = str_replace("&nbsp;", " ", $string);
        $string = strip_tags($string);
        return $string;
    }

    /**
     * @param $notifiable
     * @return array
     */
    public function toArray($notifiable): array
    {
        return [
            'title' => $this->title,
            'message' => $this->message,
            'data' => [
                'click_action' => 'FLUTTER_NOTIFICATION_CLICK'
            ],
        ];
    }
}
