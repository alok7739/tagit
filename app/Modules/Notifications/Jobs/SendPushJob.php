<?php
/**
 * Created by Maksym Ignatchenko, Appus Studio LP on 23.09.19
 *
 */

namespace App\Modules\Notifications\Jobs;

use Edujugon\PushNotification\PushNotification;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class SendPushJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var array
     */
    protected $tokens;

    /**
     * @var array
     */
    protected $data;

    /**
     * @var string
     */
    protected $pushServiceName;

    /**
     * SendPushJob constructor.
     * @param string $pushServiceName
     * @param array $tokens
     * @param array $data
     */
    public function __construct(string $pushServiceName, array $tokens, array $data)
    {
        $this->pushServiceName = $pushServiceName;
        $this->tokens = $tokens;
        $this->data = $data;
    }


    public function handle(): void
    {
        $pushService = new PushNotification($this->pushServiceName);
        $feedBack = $pushService->setMessage($this->data)
            ->setDevicesToken($this->tokens)
            ->send()
            ->getFeedback();
        if (! $feedBack->success) {
            Log::error(json_encode(['data' => $this->data, 'tokens' => $this->tokens, 'feedback' => $feedBack]));
        }
    }
}
