<?php
/**
 * Created by Maksym Ignatchenko, Appus Studio LP on 23.09.19
 *
 */

namespace App\Modules\Notifications\Models;

class DatabaseNotification extends \Illuminate\Notifications\DatabaseNotification
{
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'data' => 'array',
    ];

    /**
     * @return array
     */
    public function toArray(): array
    {
        $structure = [
            'id' => $this->id,
            'title' => $this->data['title'],
            'message' => $this->data['message'],
            'data' => $this->data['data'] ?? null,
            'created_at' => $this->created_at->format('U'),
        ];
        return $structure;
    }
}
