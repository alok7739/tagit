<?php
/**
 * Created by Maksym Ignatchenko, Appus Studio LP on 16.09.19
 *
 */

namespace App\Modules\Notifications\Helpers;

class PushNotificationHelper
{
    public const MESSAGE_SIZE_LIMIT = 1024;
    public const TITLE_SIZE_LIMIT = 64;
    public const FLUTTER_CLICK_ACTION = 'FLUTTER_NOTIFICATION_CLICK';
    public const MAX_DEVICES_PER_REQUEST = 100;
}
