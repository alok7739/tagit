<?php
/**
 * Created by Maksym Ignatchenko, Appus Studio LP on 23.09.19
 *
 */

Route::group(['middleware' => ['api', 'auth:api', 'auth.active']], function () {
    Route::get('me/notification', 'UserNotificationController@index');
    Route::delete('me/notification/{notification}', 'UserNotificationController@destroy');
    Route::delete('me/notification', 'UserNotificationController@destroyAll');
});
