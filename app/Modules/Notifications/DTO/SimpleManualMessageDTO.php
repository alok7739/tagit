<?php
/**
 * Created by Maksym Ignatchenko, Appus Studio LP on 17.09.19
 *
 */

namespace App\Modules\Notifications\DTO;

class SimpleManualMessageDTO extends AbstractManualMessageDTO
{
    /**
     * @return string
     */
    function getFormAction(): string
    {
        return route($this->formRoute);
    }

}
