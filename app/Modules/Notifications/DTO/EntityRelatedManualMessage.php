<?php
/**
 * Created by Maksym Ignatchenko, Appus Studio LP on 17.09.19
 *
 */

namespace App\Modules\Notifications\DTO;

use Illuminate\Database\Eloquent\Model;

class EntityRelatedManualMessage extends AbstractManualMessageDTO
{
    /**
     * @var Model
     */
    protected $relatedEntity;

    /**
     * EntityRelatedManualMessage constructor.
     * @param string $formRoute
     * @param string $pageTitle
     * @param Model $relatedEntity
     */
    public function __construct(string $formRoute, string $pageTitle, Model $relatedEntity)
    {
        parent::__construct($formRoute, $pageTitle);
        $this->relatedEntity = $relatedEntity;
    }

    /**
     * @return string
     */
    function getFormAction(): string
    {
        return route($this->formRoute, $this->relatedEntity->id);
    }
}
