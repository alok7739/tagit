<?php
/**
 * Created by Maksym Ignatchenko, Appus Studio LP on 17.09.19
 *
 */

namespace App\Modules\Notifications\DTO;

use App\Modules\Challenges\Enums\CountryEnum;

class SimpleMessageWithFiltersDTO extends SimpleManualMessageDTO
{
    /**
     * @var array
     */
    protected $countries;

    public function __construct(string $formRoute, string $pageTitle)
    {
        parent::__construct($formRoute, $pageTitle);
        $this->countries = CountryEnum::toAssocArray();
    }

    /**
     * @return array
     */
    public function getCountries(): array
    {
        return $this->countries + [null => 'All'];
    }

    /**
     * @return bool
     */
    public function isNeedFilters(): bool
    {
        return true;
    }
}
