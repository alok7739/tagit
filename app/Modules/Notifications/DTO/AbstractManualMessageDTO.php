<?php
/**
 * Created by Maksym Ignatchenko, Appus Studio LP on 17.09.19
 *
 */

namespace App\Modules\Notifications\DTO;

use App\Modules\Notifications\Helpers\PushNotificationHelper;

abstract class AbstractManualMessageDTO
{
    /**
     * @var int
     */
    protected $messageLimitSize;

    /**
     * @var int
     */
    protected $titleLimitSize;

    /**
     * @var string
     */
    protected $formRoute;

    /**
     * @var string
     */
    protected $defaultTitle;

    protected $pageTitle;

    /**
     * AbstractManualMessageDTO constructor.
     * @param string $formRoute
     * @param string $pageTitle
     */
    public function __construct(string $formRoute, string $pageTitle)
    {
        $this->formRoute = $formRoute;
        $this->messageLimitSize = PushNotificationHelper::MESSAGE_SIZE_LIMIT;
        $this->titleLimitSize = PushNotificationHelper::TITLE_SIZE_LIMIT;
        $this->defaultTitle = env('APP_NAME', 'Title');
        $this->pageTitle = $pageTitle;
    }

    /**
     * @return int
     */
    public function getMessageLimitSize(): int
    {
        return $this->messageLimitSize;
    }

    /**
     * @return int
     */
    public function getTitleLimitSize(): int
    {
        return $this->titleLimitSize;
    }

    /**
     * @return string
     */
    public function getDefaultTitle(): string
    {
        return $this->defaultTitle;
    }

    /**
     * @return string
     */
    public function getPageTitle(): string
    {
        return $this->pageTitle;
    }

    /**
     * @return bool
     */
    public function isNeedFilters(): bool
    {
        return false;
    }

    /**
     * @return string
     */
    abstract function getFormAction(): string;
}
