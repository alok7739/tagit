<?php
/**
 * Created by Maksym Ignatchenko, Appus Studio LP on 23.09.19
 *
 */

namespace App\Modules\Notifications\Traits;

use Illuminate\Notifications\RoutesNotifications;

trait Notifiable
{
    use HasDatabaseNotifications, RoutesNotifications;
}
