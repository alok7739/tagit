<?php
/**
 * Created by Maksym Ignatchenko, Appus Studio LP on 13.09.19
 *
 */

namespace App\Modules\Notifications\Channels;

use App\Modules\Notifications\Helpers\PushNotificationHelper;
use App\Modules\Notifications\Jobs\SendPushJob;
use Edujugon\PushNotification\Channels\FcmChannel as BaseFcmChannel;
use Edujugon\PushNotification\Messages\PushMessage;
use Illuminate\Notifications\Notification;

class FcmChannel extends BaseFcmChannel
{
    /**
     * Send the given notification.
     *
     * @param  mixed  $notifiable
     * @param  \Illuminate\Notifications\Notification  $notification
     * @return void
     */
    public function send($notifiable, Notification $notification)
    {
        if (! $to = $notifiable->routeNotificationFor($this->notificationFor())) {
            return;
        }

        $iosTokens = [];
        $androidTokens = [];
        if (is_array($to)) {
            foreach ($to as $token) {

                switch($token->type) {
                    case 'ios' :
                        $iosTokens[] = $token->token;
                        break;
                    case 'android' :
                        $androidTokens[] = $token->token;
                        break;
                }
            }
        }

        $message = $this->buildMessage($notifiable, $notification);

        $androidData = $this->buildAndroidData($message);
        $iosData = $this->buildIosData($message);

        if (!empty($iosTokens)) {
            $chunkedIosTokens = $this->chunkTokens($iosTokens);
            foreach($chunkedIosTokens as $chunk) {
                dispatch(new SendPushJob($this->pushServiceName(), $chunk,  $iosData));
            }

        }

        if (!empty($androidTokens)) {
            $chunkedAndroidTokens = $this->chunkTokens($androidTokens);
            foreach($chunkedAndroidTokens as $chunk) {
                dispatch(new SendPushJob($this->pushServiceName(), $chunk,  $androidData));
            }

        }
    }

    /**
     * @param array $tokens
     * @return array
     */
    protected function chunkTokens(array $tokens): array
    {
        return array_chunk($tokens, PushNotificationHelper::MAX_DEVICES_PER_REQUEST);
    }

    /**
     * @param PushMessage $message
     * @return array
     */
    protected function buildIosData(PushMessage $message): array
    {
        $data = [];

        if (! empty($message->extra)) {
            $data['data'] = $message->extra;
        }
        $data['notification'] = [
            'title' => $message->title ?? '',
            'body' => $message->body ?? '',
        ];

        return $data;
    }

    /**
     * @param PushMessage $message
     * @return array
     */
    protected function buildAndroidData(PushMessage $message): array
    {
        $data = [];
        if ($message->title != null || $message->body != null || $message->click_action != null) {
            $data = [
                'notification' => [
                    'title' => $message->title,
                    'body' => $message->body,
                    'sound' => $message->sound,
                ],
            ];

            // Set custom badge number when isset in PushMessage
            if (! empty($message->badge)) {
                $data['notification']['badge'] = $message->badge;
            }

            // Set icon when isset in PushMessage
            if (! empty($message->icon)) {
                $data['notification']['icon'] = $message->icon;
            }
        }

        if (! empty($message->extra)) {
            $data['data'] = $message->extra;
            $data['data']['click_action'] = PushNotificationHelper::FLUTTER_CLICK_ACTION;
        }

        return $data;
    }
}
