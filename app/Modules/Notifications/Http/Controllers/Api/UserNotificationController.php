<?php
/**
 * Created by Maksym Ignatchenko, Appus Studio LP on 23.09.19
 *
 */

namespace App\Modules\Notifications\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Modules\Notifications\Models\DatabaseNotification;
use App\Services\ResponseBuilder\ApiCode;
use App\Services\ResponseBuilder\CustomResponseBuilder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Symfony\Component\HttpFoundation\Response;

class UserNotificationController extends Controller
{
    /**
     * @param Request $request
     * @return Response
     */
    public function index(Request $request): Response
    {
        $user = $request->user();
        $limitPerPage = (int) $request->get('limit') ?? config('custom.user_notifications_per_page');
        $notifications = $user->notifications()->paginate($limitPerPage);
        return CustomResponseBuilder::success($notifications);
    }

    /**
     * @param Request $request
     * @param DatabaseNotification $notification
     * @return Response
     * @throws \Exception
     */
    public function destroy(Request $request, DatabaseNotification $notification): Response
    {
        if (Gate::forUser($request->user())->allows('destroy-notification', $notification)) {
            $notification->delete();
            return CustomResponseBuilder::success();
        }
        return CustomResponseBuilder::error(ApiCode::UNAUTHORISED_ACTION);

    }

    /**
     * @param Request $request
     * @return Response
     */
    public function destroyAll(Request $request): Response
    {
        $user = $request->user();
        $user->notifications()->delete();
        return CustomResponseBuilder::success();
    }
}
