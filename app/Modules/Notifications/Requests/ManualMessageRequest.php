<?php
/**
 * Created by Maksym Ignatchenko, Appus Studio LP on 16.09.19
 *
 */

namespace App\Modules\Notifications\Requests;

use App\Modules\Challenges\Enums\CountryEnum;
use App\Modules\Notifications\Helpers\PushNotificationHelper;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ManualMessageRequest extends FormRequest
{
    /**
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'message' => 'required|string|max:' . PushNotificationHelper::MESSAGE_SIZE_LIMIT,
            'title' => 'required|string|max:' . PushNotificationHelper::TITLE_SIZE_LIMIT,
            'country' => ['nullable', 'string', 'min:1', 'max:100'],
        ];
    }
}
