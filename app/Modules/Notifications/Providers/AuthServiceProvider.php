<?php
/**
 * Created by Maksym Ignatchenko, Appus Studio LP on 23.09.19
 *
 */

namespace App\Modules\Notifications\Providers;

use App\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('destroy-notification', function ($user, $notification) {
            return $user->id == $notification->notifiable_id;
        });
    }
}
