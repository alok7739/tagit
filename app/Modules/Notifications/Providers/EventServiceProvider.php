<?php
/**
 * Created by Maksym Ignatchenko, Appus Studio LP on 13.09.19
 *
 */

namespace App\Modules\Notifications\Providers;

use App\Modules\Notifications\Events\Interfaces\NotifiableEventInterface;
use App\Modules\Notifications\Listeners\NotificationListener;
use App\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    protected $listen = [
        NotifiableEventInterface::class => [
            NotificationListener::class,
        ],
    ];
}
