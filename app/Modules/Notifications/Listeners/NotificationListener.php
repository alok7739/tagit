<?php
/**
 * Created by Maksym Ignatchenko, Appus Studio LP on 13.09.19
 *
 */

namespace App\Modules\Notifications\Listeners;

use App\Modules\Notifications\Events\Interfaces\NotifiableEventInterface;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Notification;

class NotificationListener
{
    public function handle(NotifiableEventInterface $event)
    {
        Notification::send($event->getNotifiables(),$event->getNotification());
    }

    public function failed(NotifiableEventInterface $event, \Exception $exception)
    {
        Log::warning($exception->getMessage());
    }
}
