<?php
/**
 * Created by Maksym Ignatchenko, Appus Studio LP on 13.09.19
 *
 */

namespace App\Modules\Notifications\Events;

use App\Modules\Notifications\ConcreteNotifications\ManualMessageNotification;
use App\Modules\Notifications\Events\Interfaces\NotifiableEventInterface;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Collection;

class MessageSentEvent implements NotifiableEventInterface
{
    /**
     * @var array
     */
    private $users;

    /**
     * @var string
     */
    private $message;

    /**
     * @var
     */
    private $title;

    /**
     * MessageSentEvent constructor.
     * @param Collection $users
     * @param string $message
     * @param string $title
     */
    public function __construct(Collection $users, string $message, string $title)
    {
        $this->users = $users;
        $this->message = $message;
        $this->title = $title;
    }

    /**
     * @return Collection
     */
    public function getNotifiables(): Collection
    {
        return $this->users;
    }

    /**
     * @return Notification
     */
    public function getNotification(): Notification
    {
        return new ManualMessageNotification($this->message, $this->title);
    }

}
