<?php
/**
 * Created by Maksym Ignatchenko, Appus Studio LP on 13.09.19
 *
 */

namespace App\Modules\Notifications\Events\Interfaces;

use Illuminate\Support\Collection;

interface NotifiableEventInterface
{
    public function getNotification();

    public function getNotifiables(): Collection;
}
