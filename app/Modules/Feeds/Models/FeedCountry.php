<?php
/**
 * Created by Maksym Ignatchenko, Appus Studio LP on 15.04.19
 *
 */

namespace App\Modules\Feeds\Models;

use App\Models\BaseModel;
use App\Modules\Challenges\Models\ChallengeWithoutAppends;
use App\Modules\Challenges\Models\Proof;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class FeedCountry extends BaseModel
{
    public $table = 'feeds_country';

    /**
     * @var array
     */
    protected $with = [
        'challenge',
        'proof',
    ];

    /**
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime:U',
        'updated_at' => 'datetime:U',
    ];

    /**
     * @return BelongsTo
     */
    public function challenge() : BelongsTo
    {
        return $this->belongsTo(ChallengeWithoutAppends::class, 'challenge_id', 'id', 'challenges');
    }

    /**
     * @return BelongsTo
     */
    public function proof() : BelongsTo
    {
        return $this->belongsTo(Proof::class)->with(['user', 'challenge']);
    }

    /**
     * @param $query
     * @param $userCountry
     * @return Builder
     */
    public function scopeCountry($query, $userCountry) : Builder
    {
        return $query->where('country', $userCountry);
    }

    /**
     * @param $query
     * @return Builder
     */
    public function scopeFinishedChallenges($query) : Builder
    {
        return $query->where(function ($q1) {
            $q1->where(function ($q5) {
                $q5->whereHas('challenge', function ($q6) {
                    $q6->where('status', 'end');
                });
            })->orWhere(function ($q2) {
                $q2->whereNull('challenge_id')
                    ->whereHas('proof', function ($q3) {
                        $q3->whereHas('challenge', function ($q4) {
                            $q4->where('status', 'end');
                        });
                    });
            });
        });
    }
}
