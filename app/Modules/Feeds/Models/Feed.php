<?php
/**
 * Created by Maksym Ignatchenko, Appus Studio LP on 15.04.19
 *
 */

namespace App\Modules\Feeds\Models;

use App\Models\BaseModel;
use App\Modules\Challenges\Models\Challenge;
use App\Modules\Challenges\Models\ChallengeWithoutAppends;
use App\Modules\Challenges\Models\Proof;
use App\Modules\Companies\Models\Company;
use App\Modules\Users\User\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Feed extends BaseModel
{
    /**
     * @var array
     */
    protected $with = [
        'challenge',
        'proof',
        'companies'
    ];

    /**
     * @var array
     */
    protected $appends = [
        'likes_count',
        'is_liked',
        'comments_count',
    ];

    /**
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime:U',
        'updated_at' => 'datetime:U',
    ];

    /**
     * @return BelongsTo
     */
    public function challenge() : BelongsTo
    {
        return $this->belongsTo(ChallengeWithoutAppends::class, 'challenge_id', 'id', 'challenges');
    }

    /**
     * @return BelongsTo
     */
    public function proof() : BelongsTo
    {
        return $this->belongsTo(Proof::class)->with(['user', 'challenge']);
    }

    public function companies()
    {
        return $this->belongsTo(Company::class, 'company_id');
    }

    /**
     * @param $query
     * @param $userCountry
     * @return Builder
     */
    public function scopeCountry($query, $userCountry) : Builder
    {
        return $query->where('feeds.country', $userCountry);
    }

    /**
     * @param $query
     * @return Builder
     */
    public function scopeFinishedChallenges($query) : Builder
    {
        return $query->where(function ($q1) {
            $q1->where(function ($q5) {
                $q5->whereHas('challenge', function ($q6) {
                    $q6->where('status', 'end');
                });
            })->orWhere(function ($q2) {
                $q2->whereNull('challenge_id')
                    ->whereHas('proof', function ($q3) {
                        $q3->whereHas('challenge', function ($q4) {
                            $q4->where('status', 'end');
                        });
                    });
            });
        });
    }

    /**
     * @param $query
     * @param $company_id
     * @return Builder
     */
    public function scopeCompany($query, $company_id) : Builder
    {
        return $query->where('feeds.company_id', $company_id);
    }

    public function scopeVisible($query) : Builder
    {
        return $query->where('visible', '1');
    }

    public function scopeDependent($query) : Builder
    {
        $ids = Challenge::getAvailableChallengeIds(\Auth::user());
        $ids = $ids ? implode(',', $ids) : 0;

        return $query->leftJoin('challenges', 'challenges.id', 'feeds.challenge_id')
                    ->leftJoin('proofs', 'proofs.id', 'feeds.proof_id')
                    ->whereRaw('(challenges.id IN (' . $ids . ') OR proofs.challenge_id IN (' . $ids . '))');
    }

    public function likes()
    {
        return $this->belongsToMany(User::class, 'challenge_like', 'feed_id', 'user_id')->withTimestamps();
    }

    public function comments()
    {
        return $this->belongsToMany(User::class, 'challenge_comment', 'feed_id', 'user_id')->withTimestamps();
    }

    public function getLikesCountAttribute(): int
    {
        return $this->likes()->count();
    }

    public function getCommentsCountAttribute(): int
    {
        return $this->comments()->count();
    }

    public function getIsLikedAttribute(): bool
    {
        return $this->likes()
            ->wherePivot('user_id', \Auth::id())
            ->get()
            ->isNotEmpty();
    }
}
