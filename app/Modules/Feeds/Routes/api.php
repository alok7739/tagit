<?php
/**
 * Created by Maksym Ignatchenko, Appus Studio LP on 15.04.19
 *
 */

Route::group(['middleware' => ['api', 'auth:api', 'auth.active']], function () {
    Route::resource('feed', 'FeedController')->only(['index', 'show']);
});
