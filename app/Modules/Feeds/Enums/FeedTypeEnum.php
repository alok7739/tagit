<?php
/**
 * Created by Maksym Ignatchenko, Appus Studio LP on 15.04.19
 *
 */

namespace App\Modules\Feeds\Enums;

class FeedTypeEnum
{
    public const CHALLENGE_STARTED = 'challenge_started';
    public const CHALLENGE_ENDED = 'challenge_ended';
    public const PROOF_SENT = 'proof_sent';
}