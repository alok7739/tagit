<?php
/**
 * Created by Maksym Ignatchenko, Appus Studio LP on 15.04.19
 *
 */

namespace App\Modules\Feeds\Events;

class ChallengeStartedEventListener extends ChallengeStatusChangedAbstractEvent
{
}