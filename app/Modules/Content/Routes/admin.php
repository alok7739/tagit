<?php
/**
 * Created by Artem Petrov, Appus Studio LP on 17.11.2017
 */

Route::group(['middleware' => 'auth:admin', 'prefix' => 'content'], function () {
    Route::get('/', 'ContentController@index')->name('content.index')->middleware('can:view,App\Modules\Content\Models\Content');
    Route::put('/{content}', 'ContentController@update')->name('content.update')->middleware('can:edit,App\Modules\Content\Models\Content');
});
