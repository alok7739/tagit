<?php
/**
 * Created by Artem Petrov, Appus Studio LP on 17.11.2017
 */

Route::group(['middleware' => ['api', 'auth.active'], 'prefix' => 'content'], function () {
    Route::get('/{content}', 'ContentController@get');
});
