<?php


namespace App\Modules\Content\Policies;


use App\Modules\Users\Admin\Models\Admin;

class ContentPolicy
{
    public function view(Admin $user)
    {
        return $user->isAdmin();
    }

    public function edit(Admin $user)
    {
        return $user->isAdmin();
    }
}
