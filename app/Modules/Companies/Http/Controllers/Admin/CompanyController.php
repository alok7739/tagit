<?php
/**
 * Created by Maksym Ignatchenko, Appus Studio LP on 26.02.19
 *
 */

namespace App\Modules\Companies\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Modules\Challenges\Datatables\CompanyChallengeDataTable;
use App\Modules\Challenges\Datatables\CompanyChallengeDataTableScope;
use App\Modules\Companies\Models\Company;
use App\Modules\Companies\Datatables\CompanyDataTable;
use App\Modules\Companies\Http\Requests\Admin\StoreCompanyRequest;
use App\Modules\Companies\Http\Requests\Admin\UpdateCompanyRequest;
use App\Services\ResponseBuilder\CustomResponseBuilder;
use App\Modules\Challenges\Models\Challenge;
use App\Modules\Challenges\Enums\ProofStatusEnum;
use App\Modules\Challenges\Enums\ChallengeStatusEnum;
use App\Modules\Onboarding\Models\Cart;
use App\Modules\Onboarding\Models\CompanyPackages;
use App\Modules\Onboarding\Models\Packages;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Laracasts\Flash\Flash;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param CompanyDataTable $dataTable
     * @return mixed
     */
    public function index(CompanyDataTable $dataTable)
    {
        return $dataTable->render('company.index');
    }

    /**
     * Display the specified resource.
     *
     * @param CompanyChallengeDataTable $dataTable
     * @param Company $company
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(CompanyChallengeDataTable $dataTable, Company $company, string $type = null)
    {
        $session = request()->get('company_session', 0);

        if($session) {
            request()->session()->put('company_id', $company->id);
        }

        $total_challenges = Challenge::where('company_id', $company->id)->count();
        $active_challenges = Challenge::where('company_id', $company->id)->where('status', 'active')->count();

        $total_users = DB::table('challenge_user')
                            ->select([DB::raw('COUNT(DISTINCT user_id) AS cnt')])
                            ->whereIn('challenge_id', function ($query) use ($company) {
                                $query->select(['id'])
                                        ->from('challenges')
                                        ->where('company_id', $company->id)
                                        ->where('status', '!=', ChallengeStatusEnum::ARCHIVE);
                            })
                            ->first();

        $pending_proofs = DB::table('proofs')
                                ->select([DB::raw('COUNT(user_id) AS cnt')])
                                ->where('status', ProofStatusEnum::PENDING)
                                ->whereIn('challenge_id', function ($query) use ($company) {
                                    $query->select(['id'])
                                            ->from('challenges')
                                            ->where('company_id', $company->id)
                                            ->where('status', '!=', ChallengeStatusEnum::ARCHIVE);
                                })
                                ->first();

        $total_users = $total_users ? $total_users->cnt : 0;
        $pending_proofs = $pending_proofs ? $pending_proofs->cnt : 0;

        $can_create = $company->canCreateChallenge();
        $package = $company->getCurrentPackage();

        $current_limit = $package && $package->games ? $package->games : 0;
        $can_upgrade = Packages::where('challenges', '>', $current_limit)->count();

        return $dataTable
            ->addScope(new CompanyChallengeDataTableScope($company->id, $type))
            ->render('company.show', compact('company', 'package', 'type', 'total_users', 'total_challenges', 'active_challenges', 'pending_proofs', 'can_create', 'can_upgrade'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\View\View
     */
    public function create()
    {
        return view('company.create');
    }

    /**
     * @param StoreCompanyRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreCompanyRequest $request)
    {
        $company = app()[Company::class];
        $company->fill($request->all());
        $company->save();
        Flash::success('Company created successfully.');

        if(\Auth::user()->isSuperAdmin()) {
            $package = CompanyPackages::find($company->package_id);
            $package = $package ? $package : new CompanyPackages;
            $package->fill($request->all());
            $package->company_id = $company->id;
            $package->save();

            $company->package_id = $package->id;
            $company->save();
        }

        return redirect()->route('company.index');
    }

    /**
     * @param Company $company
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Company $company)
    {
        $package = CompanyPackages::find($company->package_id);

        if(\Auth::user()->isSuperAdmin() && !$package) {
            $package = new CompanyPackages;

            $package->expiry     = null;
            $package->users      = null;
            $package->games      = null;
            $package->true_false = null;
            $package->sc_ques    = null;
            $package->mc_ques    = null;
            $package->checkin    = null;
            $package->qr_code    = null;
            $package->images     = null;
            $package->screenshots = null;
            $package->videos     = null;
            $package->locking    = null;
        }

        return view('company.edit', ['company' => $company, 'package' => $package]);
    }

    /**
     * @param UpdateCompanyRequest $request
     * @param Company $company
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateCompanyRequest $request, Company $company)
    {
        $company->update($request->all());
        Flash::success('Company updated successfully.');

        if(\Auth::user()->isSuperAdmin()) {
            $package = CompanyPackages::find($company->package_id);
            $package = $package ? $package : new CompanyPackages;
            $package->fill($request->all());
            $package->company_id = $company->id;
            $package->save();

            $company->package_id = $package->id;
            $company->save();
        }

        $company_id = (\Auth::user()->company_id !== null ? \Auth::user()->company_id : 0);

        if($company_id) {
            return redirect()->route('company.show', ['company' => $company_id]);
        } else {
            return redirect()->route('company.index');
        }
    }

    /**
     * @param Company $company
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function destroy(Company $company)
    {
        $company->delete();
        Flash::success('Company deleted successfully.');

        return CustomResponseBuilder::success();
    }

    public function resetCoins(Company $company)
    {
        $company->resetCoins();

        Flash::success('Points and rating have been reset');

        return redirect()->back();
    }

    public function upgrade(Company $company)
    {
        $current_packege = $company->getCurrentPackage();
        $current_limit = $current_packege ? $current_packege->games : 0;

        $packages = Packages::where('challenges', '>', $current_limit)->get();

        return view('company.upgrade', compact('packages', 'current_packege'));
    }

    public function storeUpgrade(Company $company)
    {
        $data = request()->except('_token');

        $id = request()->get('pid', 0);
        $package = Packages::find($id);

        $cart_id = md5(time() . \Auth::user()->email);
        $response = [];

        if($package && $package->price > 0)
        {
            $paymentData = [
                "profile_id"       => env('PAYTAB_ID'),
                "tran_type"        => "sale",
                "tran_class"       => "ecom",
                "cart_description" => $package->name . ' - ' . $data['users'] . ' user',
                "cart_id"          => $cart_id,
                "cart_currency"    => "SAR",
                "cart_amount"      => $package->price * $package->time_limit * $data['users'],
                "return"           => url('/admin/upgrade-callback', [], true),
                "hide_shipping"    => true,
                "customer_details" =>  [
                    "name"    => $company->name,
                    "email"   => \Auth::user()->email,
                    "city"    => "Jeddah",
                    "country" => "SA",
                ]
            ];

            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => env('PAYTAB_URL'),
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_SSL_VERIFYPEER => FALSE,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS => json_encode($paymentData),
                CURLOPT_HTTPHEADER => array(
                    "Authorization: " . env('PAYTAB_SECRET'),
                    "content-type: application/json"
                ),
            ));

            $response = curl_exec($curl);
            $curl_err = curl_error($curl);
            curl_close($curl);

            $response = json_decode($response, 1);

            if(empty($response['redirect_url'])) {
                \Flash::error('Unable to process your requested, please try again later.');
                return redirect('/register?package=' . $id);
            }

            $data['company_id'] = $company->id;
            $data['package'] = $package->toArray();
        } else {
            return redirect()->route('company.show', ['company' => $company->id]);
        }

        // Insert into cart
        $cart = new Cart;
        $cart->cart_hash = $cart_id;
        $cart->tran_ref  = isset($response['tran_ref']) ? $response['tran_ref'] : '';
        $cart->data      = json_encode($data);
        $cart->save();

        return redirect($response['redirect_url']);
    }

    public function paymentCallback(Request $request)
    {
        $data = $request->except('signature');
        ksort($data);

        $query = http_build_query($data);

        $signature = hash_hmac('sha256', $query, env('PAYTAB_SECRET'));

        if (hash_equals($signature, $request->get("signature")) === TRUE)
        {
            $cart = Cart::where('cart_hash', $request->get('cartId'))->first();
            $data = json_decode($cart->data);

            $company = Company::find($data->company_id);

            // Insert in company_packages
            $package = new CompanyPackages;
            $package->company_id = $company->id;
            $package->package_id = $data->pid;
            $package->users      = $data->users;
            $package->games      = $data->package->challenges;
            $package->amount     = $data->package->price * $data->package->time_limit * $data->users;
            $package->expiry     = $data->package->time_limit > 0 ? date('Y-m-d', strtotime('+' . $data->package->time_limit . ' months')) : null;
            $package->true_false = $data->package->true_false;
            $package->sc_ques    = $data->package->sc_ques;
            $package->mc_ques    = $data->package->mc_ques;
            $package->checkin    = $data->package->checkin;
            $package->qr_code    = $data->package->qr_code;
            $package->images     = $data->package->images;
            $package->screenshots = $data->package->screenshots;
            $package->videos     = $data->package->videos;
            $package->locking    = $data->package->locking;
            $package->payment_response = json_encode($request->all());
            $package->save();

            // Update companies package_id
            $company->package_id = $package->id;
            $company->save();

            $cart->delete();

            // Redirect
            Flash::success('Your account has been upgraded successfully.');
            return redirect()->route('company.show', ['company' => $company->id]);
        }
        else {
            return redirect('/admin');
        }
    }
}
