<?php
/**
 * Created by Maksym Ignatchenko, Appus Studio LP on 19.09.19
 *
 */

namespace App\Modules\Companies\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Modules\Companies\Models\Company;
use App\Modules\Notifications\DTO\EntityRelatedManualMessage;
use App\Modules\Notifications\Events\MessageSentEvent;
use App\Modules\Notifications\Requests\ManualMessageRequest;
use App\Modules\Users\User\Models\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

class NotificationController extends Controller
{
    public function showGroupOfUsersNotificationForm(Company $company): View
    {
        $dto = new EntityRelatedManualMessage('company.users.notification-send', 'Send messages to users related to at least one of the company challenge', $company);
        return view('message', ['dto' => $dto]);
    }

    public function sendGroupNotification(ManualMessageRequest $request, Company $company): RedirectResponse
    {
        $userModel = app(User::class);
        $users = $userModel->hasCompanyChallenge($company->id)->get();

        event(new MessageSentEvent($users, $request->message, $request->title));
        return redirect()->route('company.show', $company->id);
    }
}
