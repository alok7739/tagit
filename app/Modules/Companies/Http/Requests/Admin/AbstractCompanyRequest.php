<?php
/**
 * Created by Maksym Ignatchenko, Appus Studio LP on 26.02.19
 *
 */

namespace App\Modules\Companies\Http\Requests\Admin;

use App\Modules\Companies\Rules\ChallengeLimitRule;
use Illuminate\Foundation\Http\FormRequest;

abstract class AbstractCompanyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $commonRules = [
            'logo' => 'nullable|string|max:100',
            'info' => 'required|string|max:1000',
            'games' => ['nullable', 'int', new ChallengeLimitRule($this->company)],
            'youtube_link' => ['nullable', 'url', 'max:255', 'regex:/^((?:https?:)\/\/)((?:www|m)\.)?((?:youtube\.com|youtu.be))(\/(?:[\w\-]+\?v=|embed\/|v\/)?)([\w\-]+)(\S+)?$/m']
        ];
        return array_merge(
            $commonRules,
            $this->getNameRule(),
            $this->getJoinCodeRule()
        );
    }

    public function attributes()
    {
        return [
            'join_code' => 'Join code',
        ];
    }

    /**
     * @return array
     */
    abstract public function getNameRule(): array;

    /**
     * @return array
     */
    abstract public function getJoinCodeRule(): array;
}
