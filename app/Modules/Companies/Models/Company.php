<?php
/**
 * Created by Maksym Ignatchenko, Appus Studio LP on 25.02.19
 *
 */

namespace App\Modules\Companies\Models;

use App\Models\BaseModel;
use App\Modules\Challenges\Models\Challenge;
use App\Modules\Companies\Enums\CompanyTypeEnum;
use App\Modules\Onboarding\Models\CompanyPackages;
use App\Modules\Users\User\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class Company extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'companies';

    public $fillable = [
        'name',
        'logo',
        'info',
        'join_code',
        'youtube_link',
        'email_sent',
        'person_name',
        'phone'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['join_code'];

    /**
     * The model's default values for attributes.
     *
     * @var array
     */
    protected $attributes = [
        'type' => CompanyTypeEnum::COMMERCIAL,
    ];

    protected const DEFAULT_MODEL_TYPE = CompanyTypeEnum::ARCHIEVE;

    public function users()
    {
        return $this->hasMany(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function challenges()
    {
        return $this->hasMany(Challenge::class);
    }

    /**
     * @return string
     */
    public function getLogoAttribute($value): ?string
    {
        return $value ? Storage::url($value) : null;
    }

    /**
     * @return string
     */
    public function getLogoWithDefaultAttribute($value): ?string
    {
        return $value ? Storage::url($value) : '/assets/images/default_company.svg';
    }

    /**
     * @param $attribute
     */
    public function setLogoAttribute($attribute)
    {
        if (null !== $this->logo) {
            Storage::delete($this->logo);
        }

        $this->attributes['logo'] = $attribute;
    }

    /**
     * @param string $joinCode
     * @return Company
     */
    public function getCompanyByJoinCode(?string $joinCode) : Company
    {
        return $this->withCount(['users'])->whereNotNull('join_code')->where('join_code', 'ILIKE', $joinCode)->first() ?? $this;
    }

    public function generateUniqueJoinPassword(): void
    {
        do {
            $randomCode = str_random(config('custom.company_join_code_length'));
        } while ($this->getCompanyByJoinCode($randomCode)->exists);
        $this->attributes['join_code'] = $randomCode;
    }

    public function detachCompanies(): void
    {
        $this->challenges()->update([
            'company_id' => $this->getDefaultCompanyId(),
        ]);
    }

    /**
     * @return int
     */
    public function getDefaultCompanyId(): int
    {
        return $this->where('type', self::DEFAULT_MODEL_TYPE)->first(['id'])->id;
    }

    public function detachUsers(): void
    {
        $this->users()->update([
            'company_id' => null,
        ]);
    }

    /**
     * @param $query
     * @return Builder
     */
    public function scopeCommercial($query): Builder
    {
        return $query->where('type', CompanyTypeEnum::COMMERCIAL);
    }

    public function resetCoins()
    {
        $id = $this->id;
        $this->users->each(function (User $user) use ($id) {
            $user->resetPrivateCoinsAndRating($id);
        });
    }

    public function canCreateChallenge()
    {
        if($this->package_id) {
            $package = CompanyPackages::find($this->package_id);
            $currentCount = Challenge::where('company_id', $this->id)
                                    ->whereBetween('created_at', [date('Y-m-01'), date('Y-m-t')])
                                    ->count();

            if($package->expiry) {
                $expiry = strtotime($package->expiry . ' 23:59:59');
                $today  = time();

                $valid_expiry = $expiry > $today;
            } else {
                $valid_expiry = true;
            }

            if($package->games > 0) {
                $valid_limit = $currentCount < $package->games;
                return $valid_expiry && $valid_limit;
            }else
                return $valid_expiry;


        }

        return false;
    }

    public function isChallengeTypeAllowed($type)
    {
        if($this->package_id) {
            $package = CompanyPackages::find($this->package_id);

            switch ($type) {
                case 'photo':
                case 'multiple_photos':
                    $valid = $package->images;
                    break;

                case 'video':
                case 'multiple_videos':
                    $valid = $package->videos;
                    break;

                case 'screenshot':
                case 'multiple_screenshots':
                    $valid = $package->screenshots;
                    break;

                case 'questions':
                    $valid = $package->sc_ques;
                    break;

                case 'true_false':
                    $valid = $package->true_false;
                    break;

                case 'tests':
                    $valid = $package->mc_ques;
                    break;

                case 'qrcode':
                    $valid = $package->qr_code;
                    break;

                case 'checkin':
                    $valid = $package->checkin;
                    break;

                default:
                    $valid = false;
                    break;
            }

            return $valid ? true : false;
        }

        return false;
    }

    public static function getAllowedProofTypes($companyId)
    {
        $proof_types = [];

        if($companyId) {
            $company = Company::find($companyId);
            $package = CompanyPackages::find($company->package_id);

            if($package) {
                if($package->true_false) {
                   $proof_types[] = 'true_false';
                }

                if($package->sc_ques) {
                    $proof_types[] = 'questions';
                }

                if($package->mc_ques) {
                    $proof_types[] = 'tests';
                }

                if($package->checkin) {
                    $proof_types[] = 'checkin';
                }

                if($package->qr_code) {
                    $proof_types[] = 'qrcode';
                }

                if($package->images) {
                    $proof_types[] = 'photo';
                    $proof_types[] = 'multiple_photos';
                }

                if($package->screenshots) {
                    $proof_types[] = 'screenshot';
                    $proof_types[] = 'multiple_screenshots';
                }

                if($package->videos) {
                    $proof_types[] = 'video';
                    $proof_types[] = 'multiple_videos';
                }
            }
        }

        return $proof_types;
    }

    public function getCurrentPackage()
    {
        if($this->package_id) {
            $package = CompanyPackages::find($this->package_id);
            return $package;
        }

        return false;
    }
}
