<?php
/**
 * Created by Maksym Ignatchenko, Appus Studio LP on 25.02.19
 *
 */

Route::group(['middleware' => 'auth:admin'], function () {
    Route::post('company', 'CompanyController@store')->name('company.store')->middleware('can:create,App\Modules\Companies\Models\Company');
    Route::get('company', 'CompanyController@index')->name('company.index');
    Route::get('company/create', 'CompanyController@create')->name('company.create')->middleware('can:create,App\Modules\Companies\Models\Company');

    Route::get('company/{company}/upgrade', 'CompanyController@upgrade')->name('company.upgrade');
    Route::post('company/{company}/upgrade', 'CompanyController@storeUpgrade')->name('company.upgrade');

    Route::delete('company/{company}', 'CompanyController@destroy')->name('company.destroy')->middleware('can:destroy,App\Modules\Companies\Models\Company');
    Route::get('company/{company}/edit', 'CompanyController@edit')->name('company.edit')->middleware('can:edit,company');
    Route::get('company/{company}/{type?}', 'CompanyController@show')->name('company.show')->middleware('can:view,company');

    Route::put('company/{company}/challenges/reset-coins', 'CompanyController@resetCoins')
        ->name('company.challenges.reset-coins')
        ->middleware('can:resetCoins,company');

    Route::match(array('PATCH', 'PUT'), "company/{company}", array(
        'uses' => 'CompanyController@update',
        'as' => 'company.update'
    ))->middleware('can:edit,company');

    Route::get('company/{company}/users/notification', 'NotificationController@showGroupOfUsersNotificationForm')->name('company.users.notification-form')->middleware('can:notification,company');
    Route::post('company/{company}/users/notification', 'NotificationController@sendGroupNotification')->name('company.users.notification-send')->middleware('can:notification,company');

});

Route::post('upgrade-callback', 'CompanyController@paymentCallback');
