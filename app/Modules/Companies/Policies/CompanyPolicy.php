<?php


namespace App\Modules\Companies\Policies;


use App\Modules\Companies\Models\Company;
use App\Modules\Users\Admin\Models\Admin;

class CompanyPolicy
{
    public function view(Admin $admin, Company $company)
    {
        if ($admin->isNotAdmin() && $admin->company_id !== $company->id) {
            abort(403, 'Sorry, you are not allowed to enter other companies');
        }

        return true;
    }

    public function edit(Admin $admin, Company $company)
    {
        if ($admin->isNotAdmin() && $admin->company_id !== $company->id) {
            abort(403, 'Sorry, you are not allowed to enter other companies');
        }

        return true;
    }

    public function create(Admin $admin)
    {
        return $admin->isAdmin();
    }

    public function destroy(Admin $admin)
    {
        return $admin->isAdmin();
    }

    public function notification(Admin $admin, Company $company)
    {
        if ($admin->isNotAdmin() && $admin->company_id !== $company->id) {
            abort(403, 'Sorry, you are not allowed to enter other companies');
        }

        return true;
    }

    public function resetCoins(Admin $admin, Company $company)
    {
        if ($admin->isNotAdmin() && $admin->company_id !== $company->id) {
            abort(403, 'Sorry, you are not allowed to enter other companies');
        }

        return true;
    }
}
