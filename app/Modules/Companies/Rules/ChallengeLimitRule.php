<?php

namespace App\Modules\Companies\Rules;

use App\Modules\Challenges\Models\Challenge;
use Illuminate\Contracts\Validation\Rule;

class ChallengeLimitRule implements Rule
{
    private $limit = 0;
    private $company;

    public function __construct($company)
    {
        $this->company = $company;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if(!$this->company) {
            return true;
        }

        $count = Challenge::where('company_id', $this->company->id)->count();
        $this->limit = $count;

        return (int) $value < $count ? false : true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The challenges may not be less than ' . $this->limit;
    }
}
