<?php
/**
 * Created by Maksym Ignatchenko, Appus Studio LP on 26.02.19
 *
 */

namespace App\Modules\Companies\Datatables;

use App\Modules\Companies\Helpers\CompanyViewHelper;
use App\Modules\Companies\Models\Company;
use App\Modules\Companies\Enums\CompanyTypeEnum;
use Illuminate\Database\Eloquent\Builder;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Builder as YajraBuilder;
use Yajra\DataTables\Services\DataTable;

class CompanyDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Result from query() method.
     * @return EloquentDataTable
     */
    public function dataTable($query): EloquentDataTable
    {
        $dataTable = new EloquentDataTable($query);
        return $dataTable
            ->addColumn('action', 'company.datatables_actions')
            ->editColumn('type', function ($query) {
                $badgeClass = CompanyViewHelper::getTypeContainerClass($query->type);
                return "<span class='badge " . $badgeClass . "'>$query->type</span>";
            })
            ->editColumn('name', function ($query) {
                return CompanyViewHelper::getPrettyShortName($query->name);
            })
            ->editColumn('logo', function ($query) {
                return ($query->logo ? ("<img height='50' src=" . $query->logo) . " />" : (''));
            })
            ->editColumn('info', function ($query) {
                return CompanyViewHelper::getPrettyShortInfo($query->info);
            })
            ->editColumn('created_at', function ($query) {
                return date('d-M-Y', strtotime($query->created_at));
            })
            ->rawColumns(['name', 'logo', 'type', 'info', 'created_at', 'action']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param Company $model
     * @return Company
     */
    public function query(Company $model): Builder
    {
        if (auth()->user()->role_id !== 1) {
            return $model->newQuery()->where('type', CompanyTypeEnum::COMMERCIAL)->where('id', auth()->user()->company_id)->withCount(['users', 'challenges']);
        }

        return $model->newQuery()->where('type', CompanyTypeEnum::COMMERCIAL)->withCount(['users', 'challenges']);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return YajraBuilder
     */
    public function html(): YajraBuilder
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax(url()->current())
            ->addAction(['width' => '10%'])
            ->parameters([
                'dom'     => 'frtip',
                'order'   => [[0, 'desc']],
                'responsive' => true,
                'drawCallback' => '
                    function() {
                        $.SweetAlert.init();

                    }',
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns(): array
    {
        return [
            [
                'name' => 'name',
                'data' => 'name',
                'title' => 'Name',
                'width' => '20%',
            ],
            [
                'name' => 'phone',
                'data' => 'phone',
                'title' => 'Phone',
                'width' => '15%',
            ],
            [
                'name' => 'challenges_count',
                'data' => 'challenges_count',
                'title' => 'Challenges',
                'width' => '10%',
                'searchable' => false,
            ],
            [
                'name' => 'users_count',
                'data' => 'users_count',
                'title' => 'Users',
                'width' => '10%',
                'searchable' => false,
            ],
            [
                'name' => 'logo',
                'data' => 'logo',
                'title' => 'Logo',
                'width' => '20%',
                'searchable' => false,
                'orderable' => false,
            ],
            [
                'name' => 'created_at',
                'data' => 'created_at',
                'title' => 'Registered On',
                'width' => '20%',
                'orderable' => true,
                'searchable' => true,
            ],
        ];
    }
}
