<div class="btn-group" role="group">
    <button id="btnGroupDrop1" type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown"
            aria-haspopup="true" aria-expanded="false">
        Actions
    </button>
    <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
        <a href="{{ route('company.show', $id) }}?company_session=1" class='dropdown-item'>
            Show
        </a>
        <a href="{{ route('company.edit', $id) }}" class='dropdown-item'>
            Edit
        </a>
        @can('destroy', 'App\Modules\Companies\Models\Company')
            <button      class='dropdown-item delete-company-button'
                    data-target-location="{{ route('company.index') }}"
                    data-url="{{ route('company.destroy', $id) }}">
                Delete
            </button>
        @endcan
    </div>
</div>
