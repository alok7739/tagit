@extends('layouts.app')
@section('title', 'Companies')

@section('content')
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box">
            <div class="box-body">
                <h3 class="new_heading pull-left">Manage Companies</h3>
                @can('create', 'App\Modules\Companies\Models\Company')
                    <a href="{{route('company.create')}}" class="btn btn-primary pull-right create-article">New company</a>
                @endcan
                <div class="clearfix"></div>
                @include('company.table')
            </div>
        </div>
    </div>
@endsection
