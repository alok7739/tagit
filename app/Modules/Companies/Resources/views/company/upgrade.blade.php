@extends('layouts.app-blank')
@section('title', 'Upgrade company')

@section('content')
@if(count($packages) > 0)
<style>.tab-content > .active { border: 0;}</style>
<div id="upgrade-step1">
    <section class="home-body-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-md-offset-3">
                    <h2 style="font-family:'Baloo Thambi 2',cursive">Select Package To Upgrade</h2>
                </div>
            </div>
        </div>
    </section>

    <section class="price-section" style="margin-top: 20px">
        <div class="container">
            <div class="row">
                <div class="col-lg-10 col-md-10 col-md-offset-1">
                    <div class="owl-carousel">
                        @php $c = 1; @endphp
                        @foreach($packages as $p)
                        <div id="package{{ $p->id }}" class="item">
                            <div class="price-box">
                                <div class="price-name">
                                    <h3>{{ $p->name }}</h3>
                                </div>
                                <div class="include-name">
                                    <h4>{{ $p->short_desc }}</h4>
                                </div>
                                <div class="package-list">
                                    <div class="contentheg">
                                    {!! str_replace('<li>', '<li><img src="/build/images/tick.png" /> ', $p->long_desc) !!}
                                    </div>
                                    <div class="rates">
                                        <span>{{ number_format($p->price) }} SR / user / month</span>
                                    </div>
                                </div>
                                <div class="price-footer">
                                    <a id="step1-submit" href="javascript:void(0)" data-id="{{ $p->id }}" class="price-btn contact-btn">Upgrade</a>
                                </div>
                            </div>
                        </div>
                        @php $c++; @endphp
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<div id="upgrade-step2" style="display: none;">
    <section class="login-screen step-form h-100">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-8 col-md-offset-2">
                    <div class="wizard mt-100">
                        <div class="tab-content">
                            <div class="tab-pane active" role="tabpanel" id="step1">
                                <h3>Upgrade <span>Details</span></h3>
                                <form id="upgradeFrm" name="upgradeFrm" action="" method="POST">
                                    @csrf
                                    <input type="hidden" id="pid" name="pid" value="" />

                                    <div id="step2">
                                        <hr />
                                        <div class="row">
                                            <div class="col-lg-4 col-md-4 col-sm-6 col-12">
                                                <div class="row xs-top">
                                                    <div class="col-lg-4 col-md-4 col-xs-3">
                                                        <img src="/build/images/package-icon.svg" />
                                                    </div>
                                                    <div class="col-lg-8 col-md-8 col-xs-9">
                                                        <p>Package</p>
                                                        <p><b id="package-name"></b></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-6 col-12">
                                                <div class="row xs-top">
                                                    <div class="col-lg-4 col-md-4 col-xs-3">
                                                        <img src="/build/images/no-game-icon.svg" />
                                                    </div>
                                                    <div class="col-lg-8 col-md-8 col-xs-9">
                                                        <p>Challenges</p>
                                                        <p><b id="package-challenges"></b></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-6 col-12">
                                                <div class="row xs-top">
                                                    <div class="col-lg-4 col-md-4 col-xs-3">
                                                        <img src="/build/images/no-of-user-icon.svg" />
                                                    </div>
                                                    <div class="col-lg-8 col-md-8 col-xs-9">
                                                        <p>Price Per Users</p>
                                                        <p><b id="package-price"></b> </b>SR / Month</b></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <hr />
                                    </div>

                                    <div class="row">
                                        <div class="col-lg-6 col-md-6">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-users"></i></span>
                                                <input id="users" type="text" class="form-control numberonly" name="users" placeholder="Enter number of users" maxlength="4" min="1" value="{{ $current_packege ? $current_packege->users : '' }}" />
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6">
                                            <h3 class="price-lbl">
                                                Total Price:
                                                <span id="price">0</span>
                                                <span>SR</span>
                                            </h3>
                                        </div>
                                    </div>
                                    <hr />

                                    <div class="row">
                                        <div class="col-lg-12 col-md-12">
                                            <div class="submit-bth">
                                                <button type="submit" class="btn btn-login next-step">Proceed To Payment</button>
                                                <p><a href="javascript:void(0)" class="text-orange show-step1">Back</a></p>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<script>
var packages = {!! json_encode($packages) !!};
var months = 0, price = 0;
$(document).ready(function(){
    $('.owl-carousel').owlCarousel({
        loop:false,
        nav:true,
        margin:20,
        autoHeight:true,
        responsiveClass: true,
        responsive:{
            0:{
                items:1,
                loop:true
            },
            600: {
                items:2,
                nav:false
            },
            1100:{
                items:3
            }
        }
    });

    $(document).on('click', '#step1-submit', function(){
        $('#upgrade-step1').hide();
        $('#upgrade-step2').slideDown();
        $("html, body").animate({ scrollTop: 0 }, 500);

        var id = parseInt($(this).attr('data-id'));
        $('#pid').val(id);

        $.each(packages, function(i, v){
            if(v.id == id) {
                $('#package-name').text(v.name);
                $('#package-challenges').text(v.challenges);
                $('#package-price').text(v.price);

                months = parseInt(v.time_limit);
                price = parseFloat(v.price).toFixed(2);

                $('#users').trigger('keyup');
            }
        })
    });

    $('#users').keyup(function(){
        var u = parseInt($(this).val());

        var total = u * price * months;
        total = total ? total.toFixed(2).replace('.00', '').toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") : 0;

        $('#price').text(total);
    });

    $('.show-step1').click(function(){
        $('#upgrade-step1').show();
        $('#upgrade-step2').hide();
        $("html, body").animate({ scrollTop: 0 }, 500);
    });

    $("#upgradeFrm").on("submit", function(){
        var users = parseInt($('#users').val());

        if(!users) {
            alert('Please enter number of users first.');
            return false;
        }

        return true;
    });
});
</script>
@else
<section class="home-body-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-md-offset-3">
                <h2 style="font-family:'Baloo Thambi 2',cursive; padding: 100px 0 200px;">No packages available to upgrade.</h2>
            </div>
        </div>
    </div>
</section>
@endif
@endsection
