<div class="row form-justify-container">
    <div class="col-md-12">
        <div class="box">
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-6">
                        <!-- Name Field -->
                        <div class="form-group">
                            <p>
                                {{ Form::label('name', 'Name ',  ['class' => 'required']) }}
                                {!! Form::text('name', null, ['class' => 'form-control', 'maxlength' => 50]) !!}
                            </p>
                            @if ($errors->has('name'))
                                <div class="text-red">{{ $errors->first('name') }}</div>
                            @endif
                        </div>

                        <!-- Code for join Field -->
                        <div class="form-group">
                            <p>
                                {{ Form::label('join_code', 'Join Code ') }}
                                {!! Form::text('join_code', null, ['class' => 'form-control', 'maxlength' => config('custom.company_join_code_max_length')]) !!}
                            </p>
                            @if ($errors->has('join_code'))
                                <div class="text-red">{{ $errors->first('join_code') }}</div>
                            @endif
                        </div>

                        <div class="form-group">
                            <p>
                                {{ Form::label('youtube_link', 'Youtube Intro Video ') }}
                            </p>
                            {!! Form::text('youtube_link', null, ['class' => 'form-control', 'maxlength' => 255]) !!}
                            @if ($errors->has('youtube_link'))
                                <div class="text-red">{{ $errors->first('youtube_link') }}</div>
                            @endif
                        </div>

                        <div class="form-group">
                            <p>
                                {{ Form::label('info', 'Info ',  ['class' => 'required']) }}
                            </p>
                            {!! Form::textarea('info', null, ['class' => 'form-control', 'maxlength' => 1000, 'rows' => 3]) !!}
                            @if ($errors->has('info'))
                                <div class="text-red">{{ $errors->first('info') }}</div>
                            @endif
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <!-- Company logo Field -->
                        <div class="form-g`roup">
                            {!! Form::label('company-logo', 'Company logo ') !!}
                        </div>
                        <div class="form-group dropzone company-logo-dropzone dz-clickable"></div>
                        <div class="form-group" hidden>
                            {!! Form::text('logo', old('logo'), ['class' => 'form-control']) !!}
                        </div>
                        @if (old('logo'))
                            <img class="dashboard-image" src="{{Storage::url(old('logo'))}}">
                        @else
                            <img class="dashboard-image" style="display:none" src="">
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@if(Auth::user()->isSuperAdmin())
<hr />

<h3 class="new_heading">Allowed Features</h3>

<div class="row">
    <div class="col-sm-4">
        <div class="form-group">
            {{ Form::label('expiry', 'Expiry Date:') }}
            {!! Form::text('expiry', null, ['id' => 'expiry', 'class' => 'form-control date-picker', 'readonly' => true]) !!}
        </div>
    </div>

    <div class="col-sm-4">
        <div class="form-group">
            {{ Form::label('users', 'Users: ') }}
            {!! Form::text('users', null, ['id' => 'users', 'class' => 'form-control numberonly', 'maxlength' => 3, 'min' => 1]) !!}
        </div>
    </div>

    <div class="col-sm-4">
        <div class="form-group">
            {{ Form::label('games', 'Challenges: ') }}
            {!! Form::text('games', null, ['id' => 'games', 'class' => 'form-control numberonly', 'maxlength' => 3, 'min' => 1]) !!}
            @if ($errors->has('games'))
                <div class="text-red">{{ $errors->first('games') }}</div>
            @endif
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-3">
        <div class="form-group">
            {{ Form::label('true_false', 'True/False: ', ['class' => 'required']) }}
            {!! Form::select('true_false', ['1' => 'Allowed', '0' => 'Not Allowed'], null, ['id' => 'true_false', 'class' => 'form-control']) !!}
        </div>
    </div>

    <div class="col-sm-3">
        <div class="form-group">
            {{ Form::label('sc_ques', 'Text Q: ', ['class' => 'required']) }}
            {!! Form::select('sc_ques', ['1'=> 'Allowed', '0' => 'Not Allowed'], null, ['id' => 'sc_ques', 'class' => 'form-control']) !!}
        </div>
    </div>

    <div class="col-sm-3">
        <div class="form-group">
            {{ Form::label('mc_ques', 'Multiple Choice Q: ', ['class' => 'required']) }}
            {!! Form::select('mc_ques', ['1' => 'Allowed', '0' => 'Not Allowed'], null, ['id' => 'mc_ques', 'class' => 'form-control']) !!}
        </div>
    </div>

    <div class="col-sm-3">
        <div class="form-group">
            {{ Form::label('checkin', 'Check In: ', ['class' => 'required']) }}
            {!! Form::select('checkin', ['1' => 'Allowed', '0' => 'Not Allowed'], null, ['id' => 'checkin', 'class' => 'form-control']) !!}
        </div>
    </div>

    <div class="col-sm-3">
        <div class="form-group">
            {{ Form::label('qr_code', 'QR Code: ', ['class' => 'required']) }}
            {!! Form::select('qr_code', ['1' => 'Allowed', '0' => 'Not Allowed'], null, ['id' => 'qr_code', 'class' => 'form-control']) !!}
        </div>
    </div>

    <div class="col-sm-3">
        <div class="form-group">
            {{ Form::label('images', 'Images: ', ['class' => 'required']) }}
            {!! Form::select('images', ['1' => 'Allowed', '0' => 'Not Allowed'], null, ['id' => 'images', 'class' => 'form-control']) !!}
        </div>
    </div>

    <div class="col-sm-3">
        <div class="form-group">
            {{ Form::label('screenshots', 'Screenshots: ', ['class' => 'required']) }}
            {!! Form::select('screenshots', ['1' => 'Allowed', '0' => 'Not Allowed'], null, ['id' => 'screenshots', 'class' => 'form-control']) !!}
        </div>
    </div>

    <div class="col-sm-3">
        <div class="form-group">
            {{ Form::label('videos', 'Videos: ', ['class' => 'required']) }}
            {!! Form::select('videos', ['1' => 'Allowed', '0' => 'Not Allowed'], null, ['id' => 'videos', 'class' => 'form-control']) !!}
        </div>
    </div>

    <div class="col-sm-3">
        <div class="form-group">
            {{ Form::label('locking', 'Challenge Locking: ', ['class' => 'required']) }}
            {!! Form::select('locking', ['1' => 'Allowed', '0' => 'Not Allowed'], null, ['id' => 'locking', 'class' => 'form-control']) !!}
        </div>
    </div>
</div>
<script>
jQuery(function($){
    $('.date-picker').datepicker({
        minDate: new Date(),
        dateFormat: 'mm/dd/yyyy',
    });
})
</script>
@endif

<div class="row form-justify-container">
    <div class="col-md-12">
        <div class="box">
            <div class="box-body">
                <!-- Submit Field -->
                <div class="form-group text-right d-flex justify-content-center">
                    {!! Form::submit('Save', ['class' => 'btn btn-success']) !!}
                </div>
            </div>
        </div>
    </div>
</div>
