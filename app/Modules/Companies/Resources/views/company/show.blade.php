@extends('layouts.app-blank')
@section('title', 'Company details')
@section('content')
<section class="dashboard-screen mt-180" style="margin-bottom: 20px;">
    <div class="container">
        <div class="row">
            <div class="col-lg-9 col-md-9 pd-5">
                <div class="dashboard-leftside inner-dashboard-screens">
                    <div class="clearfix"></div>
                    @include('flash::message')

                    @if(!$can_create)
                        @if(auth()->user()->isNotAdmin())
                        <div class="alert alert-danger">Your challenge creation limit exausted, <a href="{{ route('company.upgrade', $company->id) }}" class="text-orange">Click Here</a> to upgrade your account.</div>
                        @else
                        <div class="alert alert-danger">Company challenge creation limit exausted.</div>
                        @endif
                    @endif

                    <div class="clearfix"></div>

                    <div class="tabbable-panel">
                        <div class="tabbable-line">
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a>
                                        <figure>
                                            <img src="/build/images/total-no-of-users.svg" />
                                        </figure>
                                        <ul>
                                            <li>Total no of Users</li>
                                            <li><span><?= $total_users ?></span></li>
                                        </ul>
                                    </a>
                                </li>
                                <li class="dark-active">
                                    <a>
                                        <figure>
                                            <img src="/build/images/total-challenges-icon.svg" />
                                        </figure>
                                        <ul>
                                            <li>Total Challenges</li>
                                            <li><span><?= $total_challenges ?></span></li>
                                        </ul>
                                    </a>
                                </li>
                                <li class="active">
                                    <a>
                                        <figure>
                                            <img src="/build/images/active-challenges-icon.svg" />
                                        </figure>
                                        <ul>
                                            <li>Active Challenges</li>
                                            <li><span><?= $active_challenges ?></span></li>
                                        </ul>
                                    </a>
                                </li>
                                <li class="dark-active">
                                    <a>
                                        <figure>
                                            <img src="/build/images/pending-approved-icon.svg" />
                                        </figure>
                                        <ul>
                                            <li>Pending Proofs</li>
                                            <li><span><?= $pending_proofs ?></span></li>
                                        </ul>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="tabs-listing">
                        <div id="exTab1">
                            <ul class="nav nav-pills">
                                @if(Auth::user()->status == '1' && $can_create)
                                <li><a href="{{route('challenge.create', ['companyId' => $company->id])}}">New challenge</a></li>
                                @endif

                                <li>
                                    <a href="#" onclick="document.getElementById('reset-coins-public-challenges').click()">
                                        {!! Form::open(['method'=>'PUT', 'url'=> route('company.challenges.reset-coins', $company)]) !!}
                                        <button hidden id="reset-coins-public-challenges" data-toggle="tooltip" data-placement="top" title="Reset coins for all public challenges" type="submit" class="dropdown-item" onclick="return confirm('Are you sure you want reset points for all public challenges?');">
                                        </button>
                                        {!! Form::close() !!}
                                        Reset points and rating
                                    </a>
                                </li>

                                <li><a href="{{ route('ranking.index', ['company_id' => $company->id]) }}">View Ranking</a></li>
                                <li><a href="{{ route('ranking.live', ['company_id' => $company->id]) }}">Live Ranking</a></li>
                                <li><a href="{{ route('chat', ['company' => $company->id]) }}">Chats</a></li>
                                <li><a href="{{ route('comments', ['company' => $company->id]) }}">Comments</a></li>

                                <li><a href="{{route('company.users.notification-form', ['company' => $company->id])}}">Send message to challengers</a></li>

                                <li>
                                    @if($type)
                                    <a href="{{ route('company.show', ['company' => $company->id]) }}">All challenges list</a>
                                    @else
                                    <a href="{{ route('company.show', ['company' => $company->id, 'type' => 'archive']) }}">Archived challenges list</a>
                                    @endif
                                </li>

                                <li><a href="{{route('library.view', ['companyId' => $company->id])}}">Challenge Library</a></li>
                                <li><a href="{{route('library.my', ['companyId' => $company->id])}}">My Library</a></li>
                            </ul>

                            <div id="n-challenge">
                                <h3>Challenges of the company</h3>

                                <div class="lis-table-data">
                                    <div class="table-responsive">
                                        @include('company.table')
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-3 pd-5">
                <div class="dashboard-leftside chat-height">
                    <div class="row">
                        <div class="col-lg-12 col-md-12">

                            <div class="chat-section">
                                <div class="row">
                                    <div class="col-lg-4 col-md-3 col-sm-4 col-xs-4">
                                        <figure>
                                            <img src="{{ $company->logo ?? $company->logo_with_default }}" class="img-responsive" />
                                        </figure>
                                    </div>
                                    <div class="col-lg-8 col-md-4 col-sm-8 col-xs-8 pd">
                                        <h4>{{$company->name}}</h4>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 col-md-12">
                                        <div class="purple-box">
                                            <h3>Allowed Challenges</h3>
                                            <p>{{ $package ? $package->games . ' challenge(s) / month' : '0 challenge' }}</p>
                                            <h3>Allowed User</h3>
                                            <p>{{ $package ? $package->users : 0 }} user(s)</p>
                                            @if($package && $package->expiry)
                                            <h3>Expiry</h3>
                                            <p>{{ date('d/M/Y', strtotime($package->expiry)) }}</p>
                                            @endif

                                            @if(auth()->user()->isNotAdmin() && $can_upgrade)
                                            <p><a href="javascript:;" onclick="alert('Please contact us on info@tagitgames.com to upgrade your account')" class="text-orange"><i class="fa fa-star"></i> Upgrade Account</a></p>
                                            {{-- <p><a href="{{ route('company.upgrade', $company->id) }}" class="text-orange"><i class="fa fa-star"></i> Upgrade Account</a></p> --}}
                                            @endif
                                        </div>

                                        <div class="yellow-box">
                                            <h3>Game code</h3>
                                            <p>{{ $company->join_code ?? 'Empty' }}</p>
                                        </div>
                                        <div class="purple-box">
                                            <h3>Description</h3>
                                            <p>{{ $company->info }}</p>
                                        </div>

                                        <p style="margin-top: 20px">
                                            <a href="{{ route('company.edit', $company->id) }}" class="btn btn-primary"><i class="fa fa-edit"></i> &nbsp; Edit</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

