<?php

namespace App\Providers;


use App\Modules\Challenges\Models\Challenge;
use App\Modules\Challenges\Models\Reports;
use App\Modules\Companies\Models\Company;
use App\Modules\Challenges\Policies\ChallengesPolicy;
use App\Modules\Challenges\Policies\ReportsPolicy;
use App\Modules\Companies\Policies\CompanyPolicy;
use App\Modules\Content\Models\Content;
use App\Modules\Content\Policies\ContentPolicy;
use App\Modules\Library\Models\LibraryCategory;
use App\Modules\Library\Models\Library;
use App\Modules\Library\Policies\CategoryPolicy;
use App\Modules\Library\Policies\LibraryPolicy;
use App\Modules\Onboarding\Models\Packages;
use App\Modules\Onboarding\Policies\PackagePolicy;
use App\Modules\Users\Admin\Models\Admin;
use App\Modules\Users\Admin\Policies\AdminPolicy;
use App\Modules\Users\User\Models\User;
use App\Modules\Users\User\Policies\UserPolicy;
use App\Modules\Misc\Models\Contact;
use App\Modules\Misc\Policies\ContactPolicy;
use App\Modules\Onboarding\Models\Games;
use App\Modules\Onboarding\Models\GamesCategory;
use App\Modules\Onboarding\Policies\CategoryPolicy as GamesCategoryPolicy;
use App\Modules\Onboarding\Policies\GamePolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
        Admin::class => AdminPolicy::class,
        User::class => UserPolicy::class,
        Company::class => CompanyPolicy::class,
        Content::class => ContentPolicy::class,
        Challenge::class => ChallengesPolicy::class,
        Packages::class => PackagePolicy::class,
        Contact::class => ContactPolicy::class,
        LibraryCategory::class => CategoryPolicy::class,
        Library::class => LibraryPolicy::class,
        Reports::class => ReportsPolicy::class,
        GamesCategory::class => GamesCategoryPolicy::class,
        Games::class => GamePolicy::class
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
}
